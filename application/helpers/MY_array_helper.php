<?php

/* eexplode() helper function--------------------------------------------
Explodes a string into an array like the native php explode, however
it removes any blank values from the array---------------------------- */
function eexplode($separator, $string) {
	$array = explode($separator, $string);
	foreach($array as $key => $val) {
		if(empty($val)) {
			unset($array[$key]);
		}
	}
	return $array;
}

/* in_array_field() helper function--------------------------------------------
Finds if an object in an array has a paramter that you are looking for
---------------------------- */
function in_array_field($needle, $needle_field, $haystack, $strict = false) { 
    if ($strict) { 
        foreach ($haystack as $item) 
            if (isset($item->$needle_field) && $item->$needle_field === $needle) 
                return true; 
    } 
    else { 
        foreach ($haystack as $item) 
            if (isset($item->$needle_field) && $item->$needle_field == $needle) 
                return true; 
    } 
    return false; 
} 
