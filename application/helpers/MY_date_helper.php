<?php 
/**
* Convert MySQL's DATE (YYYY-MM-DD) or DATETIME (YYYY-MM-DD hh:mm:ss) to timestamp
*
* Returns the timestamp equivalent of a given DATE/DATETIME
*
* @todo add regex to validate given datetime
* @author Clemens Kofler <clemens.kofler@chello.at>
* @access    public
* @return    integer
*/
function mysqldatetime_to_timestamp($datetime = "")
{
  // function is only applicable for valid MySQL DATETIME (19 characters) and DATE (10 characters)
  $l = strlen($datetime);
    if(!($l == 10 || $l == 19))
      return 0;

    //
    $date = $datetime;
    $hours = 0;
    $minutes = 0;
    $seconds = 0;

    // DATETIME only
    if($l == 19)
    {
      list($date, $time) = explode(" ", $datetime);
      list($hours, $minutes, $seconds) = explode(":", $time);
    }

    list($year, $month, $day) = explode("-", $date);
    
    date_default_timezone_set('Europe/Madrid');

    return mktime($hours, $minutes, $seconds, $month, $day, $year);
}

/**
* Convert MySQL's DATE (YYYY-MM-DD) or DATETIME (YYYY-MM-DD hh:mm:ss) to date using given format string
*
* Returns the date (format according to given string) of a given DATE/DATETIME
*
* @author Clemens Kofler <clemens.kofler@chello.at>
* @access    public
* @return    integer
*/
function mysqldatetime_to_date($datetime = "", $format = "d.m.Y, H:i:s")
{
    return date($format, mysqldatetime_to_timestamp($datetime));
}

/**
* Convert timestamp to MySQL's DATE or DATETIME (YYYY-MM-DD hh:mm:ss)
*
* Returns the DATE or DATETIME equivalent of a given timestamp
*
* @author Clemens Kofler <clemens.kofler@chello.at>
* @access    public
* @return    string
*/
function timestamp_to_mysqldatetime($timestamp = "", $datetime = true)
{
  
  date_default_timezone_set('Europe/Madrid');
  
  if(empty($timestamp) || !is_numeric($timestamp)) $timestamp = time();

    return ($datetime) ? date("Y-m-d H:i:s", $timestamp) : date("Y-m-d", $timestamp);
}

/**
* Convert timestamp to Human Date
*
* Returns the date (format according to given string) of a given timestamp
*
* @author    Cleiton Francisco V. Gomes <http://www.cleitonfco.com.br/>
* @access    public
* @param     string
* @param     string
* @return    string
*/
function timestamp_to_date($timestamp = "", $format = "d/m/Y H:i:s")
{
  if(empty($timestamp) || !is_numeric($timestamp)) $timestamp = time();
  return date($format, $timestamp);
}

/**
* Convert Human Date to Timestamp
*
* Returns the timestamp equivalent of a given HUMAN DATE/DATETIME
*
* @author    Cleiton Francisco V. Gomes <http://www.cleitonfco.com.br/>
* @access    public
* @param     string
* @return    integer
*/
function date_to_timestamp($datetime = "")
{
  if (!preg_match("/^(\d{1,2})[.\- \/](\d{1,2})[.\- \/](\d{2}(\d{2})?)( (\d{1,2}):(\d{1,2})(:(\d{1,2}))?)?$/", $datetime, $date))
    return FALSE;
   
  $month = $date[2];
  $day = $date[1];
  $year = $date[3];
  $hour = (empty($date[6])) ? 0 : $date[6];
  $min = (empty($date[7])) ? 0 : $date[7];
  $sec = (empty($date[9])) ? 0 : $date[9];

  return mktime($hour, $min, $sec, $month, $day, $year);
}

/**
* Convert HUMAN DATE to MySQL's DATE or DATETIME (YYYY-MM-DD hh:mm:ss)
*
* Returns the DATE or DATETIME equivalent of a given HUMAN DATE/DATETIME
*
* @author    Cleiton Francisco V. Gomes <http://www.cleitonfco.com.br/>
* @access    public
* @param     string
* @param     boolean
* @return    string
*/
function date_to_mysqldatetime($date = "", $datetime = TRUE)
{
  return timestamp_to_mysqldatetime(date_to_timestamp($date), $datetime);
}

/**
* Check if dates are within a date range
*
* Returns TRUE or FALSE if one or both set of dates falls within a specified date span
* Includes a 'qualifier' integer to allow for offsets on both the start and end of the span to be tested against
*
* @author    Michael Van Dorth <http://www.iberianmedia.com/>
* @access    public
* @param     timestamp, timestamp, timestamp, timestamp , integer
* @return    boolean
*/
function is_in_date_range($range_start="",$range_end="",$date_start="",$date_end="",$qualifier=0)
{
  // Set the defaults
  $is_valid = FALSE;
  
  // Check for a qualifier first
  if($qualifier) {
	  $range_start = strtotime(('+' . $qualifier . ' days'), $range_start);
	  $range_end = strtotime(('-' . $qualifier . ' days'), $range_end);
  }
  
  // Now make the comparison
  if( ($date_start >= $range_start && $date_start <= $range_end) || ($date_end >= $range_start && $date_end <= $range_end) ) {
     $is_valid = TRUE;
  } 
  
  // Return the boolean
  return $is_valid;
  
}

/**
* Get the number of days difference between two dates
*
* Returns the number of days between two dates (a date span). 
*
* @author    Michael Van Dorth <http://www.iberianmedia.com/>
* @access    public
* @param     timestamp, timestamp
* @return    integer
*/
function get_day_diff($range_start=0,$range_end=0,$all_days=FALSE)
{
  // Set the defaults
  $days = 0;
  
  // Get the lowest and highest of the two
  $start_day = min($range_start,$range_end);
  $end_day = max($range_start,$range_end);
  
  // If there are values compare them
  if($start_day && $end_day) {
	  
	  $diff = abs($end_day - $start_day);
	  $years = floor($diff / (365*60*60*24));
	  $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
	  $days = floor(($diff - $years * 365*60*60*24 - $months * 30*60*60*24) / (60*60*24));
	  
  }
  
  // If we must include the start date increment the value by 1
  if($all_days) {
	  $days++;
  }
  
  // Return the value
  return $days;
  
}


/**
* Get the fiscal trimester from a timestamp
*
* Returns the trimester number 
*
* @author    Michael Van Dorth <http://www.iberianmedia.com/>
* @access    public
* @param     timestamp, timestamp
* @return    integer
*/
function get_fiscal_trimester($timestamp)
{
  // Set the defaults
  $trimester = 0;
  $month = timestamp_to_date($timestamp, 'm');
  
  if($month <= 3) {
	  $trimester = 1;
  } else if($month > 3 && $month <= 6) {
	  $trimester = 2;
  } else if($month > 6 && $month <= 9) {
	  $trimester = 3;
  } else {
	  $trimester = 4;
  }
  
  // Return the value
  return $trimester;
  
}


/**
* Get the fiscal trimester from a timestamp
*
* Returns the trimester number 
*
* @author    Michael Van Dorth <http://www.iberianmedia.com/>
* @access    public
* @param     timestamp, timestamp
* @return    integer
*/
function get_fiscal_trimester_timespan($trimester=0,$year=0)
{
  // Set the defaults
  $timespan = new stdClass;
  $timespan->start = '';
  $timespan->end = '';
  
  if($trimester && $year) {
  
	  switch($trimester) {
		  case 1 :
		  	$timespan->start = mktime(0,0,0,1,1,$year);
		  	$timespan->end = mktime(0,0,0,3,31,$year);
		  	break;
		  case 2 :
		  	$timespan->start = mktime(0,0,0,4,1,$year);
		  	$timespan->end = mktime(0,0,0,6,30,$year);
		  	break;
		  case 3 :
		  	$timespan->start = mktime(0,0,0,7,1,$year);
		  	$timespan->end = mktime(0,0,0,9,30,$year);
		  	break;
		  case 4 :
		  	$timespan->start = mktime(0,0,0,10,1,$year);
		  	$timespan->end = mktime(0,0,0,12,31,$year);
		  	break;
	  }
  
  }
  
  // Return the value
  return $timespan;
  
}


/* End of file MY_date_helper.php */
/* Location: ./application/helpers/MY_date_helper.php */