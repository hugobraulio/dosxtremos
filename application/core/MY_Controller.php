<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function __construct()
	{

		// Inherit parent class methods and properties
		parent::__construct();

	}
	
	
}

/* End of file My_Controller.php */
/* Location: ./application/core/MY_Controller.php */