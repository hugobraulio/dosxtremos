<?php

$lang['upload_userfile_not_set'] = "No se ha podido encontrar un variable de post llamado userfile.";
$lang['upload_file_exceeds_limit'] = "El archivo subido supera el tama&ntilde;o m&aacute;ximo estipulado en su configuraci&oacute;n de PHP.";
$lang['upload_file_exceeds_form_limit'] = "El archivo subido supera el tama&ntilde;o m&aacute;ximo estipulado.";
$lang['upload_file_partial'] = "El archivo solo fue parcialmente subido al servidor.";
$lang['upload_no_temp_directory'] = "No existe directorio temporal.";
$lang['upload_unable_to_write_file'] = "No ha sido posible guardar el archivo.";
$lang['upload_stopped_by_extension'] = "El archivo subido ha sido parada por su extensi&oacute;n.";
$lang['upload_no_file_selected'] = "No ha seleccionado un archivo para subir al servidor.";
$lang['upload_invalid_filetype'] = "El tipo de archivo que intenta subir no esta permitido.";
$lang['upload_invalid_filesize'] = "El archivo que intenta subir sobrepasa el tama&ntilde;o m&aacute;ximo permitido.";
$lang['upload_invalid_dimensions'] = "La im&aacute;gen que intenta subir sobrepasa el tama�o m&aacute;ximo permitido.";
$lang['upload_destination_error'] = "Ha surgido un problema mientras se intentaba guardar el archivo subido a su destino final en el servidor.";
$lang['upload_no_filepath'] = "El directorio para la subida de archivos no es v&aacute;lido.";
$lang['upload_no_file_types'] = "No ha especificado tipos de archivos permitidos.";
$lang['upload_bad_filename'] = "El nombre del archivo que ha subido ya existe en el servidor.";
$lang['upload_not_writable'] = "El directorio para la subida de archivos no permite guardar en ella.";


/* End of file upload_lang.php */
/* Location: ./system/language/spanish/upload_lang.php */