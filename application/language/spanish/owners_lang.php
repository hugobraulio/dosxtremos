<?php

$lang['owners_bookings'] = "Reservas";
$lang['owners_maintenance'] = "Mantenimiento";
$lang['owners_advanced_expenses'] = "Gastos Adelantados";
$lang['owners_comments'] = "Comentarios de Inquilinos";
$lang['owners_expenses'] = "Liquidaciones";
$lang['owners_payments_due'] = "Alquiler Pendiente";
$lang['owners_documents'] = "Documentos";
$lang['owners_invoices'] = "Facturas";
$lang['owners_end_session'] = "Cerrar Sesi&oacute;n";
$lang['owners_close'] = "Cerrar";
$lang['owners_back'] = "Volver";

$lang['t1'] = 'T1';
$lang['t2'] = 'T2';
$lang['t3'] = 'T3';
$lang['t4'] = 'T4';

$lang['owners_date'] = "Fecha";
$lang['owners_dates_of_stay'] = "Fechas Estancia";
$lang['owners_date_due'] = "Fecha Pendiente";
$lang['owners_trimester'] = "Trimestre";
$lang['owners_arrival'] = "Llegada";
$lang['owners_booking'] = "Reserva";
$lang['owners_client'] = "Nombre";
$lang['owners_description'] = "Descripci&oacute;n";
$lang['owners_reference'] = "Referencia";
$lang['owners_invoice_number'] = "N&deg; Factura";
$lang['owners_received'] = "Recibido";
$lang['owners_due'] = "Alquiler Pendiente";
$lang['owners_agreed'] = "Alquiler Acordado";
$lang['owners_costs'] = "Comisión inc IVA";
$lang['owners_net'] = "Alquiler Neto";
$lang['owners_percentage'] = "Porcentaje";
$lang['owners_paid'] = "Pagado";
$lang['owners_amount'] = "Cantidad";
$lang['owners_received'] = "Alquiler Recibido";
$lang['owners_total'] = "Total";
$lang['owners_commission'] = "Comisi&oacute;n";
$lang['owners_of_payment_of'] = "de pago por";
$lang['owners_view_detail'] = "Ver Detalle";
$lang['owners_print'] = "Imprimir";
$lang['owners_administrator'] = "Administrador";
$lang['owners_just_comments'] = "Comentarios";
$lang['owners_just_expenses'] = "Gastos";
$lang['owners_file'] = "Archivo";

$lang['owners_current_totals_for'] = "Totales Actuales para";
$lang['owners_estimates'] = "Previsiones";
$lang['owners_initial_annual_estimate'] = "Previsi&oacute;n Anual Inicial";
$lang['owners_revised_annual_estimate'] = "Previsi&oacute;n Anual Actualizada";

$lang['owners_date_complete'] = "Fecha Realizada";
$lang['owners_provider'] = "Proveedor";
$lang['owners_payments_made'] = "Pagos Realizados";
$lang['owners_invoice'] = "Factura";
$lang['owners_download'] = "Descargar";

$lang['owners_payments_received'] = "Pagos Recibidos";
$lang['owners_total_commissions_and_costs'] = "Total Comisiones y Gastos de";
$lang['owners_total_liquidations'] = "Total Liquidaciones";
$lang['owners_vat'] = "IVA";
$lang['owners_commission'] = "Comisi&oacute;n";

$lang['owners_commissions'] = "Comisiones inc IVA";
$lang['owners_net_commissions_by_trimester'] = "Bases Imponibles de Comisiones por Trimestre";
$lang['owners_vat_commissions_by_trimester'] = "IVA de Comisiones por Trimestre";
$lang['owners_booking_expenses'] = "Otros Gastos de Reserva";
$lang['owners_totals_by_trimester'] = "Totales por Trimestre";
$lang['owners_total_expenses'] = "Total Liquidaciones";
$lang['owners_received_disclaimer'] = 'La fecha que aparece es la fecha en que el propietario nos confirma el pago (que no coincide necesariamente con la fecha de transferencia)';

$lang['owners_liquidated'] = "Alquiler Liquidado por DX";

$lang['owners_total_due'] = "Total Pendiente";




/* End of file upload_lang.php */
/* Location: ./system/language/spanish/owners_lang.php */
