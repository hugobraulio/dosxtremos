<?php

$lang['required']			= "El campo %s es obligatorio.";
$lang['isset']				= "El campo %s debe contener un valor.";
$lang['valid_email']		= "El campo %s debe contener una direcci&oacute;n de email v&aacute;lido.";
$lang['valid_emails']		= "El campo %s debe contener direcciones de emails v&aacute;lidos.";
$lang['valid_url']			= "El campo %s debe contener un URL v&aacute;lido.";
$lang['valid_ip']			= "El campo %s debe contener un IP v&aacute;lido.";
$lang['min_length']			= "El campo %s debe contener un m&iacute;nimo de %s caracteres.";
$lang['max_length']			= "El campo %s no puede exceder los %s caracteres.";
$lang['exact_length']		= "El campo %s debe tener exactamente %s caracteres.";
$lang['alpha']				= "El campo %s solo debe incluir caracteres alfabeticas.";
$lang['alpha_numeric']		= "El campo %s solo debe incluir caracteres alfa-numericas.";
$lang['alpha_dash']			= "El campo %s solo debe incluir caracteres alfa-numericas, guiones, y guiones bajos.";
$lang['numeric']			= "El campo %s solo debe incluir n&uacute;meros.";
$lang['is_numeric']			= "El campo %s solo debe contener carateres numericas.";
$lang['integer']			= "EL campo %s solo debe contener un n&uacute;mero.";
$lang['regex_match']		= "El campo %s no esta en el formato correcto.";
$lang['matches']			= "El campo %s no c&oacute;ordina con el campo %s.";
$lang['is_unique'] 			= "El campo %s debe contener un valor &uacute;nico.";
$lang['is_natural']			= "El campo %s solo debe contener n&uacute;meros positivos.";
$lang['is_natural_no_zero']	= "El campo %s debe contener un n&uacute;mero mayor que cero.";
$lang['decimal']			= "El campo %s debe contener un n&uacute;mero decimal.";
$lang['less_than']			= "El campo %s debe contener un n&uacute;mero menor de %s.";
$lang['greater_than']		= "El campo %s debe contener un n&uacute;mero mayor de %s.";
$lang['valid_time']			= "El campo %s debe contener una hora v&aacute;lida en formato 24 horas (HH:MM)";


/* End of file form_validation_lang.php */
/* Location: ./system/language/spanish/form_validation_lang.php */