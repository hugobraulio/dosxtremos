<?php

$lang['owners_bookings'] = "Bookings";
$lang['owners_maintenance'] = "Mantenance";
$lang['owners_advanced_expenses'] = "Expenses Advanced";
$lang['owners_comments'] = "Visitor Comments";
$lang['owners_expenses'] = "Accounts";
$lang['owners_payments_due'] = "Rent Due";
$lang['owners_documents'] = "Documents";
$lang['owners_invoices'] = "Invoices";
$lang['owners_end_session'] = "Logout";
$lang['owners_close'] = "Close";
$lang['owners_back'] = "Back";

$lang['t1'] = 'Q1';
$lang['t2'] = 'Q2';
$lang['t3'] = 'Q3';
$lang['t4'] = 'Q4';

$lang['owners_date'] = "Date";
$lang['owners_dates_of_stay'] = "Dates of Stay";
$lang['owners_date_due'] = "Date Due";
$lang['owners_trimester'] = "Quarter";
$lang['owners_arrival'] = "Arrival";
$lang['owners_booking'] = "Reservation";
$lang['owners_client'] = "Name";
$lang['owners_description'] = "Description";
$lang['owners_reference'] = "Reference";
$lang['owners_invoice_number'] = "Invoice #";
$lang['owners_received'] = "Received rent";
$lang['owners_due'] = "Due rent";
$lang['owners_agreed'] = "Agreed rent";
$lang['owners_costs'] = "Costs inc VAT";
$lang['owners_net'] = "Net rent";
$lang['owners_percentage'] = "Percentage";
$lang['owners_paid'] = "Paid";
$lang['owners_amount'] = "Amount";
$lang['owners_received'] = "Received rent";
$lang['owners_total'] = "Total";
$lang['owners_commission'] = "Commission";
$lang['owners_of_payment_of'] = "of payment of";
$lang['owners_view_detail'] = "View Detail";
$lang['owners_print'] = "Print";
$lang['owners_administrator'] = "Administrator";
$lang['owners_just_comments'] = "Comments";
$lang['owners_just_expenses'] = "Expenses";
$lang['owners_file'] = "File";

$lang['owners_current_totals_for'] = "Current Totals for";
$lang['owners_estimates'] = "Estimates";
$lang['owners_initial_annual_estimate'] = "Initial Full-year Estimate";
$lang['owners_revised_annual_estimate'] = "Revised Full-year Estimate";

$lang['owners_date_complete'] = "Date Completed";
$lang['owners_provider'] = "Supplier";
$lang['owners_payments_made'] = "Payments Made";
$lang['owners_invoice'] = "Invoice";
$lang['owners_download'] = "Download";

$lang['owners_payments_received'] = "Payments Received";
$lang['owners_total_commissions_and_costs'] = "Total Commissions and Costs of";
$lang['owners_total_liquidations'] = "Total Liquidations";
$lang['owners_vat'] = "VAT";
$lang['owners_commission'] = "Commission";

$lang['owners_commissions'] = "Commissions inc VAT";
$lang['owners_net_commissions_by_trimester'] = "Net Commissions by Quarter";
$lang['owners_vat_commissions_by_trimester'] = "Commissions VAT by Quarter";
$lang['owners_booking_expenses'] = "Other Booking Expenses";
$lang['owners_totals_by_trimester'] = "Totals by Quarter";
$lang['owners_total_expenses'] = "Total Due";
$lang['owners_received_disclaimer'] = 'The date that appears is the date on which the owner confirms the payment (which does not necessarily coincide with the actual transfer date).';

$lang['owners_liquidated'] = "Rent liquidated by DX";

$lang['owners_total_due'] = "Total Due";


/* End of file upload_lang.php */
/* Location: ./system/language/english/owners_lang.php */
