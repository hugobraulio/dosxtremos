<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MY_Controller {
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Main()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load the language helper
		$this->load->helper('language');
		

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the public homepage
	----------------------------------------------------------------*/
	
	public function index($lang='es')
	{	
		
		// Load the language file
		$this->lang->load('public', ($lang == 'es' ? 'spanish' : 'english'));
		$data['lang'] = $lang;
		$data['login_url'] = site_url(array('owners','sessions','authenticate',$lang));
		
		// Load the view with the data
		$this->load->view('public/main', $data);
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */