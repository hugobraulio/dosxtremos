<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bookings extends Owners_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Bookings()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load any models we will need
		$this->load->model('home');

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page. Uses parent id (home)
	and year to display correct info
	----------------------------------------------------------------*/
	
	public function index($home_id=0,$year=0)
	{	
		
		// Get an array of the users homes
		$home_array = explode(',', $this->session->userdata('homes'));
		
		// Default the year and home id
		$home_id = ($home_id ? $home_id : $home_array[0]);
		$year = ($year ? $year : date('Y'));
		
		// Prevent URL hacks on homes
		if(!in_array($home_id, $home_array)) {
			redirect('/owners/sessions/logout', 'refresh');
	        exit();
		}
		
		// Get the home data and bookings report rows for the home
		$data['home'] = $this->home->initialize($home_id);
		$data['bookings'] = $this->home->get_booking_amounts($home_id,$year)->result();
		
		// Create a structure for the totals
		$data['totals'] = new stdClass;
		$data['totals']->agreed = 0;
		$data['totals']->received = 0;
		$data['totals']->liquidated = 0;
		$data['totals']->due = 0;
		$data['totals']->costs = 0;
		$data['totals']->net = 0;
		
		// Set the locale for date formatting based on user language
		if($this->session->userdata('language_id') == 'es') {
			setlocale(LC_TIME, "es_ES");
		}
		
		// Loop through the rows to format dates, create urls, and calculate totals
		foreach($data['bookings'] as $booking) {
			
			// Temp variables for total costs and net amounts
			$tmp_costs = ($booking->amount_commission + ($booking->amount_commission * (21/100)) + ($booking->amount_expense));
			$tmp_net = ($booking->agreed_price - ($booking->amount_commission + ($booking->amount_commission * (21/100)) + ($booking->amount_expense)));
			
			// Add the amounts to the totals
			$data['totals']->agreed += $booking->agreed_price;
			$data['totals']->received += $booking->amount_received;
			$data['totals']->liquidated += $booking->amount_liquidated;
			$data['totals']->due += $booking->amount_due;
			$data['totals']->costs += $tmp_costs;
			$data['totals']->net += $tmp_net;
			
			// Create the view links for payments received and costs
			$booking->view_payments_url = ($booking->amount_received ? site_url(array('owners','bookings','payments',$booking->item_id)) : NULL);
			$booking->view_costs_url = ($tmp_costs ? site_url(array('owners','bookings','costs',$booking->item_id)) : NULL);
			
			// Format the dates and any null amount values
			$booking->date_from = ucfirst(strftime('%b. %d', mysqldatetime_to_timestamp($booking->date_from)));
			$booking->date_to = ucfirst(strftime('%b. %d', mysqldatetime_to_timestamp($booking->date_to)));
			$booking->amount_received = ($booking->amount_received ? number_format($booking->amount_received, 2, '.', ',') : '0.00');
			$booking->amount_liquidated = ($booking->amount_liquidated ? number_format($booking->amount_liquidated, 2, '.', ',') : '0.00');
			$booking->amount_due = ($booking->amount_due ? number_format($booking->amount_due, 2, '.', ',') : '0.00');
			$booking->agreed_price = ($booking->agreed_price ? number_format($booking->agreed_price, 2, '.', ',') : '0.00');
			$booking->costs = ($tmp_costs ? number_format($tmp_costs, 2, '.', ',') : '0.00');
			$booking->net = ($tmp_net ? number_format($tmp_net, 2, '.', ',') : '0.00');
			
		}
		
		// Format the totals
		$data['totals']->agreed = number_format($data['totals']->agreed, 2, '.', ',');
		$data['totals']->received = number_format($data['totals']->received, 2, '.', ',');
		$data['totals']->liquidated = number_format($data['totals']->liquidated, 2, '.', ',');
		$data['totals']->due = number_format($data['totals']->due, 2, '.', ',');
		$data['totals']->costs = number_format($data['totals']->costs, 2, '.', ',');
		$data['totals']->net = number_format($data['totals']->net, 2, '.', ',');
		
		// Get the booking estimates data and format the numbers
		$data['estimates'] = $this->home->get_booking_estimates($home_id, $year);
		$data['estimates']->initial = number_format($data['estimates']->initial, 2, '.', ',');
		$data['estimates']->revised = number_format($data['estimates']->revised, 2, '.', ',');
		
		// Add the year and year navigation urls
		$data['current_year'] = $year;
		$data['back_url'] = site_url(array('owners','bookings','index',$home_id,($year - 1)));
		$data['forward_url'] = site_url(array('owners','bookings','index',$home_id,($year + 1)));
		
		
		// Sub-navigation
		$data['current_view'] = 'bookings';
		$data['home_id'] = $home_id;
		
		// Load the view with the data
		$this->load->view('owners/bookings_list', $data);
		
	}
	
	
	/* PAYMENTS METHOD 
	------------------------------------------------------------------
	Description: Loads the bookings payments view modal with data
	
	For AJAX Calls
	----------------------------------------------------------------*/
	
	public function payments($item_id=0)
	{	
		
		// Load the payment model and get the payments for this booking
		$this->load->model('payment');
		$data['payments'] = $this->payment->list_entries_by_booking($item_id,TRUE)->result();
		$data['total'] = 0;
		
		// Loop through payments to format dates and increment the total
		foreach($data['payments'] as $payment) {
			$data['total'] += $payment->amount;
			$payment->date_due = mysqldatetime_to_date($payment->date_due, 'd/m/Y');
			$payment->date_paid = mysqldatetime_to_date($payment->date_paid, 'd/m/Y');
			$payment->amount = number_format($payment->amount, 2, '.', ',');
		}
		
		// Format the totals
		$data['total'] = number_format($data['total'], 2, '.', ',');
		
		// Load the view with the data
		$modal = $this->load->view('owners/payment_modal',$data,TRUE);
		
		// Echo back the rendered view
		echo $modal;
		
	}
	
	
	/* VIEW COSTS METHOD 
	------------------------------------------------------------------
	Description: Loads the bookings costs view modal with data
	
	For AJAX Calls
	----------------------------------------------------------------*/
	
	public function costs($item_id=0, $trimester=0, $year=0)
	{	
		
		// Get payments and expenses from this booking via models
		$this->load->model('payment');
		$this->load->model('expense');
		
		$payments = $this->payment->list_entries_by_trimester($item_id,$trimester,$year,FALSE)->result();
		$expenses = $this->expense->list_entries_by_trimester($item_id,$trimester,$year)->result();
		
		$data['total_gross'] = 0;
		$data['total_vat'] = 0;
		$data['total_net'] = 0;
		$data['costs'] = Array();
		
		/*
		
		REMOVED BY CLIENT REQUEST. HE ONLY WANTS ONE LINE FOR COMMISSIONS.
		THIS CODE KEEPS PAYMENTS AS LINE ITEMS, WHEREAS THE FOLLOWING CREATES A LUMP SUM
		
		// Loop through payments
		foreach($payments as $payment) {
			
			// Calculate the amounts
			$tmp_net = $payment->amount * ($payment->commission_percent/100);
			$tmp_vat = $tmp_net * (21/100);
			$tmp_total = $tmp_net + $tmp_vat;
			
			// Increment the totals
			$data['total_net'] += $tmp_net;
			$data['total_vat'] += $tmp_vat;
			$data['total_gross'] += $tmp_total;
			
			// Create and insert into the costs array
			$tmp_payment = new stdClass;
			if($this->session->userdata('language_id') == 'es') {
				$tmp_payment->description = 'Comisi&oacute;n (' . $payment->commission_percent . '% de pago recibido por ' . number_format($payment->amount, 2, '.', ',') . ' &euro;)';
			} else {
				$tmp_payment->description = 'Commission (' . $payment->commission_percent . '% of payment received of ' . number_format($payment->amount, 2, '.', ',') . ' &euro;)';
			}
			$tmp_payment->amount_net = number_format($tmp_net, 2, '.', ',') . ' &euro;';
			$tmp_payment->amount_vat = number_format($tmp_vat, 2, '.', ',') . ' &euro;';
			$tmp_payment->amount_total = number_format($tmp_total, 2, '.', ',') . ' &euro;';
			array_push($data['costs'], $tmp_payment);
			
		}
		
		*/
		
		// If we have payments, insert the data into the costs array
		if($payments) {
			
			// Set the payment totals to 0
			$total_payments_amount = 0;
			$total_payments_gross = 0;
			$total_payments_vat = 0;
			$total_payments_net = 0;
			
			// Loop through each payment to add it to the totals
			foreach($payments as $payment) {
				
				// Caclulate the amounts
				$tmp_gross = $payment->amount * ($payment->commission_percent/100);
				$tmp_vat = $tmp_gross * (21/100);
				$tmp_net = $tmp_gross + $tmp_vat;
				
				// Increment the main totals
				$data['total_gross'] += $tmp_gross;
				$data['total_vat'] += $tmp_vat;
				$data['total_net'] += $tmp_net;
				
				// Increment the total payments
				$total_payments_amount += $payment->amount;
				$total_payments_gross += $tmp_gross;
				$total_payments_vat += $tmp_vat;
				$total_payments_net += $tmp_net;
				
			}
			
			// Create the payment line to insert into the costs array
			$tmp_payment = new stdClass;
			$tmp_payment->description = ($this->session->userdata('language_id') == 'es' ? 'Comisiones' : 'Commissions');
			$tmp_payment->amount_gross = number_format($total_payments_gross, 2, '.', ',') . ' &euro;';
			$tmp_payment->amount_vat = number_format($total_payments_vat, 2, '.', ',') . ' &euro;';
			$tmp_payment->amount_net = number_format($total_payments_net, 2, '.', ',') . ' &euro;';
			
			// Add it to the costs array
			array_push($data['costs'], $tmp_payment);
			
		}
		
		
		// Loop through expenses
		foreach($expenses as $expense) {
			
			// Increment the totals (no VAT or Commissions to calculate)
			$data['total_net'] += $expense->amount;
			$data['total_gross'] += $expense->amount;
			
			// Create and insert into the costs array
			$tmp_expense = new stdClass;
			$tmp_expense->description = $expense->description;
			$tmp_expense->amount_gross = number_format($expense->amount, 2, '.', ',') . ' &euro;';
			$tmp_expense->amount_vat = number_format(0, 2, '.', ',') . ' &euro;';
			$tmp_expense->amount_net = number_format($expense->amount, 2, '.', ',') . ' &euro;';
			array_push($data['costs'], $tmp_expense);
			
		}
		
		// Format the totals
		$data['total_gross'] = number_format($data['total_gross'], 2, '.', ',');
		$data['total_vat'] = number_format($data['total_vat'], 2, '.', ',');
		$data['total_net'] = number_format($data['total_net'], 2, '.', ',');
		
		// Load the view with the data
		$modal = $this->load->view('admin/costs_modal_vat',$data,TRUE);
		
		// Echo back the rendered view
		echo $modal;
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/owners/bookings.php */