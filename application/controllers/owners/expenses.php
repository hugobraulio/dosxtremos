<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expenses extends Owners_Controller {


	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/

	function Expenses()
	{

		// Inherit parent class methods and properties
		parent::__construct();

		// Load any models we will need
		$this->load->model('home');
		$this->load->model('maintenance');

	}


	/* DEFAULT METHOD
	------------------------------------------------------------------
	Description: Loads the items view page. Uses parent id (home)
	and year to display correct info
	----------------------------------------------------------------*/

	public function index($home_id=0,$year=0)
	{

		// Get an array of the users homes
		$home_array = explode(',', $this->session->userdata('homes'));

		// Default the year and home id
		$home_id = ($home_id ? $home_id : $home_array[0]);
		$year = ($year ? $year : date('Y'));

		// Prevent URL hacks on homes
		if(!in_array($home_id, $home_array)) {
			redirect('/owners/sessions/logout', 'refresh');
	        exit();
		}

		// Get the home data
		$data['home'] = $this->home->initialize($home_id);

		// Initialize the totals we will use

		$t1_bookings_net = 0;
		$t2_bookings_net = 0;
		$t3_bookings_net = 0;
		$t4_bookings_net = 0;

		$t1_bookings_vat = 0;
		$t2_bookings_vat = 0;
		$t3_bookings_vat = 0;
		$t4_bookings_vat = 0;

		$t1_bookings_total = 0;
		$t2_bookings_total = 0;
		$t3_bookings_total = 0;
		$t4_bookings_total = 0;

		$t1_bookings_liquidations_total = 0;
		$t2_bookings_liquidations_total = 0;
		$t3_bookings_liquidations_total = 0;
		$t4_bookings_liquidations_total = 0;

		$t1_maintenance_total = 0;
		$t2_maintenance_total = 0;
		$t3_maintenance_total = 0;
		$t4_maintenance_total = 0;

		$t1_grand_total = 0;
		$t2_grand_total = 0;
		$t3_grand_total = 0;
		$t4_grand_total = 0;

		// Get the bookings commissions

		$data['bookings'] = $this->home->booking_report($home_id,$year)->result();

		foreach($data['bookings'] as $booking) {

			// Add amounts to totals
			$t1_bookings_net += $booking->t1_commissions;
			$t2_bookings_net += $booking->t2_commissions;
			$t3_bookings_net += $booking->t3_commissions;
			$t4_bookings_net += $booking->t4_commissions;

			$t1_bookings_vat += ($booking->t1_commissions * (21/100));
			$t2_bookings_vat += ($booking->t2_commissions * (21/100));
			$t3_bookings_vat += ($booking->t3_commissions * (21/100));
			$t4_bookings_vat += ($booking->t4_commissions * (21/100));

			$t1_bookings_liquidations_total += $booking->t1_liquidations;
			$t2_bookings_liquidations_total += $booking->t2_liquidations;
			$t3_bookings_liquidations_total += $booking->t3_liquidations;
			$t4_bookings_liquidations_total += $booking->t4_liquidations;

			// Format Agreed Price to Currency
			$booking->agreed_price = number_format($booking->agreed_price, 2, '.', ',');

			// Format payments to currency
			$booking->t1_payments = ($booking->t1_payments ? (number_format($booking->t1_payments, 2, '.', ',') . ' &euro;') : '-');
			$booking->t2_payments = ($booking->t2_payments ? (number_format($booking->t2_payments, 2, '.', ',') . ' &euro;') : '-');
			$booking->t3_payments = ($booking->t3_payments ? (number_format($booking->t3_payments, 2, '.', ',') . ' &euro;') : '-');
			$booking->t4_payments = ($booking->t4_payments ? (number_format($booking->t4_payments, 2, '.', ',') . ' &euro;') : '-');

			// Add VAT to Commissions
			$booking->t1_commissions = $booking->t1_commissions + ($booking->t1_commissions * (21/100));
			$booking->t2_commissions = $booking->t2_commissions + ($booking->t2_commissions * (21/100));
			$booking->t3_commissions = $booking->t3_commissions + ($booking->t3_commissions * (21/100));
			$booking->t4_commissions = $booking->t4_commissions + ($booking->t4_commissions * (21/100));

			// Format Commissions to Currency and create links
			$booking->t1_commissions = ($booking->t1_commissions ? anchor(array('owners','expenses','costs',$booking->item_id,1,$year), (number_format($booking->t1_commissions, 2, '.', ',') . ' &euro;'), 'class="detail" title="Ver Detalle"' )  : '-' );
			$booking->t2_commissions = ($booking->t2_commissions ? anchor(array('owners','expenses','costs',$booking->item_id,2,$year), (number_format($booking->t2_commissions, 2, '.', ',') . ' &euro;'), 'class="detail" title="Ver Detalle"' )  : '-' );
			$booking->t3_commissions = ($booking->t3_commissions ? anchor(array('owners','expenses','costs',$booking->item_id,3,$year), (number_format($booking->t3_commissions, 2, '.', ',') . ' &euro;'), 'class="detail" title="Ver Detalle"' )  : '-' );
			$booking->t4_commissions = ($booking->t4_commissions ? anchor(array('owners','expenses','costs',$booking->item_id,4,$year), (number_format($booking->t4_commissions, 2, '.', ',') . ' &euro;'), 'class="detail" title="Ver Detalle"' )  : '-' );

			// Format liquidations to currency and create links
			$booking->t1_liquidations = ($booking->t1_liquidations ? (number_format($booking->t1_liquidations, 2, '.', ',') . ' &euro;') : '-' ) ;
			$booking->t2_liquidations = ($booking->t2_liquidations ? (number_format($booking->t2_liquidations, 2, '.', ',') . ' &euro;')  : '-' );
			$booking->t3_liquidations = ($booking->t3_liquidations ? (number_format($booking->t3_liquidations, 2, '.', ',') . ' &euro;')  : '-' );
			$booking->t4_liquidations = ($booking->t4_liquidations ? (number_format($booking->t4_liquidations, 2, '.', ',') . ' &euro;')  : '-' );

			// Format date and nav url
			$booking->date_from = mysqldatetime_to_date($booking->date_from, 'd/m/y');

		}

		//// Get Booking Totals (net + vat) for each trimester
		$t1_bookings_total = $t1_bookings_net + $t1_bookings_vat;
		$t2_bookings_total = $t2_bookings_net + $t2_bookings_vat;
		$t3_bookings_total = $t3_bookings_net + $t3_bookings_vat;
		$t4_bookings_total = $t4_bookings_net + $t4_bookings_vat;

		// Setup and format booking totals for view

		$data['bookings_totals'] = new stdClass;

		$data['bookings_totals']->t1_net = ($t1_bookings_net ? (number_format($t1_bookings_net, 2, '.', ',') . ' &euro;') : '-');
		$data['bookings_totals']->t2_net = ($t2_bookings_net ? (number_format($t2_bookings_net, 2, '.', ',') . ' &euro;') : '-');
		$data['bookings_totals']->t3_net = ($t3_bookings_net ? (number_format($t3_bookings_net, 2, '.', ',') . ' &euro;') : '-');
		$data['bookings_totals']->t4_net = ($t4_bookings_net ? (number_format($t4_bookings_net, 2, '.', ',') . ' &euro;') : '-');

		$data['bookings_totals']->t1_vat = ($t1_bookings_vat ? (number_format($t1_bookings_vat, 2, '.', ',') . ' &euro;') : '-');
		$data['bookings_totals']->t2_vat = ($t2_bookings_vat ? (number_format($t2_bookings_vat, 2, '.', ',') . ' &euro;') : '-');
		$data['bookings_totals']->t3_vat = ($t3_bookings_vat ? (number_format($t3_bookings_vat, 2, '.', ',') . ' &euro;') : '-');
		$data['bookings_totals']->t4_vat = ($t4_bookings_vat ? (number_format($t4_bookings_vat, 2, '.', ',') . ' &euro;') : '-');

		$data['bookings_totals']->t1_total = ($t1_bookings_total ? (number_format($t1_bookings_total, 2, '.', ',') . ' &euro;') : '-');
		$data['bookings_totals']->t2_total = ($t2_bookings_total ? (number_format($t2_bookings_total, 2, '.', ',') . ' &euro;') : '-');
		$data['bookings_totals']->t3_total = ($t3_bookings_total ? (number_format($t3_bookings_total, 2, '.', ',') . ' &euro;') : '-');
		$data['bookings_totals']->t4_total = ($t4_bookings_total ? (number_format($t4_bookings_total, 2, '.', ',') . ' &euro;') : '-');

		$data['bookings_totals']->t1_liquidations_total = ($t1_bookings_liquidations_total ? (number_format($t1_bookings_liquidations_total, 2, '.', ',') . ' &euro;') : '-');
		$data['bookings_totals']->t2_liquidations_total = ($t2_bookings_liquidations_total ? (number_format($t2_bookings_liquidations_total, 2, '.', ',') . ' &euro;') : '-');
		$data['bookings_totals']->t3_liquidations_total = ($t3_bookings_liquidations_total ? (number_format($t3_bookings_liquidations_total, 2, '.', ',') . ' &euro;') : '-');
		$data['bookings_totals']->t4_liquidations_total = ($t4_bookings_liquidations_total ? (number_format($t4_bookings_liquidations_total, 2, '.', ',') . ' &euro;') : '-');

		// Get the maintenance jobs

		$data['maintenances'] = $this->maintenance->list_entries($home_id,$year)->result();

		foreach($data['maintenances'] as $maintenance) {

			// Add amounts to totals
			$t1_maintenance_total += ($maintenance->trimester == 1 ? $maintenance->amount : 0);
			$t2_maintenance_total += ($maintenance->trimester == 2 ? $maintenance->amount : 0);
			$t3_maintenance_total += ($maintenance->trimester == 3 ? $maintenance->amount : 0);
			$t4_maintenance_total += ($maintenance->trimester == 4 ? $maintenance->amount : 0);

			// Format Payments
			$maintenance->t1_payments = ($maintenance->trimester == 1 ? ($maintenance->amount . ' &euro;') : '-');
			$maintenance->t2_payments = ($maintenance->trimester == 2 ? ($maintenance->amount . ' &euro;') : '-');
			$maintenance->t3_payments = ($maintenance->trimester == 3 ? ($maintenance->amount . ' &euro;') : '-');
			$maintenance->t4_payments = ($maintenance->trimester == 4 ? ($maintenance->amount . ' &euro;') : '-');

			// Format date and nav url
			$maintenance->date_payed = ucfirst(strftime('%b. %d', mysqldatetime_to_timestamp($maintenance->date_payed)));
			$maintenance->view_url = site_url(array('owners','maintenances','view',$maintenance->item_id));

		}

		// Format Maintenance amounts to currency
		$data['maintenance_totals'] = new stdClass;
		$data['maintenance_totals']->t1_total = ($t1_maintenance_total ? (number_format($t1_maintenance_total, 2, '.', ',') . ' &euro;') : '-');
		$data['maintenance_totals']->t2_total = ($t2_maintenance_total ? (number_format($t2_maintenance_total, 2, '.', ',') . ' &euro;') : '-');
		$data['maintenance_totals']->t3_total = ($t3_maintenance_total ? (number_format($t3_maintenance_total, 2, '.', ',') . ' &euro;') : '-');
		$data['maintenance_totals']->t4_total = ($t4_maintenance_total ? (number_format($t4_maintenance_total, 2, '.', ',') . ' &euro;') : '-');

		// Get the Grand Totals (Booking Totals + Maintenance Totals)
		$t1_grand_total = $t1_bookings_total + $t1_maintenance_total;
		$t2_grand_total = $t2_bookings_total + $t2_maintenance_total;
		$t3_grand_total = $t3_bookings_total + $t3_maintenance_total;
		$t4_grand_total = $t4_bookings_total + $t4_maintenance_total;

		// Format the Grand Total amounts to currency
		$data['grand_totals'] = new stdClass;
		$data['grand_totals']->t1_total = ($t1_grand_total ? (number_format($t1_grand_total, 2, '.', ',') . ' &euro;') : '-');
		$data['grand_totals']->t2_total = ($t2_grand_total ? (number_format($t2_grand_total, 2, '.', ',') . ' &euro;') : '-');
		$data['grand_totals']->t3_total = ($t3_grand_total ? (number_format($t3_grand_total, 2, '.', ',') . ' &euro;') : '-');
		$data['grand_totals']->t4_total = ($t4_grand_total ? (number_format($t4_grand_total, 2, '.', ',') . ' &euro;') : '-');

		// Add the year and year navigation urls
		$data['current_year'] = $year;
		$data['back_url'] = site_url(array('owners','expenses','index',$home_id,($year - 1)));
		$data['forward_url'] = site_url(array('owners','expenses','index',$home_id,($year + 1)));

		// Sub-navigation
		$data['current_view'] = 'expenses';
		$data['home_id'] = $home_id;

		// Load the view with the data
		$this->load->view('owners/expenses_list', $data);

	}


	/* VIEW COSTS METHOD
	------------------------------------------------------------------
	Description: Loads the bookings costs view modal with data

	For AJAX Calls
	----------------------------------------------------------------*/

	public function costs($item_id=0, $trimester=0, $year=0)
	{

		$data['title'] = '';
		$data['total_net'] = 0;
		$data['total_vat'] = 0;
		$data['total_gross'] = 0;
		$data['costs'] = Array();

		// Get payments from this booking via models
		$this->load->model('booking');
		$this->load->model('payment');

		$payments = $this->payment->list_entries_by_trimester($item_id,$trimester,$year,TRUE)->result();

		$booking_data = $this->booking->get_entry($item_id)->row();
		if($booking_data) {
			$data['title'] = $booking_data->fname . ' ' . $booking_data->lname . ' (' . mysqldatetime_to_date($booking_data->date_from, 'd/m/Y') . ' - ' . mysqldatetime_to_date($booking_data->date_to, 'd/m/Y') . ')';
		}

		// If we have payments, insert the data into the costs array
		if($payments) {

			// Set the payment totals to 0
			$total_payments_gross = 0;
			$total_payments_vat = 0;
			$total_payments_net = 0;

			// Loop through each payment to add it to the totals
			foreach($payments as $payment) {

				// Caclulate the amounts
				$tmp_gross = $payment->amount * ($payment->commission_percent/100);
				$tmp_vat = $tmp_gross * (21/100);
				$tmp_net = $tmp_gross + $tmp_vat;

				// Increment the main totals
				$data['total_gross'] += $tmp_gross;
				$data['total_vat'] += $tmp_vat;
				$data['total_net'] += $tmp_net;

				// Increment the total payments
				$total_payments_gross += $tmp_gross;
				$total_payments_vat += $tmp_vat;
				$total_payments_net += $tmp_net;
			}

			// Create the payment line to insert into the costs array
			$tmp_payment = new stdClass;
			$tmp_payment->description = ($this->session->userdata('language_id') == 'es' ? 'Comisiones' : 'Commissions');
			$tmp_payment->amount_gross = number_format($total_payments_gross, 2, '.', ',') . ' &euro;';
			$tmp_payment->amount_vat = number_format($total_payments_vat, 2, '.', ',') . ' &euro;';
			$tmp_payment->amount_net = number_format($total_payments_net, 2, '.', ',') . ' &euro;';

			// Add it to the costs array
			array_push($data['costs'], $tmp_payment);

		}

		// Format the totals
		$data['total_net'] = number_format($data['total_net'], 2, '.', ',');
		$data['total_vat'] = number_format($data['total_vat'], 2, '.', ',');
		$data['total_gross'] = number_format($data['total_gross'], 2, '.', ',');

		// Load the view with the data
		$modal = $this->load->view('owners/costs_modal_vat',$data,TRUE);

		// Echo back the rendered view
		echo $modal;

	}


	/* VIEW LIQUIDATIONS METHOD
	------------------------------------------------------------------
	Description: Loads the bookings costs view modal with data

	For AJAX Calls
	
	19/APR/2018, HUGO:  Not being used yet. We thought they wanted a pop-up with the commission deducted from the liquidation,
                        and showed in detail in the pop-up. But they want liquidation = payment so far
	----------------------------------------------------------------*/

	/*public function liquidations($item_id=0, $trimester=0, $year=0)
	{

		$data['title'] = '';
		$data['total_net'] = 0;
		$data['total_commissions'] = 0;
		$data['total_gross'] = 0;
		$data['liquidations'] = Array();

		// Get payments from this booking via models
		$this->load->model('booking');
		$this->load->model('payment');

		$liq_payments = $this->payment->list_entries_by_trimester($item_id,$trimester,$year,TRUE,TRUE)->result();
		$booking_data = $this->booking->get_entry($item_id)->row();
		if($booking_data) {
			$data['title'] = $booking_data->fname . ' ' . $booking_data->lname . ' (' . mysqldatetime_to_date($booking_data->date_from, 'd/m/Y') . ' - ' . mysqldatetime_to_date($booking_data->date_to, 'd/m/Y') . ')';
		}

		// If we have payments, insert the data into the costs array
		if($liq_payments) {

			// Set the payment totals to 0
			$total_liquidations_net = 0;
			$total_commissions = 0;
			$total_liquidations_gross = 0;

			// Loop through payments
			foreach($liq_payments as $liq_payment) {

				// Calculate the commission received by DosExtrem
				$commission_gross = $liq_payment->amount * ($liq_payment->commission_percent/100);
				$commission_vat = $commission_vat = $commission_gross * (21/100);
				$commission_net = $commission_gross + $commission_vat;

				// Calculate the liquidations: payment - commission
				$liquidation_gross = $liq_payment->amount;
				$liquidation_net = $liquidation_gross - $commission_net;

				// Increment the totals
				$data['total_gross'] += $liquidation_gross;
				$data['total_commissions'] += $commission_net;
				$data['total_net'] += $liquidation_net;

				// Increment the total liquidations
				$total_liquidations_gross += $liquidation_gross;
				$total_commissions += $commission_net;
				$total_liquidations_net += $liquidation_net;
			}

			// Create and insert into the liquidations array
			$tmp_liquidation = new stdClass;
			$tmp_liquidation->description = ($this->session->userdata('language_id') == 'es' ? 'Liquidaciones' : 'Liquidations');
			$tmp_liquidation->amount_gross = number_format($total_liquidations_gross, 2, '.', ',') . ' &euro;';
			$tmp_liquidation->amount_commissions = number_format($total_commissions, 2, '.', ',') . ' &euro;';
			$tmp_liquidation->amount_net = number_format($total_liquidations_net, 2, '.', ',') . ' &euro;';
			array_push($data['liquidations'], $tmp_liquidation);

		}

		// Format the totals
		$data['total_net'] = number_format($data['total_net'], 2, '.', ',');
		$data['total_commissions'] = number_format($data['total_commissions'], 2, '.', ',');
		$data['total_gross'] = number_format($data['total_gross'], 2, '.', ',');

		// Load the view with the data
		$modal = $this->load->view('owners/liquidations_modal',$data,TRUE);

		// Echo back the rendered view
		echo $modal;

	}*/


}

/* End of file main.php */
/* Location: ./application/controllers/owners/expenses.php */
