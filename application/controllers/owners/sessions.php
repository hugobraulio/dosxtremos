<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sessions extends CI_Controller {
    
    
    /* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Initializes and loads CI native sessions library.
	----------------------------------------------------------------*/
    
    function Sessions()
    {
        
        // Inherit parent class methods and properties
		parent::__construct();
		
		// Load the language helper
		$this->load->helper('language');

    }
	
	/* LOGIN METHOD
	------------------------------------------------------------------
	Description: Loads the admin login form
	----------------------------------------------------------------*/
	
    function login($failed='',$lang='es')
    {
		
		// Set the language file
		$this->lang->load('public', ($lang == 'es' ? 'spanish' : 'english'));
		$data['lang'] = $lang;
		
		// Check for failed login attempts
		$data['failed'] = $failed;
		$data['login_url'] = site_url(array('owners','sessions','authenticate',$lang));
		
        // Load the login form view
        $this->load->view('public/main', $data);
    
    }
	
	/* AUTHENTICATE METHOD
	------------------------------------------------------------------
	Description: Calls the user_model and authenticates the user based
	on vars posted from the login form
	----------------------------------------------------------------*/
	
    function authenticate($lang='es')
    {
        
        // Load the owner model
        $this->load->model('owner', '', true);
        
        // Redirect string (for failed logins)
        $redirect_string = '/owners/sessions/login/failed/' . $lang;
        
        // Run the authentication
        $owner_data = $this->owner->authenticate($this->input->post('username'), $this->input->post('password'));
        
        // If authentication was succesful, set the session vars and sent the user to the main page (dashboard)
        if ($owner_data['authenticated']) {
            
            // Set the session data
            $this->session->set_userdata('loggedin', true);
            $this->session->set_userdata('item_id', $owner_data['item_id']);
            $this->session->set_userdata('name', $owner_data['name']);
            $this->session->set_userdata('homes', $owner_data['homes']);
            $this->session->set_userdata('home_names', $owner_data['home_names']);
            $this->session->set_userdata('language_id', $owner_data['language_id']);
            
            // Redirect the user to the main admin area page (only if they have homes)
            if($owner_data['homes']) {
            	redirect('/owners/bookings/index/', 'refresh');
            } else {
	            // Redirect the user to the login form and exit the method
	            redirect($redirect_string, 'refresh');
	            exit();
            }
            
        } else {
        
        	// Redirect the user to the login form and exit the method
            redirect($redirect_string, 'refresh');
            exit();
            
        }
        
    }
	
	/* LOGOUT METHOD
	------------------------------------------------------------------
	Description: Logs the admin user out by unsetting the users
	session variable
	----------------------------------------------------------------*/
	
    function logout()
    {
    
        // Remove the session data
        $this->session->unset_userdata('loggedin');
        $this->session->unset_userdata('item_id');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('homes');
        $this->session->unset_userdata('language_id');
        
        // Redirect and exit the method
        redirect('/owners/sessions/login/', 'refresh');
        exit();
        
    }
    
}

/* End of file sessions.php */
/* Location: ./application/controllers/owners/sessions.php */