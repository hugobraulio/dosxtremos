<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoices extends Owners_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Invoices()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load the models we will need
		$this->load->model('invoice');
		$this->load->model('system_config');

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page. Uses the users session
	scoped var and year to display correct info
	----------------------------------------------------------------*/
	
	public function index($year=0)
	{	
		
		// Default the year to current
		$year = (!$year ? date('Y') : $year);
		
		// Get the users sessions scoped id
		$owner_id = $this->session->userdata('item_id');
		
		// Get the list of invoices for the year
		$data['invoices'] = $this->invoice->list_entries($owner_id, $year)->result();
		
		// Loop through them to format dates and create links
		foreach($data['invoices'] as $invoice) {
			$invoice->trimester = get_fiscal_trimester(mysqldatetime_to_timestamp($invoice->invoice_date));
			$invoice->invoice_date = mysqldatetime_to_date($invoice->invoice_date, 'd/m/Y');
			$invoice->print_url = site_url(array('owners','invoices','print_invoice',$invoice->item_id));
		}
		
		// Add year and year navigation urls
		$data['current_year'] = $year;
		$data['back_url'] = site_url(array('owners','invoices',($year - 1)));
		$data['forward_url'] = site_url(array('owners','invoicess',($year + 1)));
		
		// Load the view with the data
		$this->load->view('owners/invoices_list', $data);
		
	}
	
	
	/* PRINT INVOICE METHOD 
	------------------------------------------------------------------
	Description: Loads the items print view with data. Uses the users
	session scoped vars to display in the correct language
	----------------------------------------------------------------*/
	
	public function print_invoice($item_id=0)
	{	
		
		// Initialize the invoice
		$data['invoice'] = $this->invoice->initialize($item_id);
		$data['system_config'] = $this->system_config->initialize(1);
		
		// Check for URL hacks and make sure user can view it
		if(($data['invoice']->is_new === TRUE) || ($data['invoice']->owner->item_id != $this->session->userdata('item_id'))) {
			redirect('/owners/sessions/logout', 'refresh');
	        exit();
		}
		
		// Prep the data
		$data['invoice']->invoice_date = timestamp_to_date($data['invoice']->invoice_date, 'd/m/Y');
		
		// Add the users language ID
		$data['language_id'] = $this->session->userdata('language_id');
		
		// Load the view with the data
		$this->load->view('common/invoices_print', $data);
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/owners/bookings.php */