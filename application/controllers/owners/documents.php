<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Documents extends Owners_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Documents()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load any models we will need
		$this->load->model('document');
		$this->load->model('home');

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page based on the parent id
	and the year
	----------------------------------------------------------------*/
	
	public function index($home_id=0,$year=0)
	{	
		
		// Get an array of the users homes
		$home_array = explode(',', $this->session->userdata('homes'));
		
		// Default the year and home id
		$home_id = ($home_id ? $home_id : $home_array[0]);
		$year = ($year ? $year : date('Y'));
		
		// Prevent URL hacks on homes
		if(!in_array($home_id, $home_array)) {
			redirect('/owners/sessions/logout', 'refresh');
	        exit();
		}
		
		// Get the home data
		$data['home'] = $this->home->initialize($home_id);
		
		// Get the list of documents to display
		$data['documents'] = $this->document->list_entries_by_home($home_id, $year)->result();
		foreach($data['documents'] as $document) {
			$document->download_url = site_url(array('owners','documents','download',$document->filename,0));
			$document->date_from = mysqldatetime_to_date($document->date_from, 'd/m/Y');
			$document->date_to = mysqldatetime_to_date($document->date_to, 'd/m/Y');
		}
		
		// Add the year and year navigation urls
		$data['current_year'] = $year;
		$data['back_url'] = site_url(array('owners','documents','index',$home_id,($year - 1)));
		$data['forward_url'] = site_url(array('owners','documents','index',$home_id,($year + 1)));
		
		// Sub-navigation
		$data['home_id'] = $home_id;
		$data['current_view'] = 'documents';
		
		// Load the view with the data
		$this->load->view('owners/documents_list', $data);
		
	}
	
	
	/* DOWNLOAD METHOD 
	------------------------------------------------------------------
	Description: Sets a forced download of the items attached file
	----------------------------------------------------------------*/
	
	public function download($filename='', $hash=0)
	{	
		
		// Load the libraries and helpers
		$this->load->helper('download');
		
		// Read the files contents and give it a name
		$filepath = base_url() . 'uploads/booking_documents/' . $filename;
		$data = file_get_contents($filepath);
		$name = $filename;
		
		// Force a download
		force_download($name, $data);
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/owners/documents.php */