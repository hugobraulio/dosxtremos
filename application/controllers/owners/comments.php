<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comments extends Owners_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Comments()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load any models we will need
		$this->load->model('comment');
		$this->load->model('home');

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page based on the parent id
	and the year
	----------------------------------------------------------------*/
	
	public function index($home_id=0,$year=0)
	{	
		
		// Get an array of the users homes
		$home_array = explode(',', $this->session->userdata('homes'));
		
		// Default the year and home id
		$home_id = ($home_id ? $home_id : $home_array[0]);
		$year = ($year ? $year : date('Y'));
		
		// Prevent URL hacks on homes
		if(!in_array($home_id, $home_array)) {
			redirect('/owners/sessions/logout', 'refresh');
	        exit();
		}
		
		// Get the home data
		$data['home'] = $this->home->initialize($home_id);
		
		// Get the comments and loop through them to prep the data
		$data['comments'] = $this->comment->list_entries($year,0,$home_id)->result();
		foreach($data['comments'] as $comment) {
			$comment->view_url = site_url(array('owners','comments','view',$comment->item_id));
			$comment->booking_dates = mysqldatetime_to_date($comment->date_from, 'd/m/Y') . ' - ' . mysqldatetime_to_date($comment->date_to, 'd/m/Y');
		}
		
		// Add the year and year navigation urls
		$data['current_year'] = $year;
		$data['back_url'] = site_url(array('owners','comments','index',$home_id,($year - 1)));
		$data['forward_url'] = site_url(array('owners','comments','index',$home_id,($year + 1)));
		
		// Sub-navigation
		$data['home_id'] = $home_id;
		$data['current_view'] = 'comments';
		
		// Load the view with the data
		$this->load->view('owners/comments_list', $data);
		
	}
	
	
	/* VIEW METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with data
	----------------------------------------------------------------*/
	
	public function view($item_id=0)
	{	
		
		// Get the comment data
		$data['comment'] = $this->comment->initialize($item_id);
		
		// Parent link (back up to list)
		$data['list_url'] = site_url(array('owners','comments','index',$data['comment']->booking->home->item_id,timestamp_to_date($data['comment']->date_comment, 'Y')));
		
		// Format any dates
		$data['comment']->date_comment = timestamp_to_date($data['comment']->date_comment, 'd/m/Y');
		$data['comment']->booking->date_from = timestamp_to_date($data['comment']->booking->date_from, 'd/m/Y');
		$data['comment']->booking->date_to = timestamp_to_date($data['comment']->booking->date_to, 'd/m/Y');
		
		// Sub-navigation
		$data['home_id'] = $data['comment']->booking->home->item_id;
		$data['current_view'] = 'comments';
		
		// Load the view with the data
		$this->load->view('owners/comments_view', $data);
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/owners/comments.php */