<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maintenances extends Owners_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Maintenances()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load any models we will need
		$this->load->model('maintenance');
		$this->load->model('home');

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page based on the parent id
	and the year
	----------------------------------------------------------------*/
	
	public function index($home_id=0,$year=0)
	{	
		
		// Get an array of the users homes
		$home_array = explode(',', $this->session->userdata('homes'));
		
		// Default the year and home id
		$home_id = ($home_id ? $home_id : $home_array[0]);
		$year = ($year ? $year : date('Y'));
		
		// Prevent URL hacks on homes
		if(!in_array($home_id, $home_array)) {
			redirect('/owners/sessions/logout', 'refresh');
	        exit();
		}
		
		// Get the home data
		$data['home'] = $this->home->initialize($home_id);
		
		// Set the total
		$data['total'] = 0;
		
		// Get the maintenanace data from the model and loop though it to prep
		$data['maintenances'] = $this->maintenance->list_entries($home_id,$year)->result();
		foreach($data['maintenances'] as $maintenance) {
			// Increment the total
			$data['total'] += $maintenance->amount;
			// Create the link and format the date and amount
			$maintenance->view_url = site_url(array('owners','maintenances','view',$maintenance->item_id));
			$maintenance->date_payed = mysqldatetime_to_date($maintenance->date_payed, 'd/m/Y');
			$maintenance->amount = number_format($maintenance->amount, 2, '.', ',');
		}
		
		// Format the total
		$data['total'] = number_format($data['total'], 2, '.', ',');
		
		// Add the year and year navigation urls
		$data['current_year'] = $year;
		$data['back_url'] = site_url(array('owners','maintenances','index',$home_id,($year - 1)));
		$data['forward_url'] = site_url(array('owners','maintenances','index',$home_id,($year + 1)));
		
		// Sub-navigation
		$data['home_id'] = $home_id;
		$data['current_view'] = 'maintenance';
		
		// Load the view with the data
		$this->load->view('owners/maintenance_list', $data);
		
	}
	
	
	/* VIEW METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with data
	----------------------------------------------------------------*/
	
	public function view($item_id=0)
	{	
		
		// Get the maintenenace data
		$data['maintenance'] = $this->maintenance->initialize($item_id);
		
		// Parent link (back up to list)
		$data['list_url'] = site_url(array('owners','maintenances','index',$data['maintenance']->home->item_id,timestamp_to_date($data['maintenance']->date_payed, 'Y')));
		
		// Format dates and download link
		$data['maintenance']->date_complete = timestamp_to_date($data['maintenance']->date_complete, 'd/m/Y');
		$data['maintenance']->date_payed = timestamp_to_date($data['maintenance']->date_payed, 'd/m/Y');
		$data['maintenance']->download_url = ($data['maintenance']->receipt_file ? site_url(array('owners','maintenances','download', $data['maintenance']->receipt_file,0)) : NULL);
		
		// Sub-navigation
		$data['home_id'] = $data['maintenance']->home->item_id;
		$data['current_view'] = 'maintenance';
		
		// Load the view with the data
		$this->load->view('owners/maintenance_view', $data);
		
	}
	
	
	/* DOWNLOAD METHOD 
	------------------------------------------------------------------
	Description: Sets a forced download of the items attached file
	----------------------------------------------------------------*/
	
	public function download($filename='', $hash=0)
	{	
		
		// Load the libraries and helpers
		$this->load->helper('download');
		
		// Read the files contents and give it a name
		$filepath = base_url() . 'uploads/maintenance_receipts/' . $filename;
		$data = file_get_contents($filepath);
		$name = $filename;
		
		// Force a download
		force_download($name, $data);
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/owners/maintenance.php */