<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Documents extends Admin_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Documents()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		$this->load->model('document');

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Redirect default method
	----------------------------------------------------------------*/
	
	public function index()
	{	
		
		redirect('/admin/bookings/index', 'refresh');
		exit();
		
	}
	
	
	/* DOWNLOAD METHOD 
	------------------------------------------------------------------
	Description: Sets a forced download of the document file
	----------------------------------------------------------------*/
	
	public function download($filename='', $hash=0)
	{	
		
		// Load the libraries and helpers
		$this->load->helper('download');
		
		// Read the files contents and give it a name
		$filepath = base_url() . 'uploads/booking_documents/' . $filename;
		$data = file_get_contents($filepath);
		$name = urldecode($filename);
		
		// Force a download
		force_download($name, $data);
		
	}
	
	
	/* DELETE METHOD 
	------------------------------------------------------------------
	Description: Calls the documents model to delete a document and
	returns JSON response for javascript (used with AJAX)
	----------------------------------------------------------------*/
	
	public function delete($item_id)
	{
		
		// Set the defaults
		$status = '';
		$msg = '';
		
		$this->document->initialize($item_id);
		$this->document->delete_entry();
		
		$status = 'success';
		$msg = 'El archivo se ha borrado con exito';
		
		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg, 'item_id' => $item_id));
		
	}

	
}

/* End of file main.php */
/* Location: ./application/controllers/admin/documents.php */