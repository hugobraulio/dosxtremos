<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payments extends Admin_Controller {

	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/

	function Payments()
	{

		// Inherit parent class methods and properties
		parent::__construct();

		// Load the models we will use in this controller
		$this->load->model('payment');
		$this->load->model('receipt');
        $this->load->model('alert');
		$this->load->model('booking');

	}


	/* DEFAULT METHOD
	------------------------------------------------------------------
	Description: Loads the items listing page
	----------------------------------------------------------------*/

	public function index()
	{

		// Leave blank for now
		redirect('/admin/bookings/index', 'refresh');
		exit();

	}


	/* PAY METHOD
	------------------------------------------------------------------
	Description: Used with AJAX call to confirm a payment and insert
	a receipt. Returns JSON string
	----------------------------------------------------------------*/

	public function pay($item_id=0)
	{

		// Set the defaults
		$status = '';
		$msg = '';
		$receipt_id = 0;
		$receipt_date = '';

		// Initialize the payment and set its date paid to today
		$payment = $this->payment->initialize($item_id);
		$payment->date_paid = now();
		$payment->trimester_paid = get_fiscal_trimester(now());
		$payment->save_entry();

		// Initialize the receipt and set its data
		$receipt = $this->receipt->initialize(0,$item_id);
		$receipt->payment_id = $item_id;
		$receipt->generate_content($item_id);
		$receipt->save_entry();
        
        // Delete the alert if exists
		$alerts_deleted = $this->alert->delete_alerts($item_id);
        
		$status = 'success';
		$msg = 'El pago ha sido marcada como pagado y se ha generado un recibo.';
		$item_id = $payment->item_id;
		$receipt_id = $receipt->item_id;
		$receipt_date = timestamp_to_date($receipt->date_created, 'd/m/Y');

		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg, 'item_id' => $item_id, 'receipt_id' => $receipt_id, 'receipt_date' => $receipt_date, 'alerts_deleted' => $alerts_deleted));

	}


	/* UNPAY METHOD
	------------------------------------------------------------------
	Description: Used with AJAX call to cancel a payment and remove
	a receipt. Returns JSON string
	----------------------------------------------------------------*/

	public function unpay($item_id=0)
	{

		// Set the defaults
		$status = '';
		$msg = '';

		// Get the receipt and remove it first
		$receipt = $this->receipt->initialize(0,$item_id);
		$this->receipt->delete_entry($receipt->item_id);

		// Initialize the payment and set its date paid and liquidated to null
		$payment = $this->payment->initialize($item_id);
		$payment->date_paid = '';
		$payment->trimester_paid = 0;
		$payment->date_liquidated = '';
		$payment->trimester_liquidated = 0;
		$payment->save_entry();
		
		//unmark the alerts from booking so that they can be created again
		$this->booking->unmark_alerts($payment->booking_id, $payment->sort_order);
		
		$status = 'success';
		$msg = 'El pago ha sido marcado como no pagado y se ha borrado el recibo.';
		$item_id = $payment->item_id;

		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg, 'item_id' => $item_id));

	}


	/* DELETE METHOD
	------------------------------------------------------------------
	Description: Deletes the payment and returns JSON for Ajax calls
	----------------------------------------------------------------*/

	public function delete($item_id=0)
	{

		// Set the defaults
		$status = '';
		$msg = '';

		// Delete the entry using the model
		$this->payment->delete_entry($item_id);

		$status = 'success';
		$msg = 'Se ha borrado el pago';

		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg, 'item_id' => $item_id));

	}

    	/* LIQUIDATE METHOD
	------------------------------------------------------------------
	Description: Used with AJAX call to confirm a payment and insert
	a receipt. Returns JSON string
	----------------------------------------------------------------*/

	public function liquidate($item_id=0)
	{

		// Set the defaults
		$status = '';
		$msg = '';

		// Initialize the payment and set its date liquidated to today
		$payment = $this->payment->initialize($item_id);
		$payment->date_liquidated = now();
    $payment->trimester_liquidated = get_fiscal_trimester(now());
		$payment->save_entry();
    $liquidation_date = timestamp_to_date(now(), 'd/m/Y');

		$status = 'success';
		$msg = 'El pago ha sido marcado como liquidado.';
		$item_id = $payment->item_id;

		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg, 'item_id' => $item_id, 'liquidation_date' => $liquidation_date));

	}

    /* DELETE LIQUIDATION METHOD
	-----------------------------------------------------------------------------------
	Description: Deletes the liquidation of the payment and returns JSON for Ajax calls
	---------------------------------------------------------------------------------*/

    public function delete_liquidation($item_id=0)
	{

		// Set the defaults
		$status = '';
		$msg = '';

		// Initialize the payment and set its date liquidated to today
		$payment = $this->payment->initialize($item_id);
		$payment->date_liquidated = NULL;
    $payment->trimester_liquidated = 0;
    $payment->save_entry();

		$status = 'success';
		$msg = 'Se ha borrado la liquidación del pago.';
		$item_id = $payment->item_id;

		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg, 'item_id' => $item_id));

	}

}

/* End of file main.php */
/* Location: ./application/controllers/admin/payments.php */
