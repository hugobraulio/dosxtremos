<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sessions extends CI_Controller {
    
    
    /* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Initializes and loads CI native sessions library.
	----------------------------------------------------------------*/
    
    function Sessions()
    {
        
        // Inherit parent class methods and properties
		parent::__construct();
		
		// Load the sessions library to keep track of user sessions
		// $this->load->library('session');

    }
	
	/* LOGIN METHOD
	------------------------------------------------------------------
	Description: Loads the admin login form
	----------------------------------------------------------------*/
	
    function login($failed='')
    {
		
		// Check for failed login attempts
		$data['failed'] = $failed;
		
        // Load the login form view
        $this->load->view('admin/login', $data);
    
    }
	
	/* AUTHENTICATE METHOD
	------------------------------------------------------------------
	Description: Calls the user_model and authenticates the user based
	on vars posted from the login form
	----------------------------------------------------------------*/
	
    function authenticate()
    {
        
        // Load the owner model
        $this->load->model('user', '', true);
        
        // Run the authentication
        $user_data = $this->user->authenticate($this->input->post('username'), $this->input->post('password'));
        
        // If authentication was succesful, set the session vars and sent the user to the main page (dashboard)
        if ($user_data['authenticated']) {
            
            // Set the session data
            $this->session->set_userdata('admin_loggedin', true);
            $this->session->set_userdata('admin_item_id', $user_data['item_id']);
            $this->session->set_userdata('admin_name', $user_data['name']);
            $this->session->set_userdata('admin_role', $user_data['role']);
            
            // Redirect the user to the main admin area page
            redirect('/admin/bookings/index/', 'refresh');
            
        } else {
        
        	// Redirect the user to the login form and exit the method
            redirect('/admin/sessions/login/failed', 'refresh');
            exit();
            
        }
        
    }
	
	/* LOGOUT METHOD
	------------------------------------------------------------------
	Description: Logs the admin user out by unsetting the users
	session variable
	----------------------------------------------------------------*/
	
    function logout()
    {
    
        // Remove the session data
        $this->session->unset_userdata('admin_loggedin');
        $this->session->unset_userdata('admin_item_id');
        $this->session->unset_userdata('admin_name');
        $this->session->unset_userdata('admin_role');
        
        // Redirect and exit the method
        redirect('/admin/sessions/login/', 'refresh');
        exit();
        
    }
    
}

/* End of file sessions.php */
/* Location: ./application/controllers/owners/sessions.php */