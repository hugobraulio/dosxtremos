<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Admin_Controller {
	
	
	// Create a property for this class
	// (used in checking for existing usernames in the check_username method of this class)
	var $user_item_id;
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Users()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load the models we will use in this controller
		$this->load->model('user');
		
		// Initialize this property to 0
		$this->user_item_id = 0;

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page
	----------------------------------------------------------------*/
	
	public function index()
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Load any extra models and/or libraries
		$this->load->library('pagination');
		
		// Set the defaults (based on URI segments)
		$offset = ($this->uri->segment(3) !== FALSE ? $this->uri->segment(3) : 0);
		$limit = 12;
		
		// Get the list of elements from the model and prep them for the view
		$data['users'] = $this->user->list_entries($limit,$offset)->result();
		foreach($data['users'] as $user) {
			$user->view_url = site_url(array('admin','users','view',$user->item_id));
			$user->edit_url = site_url(array('admin','users','action','edit',$user->item_id));
			$user->delete_url = site_url(array('admin','users','delete',$user->item_id));
		}
		
		// Add and list links
		$data['list_url'] = site_url(array('admin','users','index'));
		$data['add_url'] = site_url(array('admin','users','action','new'));
		
		// Configure the pagination
		$config['base_url'] = site_url(array('admin','users','index'));
		$config['total_rows'] = $this->user->get_total_entries();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 3;
		$config['num_links'] = 4;
		$config['anchor_class'] = 'btn';
		$config['cur_tag_open'] = '<span class="btn disabled">';
		$config['cur_tag_close'] = '</span>';
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		
		// Load the view with the data
		$this->load->view('admin/users_list', $data);
		
	}
	
	
	/* VIEW METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with data
	----------------------------------------------------------------*/
	
	public function view($item_id=0,$delete_item=FALSE)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Initialize an owner object with its data
		$data['user'] = $this->user->initialize($item_id);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($data['user']->is_new === TRUE) {
			redirect('/admin/users/index', 'refresh');
			exit();
		}
		
		// Add and prep any entry navigation links
		$data['list_url'] = site_url('admin/users');
		$data['edit_url'] = site_url(array('admin','users','action','edit',$item_id));
		$data['delete_url'] = site_url(array('admin','users','delete',$item_id));
		
		// Add the delete flag
		$data['delete_item'] = $delete_item;
		
		// Load the view with the data
		$this->load->view('admin/users_view', $data);
		
	}
	
	
	/* ACTION METHOD 
	------------------------------------------------------------------
	Description: Loads the items form page for 'new' or 'edit'
	----------------------------------------------------------------*/
	
	public function action($action='new',$item_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for cancels first
		if($this->input->post('submit') !== FALSE && $this->input->post('submit') == 'Cancelar') {
			redirect('/admin/users/index', 'refresh');
		}
		
		// Set the user_item_id variable of this class to the user that is being edited or created
		$this->user_item_id = $item_id;
		
		// Load any necessary libraries and helpers
		$this->load->library('form_validation');
		
		// Set the form validation rules on required elements
		$this->form_validation->set_rules('fname', 'Nombre', 'trim|required');
		$this->form_validation->set_rules('lname', 'Apellido(s)', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
		$this->form_validation->set_rules('username', 'Nombre Usuario', 'trim|required|min_length[8]|max_length[12]|callback_username_check');
		$this->form_validation->set_rules('password', 'Contrase&ntilde;a', 'trim|required|min_length[8]|max_length[12]');
		$this->form_validation->set_rules('role_id', 'Nivel', 'trim|required');
		
		// Set the error message delimeters
		$this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');
		
		// Initialize the owner model with the data
		$user = $this->user->initialize($item_id);
		
		// Check for form submissions
		if ($this->form_validation->run() == FALSE) {
		
			// INVALID SUBMISSION OR NO SUBMISSION - Display the form
			
			// Load the dependancies (for select and check boxes)
			$this->load->model('role');
			$this->load->helper('form');
			
			// Add the owner data for the form
			$data['user'] = $user;
			
			// Add the navigation data
			$data['list_url'] = site_url(array('admin','users','index'));
			$data['action'] = $action;
			$data['action_url'] = site_url(array('admin','users','action',$action,$item_id));
			$data['action_title'] = ($action == 'edit' ? ('Editar ' . $user->fname . ' ' . $user->lname) : 'A&ntilde;adir Usuario Nuevo');
			
			// Add the form select and check box data
			$data['all_roles'] = $this->role->list_entries()->result();
			
			// Load the form view with the data
			$this->load->view('admin/users_form', $data);
			
		
		} else {
		
			// VALID SUBMISSION - Set its parameters and save the entry
			
			// Set the 'simple' data parameters directly
			$user->fname = $this->input->post('fname');
			$user->lname = $this->input->post('lname');
			$user->email = $this->input->post('email');
			$user->username = $this->input->post('username');
			$user->password = $this->input->post('password');
			
			// Set the more 'complex' data types using class setters
			$user->set_role($this->input->post('role_id'));
			
			// Call the models save method
			$user->save_entry();
			
			// Send the user on their way to the listing page via an http redirect
			redirect('/admin/users/index', 'refresh');
		
		}
		
	}
	
	
	/* DELETE METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with a delete warning form
	or deletes the item on form submission.
	----------------------------------------------------------------*/
	
	public function delete($item_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for form confirmation
		if($this->input->post('submit')) {
			
			// Check the confirmation
			if ($this->input->post('submit') == 'Borrar') {
				
				// Delete the entry
				$this->user->delete_entry($item_id);
				
			}
			
			// Redirect the user to the list page via http redirect
			redirect('/admin/users/index');
			
		} else {
			
			// Call the view method with a delete flag to display the form
			$this->view($item_id,TRUE);
			
		}
		
	}
	
	
	/* USERNAME CHECK CALLBACK FUNCTION
	------------------------------------------------------------------
	Description: Used in conjunction with the form validation methods
	in the action method to test if a username is not already taken
	----------------------------------------------------------------*/
	
	public function username_check($str)
	{
		
		// Call the database directly to check for existing record
		$this->db->select('item_id, username');
		$this->db->from('users');
		$this->db->where('username', $str);
		$this->db->where('item_id !=', $this->user_item_id);
		$query_result = $this->db->get()->result();
		
		if ($query_result)
		{
			$error_msg = 'El nombre de usuario ' . $str . ' ya esta siendo utilizado por otro usuario';
			$this->form_validation->set_message('username_check', $error_msg);
			return FALSE;
		}
		else
		{
			return TRUE;
		}
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/admin/users.php */