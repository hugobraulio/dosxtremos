<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System_configs extends Admin_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function System_configs()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load the models we will use in this controller
		$this->load->model('system_config');

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items form with data and saves any form
	submission data
	----------------------------------------------------------------*/
	
	public function index($item_id=1)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Load any necessary libraries and helpers
		$this->load->library('form_validation');
		
		// Set the form validation rules on required elements
		$this->form_validation->set_rules('pay_percent_1', 'Primer Pago', 'trim|required|integer');
		$this->form_validation->set_rules('pay_percent_2', 'Segundo Pago', 'trim|required|integer');
		$this->form_validation->set_rules('commission_percent', 'Comisi&oacute;n', 'trim|required|integer');
		$this->form_validation->set_rules('tax_id', 'CIF/NIF/NIE', 'trim|required');
		$this->form_validation->set_rules('vat', 'IVA', 'trim|required|integer');
		$this->form_validation->set_rules('bank_name', 'Nombre Banco', 'trim|required');
		$this->form_validation->set_rules('bank_nationality', 'Nacionalidad Banco', 'trim|required');
		$this->form_validation->set_rules('bank_iban', 'C&oacute;digo IBAN', 'trim|required');
		$this->form_validation->set_rules('bank_bic', 'C&oacute;digo BIC', 'trim|required');
		$this->form_validation->set_rules('bank_account_name', 'Nombre Cuenta Bancaria', 'trim|required');
		$this->form_validation->set_rules('bank_account_number', 'N&uacute;mero de Cuenta Bancaria', 'trim|required');
		
		// Set the error message delimeters
		$this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');
		
		// Set the confirmation flag
		$data['confirm'] = FALSE;
		
		// Initialize the config model with the data
		$system_config = $this->system_config->initialize($item_id);
		
		// Check for form submissions
		if ($this->form_validation->run() == TRUE) {
		
			// VALID SUBMISSION - Set its parameters and save the entry
			
			// Set the 'simple' data parameters directly
			$system_config->pay_percent_1 = $this->input->post('pay_percent_1');
			$system_config->pay_percent_2 = $this->input->post('pay_percent_2');
			$system_config->commission_percent = $this->input->post('commission_percent');
			$system_config->tax_id = $this->input->post('tax_id');
			$system_config->vat = $this->input->post('vat');
			$system_config->bank->name = $this->input->post('bank_name');
			$system_config->bank->nationality = $this->input->post('bank_nationality');
			$system_config->bank->iban = $this->input->post('bank_iban');
			$system_config->bank->bic = $this->input->post('bank_bic');
			$system_config->bank->account->name = $this->input->post('bank_account_name');
			$system_config->bank->account->number = $this->input->post('bank_account_number');
			
			// Call the models save method
			$system_config->save_entry();
			
			// Set the confirmation flag
			$data['confirm'] = TRUE;
		
		}
		
		// Load the dependancies
		$this->load->helper('form');
		$data['all_nationalities'] = array('es' => 'Espa&ntilde;ol', 'en' => 'Ingl&eacute;s');
		
		// Get the data to pass to the view
		$data['system_config'] = $system_config;
		$data['action_url'] = site_url(array('admin','system_configs','index',$item_id));
		
		// Load the form view with the data
		$this->load->view('admin/config_form', $data);
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/admin/config.php */