<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conditions extends Admin_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Conditions()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load the models we will use in this controller
		$this->load->model('condition');

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page
	----------------------------------------------------------------*/
	
	public function index()
	{	
		
		// Leave blank for now
		redirect('/admin/bookings/index', 'refresh');
		exit();
		
	}
	
	
	/* VIEW METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with data
	----------------------------------------------------------------*/
	
	public function view($item_id=0,$lang_id='es')
	{	
		
		// Initialize an owner object with its data
		$condition = $this->condition->initialize($item_id, 0);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($condition->is_new === TRUE) {
			redirect('/admin/bookings/index', 'refresh');
			exit();
		}
		
		$data['item_id'] = $condition->item_id;
		$data['booking_id'] = $condition->booking_id;
		$data['date_created'] = timestamp_to_date($condition->date_created);
		$data['content'] = ($lang_id == 'en' ? $condition->content->en : $condition->content->es);
		
		// Load the view with the data
		$this->load->view('common/conditions_print', $data);
		
	}
	
	
	/* ACTION METHOD 
	------------------------------------------------------------------
	Description: Loads the items form page for 'new' or 'edit'
	----------------------------------------------------------------*/
	
	public function action($action='edit',$item_id=0,$booking_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Redirect string to use for cancels and succesful submissions
		$redirect_string = '/admin/bookings/view/' . $booking_id . '/documents';
		
		// Check for cancels first
		if($this->input->post('submit') !== FALSE && $this->input->post('submit') == 'Cancelar') {
			redirect($redirect_string, 'refresh');
		}
		
		// Load any necessary libraries and helpers
		$this->load->library('form_validation');
		
		// Set the form validation rules on required elements
		$this->form_validation->set_rules('date_created', 'Fecha', 'trim|required');
		$this->form_validation->set_rules('content_en', 'Texto en Ingl�s', 'trim|required');
		$this->form_validation->set_rules('content_es', 'Texto en Espa�ol', 'trim|required');
		
		// Set the error message delimeters
		$this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');
		
		// Initialize the condition model with the data
		$condition = $this->condition->initialize($item_id);
		
		// Check for form submissions
		if ($this->form_validation->run() == FALSE) {
		
			// INVALID SUBMISSION OR NO SUBMISSION - Display the form
			
			// Add the condition data for the form
			$data['condition'] = $condition;
			$data['condition']->date_created = timestamp_to_date($condition->date_created, 'd/m/Y');
			
			// Add the navigation data
			$data['list_url'] = site_url(array('admin','bookings','view',$booking_id,'documents'));
			$data['parent_url'] = site_url(array('admin','bookings','view',$booking_id));
			$data['action'] = $action;
			$data['action_url'] = site_url(array('admin','conditions','action',$action,$item_id,$booking_id));
			$data['action_title'] = ($action == 'edit' ? ('Editar Condiciones de Reserva para Booking N&deg; ' . $condition->reference_num) : 'A&ntilde;adir Usuario Nuevo');
			
			// Load the form view with the data
			$this->load->view('admin/conditions_form', $data);
			
		
		} else {
		
			// VALID SUBMISSION - Set its parameters and save the entry
			
			// Set the 'simple' data parameters directly
			$condition->booking_id = $booking_id;
			$condition->date_created = strtotime(str_replace('/', '-', $this->input->post('date_created')));
			$condition->content->es = $this->input->post('content_es');
			$condition->content->en = $this->input->post('content_en');
			
			// Call the models save method
			$condition->save_entry();
			
			// Send the user on their way to the listing page via an http redirect
			redirect($redirect_string, 'refresh');
		
		}
		
	}
	
	
	/* DELETE METHOD 
	------------------------------------------------------------------
	Description: Used with AJAX to delete a conditions document.
	Returns JSON
	----------------------------------------------------------------*/
	
	public function delete($item_id=0)
	{	
		
		// Set the defaults
		$status = '';
		$msg = '';
		
		// Delete the entry using the model
		$this->condition->delete_entry($item_id);
		
		$status = 'success';
		$msg = 'Se ha borrado las condiciones de reserva';
		
		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg, 'item_id' => $item_id));
		
	}
	
	
	
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/admin/users.php */