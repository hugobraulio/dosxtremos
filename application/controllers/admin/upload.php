<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: This controller is intended for AJAX Interaction only
	The constructor loads the CI File Upload libraries.
	----------------------------------------------------------------*/
	
	public function __construct()
	{
		parent::__construct();
		// Load the required libraries
		$this->load->library('upload');
		$this->load->helper('path');
	}
	
	
	/* DEFAULT METHOD
	------------------------------------------------------------------
	Description: Returns FALSE. Specific methods must be called within
	this controller.
	----------------------------------------------------------------*/
	
	public function index()
	{
		
		// Echo out JSON encoded response
		echo json_encode(array('status' => 'error', 'msg' => 'You must select a specific method in this controller to perform an upload.', 'file_name' => '', 'file_url' => ''));
		
	}
	
	
	/* BOOKING DOCUMENT UPLOAD
	------------------------------------------------------------------
	Description: Uploads booking document files
	----------------------------------------------------------------*/
	
	public function booking_document($booking_id=0)
	{
		
		// Set the defaults
		$status = '';
		$msg = '';
		$item_id = 0;
		$file_name = '';
		$download_url = '';
		$delete_url = '';
		$file_element_name = 'document_file';
		
		// Configure the file upload
		$config['upload_path'] = './uploads/booking_documents/';
		$config['allowed_types'] = 'pdf|gif|jpg|png';
		$config['max_size']  = 1024 * 8;
	
		// Initialize the upload library with the configutation
		$this->upload->initialize($config);
	
		if (!$this->upload->do_upload($file_element_name)) {
			
			// Something went wrong, set the error message
			$status = 'error';
			$msg = 'Error de subida de archivo : ' . $this->upload->display_errors('', '');
			
		} else {
		
			// Upload successful, get the file data
			$data = $this->upload->data();
			
			// Call the document model to insert the document into the database
			$this->load->model('document');
			$this->document->initialize();
			$this->document->filename = $data['file_name'];
			$this->document->set_booking($booking_id);
			$this->document->save_entry();
			
			// Collect the data to send back
			$status = 'success';
			$msg = 'El archivo ha sido subido con exito';
			$item_id = $this->document->item_id;
			$file_name = $data['file_name'];
			$download_url = site_url(array('admin','documents','download',$file_name,0));
			$delete_url = site_url(array('admin','documents','delete',$item_id));
			
			// Remove the temp post file (in memory)
			@unlink($_FILES[$file_element_name]);
			
		}
		
		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg, 'item_id' => $item_id, 'file_name' => $file_name, 'download_url' => $download_url, 'delete_url' => $delete_url));
		
	}
	
	
	/* MAINTENANCE RECEIPT UPLOAD
	------------------------------------------------------------------
	Description: Uploads the maintenance receipt files
	----------------------------------------------------------------*/
	
	public function maintenance_receipt()
	{
		
		// Set the defaults
		$status = '';
		$msg = '';
		$file_name = '';
		$file_url = '';
		$file_element_name = 'receipt_file';
		
		// Configure the file upload
		$config['upload_path'] = './uploads/maintenance_receipts/';
		$config['allowed_types'] = 'pdf|gif|jpg|png';
		$config['max_size']  = 1024 * 8;
	
		// Initialize the upload library with the configutation
		$this->upload->initialize($config);
	
		if (!$this->upload->do_upload($file_element_name)) {
			
			// Something went wrong, set the error message
			$status = 'error';
			$msg = 'Error de subida de archivo : ' . $this->upload->display_errors('', '');
			
		} else {
		
			// Upload successful, get the file data
			$data = $this->upload->data();
			
			// Collect the data to send back
			$status = 'success';
			$msg = 'El archivo ha sido subido con exito';
			$file_name = $data['file_name'];
			$file_url = base_url() . 'uploads/maintenance_receipts/' . $file_name;
			
			// Remove the temp post file (in memory)
			@unlink($_FILES[$file_element_name]);
			
		}
		
		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg, 'file_name' => $file_name, 'file_url' => $file_url));
		
	}
	
	
}

/* End of file upload.php */
/* Location: ./application/controllers/admin/upload.php */