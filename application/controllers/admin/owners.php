<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Owners extends Admin_Controller {
	
	// Create a property for this class
	// (used in checking for existing usernames in the check_username method of this class)
	var $owner_item_id;
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Owners()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load the models we will use in this controller
		$this->load->model('owner');
		
		// Initialize this property to 0
		$this->owner_item_id = 0;

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page
	----------------------------------------------------------------*/
	
	public function index()
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Load any extra models and/or libraries
		$this->load->library('pagination');
		
		// Set the defaults (based on URI segments)
		$offset = ($this->uri->segment(4) !== FALSE ? $this->uri->segment(4) : 0);
		$limit = 12;
		
		// Get the list of elements from the model and prep them for the view
		$data['owners'] = $this->owner->list_entries($limit,$offset)->result();
		foreach($data['owners'] as $owner) {
			$owner->view_url = site_url(array('admin','owners','view',$owner->item_id));
			$owner->edit_url = site_url(array('admin','owners','action','edit',$owner->item_id));
			$owner->delete_url = site_url(array('admin','owners','delete',$owner->item_id));
		}
		
		// Add and list links
		$data['list_url'] = site_url(array('admin','owners','index'));
		$data['add_url'] = site_url(array('admin','owners','action','new'));
		
		// Configure the pagination
		$config['base_url'] = site_url(array('admin','owners','index'));
		$config['total_rows'] = $this->owner->get_total_entries();
		$config['per_page'] = $limit;
		$config['uri_segment'] = 4;
		$config['num_links'] = 4;
		$config['anchor_class'] = 'class="btn"';
		$config['cur_tag_open'] = '<span class="btn disabled">';
		$config['cur_tag_close'] = '</span>';
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		
		// Load the view with the data
		$this->load->view('admin/owners_list', $data);
		
	}
	
	
	/* VIEW METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with data
	----------------------------------------------------------------*/
	
	public function view($item_id=0,$delete_item=FALSE)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Initialize an owner object with its data
		$data['owner'] = $this->owner->initialize($item_id);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($data['owner']->is_new === TRUE) {
			redirect('/admin/owners/index', 'refresh');
			exit();
		}
		
		// Add and prep any entry navigation links
		$data['list_url'] = site_url('admin/owners');
		$data['expenses_url'] = site_url(array('admin','owners','expenses',$item_id,date('Y')));
		$data['invoices_url'] = site_url(array('admin','invoices','index',$item_id,date('Y')));
		$data['edit_url'] = site_url(array('admin','owners','action','edit',$item_id));
		$data['delete_url'] = site_url(array('admin','owners','delete',$item_id));
		$data['status_url'] = site_url(array('admin','owners','set_active',$item_id,($data['owner']->is_active ? 0 : 1)));
		
		// Home links
		foreach($data['owner']->homes as $home) {
			$home->view_url = site_url(array('admin','homes','view',$home->item_id));
		}
		
		// Add the delete flag
		$data['delete_item'] = $delete_item;
		
		// Load the view with the data
		$this->load->view('admin/owners_view', $data);
		
	}
	
	
	/* EXPENSES METHOD 
	------------------------------------------------------------------
	Description: Displays the items expenses page with data
	----------------------------------------------------------------*/
	
	public function expenses($item_id=0,$year=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Get the owner data structure
		$data['owner'] = $this->owner->initialize($item_id);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($data['owner']->is_new === TRUE) {
			redirect('/admin/owners/index', 'refresh');
			exit();
		}
		
		// Default the year to current and init the models we will need
		$year = (!$year ? date('Y') : $year);
		$this->load->model('home');
		$this->load->model('maintenance');
		
		// For each home the owner has created an expenses dataset
		$counter = 0;
		foreach($data['owner']->homes as $home) {
			
			// Set the sort order
			$home->sort_order = $counter;
			
			// Totals
			
			$t1_bookings_net = 0;
			$t2_bookings_net = 0;
			$t3_bookings_net = 0;
			$t4_bookings_net = 0;
			
			$t1_bookings_vat = 0;
			$t2_bookings_vat = 0;
			$t3_bookings_vat = 0;
			$t4_bookings_vat = 0;
			
			$t1_bookings_liquidations = 0;
			$t2_bookings_liquidations = 0;
			$t3_bookings_liquidations = 0;
			$t4_bookings_liquidations = 0;
			
			$t1_bookings_total = 0;
			$t2_bookings_total = 0;
			$t3_bookings_total = 0;
			$t4_bookings_total = 0;
			
			$t1_bookings_liquidations_total = 0;
			$t2_bookings_liquidations_total = 0;
			$t3_bookings_liquidations_total = 0;
			$t4_bookings_liquidations_total = 0;
			
			$t1_maintenance_total = 0;
			$t2_maintenance_total = 0;
			$t3_maintenance_total = 0;
			$t4_maintenance_total = 0;
			
			$t1_grand_total = 0;
			$t2_grand_total = 0;
			$t3_grand_total = 0;
			$t4_grand_total = 0;
			
			$home->bookings = $this->home->booking_report($home->item_id,$year)->result();
			
			foreach($home->bookings as $booking) { 
				
				// Add amounts to totals
				$t1_bookings_net += $booking->t1_commissions;
				$t2_bookings_net += $booking->t2_commissions;
				$t3_bookings_net += $booking->t3_commissions;
				$t4_bookings_net += $booking->t4_commissions;
				
				$t1_bookings_vat += ($booking->t1_commissions * (21/100));
				$t2_bookings_vat += ($booking->t2_commissions * (21/100));
				$t3_bookings_vat += ($booking->t3_commissions * (21/100));
				$t4_bookings_vat += ($booking->t4_commissions * (21/100));
				
				$t1_bookings_liquidations_total += $booking->t1_liquidations;
				$t2_bookings_liquidations_total += $booking->t2_liquidations;
				$t3_bookings_liquidations_total += $booking->t3_liquidations;
				$t4_bookings_liquidations_total += $booking->t4_liquidations;
				
				// Format payments to currency
				$booking->t1_payments = ($booking->t1_payments ? (number_format($booking->t1_payments, 2, '.', ',') . ' &euro;') : '-');
				$booking->t2_payments = ($booking->t2_payments ? (number_format($booking->t2_payments, 2, '.', ',') . ' &euro;') : '-');
				$booking->t3_payments = ($booking->t3_payments ? (number_format($booking->t3_payments, 2, '.', ',') . ' &euro;') : '-');
				$booking->t4_payments = ($booking->t4_payments ? (number_format($booking->t4_payments, 2, '.', ',') . ' &euro;') : '-');
				
				// Add VAT to Commissions
				$booking->t1_commissions = $booking->t1_commissions + ($booking->t1_commissions * (21/100));
				$booking->t2_commissions = $booking->t2_commissions + ($booking->t2_commissions * (21/100));
				$booking->t3_commissions = $booking->t3_commissions + ($booking->t3_commissions * (21/100));
				$booking->t4_commissions = $booking->t4_commissions + ($booking->t4_commissions * (21/100));
				
				// Format Commissions to Currency and create links
				$booking->t1_commissions = ($booking->t1_commissions ? anchor(array('admin','bookings','costs',$booking->item_id,1,$year), (number_format($booking->t1_commissions, 2, '.', ',') . ' &euro;'), 'class="detail" title="Ver Detalle"' )  : '-' );
				$booking->t2_commissions = ($booking->t2_commissions ? anchor(array('admin','bookings','costs',$booking->item_id,2,$year), (number_format($booking->t2_commissions, 2, '.', ',') . ' &euro;'), 'class="detail" title="Ver Detalle"' )  : '-' );
				$booking->t3_commissions = ($booking->t3_commissions ? anchor(array('admin','bookings','costs',$booking->item_id,3,$year), (number_format($booking->t3_commissions, 2, '.', ',') . ' &euro;'), 'class="detail" title="Ver Detalle"' )  : '-' );
				$booking->t4_commissions = ($booking->t4_commissions ? anchor(array('admin','bookings','costs',$booking->item_id,4,$year), (number_format($booking->t4_commissions, 2, '.', ',') . ' &euro;'), 'class="detail" title="Ver Detalle"' )  : '-' );
				
				// Format Liquidations to Currency and create links
				$booking->t1_liquidations = ($booking->t1_liquidations ? (number_format($booking->t1_liquidations, 2, '.', ',') . ' &euro;') : '-' );
				$booking->t2_liquidations = ($booking->t2_liquidations ? (number_format($booking->t2_liquidations, 2, '.', ',') . ' &euro;') : '-' );
				$booking->t3_liquidations = ($booking->t3_liquidations ? (number_format($booking->t3_liquidations, 2, '.', ',') . ' &euro;') : '-' );
				$booking->t4_liquidations = ($booking->t4_liquidations ? (number_format($booking->t4_liquidations, 2, '.', ',') . ' &euro;') : '-' );
				
				// Format date and nav url
				// $booking->date_from = ucfirst(strftime('%b. %d', mysqldatetime_to_timestamp($booking->date_from)));
				$booking->date_from = mysqldatetime_to_date($booking->date_from, 'd/m/y');
				$booking->agreed_price = number_format($booking->agreed_price, 2, '.', ',');
				$booking->view_url = site_url(array('admin','bookings','view',$booking->item_id));
				
			}
			
			// Get Booking Totals (net + vat) for each trimester
			$t1_bookings_total = $t1_bookings_net + $t1_bookings_vat;
			$t2_bookings_total = $t2_bookings_net + $t2_bookings_vat;
			$t3_bookings_total = $t3_bookings_net + $t3_bookings_vat;
			$t4_bookings_total = $t4_bookings_net + $t4_bookings_vat;
			
			// Get Liquidations Totals (liquidations - vat) for each trimester
			//$t1_bookings_liquidations_total = $t1_bookings_liquidations - $t1_bookings_vat;
			//$t2_bookings_liquidations_total = $t2_bookings_liquidations - $t2_bookings_vat;
			//$t3_bookings_liquidations_total = $t3_bookings_liquidations - $t3_bookings_vat;
			//$t4_bookings_liquidations_total = $t4_bookings_liquidations - $t4_bookings_vat;
			
			// Setup and format booking totals for view
			
			$home->bookings_totals = new stdClass;
			
			$home->bookings_totals->t1_net = ($t1_bookings_net ? (number_format($t1_bookings_net, 2, '.', ',') . ' &euro;') : '-');
			$home->bookings_totals->t2_net = ($t2_bookings_net ? (number_format($t2_bookings_net, 2, '.', ',') . ' &euro;') : '-');
			$home->bookings_totals->t3_net = ($t3_bookings_net ? (number_format($t3_bookings_net, 2, '.', ',') . ' &euro;') : '-');
			$home->bookings_totals->t4_net = ($t4_bookings_net ? (number_format($t4_bookings_net, 2, '.', ',') . ' &euro;') : '-');
			
			$home->bookings_totals->t1_vat = ($t1_bookings_vat ? (number_format($t1_bookings_vat, 2, '.', ',') . ' &euro;') : '-');
			$home->bookings_totals->t2_vat = ($t2_bookings_vat ? (number_format($t2_bookings_vat, 2, '.', ',') . ' &euro;') : '-');
			$home->bookings_totals->t3_vat = ($t3_bookings_vat ? (number_format($t3_bookings_vat, 2, '.', ',') . ' &euro;') : '-');
			$home->bookings_totals->t4_vat = ($t4_bookings_vat ? (number_format($t4_bookings_vat, 2, '.', ',') . ' &euro;') : '-');
			
			$home->bookings_totals->t1_total = ($t1_bookings_total ? (number_format($t1_bookings_total, 2, '.', ',') . ' &euro;') : '-');
			$home->bookings_totals->t2_total = ($t2_bookings_total ? (number_format($t2_bookings_total, 2, '.', ',') . ' &euro;') : '-');
			$home->bookings_totals->t3_total = ($t3_bookings_total ? (number_format($t3_bookings_total, 2, '.', ',') . ' &euro;') : '-');
			$home->bookings_totals->t4_total = ($t4_bookings_total ? (number_format($t4_bookings_total, 2, '.', ',') . ' &euro;') : '-');
			
			$home->bookings_totals->t1_liquidations_total = ($t1_bookings_liquidations_total ? (number_format($t1_bookings_liquidations_total, 2, '.', ',') . ' &euro;') : '-');
			$home->bookings_totals->t2_liquidations_total = ($t2_bookings_liquidations_total ? (number_format($t2_bookings_liquidations_total, 2, '.', ',') . ' &euro;') : '-');
			$home->bookings_totals->t3_liquidations_total = ($t3_bookings_liquidations_total ? (number_format($t3_bookings_liquidations_total, 2, '.', ',') . ' &euro;') : '-');
			$home->bookings_totals->t4_liquidations_total = ($t4_bookings_liquidations_total ? (number_format($t4_bookings_liquidations_total, 2, '.', ',') . ' &euro;') : '-');
			
			// Get the maintenance jobs
			$home->maintenances = $this->maintenance->list_entries($home->item_id,$year)->result();
			foreach($home->maintenances as $maintenance) {
				
				// Add amounts to totals
				$t1_maintenance_total += ($maintenance->trimester == 1 ? $maintenance->amount : 0);
				$t2_maintenance_total += ($maintenance->trimester == 2 ? $maintenance->amount : 0);
				$t3_maintenance_total += ($maintenance->trimester == 3 ? $maintenance->amount : 0);
				$t4_maintenance_total += ($maintenance->trimester == 4 ? $maintenance->amount : 0);
				
				// Format Payments
				$maintenance->t1_payments = ($maintenance->trimester == 1 ? (number_format($maintenance->amount, 2, '.', ',') . ' &euro;') : '-');
				$maintenance->t2_payments = ($maintenance->trimester == 2 ? (number_format($maintenance->amount, 2, '.', ',') . ' &euro;') : '-');
				$maintenance->t3_payments = ($maintenance->trimester == 3 ? (number_format($maintenance->amount, 2, '.', ',') . ' &euro;') : '-');
				$maintenance->t4_payments = ($maintenance->trimester == 4 ? (number_format($maintenance->amount, 2, '.', ',') . ' &euro;') : '-');
				
				// Format date and nav url
				$maintenance->date_payed = mysqldatetime_to_date($maintenance->date_payed, 'd/m/y');
				$maintenance->view_url = site_url(array('admin','maintenances','view',$maintenance->item_id));
				
			}
			
			// Format Maintenance amounts to currency
			$home->maintenance_totals = new stdClass;
			$home->maintenance_totals->t1_total = ($t1_maintenance_total ? (number_format($t1_maintenance_total, 2, '.', ',') . ' &euro;') : '-');
			$home->maintenance_totals->t2_total = ($t2_maintenance_total ? (number_format($t2_maintenance_total, 2, '.', ',') . ' &euro;') : '-');
			$home->maintenance_totals->t3_total = ($t3_maintenance_total ? (number_format($t3_maintenance_total, 2, '.', ',') . ' &euro;') : '-');
			$home->maintenance_totals->t4_total = ($t4_maintenance_total ? (number_format($t4_maintenance_total, 2, '.', ',') . ' &euro;') : '-');
			
			// Get the Grand Totals (Booking Commission Totals + Maintenance Totals)
			$t1_grand_total = $t1_bookings_total + $t1_maintenance_total;
			$t2_grand_total = $t2_bookings_total + $t2_maintenance_total;
			$t3_grand_total = $t3_bookings_total + $t3_maintenance_total;
			$t4_grand_total = $t4_bookings_total + $t4_maintenance_total;
			
			// Format the Grand Total amounts to currency
			$home->grand_totals = new stdClass;
			$home->grand_totals->t1_total = ($t1_grand_total ? (number_format($t1_grand_total, 2, '.', ',') . ' &euro;') : '-');
			$home->grand_totals->t2_total = ($t2_grand_total ? (number_format($t2_grand_total, 2, '.', ',') . ' &euro;') : '-');
			$home->grand_totals->t3_total = ($t3_grand_total ? (number_format($t3_grand_total, 2, '.', ',') . ' &euro;') : '-');
			$home->grand_totals->t4_total = ($t4_grand_total ? (number_format($t4_grand_total, 2, '.', ',') . ' &euro;') : '-');
			
			// Increment the counter for the next home
			$counter++;
			
		}
		
		// Add the year and navigation links
		$data['year'] = $year;
		$data['back_url'] = site_url(array('admin','owners','expenses',$item_id,($year - 1)));
		$data['forward_url'] = site_url(array('admin','owners','expenses',$item_id,($year + 1)));
		$data['list_url'] = site_url('admin/owners');
		$data['parent_url'] = site_url(array('admin','owners','view',$item_id));
		
		// Load the view with the data
		$this->load->view('admin/owners_expenses', $data);
		
	}

	
	/* ACTION METHOD 
	------------------------------------------------------------------
	Description: Loads the items form page for 'new' or 'edit'
	----------------------------------------------------------------*/
	
	public function action($action='new',$item_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for cancels first
		if($this->input->post('submit') !== FALSE && $this->input->post('submit') == 'Cancelar') {
			redirect('/admin/owners/index', 'refresh');
		}
		
		// Set the owner_item_id variable of this class to the owner that is being edited or created
		$this->owner_item_id = $item_id;
		
		// Load any necessary libraries and helpers
		$this->load->library('form_validation');
		
		// Set the form validation rules on required elements
		$this->form_validation->set_rules('fname', 'Nombre', 'trim|required');
		$this->form_validation->set_rules('lname', 'Apellido(s)', 'trim|required');
		$this->form_validation->set_rules('tax_id', 'NIF/NIE/CIF', 'trim|required');
		$this->form_validation->set_rules('address', 'Direcci&oacute;n', 'trim|required');
		$this->form_validation->set_rules('city', 'Municipio', 'trim|required');
		$this->form_validation->set_rules('province', 'Provincia', 'trim|required');
		$this->form_validation->set_rules('postal_code', 'C&oacute;digo Postal', 'trim|required');
		$this->form_validation->set_rules('country', 'Pa&iacute;s', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
		$this->form_validation->set_rules('telephone', 'Tel&eacute;fono Fijo', 'trim|required');
		$this->form_validation->set_rules('username', 'Nombre Usuario', 'trim|required|min_length[8]|max_length[12]|callback_username_check');
		$this->form_validation->set_rules('password', 'Contrase&ntilde;a', 'trim|required|min_length[8]|max_length[12]');
		$this->form_validation->set_rules('language_id', 'Idioma', 'trim|required');
		$this->form_validation->set_rules('bank_name', 'Nombre Banco', 'trim|required');
		$this->form_validation->set_rules('bank_nationality', 'Nacionalidad Banco', 'trim|required');
		$this->form_validation->set_rules('bank_iban', 'C&oacute;digo IBAN', 'trim|required');
		$this->form_validation->set_rules('bank_bic', 'C&oacute;digo BIC', 'trim|required');
		$this->form_validation->set_rules('bank_account_name', 'Nombre Cuenta Bancaria', 'trim|required');
		$this->form_validation->set_rules('bank_account_number', 'N&uacute;mero de Cuenta Bancaria', 'trim|required');
		
		// Optional Elements (run in if statements to check for their presence - NOTE: CI requires this to return posted values)
		if($this->input->post('mobile')) {
			$this->form_validation->set_rules('mobile', 'Tel&eacute;fono M&oacute;vil', 'trim|required');
		}
		if($this->input->post('fax')) {
			$this->form_validation->set_rules('fax', 'Fax', 'trim|required');
		}
		if($this->input->post('is_admin')) {
			$this->form_validation->set_rules('is_admin', 'Administrador', 'required');
		}
		if($this->input->post('notes')) {
			$this->form_validation->set_rules('notes', 'Notas Adicionales', 'trim|required');
		}
		
		// Set the error message delimeters
		$this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');
		
		// Initialize the owner model with the data
		$owner = $this->owner->initialize($item_id);
		
		// Check for form submissions
		if ($this->form_validation->run() == FALSE) {
		
			// INVALID SUBMISSION OR NO SUBMISSION - Display the form
			
			// Load the dependancies (for select and check boxes)
			$this->load->model('language');
			$this->load->helper('form');
			$data['all_nationalities'] = array('es' => 'Espa&ntilde;ol', 'en' => 'Ingl&eacute;s');
			
			// Add the owner data for the form
			$data['owner'] = $owner;
			
			// Add the navigation data
			$data['list_url'] = site_url(array('admin','owners','index'));
			$data['action'] = $action;
			$data['action_url'] = site_url(array('admin','owners','action',$action,$item_id));
			$data['action_title'] = ($action == 'edit' ? ('Editar ' . $owner->fname . ' ' . $owner->lname) : 'A&ntilde;adir Propietario Nuevo');
			
			// Add the form select and check box data
			$data['all_languages'] = $this->language->list_entries()->result();
			
			// Load the form view with the data
			$this->load->view('admin/owners_form', $data);
			
		
		} else {
		
			// VALID SUBMISSION - Set its parameters and save the entry
			
			// Set the 'simple' data parameters directly
			$owner->fname = $this->input->post('fname');
			$owner->lname = $this->input->post('lname');
			$owner->is_admin = $this->input->post('is_admin');
			$owner->tax_id = $this->input->post('tax_id');
			$owner->address = $this->input->post('address');
			$owner->city = $this->input->post('city');
			$owner->province = $this->input->post('province');
			$owner->postal_code = $this->input->post('postal_code');
			$owner->country = $this->input->post('country');
			$owner->email = $this->input->post('email');
			$owner->telephone = $this->input->post('telephone');
			$owner->mobile = $this->input->post('mobile');
			$owner->fax = $this->input->post('fax');
			$owner->username = $this->input->post('username');
			$owner->password = $this->input->post('password');
			$owner->notes = $this->input->post('notes');
			$owner->bank->name = $this->input->post('bank_name');
			$owner->bank->nationality = $this->input->post('bank_nationality');
			$owner->bank->iban = $this->input->post('bank_iban');
			$owner->bank->bic = $this->input->post('bank_bic');
			$owner->bank->account->name = $this->input->post('bank_account_name');
			$owner->bank->account->number = $this->input->post('bank_account_number');
			
			// Set the more 'complex' data types using class setters
			$owner->set_language($this->input->post('language_id'));
			
			// Call the models save method
			$owner->save_entry();
			
			// Send the user on their way to the listing page via an http redirect
			redirect('/admin/owners/index', 'refresh');
		
		}
		
	}
	
	
	/* SET ACTIVE METHOD 
	------------------------------------------------------------------
	Description: Sets the items status (activated/deactivated).
	
	For AJAX Calls
	----------------------------------------------------------------*/
	
	public function set_active($item_id=0,$is_active=0)
	{	
		
		// Set the defaults
		$status = '';
		$msg = '';
		
		// Call the function in the owner model to update the is_active status
		$this->owner->save_active_state($item_id,$is_active);
		$status = 'success';
		$msg = 'El propietario ha sido ' . ($is_active ? 'activado' : 'desactivado');
		
		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg, 'item_id' => $item_id, 'is_active' => $is_active));
		
	}
	
	
	/* DELETE METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with a delete warning form
	or deletes the item on form submission.
	----------------------------------------------------------------*/
	
	public function delete($item_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for form confirmation
		if($this->input->post('submit')) {
			
			// Check the confirmation
			if ($this->input->post('submit') == 'Borrar') {
				
				// Delete the entry
				$this->owner->delete_entry($item_id);
				
			}
			
			// Redirect the user to the list page via http redirect
			redirect('/admin/owners/index');
			
		} else {
			
			// Call the view method with a delete flag to display the form
			$this->view($item_id,TRUE);
			
		}
		
	}
	
	
	/* USERNAME CHECK CALLBACK FUNCTION
	------------------------------------------------------------------
	Description: Used in conjunction with the form validation methods
	in the action method to test if a username is not already taken
	----------------------------------------------------------------*/
	
	public function username_check($str)
	{
		
		// Call the database directly to check for existing record
		$this->db->select('item_id, username');
		$this->db->from('owners');
		$this->db->where('username', $str);
		$this->db->where('item_id !=', $this->owner_item_id);
		$query_result = $this->db->get()->result();
		
		if ($query_result)
		{
			$error_msg = 'El nombre de usuario ' . $str . ' ya esta siendo utilizado por otro propietario';
			$this->form_validation->set_message('username_check', $error_msg);
			return FALSE;
		}
		else
		{
			return TRUE;
		}
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/admin/owners.php */