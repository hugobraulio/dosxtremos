<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bookings extends Admin_Controller {
	
	/* Create a properties for this class
	Used in checking for existing booking reference number in the check_reference method
	of this class and the check_dates method */
	var $booking_item_id;
	var $booking_home_id;
	var $booking_old_status;
	var $required_fields;
	
	// Hash from a timestamp used to override caching problems by client
	var $hash;
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Bookings()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load the models we will use in this controller
		$this->load->model('booking');
		$this->load->model('status');
		
		// Initialize this classes properties (used by callback functions)
		$this->booking_item_id = 0;
		$this->booking_home_id = 0;
		$this->booking_old_status = $this->status->initialize(1);
		$this->required_fields = Array();
		
		// Get the hash from the current time
		$this->hash = time();

	}
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page (main admin calendar)
	----------------------------------------------------------------*/
	
	public function index($year=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Default the year to current if before October 1st or the following year if after
		if(!$year) {
			$current_month = date('n');
			$current_year = date('Y');
			if($current_month >= 10) {
				$year = $current_year + 1;
			} else {
				$year = $current_year;
			}
		}
		
		// Get the calendar
		$data['calendar'] = $this->booking->get_calendar($year);
		
		// Prep the calendar for the view
		foreach($data['calendar']->weeks as $week) {
			foreach($week->timeslots as $timeslot) {
				
				// Set class names for the timeslot based on offers, bookings and if a price has been set
				$timeslot->view_class = '';
				if($timeslot->has_prices) {
					$timeslot->view_class = ($timeslot->offers ? 'offered' : $timeslot->view_class);
					$timeslot->view_class = ($timeslot->prebooked ? 'pre-booked' : $timeslot->view_class);
					$timeslot->view_class = ($timeslot->booked ? 'booked' : $timeslot->view_class);
				}
				$timeslot->view_class = (!$timeslot->price ? $timeslot->view_class . ' no-price' : $timeslot->view_class);
				$timeslot->view_class = trim($timeslot->view_class);
				
				// Set the price to display
				$timeslot->price = ($timeslot->price ? number_format($timeslot->price, 2, '.', '') : '');
				if($timeslot->agreed_price) {
					if($timeslot->date_start <= $timeslot->booking_date && $timeslot->date_end >= $timeslot->booking_date) {
						$timeslot->view_price = $timeslot->agreed_price;
					} else {
						$timeslot->view_price = '-';
					}
				} else {
					$timeslot->view_price = $timeslot->price;
				}
				
				// $timeslot->view_price = ($timeslot->agreed_price ? $timeslot->agreed_price : $timeslot->price);
				
				// Set the view URL for this timeslot
				if($timeslot->has_prices) {
					$timeslot->url = site_url(array('admin','timeslots','view_week',$timeslot->home->item_id,$timeslot->date_start,$timeslot->date_end,$this->hash));
				} else {
					$timeslot->url = site_url(array('admin','timeslots','action','new',$timeslot->home->item_id,$timeslot->date_start,$timeslot->date_end,$this->hash));
				}
				
			}
		}
		
		// Set the homes navigation
		foreach($data['calendar']->homes as $home) {
			$home->calendar_url = site_url(array('admin','homes','calendar',$home->item_id,$year));
		}
		
		// Add year navigation urls
		$data['back_url'] = site_url(array('admin','bookings','index',($year - 1),$this->hash));
		$data['forward_url'] = site_url(array('admin','bookings','index',($year + 1),$this->hash));
		
		// Load the view with the data
		$this->load->view('admin/bookings_list', $data);
		
		
	}
	
	
	/* CANCELLED METHOD 
	------------------------------------------------------------------
	Description: Gets a list of all cancelled bookings and displays
	them by year.
	----------------------------------------------------------------*/
	
	public function cancelled($year=0) {
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Default the year to current
		$year = (!$year ? date('Y') : $year);
		
		// Get the cancelled bookings from all homes for the year and prep them fro the view
		$data['bookings'] = $this->booking->list_entries(0,$year,0)->result();
		foreach($data['bookings'] as $booking) {
			$booking->date_from = mysqldatetime_to_date($booking->date_from, 'd/m/Y');
			$booking->date_to = mysqldatetime_to_date($booking->date_to, 'd/m/Y');
			$booking->view_url = site_url(array('admin','bookings','view',$booking->item_id));
			$booking->edit_url = site_url(array('admin','bookings','action','edit',$booking->status_permalink,$booking->item_id));
			$booking->delete_url = site_url(array('admin','bookings','delete',$booking->item_id));
		}
		
		// Add year and navigation urls
		$data['current_year'] = $year;
		$data['back_url'] = site_url(array('admin','bookings','cancelled',($year - 1),$this->hash));
		$data['forward_url'] = site_url(array('admin','bookings','cancelled',($year + 1),$this->hash));
		$data['home_url'] = site_url(array('admin','bookings','index',0,$this->hash));
		$data['list_url'] = site_url(array('admin','bookings','index',0,$this->hash));
		
		// Load the view with the data
		$this->load->view('admin/bookings_list_cancelled', $data);
		
	}
	
	
	/* VIEW METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with data
	----------------------------------------------------------------*/
	
	public function view($item_id=0,$tab='',$delete_item=FALSE)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Initialize a booking object with its data
		$data['booking'] = $this->booking->initialize($item_id);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($data['booking']->is_new === TRUE) {
			redirect('/admin/bookings/index', 'refresh');
			exit();
		}
		
		// Add and prep any entry navigation links
		$data['active_tab'] = $tab;
		$data['home_url'] = site_url(array('admin','bookings','index',0,$this->hash));
		$data['list_url'] = site_url(array('admin','bookings','index',0,$this->hash));
		$data['edit_url'] = site_url(array('admin','bookings','action','edit',$data['booking']->status->permalink,$item_id,$data['booking']->home->item_id,$data['booking']->date_from,$data['booking']->date_to));
		$data['delete_url'] = site_url(array('admin','bookings','delete',$item_id));
		$data['convert_to_offer_url'] = ($data['booking']->status->item_id > 1 ? site_url(array('admin','bookings','action','edit','offer',$item_id,$data['booking']->home->item_id,$data['booking']->date_from,$data['booking']->date_to)) : NULL);
		$data['convert_to_prebooking_url'] = ($data['booking']->status->item_id == 1 ? site_url(array('admin','bookings','action','edit','prebooking',$item_id,$data['booking']->home->item_id,$data['booking']->date_from,$data['booking']->date_to)) : NULL);
		$data['convert_to_booking_url'] = ($data['booking']->status->item_id == 2 ? site_url(array('admin','bookings','action','edit','booking',$item_id,$data['booking']->home->item_id,$data['booking']->date_from,$data['booking']->date_to)) : NULL);
		
		// Prep the data for the view
		$data['booking']->date_from = timestamp_to_date($data['booking']->date_from, 'd/m/Y');
		$data['booking']->date_to = timestamp_to_date($data['booking']->date_to, 'd/m/Y');
		$data['booking']->date_offered = timestamp_to_date($data['booking']->date_offered, 'd/m/Y');
		$data['booking']->date_booked = ($data['booking']->date_booked ? timestamp_to_date($data['booking']->date_booked, 'd/m/Y') : '');
		$data['booking']->arrival_time = ($data['booking']->arrival_time ? timestamp_to_date($data['booking']->arrival_time, 'H:i') : NULL);
		
		// Status class (for label tag)
		switch($data['booking']->status->item_id) {
			case 3 :
				$data['booking']->status->class_name = 'label label-important';
				break;
			case 2 :
				$data['booking']->status->class_name = 'label label-warning';
				break;
			case 1 :
				$data['booking']->status->class_name = 'label label-success';
				break;
			default:
				$data['booking']->status->class_name = 'label';
		}
		
		// Full Address
		$data['booking']->full_address = '';
		if($data['booking']->address) {
			$data['booking']->full_address = $data['booking']->address . '<br/>';
			$data['booking']->full_address = $data['booking']->full_address . $data['booking']->postal_code;
			$data['booking']->full_address = $data['booking']->full_address . ' ' . $data['booking']->city;
			$data['booking']->full_address = $data['booking']->full_address . ' (' . $data['booking']->province . ') ';
			$data['booking']->full_address = $data['booking']->full_address . $data['booking']->country;
		}
		
		// Home Link
		$data['booking']->home->view_url = site_url(array('admin','homes','view',$data['booking']->home->item_id));
		
		// Payments
		foreach($data['booking']->payments as $payment) {
			$payment->date_due = timestamp_to_date($payment->date_due, 'd/m/Y');
			$payment->date_paid = ($payment->date_paid ? timestamp_to_date($payment->date_paid, 'd/m/Y') : '-' );
			$liquidation_done = ($payment->date_liquidated != null && $payment->date_liquidated != "-" && $payment->date_liquidated != '');
			$payment->date_liquidated = ($liquidation_done ? timestamp_to_date($payment->date_liquidated, 'd/m/Y') : '-' );
			$payment->view_url_es = ($payment->receipt_id ? site_url(array('admin','receipts','view',$payment->receipt_id,'es')) : NULL);
			$payment->view_url_en = ($payment->receipt_id ? site_url(array('admin','receipts','view',$payment->receipt_id,'en')) : NULL);
			$payment->pay_url = ($payment->receipt_id ? NULL : site_url(array('admin','payments','pay',$payment->item_id)));
			$payment->edit_url = ($payment->receipt_id ? site_url(array('admin','receipts','action','edit',$payment->receipt_id,$payment->item_id)) : NULL);
			$payment->unpay_url = ($payment->receipt_id ? site_url(array('admin','payments','unpay',$payment->item_id)) : NULL);
			$payment->liquidate_url = ($liquidation_done ? NULL : site_url(array('admin','payments','liquidate',$payment->item_id)));
			$payment->delete_liquidation_url = ($payment->date_liquidated ? site_url(array('admin','payments','delete_liquidation',$payment->item_id)) : NULL);
		}
		
		// Conditions
		if($data['booking']->conditions->item_id) {
			$data['booking']->conditions->view_url_es = site_url(array('admin','conditions','view',$data['booking']->conditions->item_id,'es'));
			$data['booking']->conditions->view_url_en = site_url(array('admin','conditions','view',$data['booking']->conditions->item_id,'en'));
			$data['booking']->conditions->edit_url = site_url(array('admin','conditions','action','edit',$data['booking']->conditions->item_id,$data['booking']->item_id));
		}
		
		// Comments
		foreach($data['booking']->comments as $comment) {
			$comment->date_comment = timestamp_to_date($comment->date_comment, 'd/m/Y');
			$comment->view_url = site_url(array('admin','comments','view',$comment->item_id));
			$comment->edit_url = site_url(array('admin','comments','action','edit',$data['booking']->item_id,$comment->item_id));
			$comment->delete_url = site_url(array('admin','comments','delete',$data['booking']->item_id,$comment->item_id));
		}
		$data['add_comment_url'] = site_url(array('admin','comments','action','new',$data['booking']->item_id));
		
		// Documents
		foreach($data['booking']->documents as $document) {
			$document->download_url = site_url(array('admin','documents','download',$document->filename,0));
			$document->delete_url = site_url(array('admin','documents','delete',$document->item_id));
		}
		$data['add_document_url'] = site_url(array('admin','upload','booking_document',$data['booking']->item_id));
		
		
		// Add the delete flag
		$data['delete_item'] = $delete_item;
		
		// Load the view with the data
		$this->load->view('admin/bookings_view', $data);
		
	}
	
	
	/* ACTION METHOD 
	------------------------------------------------------------------
	Description: Loads the items form page for 'new', 'convert', or
	'edit' operations on the item. Takes in a type parameter which is
	used by the form to display/hide fields and set the server side
	validation
	----------------------------------------------------------------*/
	
	public function action($action='new',$type='offer',$item_id=0,$home_id=0,$date_start=0,$date_end=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for cancels first
		if($this->input->post('submit') !== FALSE && $this->input->post('submit') == 'Cancelar') {
			redirect('/admin/bookings/index', 'refresh');
		}
		
		// Load any necessary libraries and helpers
		$this->load->library('form_validation');
		$this->load->model('timespan');
		
		// Initialize the booking and timeslot models with data
		$booking = $this->booking->initialize($item_id);
		
		// Save the current status (Used later to determine conversions)
		$this->booking_old_status = $booking->status;
		
		// Set the new status (From the URL param)
		$booking->set_status($type);
		
		// If this is a new offer or booking, set it's defaults
		if($action == 'new') {
			
			// For all booking types
			$timespan = $this->timespan->initialize($home_id, $date_start, $date_end);
			$booking->set_home($home_id);
			$booking->date_from = $date_start;
			$booking->date_to = $date_end;
			$booking->agreed_price = $timespan->price;
			$booking->set_default_reference_num($date_start);
			$booking->arrival_time = strtotime('+ 16 hours', $date_start);
			
		}
		
		// If this is a prebooking or a booking, or an offer being coverted to one, make sure it has a default payments, date_booked, and arrival time
		if($booking->status->item_id > 1) {
			if(!$booking->payments) {
				$booking->set_default_payments();
			}
			if(!$booking->date_booked) {
				$booking->date_booked = now();
			}
			if(!$booking->arrival_time) {
				$booking->arrival_time = strtotime('+ 16 hours', $date_start);
			}
		}
		
		// Set the controller variables of this class to the booking that is being edited or created
		$this->booking_item_id = $booking->item_id;
		$this->booking_home_id = $booking->home->item_id;
		
		// Set the universal form validation rules on required elements (required for offers, prebookings and bookings)
		$this->form_validation->set_rules('fname', 'Nombre', 'trim|required');
		$this->form_validation->set_rules('lname', 'Apellido(s)', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('reference_num', 'N. Referencia', 'trim|required|alpha_dash|exact_length[14]|callback_check_reference');
		$this->form_validation->set_rules('date_from', 'Desde', ('trim|required|valid_date'));
		$this->form_validation->set_rules('date_to', 'Hasta', ('trim|required|valid_date|callback_check_datespan'));
		
		// Add the required field names to the array
		$this->required_fields = array('fname','lname','email','reference_num','date_from','date_to');
		
		// Run a switch on the required fields for each type (offer, prebooking, booking)
		switch($booking->status->item_id) {
			
			// Offers (1)
			case 1:
				
				$this->form_validation->set_rules('date_offered', 'Fecha de la Oferta', 'trim|required|valid_date');
				
				$additional_fields = array('date_offered');
				
				break;
				
			// Prebooking (2)
			case 2:
				$this->form_validation->set_rules('date_booked', 'Fecha del Booking', 'trim|required|valid_date');
				$this->form_validation->set_rules('booking_source', 'Origen del Booking', 'trim|required');
				$this->form_validation->set_rules('agreed_price', 'Precio Acordado', 'trim|required|decimal');
				$this->form_validation->set_rules('adults', 'Adultos', 'trim|required|integer');
				$this->form_validation->set_rules('children', 'Ni&ntilde;os', 'trim|required|integer');
				$this->form_validation->set_rules('cribs', 'Cunas', 'trim|required|integer');
				$this->form_validation->set_rules('payment_ids', 'Pagos', 'trim|required');
				$this->form_validation->set_rules('payment_percentages', 'Porcentages', 'trim|required');
				$this->form_validation->set_rules('payment_amounts', 'Cantidades', 'trim|required');
				$this->form_validation->set_rules('payment_dates_due', 'Fechas Pendientes', 'trim|required');
				$this->form_validation->set_rules('payment_commissions', 'Comisiones', 'trim|required');
				$this->form_validation->set_rules('payment_sort_orders', 'Orden de Pagos', 'trim|required');
				
				$additional_fields = array('date_booked','booking_source','agreed_price','adults','children','cribs','payment_ids','payment_percentages','payment_amounts','payment_dates_due', 'payment_commissions', 'payment_sort_orders');
				
				break;
				
			// Booking (3)
			case 3:
				$this->form_validation->set_rules('address', 'Direcci&oacute;n', 'trim|required');
				$this->form_validation->set_rules('city', 'Municipio', 'trim|required');
				$this->form_validation->set_rules('province', 'Provincia', 'trim|required');
				$this->form_validation->set_rules('country', 'Pa&iacute;s', 'trim|required');
				$this->form_validation->set_rules('mobile', 'Tel. M&oacute;vil', 'trim|required');
				
				$this->form_validation->set_rules('date_booked', 'Fecha del Booking', 'trim|required|valid_date');
				$this->form_validation->set_rules('booking_source', 'Origen del Booking', 'trim|required');
				$this->form_validation->set_rules('agreed_price', 'Precio Acordado', 'trim|required|decimal');
				$this->form_validation->set_rules('adults', 'Adultos', 'trim|required|integer');
				$this->form_validation->set_rules('children', 'Ni&ntilde;os', 'trim|required|integer');
				$this->form_validation->set_rules('cribs', 'Cunas', 'trim|required|integer');
				$this->form_validation->set_rules('payment_ids', 'Pagos', 'trim|required');
				$this->form_validation->set_rules('payment_percentages', 'Porcentages', 'trim|required');
				$this->form_validation->set_rules('payment_amounts', 'Cantidades', 'trim|required');
				$this->form_validation->set_rules('payment_dates_due', 'Fechas Pendientes', 'trim|required');
				$this->form_validation->set_rules('payment_commissions', 'Comisiones', 'trim|required');
				$this->form_validation->set_rules('payment_sort_orders', 'Orden de Pagos', 'trim|required');
				$this->form_validation->set_rules('payment_type_id', 'Tipo de Pago', 'trim|required|integer');
				
				$additional_fields = array('address','city','province','country','mobile','date_booked','booking_source','agreed_price','adults','children','cribs','payment_ids','payment_percentages','payment_amounts','payment_dates_due', 'payment_commissions', 'payment_sort_orders', 'payment_type_id');
				
				break;
			
		}
		
		// Append all the required fields (based on new status) to the base required fields
		$this->required_fields = array_merge($this->required_fields, $additional_fields);
		
		// Set optional fields for all (required by CI to remember their input values)
		if($this->input->post('is_active') != '') {
			$this->form_validation->set_rules('is_active', 'Estado', 'trim|required');
		}
		if($this->input->post('expense_item_ids') != '') {
			$this->form_validation->set_rules('expense_item_ids', 'Gastos ID', 'trim|required');
			$this->form_validation->set_rules('expense_descriptions', 'Descripcion de Gastos', 'trim|required');
			$this->form_validation->set_rules('expense_amounts', 'Cantidad de Gastos', 'trim|required');
		}
		if($this->input->post('tax_id')) {
			$this->form_validation->set_rules('tax_id', 'NIF/Pasaporte', 'trim|required');
		}
		if($this->input->post('down_payment')) {
			$this->form_validation->set_rules('down_payment', 'Fianza', 'trim|required|decimal');
		}
		if($this->input->post('postal_code')) {
			$this->form_validation->set_rules('postal_code', 'C&oacutedigo Postal', 'trim|required');
		}
		if($this->input->post('notes')) {
			$this->form_validation->set_rules('notes', 'Notas Adicionales', 'trim|required');
		}
		if($this->input->post('airport_id')) {
			$this->form_validation->set_rules('airport_id', 'Aeropuerto de Llegada', 'trim|required|integer');
		}
		if($this->input->post('payment_type_id')) {
			$this->form_validation->set_rules('payment_type_id', 'Tipo de Pago', 'trim|required|integer');
		}
		if($this->input->post('arrival_time')) {
			$this->form_validation->set_rules('arrival_time', 'Hora de Llegada', 'trim|required|valid_time');
		}
		
		// Set the error message delimeters
		$this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');
		
		// Check for form submissions
		if ($this->form_validation->run() == FALSE) {
		
			// INVALID SUBMISSION OR NO SUBMISSION - Display the form
			
			// Load the dependancies (for select and check boxes)
			$this->load->model('home');
			$this->load->model('status');
			$this->load->model('airport');
			$this->load->model('payment_type');
			$this->load->helper('form');
			
			// Add the booking data and format it for the form
			$data['booking'] = $booking;
			$data['booking']->date_offered = timestamp_to_date($data['booking']->date_offered , 'd/m/Y');
			$data['booking']->date_booked = ($data['booking']->date_booked ? timestamp_to_date($data['booking']->date_booked, 'd/m/Y') : '');
			$data['booking']->date_from = timestamp_to_date($data['booking']->date_from, 'd/m/Y');
			$data['booking']->date_to = timestamp_to_date($data['booking']->date_to, 'd/m/Y');
			$data['booking']->arrival_time = ($data['booking']->arrival_time ? timestamp_to_date($data['booking']->arrival_time, 'H:i') : '');
			$data['booking']->agreed_price = ($data['booking']->agreed_price ? number_format($data['booking']->agreed_price, 2, '.', '') : '');
			$data['booking']->down_payment = ($data['booking']->down_payment ? number_format($data['booking']->down_payment, 2, '.', '') : '');
			$data['booking']->internet_cost = ($data['booking']->internet_cost ? number_format($data['booking']->internet_cost, 2, '.', '') : '');
			$data['booking']->other_cost = ($data['booking']->other_cost ? number_format($data['booking']->other_cost, 2, '.', '') : '');
			
			// Prep expenses and get the comma delimited lists of values
			$expense_item_ids = Array();
			$expense_descriptions = Array();
			$expense_amounts = Array();
			$expense_line_item_ids = Array();
			foreach($data['booking']->expenses as $expense) {
				array_push($expense_item_ids, $expense->item_id);
				array_push($expense_descriptions, $expense->description);
				array_push($expense_amounts, $expense->amount);
				array_push($expense_line_item_ids, ($expense->line_item_id ? $expense->line_item_id : 0));
			}
			$data['booking']->expense_ids = implode('|', $expense_item_ids);
			$data['booking']->expense_descriptions = implode('|', $expense_descriptions);
			$data['booking']->expense_amounts = implode('|', $expense_amounts);
			$data['booking']->expense_line_item_ids = implode('|', $expense_line_item_ids);
			
			// Prep payments and get the comma delimited lists of values
			$payment_ids = Array();
			$payment_percentages = Array();
			$payment_amounts = Array();
			$payment_dates_due = Array();
			$payment_commissions = Array();
			$payment_sort_orders = Array();
			foreach($data['booking']->payments as $payment) {
				$payment->date_due = timestamp_to_date($payment->date_due, 'd/m/Y');
				array_push($payment_ids, $payment->item_id);
				array_push($payment_percentages, $payment->percentage);
				array_push($payment_amounts, $payment->amount);
				array_push($payment_dates_due, $payment->date_due);
				array_push($payment_commissions, $payment->commission_percent);
				array_push($payment_sort_orders, $payment->sort_order);
			}
			$data['booking']->payment_ids = implode(',', $payment_ids);
			$data['booking']->payment_percentages = implode(',', $payment_percentages);
			$data['booking']->payment_amounts = implode(',', $payment_amounts);
			$data['booking']->payment_dates_due = implode(',', $payment_dates_due);
			$data['booking']->payment_commissions = implode(',', $payment_commissions);
			$data['booking']->payment_sort_orders = implode(',', $payment_sort_orders);
			
			// Include the array of required field names (for denoting a field as required with an asterisk)
			$data['required_fields'] = $this->required_fields;
			
			// Add the navigation data
			$data['home_url'] = site_url(array('admin','bookings','index',0,$this->hash));
			$data['list_url'] = site_url(array('admin','bookings','index',0,$this->hash));
			$data['action'] = $action;
			$data['type'] = $type;
			$data['action_url'] = site_url(array('admin','bookings','action',$action,$type,$item_id,$home_id,$date_start,$date_end));
			
			// Set the action title based on whether this is a conversion (a different status from the original) or new / edit
			if ((!$booking->is_new) && ($booking->status->item_id != $this->booking_old_status->item_id)) {
			
				$data['action_title'] = 'Convertir Booking ' . $this->booking_old_status->name . ' N&deg; ' . $booking->reference_num . ' a Booking ' . $booking->status->name;
				
			} elseif ((!$booking->is_new) && ($booking->status->item_id == $this->booking_old_status->item_id)) {
			
				$data['action_title'] = 'Editar Booking ' . $booking->status->name . ' N&deg; ' . $booking->reference_num;
				
			} else {
			
				$data['action_title'] = 'A&ntilde;adir Booking ' . $booking->status->name . ' Nueva';
				
			}
			
			// Add the form select and check box data
			$data['all_homes'] = $this->home->list_entries()->result();
			$data['all_statuses'] = $this->status->list_entries()->result();
			$data['all_airports'] = $this->airport->list_entries()->result();
			$data['all_payment_types'] = $this->payment_type->list_entries()->result();
			$data['all_adults'] = array(1,2,3,4,5,6,7,8,9,10);
			$data['all_children'] = array(0,1,2,3,4,5,6,7,8,9);
			$data['all_cribs'] = array(0,1,2,3,4,5,6,7,8,9);
			
			// Load the form view with the data
			$this->load->view('admin/bookings_form', $data);
			
		
		} else {
		
			// VALID SUBMISSION - Set its parameters and save the entry
			
			// Set the 'simple' data parameters directly
			$booking->is_active = $this->input->post('is_active');
			$booking->reference_num = $this->input->post('reference_num');
			$booking->date_from = strtotime(str_replace('/', '-', $this->input->post('date_from')));
			$booking->date_to = strtotime(str_replace('/', '-', $this->input->post('date_to')));
			$booking->date_offered = ($this->input->post('date_offered') ? strtotime(str_replace('/', '-', $this->input->post('date_offered'))) : NULL);
			$booking->date_booked = ($this->input->post('date_booked') ? strtotime(str_replace('/', '-', $this->input->post('date_booked'))) : NULL);
			$booking->arrival_time = $this->input->post('arrival_time');
			$booking->tax_id = $this->input->post('tax_id');
			$booking->fname = $this->input->post('fname');
			$booking->lname = $this->input->post('lname');
			$booking->email = $this->input->post('email');
			$booking->address = $this->input->post('address');
			$booking->city = $this->input->post('city');
			$booking->province = $this->input->post('province');
			$booking->postal_code = $this->input->post('postal_code');
			$booking->country = $this->input->post('country');
			$booking->mobile = $this->input->post('mobile');
			$booking->adults = $this->input->post('adults');
			$booking->children = $this->input->post('children');
			$booking->cribs = $this->input->post('cribs');
			$booking->booking_source = $this->input->post('booking_source');
			$booking->agreed_price = $this->input->post('agreed_price');
			$booking->down_payment = $this->input->post('down_payment');
			$booking->notes = $this->input->post('notes');
			
			// Set the more 'complex' data types using class setters
			$booking->set_home($this->input->post('home_id'));
			$booking->set_airport($this->input->post('airport_id'));
			$booking->set_payment_type($this->input->post('payment_type_id'));
			$booking->set_expenses(
					$this->input->post('expense_item_ids'),
					$this->input->post('expense_descriptions'),
					$this->input->post('expense_amounts'),
					$this->input->post('expense_line_item_ids')
			);
			
			// Call the models save method
			$booking->save_entry();
			
			// Save the payments (after we have saved the booking to make sure we have a valid booking id)
			if($this->input->post('payment_ids')) {
				$booking->save_payments(
					$this->input->post('payment_ids'),
					$this->input->post('payment_percentages'),
					$this->input->post('payment_amounts'),
					$this->input->post('payment_dates_due'),
					$this->input->post('payment_commissions'),
					$this->input->post('payment_sort_orders')
				);
			}
			
			// Save Document : If this is a new booking or if a booking has been edited (or coverted to booking status).
			if($booking->status->item_id > 1) {
				
				// Conditions Document
				$this->load->model('condition');
				$conditions_doc = $this->condition->initialize(0,$booking->item_id);
				$conditions_doc->booking_id = $booking->item_id;
				$conditions_doc->generate_content($booking->item_id);
				$conditions_doc->save_entry();
				
				// Receipts (Reset Payments first in case of new or removed payments)
				$booking->set_payments($booking->item_id);
				$this->load->model('receipt');
				foreach($booking->payments as $payment) {
					if($payment->receipt_id) {
						$receipt_doc = $this->receipt->initialize(0,$payment->item_id);
						$receipt_doc->payment_id = $payment->item_id;
						$receipt_doc->booking_id = $booking->item_id;
						$receipt_doc->generate_content($payment->item_id);
						$receipt_doc->save_entry();
					}
				}
				
			}
			
			// Save the alerts
			//$booking->save_alerts();
			
			// Send the user on their way to the view page via an http redirect
			$redirect_string = '/admin/bookings/view/' . $booking->item_id;
			redirect($redirect_string, 'refresh');
		
		}
		
	}
	
	
	/* CHECK REFERENCE CALLBACK FUNCTION
	------------------------------------------------------------------
	Description: Callback function used by form validation in the action
	method of this controller to confirm that a booking reference
	number is not a duplicate
	----------------------------------------------------------------*/
	
	public function check_reference($value)
	{	
		
		// Query the DB to see if any other booking is using this reference number
		$this->db->select('item_id');
		$this->db->from('bookings');
		$this->db->where('reference_num', $value);
		$this->db->where('item_id !=', $this->booking_item_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0) {
			$this->form_validation->set_message('check_reference', 'Un oferta o booking ya existe con este numero de referencia');
			return FALSE;
		} else {
			return TRUE;
		}
		
	}
	
	
	/* CHECK DATESPAN CALLBACK FUNCTION
	------------------------------------------------------------------
	Description: Callback function used by form validation in the action
	method of this controller to confirm that:
	1. A bookings datespan is correct (date_from <= date_to)
	2. A bookings datespan has enough timeslots to cover it
	3. A bookings datespan does not interfere with another booking
	All values are pulled fomr the super global POST array
	----------------------------------------------------------------*/
	
	public function check_datespan($value)
	{	
		
		// Get both dates of the span and turn them into timestamps and mysql dates
		
		$date_from = strtotime(str_replace('/', '-', $this->input->post('date_from')));
		$date_to = strtotime(str_replace('/', '-', $this->input->post('date_to')));
		$date_from_mysql = timestamp_to_mysqldatetime($date_from, FALSE);
		$date_to_mysql = timestamp_to_mysqldatetime($date_to, FALSE);
		
		// 1. Check the datespan it's self
		
		if($date_from > $date_to) {
			$this->form_validation->set_message('check_datespan', 'La fecha "Desde" es despues de la fecha "Hasta"');
			return FALSE;
		}
		
		// 2. Check for the proper amount of timeslot weeks
		
		// Run the query
		$this->db->from('timeslots');
		$this->db->where('home_id', $this->booking_home_id);
		$this->db->where('date_slot >=', $date_from_mysql);
		$this->db->where('date_slot <=', $date_to_mysql);
		$timeslots = $this->db->get()->result();
		
		$available_slots = count($timeslots);
		
		$required_slots = get_day_diff($date_from, $date_to, TRUE);
		
		// Test the values returned
		if($available_slots < $required_slots) {
			$this->form_validation->set_message('check_datespan', 'Existen dias para esta casa entre las fechas seleccionadas que no se han asignado un precio');
			return FALSE;
		}
		
		// 3. Check for interfering bookings
		
		// Get an array of the timeslot ids we just pulled
		$timeslot_ids = Array();
		foreach($timeslots as $timeslot) {
			array_push($timeslot_ids, $timeslot->item_id);
		}
		
		// Run the query
		$this->db->select('bookings.item_id');
		$this->db->distinct();
		$this->db->from('bookings');
		$this->db->join('booking_timeslot', 'booking_timeslot.booking_id = bookings.item_id', 'inner');
		$this->db->where('bookings.item_id !=', $this->booking_item_id);
		$this->db->where('bookings.is_active', 1);
		$this->db->where_in('bookings.status_id', array(2,3));
		$this->db->where_in('booking_timeslot.timeslot_id', $timeslot_ids);
		$bookings = $this->db->get();
		
		if($bookings->num_rows() > 0) {
			$this->form_validation->set_message('check_datespan', 'Un booking ya existe para esta casa entre las fechas seleccionadas');
			return FALSE;
		}
		
		// 4. Everything passed, return TRUE
		return TRUE;
		
	}
	
	
	/* VIEW COSTS METHOD 
	------------------------------------------------------------------
	Description: Loads the bookings costs data into a variable
	which is echoed out (Used for AJAX calls)
	----------------------------------------------------------------*/
	
	public function costs($item_id=0, $trimester=0, $year=0)
	{	
		
		$data['title'] = '';
		$data['total_net'] = 0;
		$data['total_vat'] = 0;
		$data['total_gross'] = 0;
		$data['costs'] = Array();
		
		// Get payments and expenses from this booking via models
		$this->load->model('booking');
		$this->load->model('payment');
		$this->load->model('expense');
		$payments = $this->payment->list_entries_by_trimester($item_id,$trimester,$year,TRUE)->result();
		$expenses = $this->expense->list_entries_by_trimester($item_id,$trimester,$year)->result();
		
		$booking_data = $this->booking->get_entry($item_id)->row();
		if($booking_data) {
			$data['title'] = $booking_data->fname . ' ' . $booking_data->lname . ' (' . mysqldatetime_to_date($booking_data->date_from, 'd/m/Y') . ' - ' . mysqldatetime_to_date($booking_data->date_to, 'd/m/Y') . ')';
		}
		
		// If we have payments, insert the data into the costs array
		if($payments) {
			
			// Set the payment totals to 0
			$total_payments_gross = 0;
			$total_payments_vat = 0;
			$total_payments_net = 0;
			
			// Loop through payments
			foreach($payments as $payment) {
				
				// Calculate the amounts
				$tmp_gross = $payment->amount * ($payment->commission_percent/100);
				$tmp_vat = $tmp_gross * (21/100);
				$tmp_net = $tmp_gross + $tmp_vat;
				
				// Increment the totals
				$data['total_gross'] += $tmp_gross;
				$data['total_vat'] += $tmp_vat;
				$data['total_net'] += $tmp_net;
				
				// Increment the total payments
				$total_payments_gross += $tmp_gross;
				$total_payments_vat += $tmp_vat;
				$total_payments_net += $tmp_net;
				
			}
				
			// Create and insert into the costs array
			$tmp_payment = new stdClass;
			$tmp_payment->description = 'Comisiones DosXtremos';
			$tmp_payment->amount_gross = number_format($total_payments_gross, 2, '.', ',') . ' &euro;';
			$tmp_payment->amount_vat = number_format($total_payments_vat, 2, '.', ',') . ' &euro;';
			$tmp_payment->amount_net = number_format($total_payments_net, 2, '.', ',') . ' &euro;';
			
			// Add it to the costs array
			array_push($data['costs'], $tmp_payment);

		}
			
		// Loop through expenses
		foreach($expenses as $expense) {
			
			// Increment the totals (no VAT or Commissions to calculate)
			$data['total_net'] += $expense->amount;
			$data['total_gross'] += $expense->amount;
			
			// Create and insert into the costs array
			$tmp_expense = new stdClass;
			$tmp_expense->description = $expense->description;
			$tmp_expense->amount_gross = number_format($expense->amount, 2, '.', ',') . ' &euro;';
			$tmp_expense->amount_vat = number_format(0, 2, '.', ',') . ' &euro;';
			$tmp_expense->amount_net = number_format($expense->amount, 2, '.', ',') . ' &euro;';
			array_push($data['costs'], $tmp_expense);
			
		}
		
		// Format the totals
		$data['total_net'] = number_format($data['total_net'], 2, '.', ',');
		$data['total_vat'] = number_format($data['total_vat'], 2, '.', ',');
		$data['total_gross'] = number_format($data['total_gross'], 2, '.', ',');
		
		// Load the view with the data
		$modal = $this->load->view('admin/costs_modal_vat',$data,TRUE);
		
		// Echo back the rendered view
		echo $modal;
		
	}
	
	
		/* VIEW LIQUIDATIONS METHOD
	------------------------------------------------------------------
	Description: Loads the bookings costs view modal with data

	For AJAX Calls
	
	19/APR/2018, HUGO:  Not being used yet. We thought they wanted a pop-up with the commission deducted from the liquidation,
                        and showed in detail in the pop-up. But they want liquidation = payment so far
	----------------------------------------------------------------*/

	/*public function liquidations($item_id=0, $trimester=0, $year=0)
	{	
		
		$data['title'] = '';
		$data['total_net'] = 0;
		$data['total_commissions'] = 0;
		$data['total_gross'] = 0;
		$data['liquidations'] = Array();
		
		// Get payments and expenses from this booking via models
		$this->load->model('booking');
		$this->load->model('payment');
		$liq_payments = $this->payment->list_entries_by_trimester($item_id,$trimester,$year,TRUE,TRUE)->result();
		
		$booking_data = $this->booking->get_entry($item_id)->row();
		if($booking_data) {
			$data['title'] = $booking_data->fname . ' ' . $booking_data->lname . ' (' . mysqldatetime_to_date($booking_data->date_from, 'd/m/Y') . ' - ' . mysqldatetime_to_date($booking_data->date_to, 'd/m/Y') . ')';
		}
		
		// If we have payments, insert the data into the costs array
		if($liq_payments) {
			
			// Set the payment totals to 0
			$total_liquidations_net = 0;
			$total_commissions = 0;
			$total_liquidations_gross = 0;
			
			// Loop through payments
			foreach($liq_payments as $liq_payment) {
				
				// Calculate the commission received by DosExtrem
				$commission_gross = $liq_payment->amount * ($liq_payment->commission_percent/100);
				$commission_vat = $commission_vat = $commission_gross * (21/100);
				$commission_net = $commission_gross + $commission_vat;
				
				// Calculate the liquidations: payment - commission
				$liquidation_gross = $liq_payment->amount;
				$liquidation_net = $liquidation_gross - $commission_net;
				
				// Increment the totals
				$data['total_gross'] += $liquidation_gross;
				$data['total_commissions'] += $commission_net;
				$data['total_net'] += $liquidation_net;
				
				// Increment the total liquidations
				$total_liquidations_gross += $liquidation_gross;
				$total_commissions += $commission_net;
				$total_liquidations_net += $liquidation_net;
			}
				
			// Create and insert into the liquidations array
			$tmp_liquidation = new stdClass;
			$tmp_liquidation->description = 'Liquidaciones';
			$tmp_liquidation->amount_gross = number_format($total_liquidations_gross, 2, '.', ',') . ' &euro;';
			$tmp_liquidation->amount_commissions = number_format($total_commissions, 2, '.', ',') . ' &euro;';
			$tmp_liquidation->amount_net = number_format($total_liquidations_net, 2, '.', ',') . ' &euro;';
			array_push($data['liquidations'], $tmp_liquidation);
		
		}
		
		// Format the totals
		$data['total_net'] = number_format($data['total_net'], 2, '.', ',');
		$data['total_commissions'] = number_format($data['total_commissions'], 2, '.', ',');
		$data['total_gross'] = number_format($data['total_gross'], 2, '.', ',');
		
		// Load the view with the data
		$modal = $this->load->view('admin/liquidations_modal',$data,TRUE);
		
		// Echo back the rendered view
		echo $modal;
		
	}*/
	
	
	/* DELETE METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with a delete warning form
	or deletes the item on form submission.
	----------------------------------------------------------------*/
	
	public function delete($item_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for form confirmation
		if($this->input->post('submit')) {
			
			// Check the confirmation
			if ($this->input->post('submit') == 'Borrar') {
				
				// Delete the entry
				$this->booking->delete_entry($item_id);
				
			}
			
			// Redirect the user to the list page via http redirect
			$redirect_string = '/admin/bookings/index/0/' . $this->hash;
			redirect($redirect_string, 'refresh');
			
		} else {
			
			// Call the view method with a delete flag to display the form
			$this->view($item_id,'',TRUE);
			
		}
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/admin/bookings.php */