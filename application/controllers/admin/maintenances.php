<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maintenances extends Admin_Controller {

	
	// Create a property for this class
	// (used in checking for existing reference codes in the check_code method of this class)
	var $maintenance_item_id;
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Maintenances()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load the models we will use in this controller
		$this->load->model('maintenance');
		$this->load->model('home');
		
		// Initialize this property to 0
		$this->maintenance_item_id = 0;

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page based on the parent id
	and the year
	----------------------------------------------------------------*/
	
	public function index($parent_id=0,$year=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Default the year to current
		if(!$year) {
			$year = date('Y');
		}
		
		// Get the data for the home
		$data['home'] = $this->home->get_entry($parent_id)->row();
		$data['current_year'] = $year;
		
		// Get the list of maintenance from the model and prep them for the view
		$data['maintenances'] = $this->maintenance->list_entries($parent_id,$year)->result();
		foreach($data['maintenances'] as $maintenance) {
			$maintenance->date_payed = mysqldatetime_to_date($maintenance->date_payed, 'd/m/Y');
			$maintenance->view_url = site_url(array('admin','maintenances','view',$maintenance->item_id));
			$maintenance->edit_url = site_url(array('admin','maintenances','action','edit',$parent_id,$maintenance->item_id));
			$maintenance->delete_url = site_url(array('admin','maintenances','delete',$parent_id,$year,$maintenance->item_id));
		}
		
		// Add and list links
		$data['parent_url'] = site_url(array('admin','homes','view',$parent_id));
		$data['back_url'] = site_url(array('admin','maintenances','index',$parent_id,($year - 1)));
		$data['forward_url'] = site_url(array('admin','maintenances','index',$parent_id,($year + 1)));
		$data['add_url'] = site_url(array('admin','maintenances','action','new',$parent_id));
		
		// Load the view with the data
		$this->load->view('admin/maintenance_list', $data);
		
	}
	
	
	/* VIEW METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with data
	----------------------------------------------------------------*/
	
	public function view($item_id=0,$delete_item=FALSE)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Initialize an owner object with its data
		$data['maintenance'] = $this->maintenance->initialize($item_id);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($data['maintenance']->is_new === TRUE) {
			redirect('/admin/homes/index', 'refresh');
			exit();
		}
		
		// Prep the data for view
		$year = timestamp_to_date($data['maintenance']->date_payed, 'Y');
		$data['current_year'] = $year;
		$data['maintenance']->date_payed = timestamp_to_date($data['maintenance']->date_payed, 'd/m/Y');
		$data['maintenance']->date_complete = timestamp_to_date($data['maintenance']->date_complete, 'd/m/Y');
		
		// Add and prep any entry navigation links
		$data['parent_url'] = site_url(array('admin','homes','view',$data['maintenance']->home->item_id));
		$data['list_url'] = site_url(array('admin','maintenances','index',$data['maintenance']->home->item_id,$year));
		$data['download_url'] = ($data['maintenance']->receipt_file ? site_url(array('admin','maintenances','download',$data['maintenance']->receipt_file,0)) : NULL);
		$data['edit_url'] = site_url(array('admin','maintenances','action','edit',$data['maintenance']->home->item_id,$item_id));
		$data['delete_url'] = site_url(array('admin','maintenances','delete',$data['maintenance']->home->item_id,$year,$item_id));
		
		// Add the delete flag
		$data['delete_item'] = $delete_item;
		
		// Load the view with the data
		$this->load->view('admin/maintenance_view', $data);
		
	}
	
	
	/* ACTION METHOD 
	------------------------------------------------------------------
	Description: Loads the items form page for 'new' or 'edit'
	----------------------------------------------------------------*/
	
	public function action($action='new',$parent_id=0,$item_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for cancels first
		if($this->input->post('submit') !== FALSE && $this->input->post('submit') == 'Cancelar') {
			$redirect_string = '/admin/maintenances/index/' . $parent_id;
			redirect($redirect_string, 'refresh');
		}
		
		// Set the home_item_id variable of this class to the home that is being edited or created
		$this->maintenance_item_id = $item_id;
		
		// Load any necessary libraries and helpers
		$this->load->library('form_validation');
		
		// Set the form validation rules on required elements
		$this->form_validation->set_rules('name', 'Nombre', 'trim|required');
		$this->form_validation->set_rules('reference_num', 'N&uacute;mero de Referencia', 'trim|required|callback_reference_check');
		$this->form_validation->set_rules('provider', 'Proveedor', 'trim|required');
		$this->form_validation->set_rules('date_complete', 'Fecha Realizada', 'trim|required');
		$this->form_validation->set_rules('date_payed', 'Fecha Pagada', 'trim|required');
		$this->form_validation->set_rules('payed_by', 'Pagador por', 'trim|required');
		$this->form_validation->set_rules('amount', 'Importe', 'trim|required|decimal');
		$this->form_validation->set_rules('trimester', 'Trimestre', 'trim|required|integer');
		
		// Optional Elements (run in if statements to check for their presence - NOTE: CI requires this to return posted values)
		if($this->input->post('receipt_filename')) {
			$this->form_validation->set_rules('receipt_filename', 'Archivo Adjunto', 'trim|required');
		}
		if($this->input->post('notes')) {
			$this->form_validation->set_rules('notes', 'Notas Adicionales', 'trim|required');
		}
		
		// Set the error message delimeters
		$this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');
		
		// Initialize the home model with the data
		$maintenance = $this->maintenance->initialize($item_id);
		
		// Set the home this belongs to if this is a new entry
		if($action == 'new') {
			$maintenance->set_home($parent_id);
		}
		
		// Check for form submissions
		if ($this->form_validation->run() == FALSE) {
		
			// INVALID SUBMISSION OR NO SUBMISSION - Display the form
			
			// Load the dependancies (for select and check boxes)
			$this->load->helper('form');
			
			// Add the home data for the form and format it
			$data['maintenance'] = $maintenance;
			$data['maintenance']->reference_num = ($maintenance->reference_num ? $maintenance->reference_num : ( 'MNT-' . $maintenance->home->code . '-' . date('dmy') ));
			$data['maintenance']->date_complete = ($maintenance->date_complete ? timestamp_to_date($maintenance->date_complete, 'd/m/Y') : date('d/m/Y'));
			$data['maintenance']->date_payed = ($maintenance->date_payed ? timestamp_to_date($maintenance->date_payed, 'd/m/Y') : date('d/m/Y'));
			
			// Add the navigation data
			$data['parent_url'] = site_url(array('admin','homes','view',$parent_id));
			$data['list_url'] = site_url(array('admin','maintenances','index',$parent_id));
			$data['action'] = $action;
			$data['action_url'] = site_url(array('admin','maintenances','action',$action,$parent_id,$item_id));
			$data['action_title'] = ($action == 'edit' ? ('Editar Gasto Adelantado : ' . $maintenance->name) : 'A&ntilde;adir Gasto Adelantado Nuevo');
			
			// Add the form select and check box data
			$data['all_trimesters'] = Array();
			for($i = 1; $i <= 4; $i++) {
				$trimester = new stdClass;
				$trimester->item_id = $i;
				$trimester->name = 'T' . $i;
				array_push($data['all_trimesters'], $trimester);
			}
			
			// Load the form view with the data
			$this->load->view('admin/maintenance_form', $data);
			
		
		} else {
		
			// VALID SUBMISSION - Set its parameters and save the entry
			
			// Set the 'simple' data parameters directly
			$maintenance->name = $this->input->post('name');
			$maintenance->reference_num = $this->input->post('reference_num');
			$maintenance->provider = $this->input->post('provider');
			$maintenance->date_complete = strtotime(str_replace('/', '-', $this->input->post('date_complete')));
			$maintenance->date_payed = strtotime(str_replace('/', '-', $this->input->post('date_payed')));
			$maintenance->payed_by = $this->input->post('payed_by');
			$maintenance->amount = $this->input->post('amount');
			$maintenance->trimester = $this->input->post('trimester');
			$maintenance->receipt_file = $this->input->post('receipt_filename');
			$maintenance->notes = $this->input->post('notes');
			
			// Set the more 'complex' data types using class setters
			$maintenance->set_home($parent_id);
			
			// Call the models save method
			$maintenance->save_entry();
			
			// Send the user on their way to the listing page via an http redirect
			$redirect_string = '/admin/maintenances/index/' . $parent_id;
			redirect($redirect_string, 'refresh');
		
		}
		
	}
	
	
	/* DOWNLOAD METHOD 
	------------------------------------------------------------------
	Description: Sets a forced download of the items attached file
	----------------------------------------------------------------*/
	
	public function download($filename='', $hash=0)
	{	
		
		// Load the libraries and helpers
		$this->load->helper('download');
		
		// Read the files contents and give it a name
		$filepath = base_url() . 'uploads/maintenance_receipts/' . $filename;
		$data = file_get_contents($filepath);
		$name = $filename;
		
		// Force a download
		force_download($name, $data);
		
	}
	
	
	/* UPLOAD METHOD 
	------------------------------------------------------------------
	Description: Uploads a receipt file to the server for this item
	
	For AJAX Calls
	----------------------------------------------------------------*/
	
	public function upload()
	{	
		
		// Set the defaults
		$status = '';
		$msg = '';
		
		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg));
		
	}
	
	
	/* DELETE METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with a delete warning form
	or deletes the item on form submission.
	----------------------------------------------------------------*/
	
	public function delete($parent_id=0,$year=0,$item_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for form confirmation
		if($this->input->post('submit')) {
			
			// Check the confirmation
			if ($this->input->post('submit') == 'Borrar') {
				
				// Delete the entry
				$this->maintenance->delete_entry($item_id);
				
			}
			
			// Redirect the user to the list page via http redirect
			$redirect_string = '/admin/maintenances/index/' . $parent_id . '/' . $year;
			redirect($redirect_string);
			
		} else {
			
			// Call the view method with a delete flag to display the form
			$this->view($item_id,TRUE);
			
		}
		
	}
	
	
	/* CODE CHECK CALLBACK FUNCTION
	------------------------------------------------------------------
	Description: Used in conjunction with the form validation methods
	in the action method to test if a reference is not already taken
	----------------------------------------------------------------*/
	
	public function reference_check($str)
	{
		
		// Call the database directly to check for existing record
		$this->db->select('item_id, reference_num');
		$this->db->from('maintenance');
		$this->db->where('reference_num', $str);
		$this->db->where('item_id !=', $this->maintenance_item_id);
		$query_result = $this->db->get()->result();
		
		if ($query_result)
		{
			$error_msg = 'El n&uacute;mero de referencia ' . $str . ' ya esta siendo utilizado por otro tarea de mantenimiento';
			$this->form_validation->set_message('reference_check', $error_msg);
			return FALSE;
		}
		else
		{
			return TRUE;
		}
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/admin/maintenances.php */