<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comments extends Admin_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Comments()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Get the models
		$this->load->model('comment');

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the HOMES listing page for comments based on the
	home id and the year
	----------------------------------------------------------------*/
	
	public function index($home_id=0,$year=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Default the year to current
		$year = (!$year ? date('Y') : $year);
		
		// Get the data for the home
		$this->load->model('home');
		$data['home'] = $this->home->get_entry($home_id)->row();
		$data['current_year'] = $year;
		
		// Get the comments based on year and home id and format for the view
		$data['comments'] = $this->comment->list_entries($year,0,$home_id)->result();
		foreach($data['comments'] as $comment) {
			$comment->date_comment = mysqldatetime_to_date($comment->date_comment, 'd/m/Y');
			$comment->view_url = site_url(array('admin','comments','view',$comment->item_id));
			$comment->edit_url = site_url(array('admin','comments','action','edit',$comment->booking_id,$comment->item_id));
			$comment->delete_url = site_url(array('admin','comments','delete',$comment->booking_id,$comment->item_id));
		}
		
		// Add and list links
		$data['parent_url'] = site_url(array('admin','homes','view',$home_id));
		$data['back_url'] = site_url(array('admin','comments','index',$home_id,($year - 1)));
		$data['forward_url'] = site_url(array('admin','comments','index',$home_id,($year + 1)));
		
		// Load the view with the data
		$this->load->view('admin/comments_list', $data);
		
	}
	
	
	/* VIEW METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with data
	----------------------------------------------------------------*/
	
	public function view($item_id=0,$delete_item=FALSE)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Initialize the comment object with data
		$data['comment'] = $this->comment->initialize($item_id);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($data['comment']->is_new === TRUE) {
			redirect('/admin/index', 'refresh');
			exit();
		}
		
		// Prep the data for the view
		$data['comment']->date_comment = timestamp_to_date($data['comment']->date_comment, 'd/m/Y');
		$data['comment']->booking->date_from = timestamp_to_date($data['comment']->booking->date_from, 'd/m/Y');
		$data['comment']->booking->date_to = timestamp_to_date($data['comment']->booking->date_to, 'd/m/Y');
		
		// Add and prep any entry navigation links
		$data['parent_url'] = site_url(array('admin','bookings','view',$data['comment']->booking->item_id));
		$data['list_url'] = site_url(array('admin','bookings','view',$data['comment']->booking->item_id,'comments'));
		$data['edit_url'] = site_url(array('admin','comments','action','edit',$data['comment']->booking->item_id,$item_id));
		$data['delete_url'] = site_url(array('admin','comments','delete',$data['comment']->booking->item_id,$item_id));
		
		// Add the delete flag
		$data['delete_item'] = $delete_item;

		
		// Load the view with the data
		$this->load->view('admin/comments_view', $data);
		
	}
	
	
	/* ACTION METHOD 
	------------------------------------------------------------------
	Description: Loads the items form page for 'new' or 'edit'
	----------------------------------------------------------------*/
	
	public function action($action='new',$parent_id=0,$item_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for cancels first
		if($this->input->post('submit') !== FALSE && $this->input->post('submit') == 'Cancelar') {
			$redirect_string = '/admin/bookings/view/' . $parent_id;
			redirect($redirect_string, 'refresh');
		}
		
		// Load any necessary libraries and helpers
		$this->load->library('form_validation');
		
		// Set the form validation rules on required elements
		$this->form_validation->set_rules('name', 'Nombre', 'trim|required');
		$this->form_validation->set_rules('date_comment', 'Fecha Comentario', 'trim|required');
		$this->form_validation->set_rules('client_comment', 'Comentario del Cliente', 'trim|required');
		
		// Optional Elements (run in if statements to check for their presence - NOTE: CI requires this to return posted values)
		if($this->input->post('admin_comment')) {
			$this->form_validation->set_rules('admin_comment', 'Comentario del Administrador', 'trim|required');
		}
		
		// Set the error message delimeters
		$this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');
		
		// Initialize the comment model with the data
		$comment = $this->comment->initialize($item_id);
		
		// Set the booking this belongs to if this is a new entry and default the name
		if($action == 'new') {
			$comment->set_booking($parent_id);
			$comment->name = $comment->booking->name;
		}
		
		// Check for form submissions
		if ($this->form_validation->run() == FALSE) {
		
			// INVALID SUBMISSION OR NO SUBMISSION - Display the form
			
			// Load the dependancies
			$this->load->helper('form');
			
			// Add the comment data for the form and format it
			$data['comment'] = $comment;
			$data['comment']->date_comment = timestamp_to_date($comment->date_comment, 'd/m/Y');
			
			// Add the navigation data
			$data['parent_url'] = site_url(array('admin','bookings','view',$parent_id));
			$data['list_url'] = site_url(array('admin','bookings','view',$parent_id,'comments'));
			$data['action'] = $action;
			$data['action_url'] = site_url(array('admin','comments','action',$action,$parent_id,$item_id));
			$data['action_title'] = ($action == 'edit' ? ('Editar Comentario de ' . $comment->name) : 'A&ntilde;adir Comentario Nuevo');
			
			// Load the form view with the data
			$this->load->view('admin/comments_form', $data);
			
		
		} else {
		
			// VALID SUBMISSION - Set its parameters and save the entry
			
			// Set the 'simple' data parameters directly
			$comment->date_comment = strtotime(str_replace('/', '-', $this->input->post('date_comment')));
			$comment->name = $this->input->post('name');
			$comment->client_comment = $this->input->post('client_comment');
			$comment->admin_comment = $this->input->post('admin_comment');
			
			// Set the more 'complex' data types using class setters
			$comment->set_booking($parent_id);
			
			// Call the models save method
			$comment->save_entry();
			
			// Send the user on their way to the listing page via an http redirect
			$redirect_string = '/admin/bookings/view/' . $parent_id . '/comments';
			redirect($redirect_string, 'refresh');
		
		}

		
	}
	
	
	/* DELETE METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with a delete warning form
	or deletes the item on form submission.
	----------------------------------------------------------------*/
	
	public function delete($parent_id=0,$item_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for form confirmation
		if($this->input->post('submit')) {
			
			// Check the confirmation
			if ($this->input->post('submit') == 'Borrar') {
				
				// Delete the entry
				$this->comment->delete_entry($item_id);
				
			}
			
			// Redirect the user to the list page via http redirect
			$redirect_string = '/admin/bookings/view/' . $parent_id . '/comments';
			redirect($redirect_string);
			
		} else {
			
			// Call the view method with a delete flag to display the form
			$this->view($item_id,TRUE);
			
		}
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/admin/comments.php */