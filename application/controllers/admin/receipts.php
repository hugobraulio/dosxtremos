<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receipts extends Admin_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Receipts()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load the models we will use in this controller
		$this->load->model('receipt');

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page
	----------------------------------------------------------------*/
	
	public function index()
	{	
		
		// Leave blank for now
		redirect('/admin/bookings/index', 'refresh');
		exit();
		
	}
	
	
	/* VIEW METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with data
	----------------------------------------------------------------*/
	
	public function view($item_id=0,$lang_id='es')
	{	
		
		// Initialize a receipt object with its data
		$receipt = $this->receipt->initialize($item_id, 0);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($receipt->is_new === TRUE) {
			redirect('/admin/bookings/index', 'refresh');
			exit();
		}
		
		$data['item_id'] = $receipt->item_id;
		$data['payment_id'] = $receipt->payment_id;
		$data['date_created'] = timestamp_to_date($receipt->date_created);
		$data['content'] = ($lang_id == 'en' ? $receipt->content->en : $receipt->content->es);
		
		// Load the view with the data
		$this->load->view('common/receipts_print', $data);
		
	}
	
	
	/* ACTION METHOD 
	------------------------------------------------------------------
	Description: Loads the items form page for 'new' or 'edit'
	----------------------------------------------------------------*/
	
	public function action($action='edit',$item_id=0,$payment_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Initialize the receipt model with the data
		$receipt = $this->receipt->initialize($item_id);
		
		// Redirect string to use for cancels and succesful submissions
		$redirect_string = '/admin/bookings/view/' . $receipt->booking_id . '/payments';
		
		// Check for cancels first
		if($this->input->post('submit') !== FALSE && $this->input->post('submit') == 'Cancelar') {
			redirect($redirect_string, 'refresh');
		}
		
		// Load any necessary libraries and helpers
		$this->load->library('form_validation');
		
		// Set the form validation rules on required elements
		$this->form_validation->set_rules('date_created', 'Fecha', 'trim|required');
		$this->form_validation->set_rules('content_en', 'Texto en Ingl�s', 'trim|required');
		$this->form_validation->set_rules('content_es', 'Texto en Espa�ol', 'trim|required');
		
		// Set the error message delimeters
		$this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');
		
		// Check for form submissions
		if ($this->form_validation->run() == FALSE) {
		
			// INVALID SUBMISSION OR NO SUBMISSION - Display the form
			
			// Add the condition data for the form
			$data['receipt'] = $receipt;
			$data['receipt']->date_created = timestamp_to_date($receipt->date_created, 'd/m/Y');
			
			// Add the navigation data
			$data['list_url'] = site_url(array('admin','bookings','view',$receipt->booking_id,'payments'));
			$data['parent_url'] = site_url(array('admin','bookings','view',$receipt->booking_id));
			$data['action'] = $action;
			$data['action_url'] = site_url(array('admin','receipts','action',$action,$item_id,$payment_id));
			$data['action_title'] = ($action == 'edit' ? ('Editar Recibo de Booking N&deg; ' . $receipt->reference_num) : 'A&ntilde;adir Recibo');
			
			// Load the form view with the data
			$this->load->view('admin/receipts_form', $data);
			
		
		} else {
		
			// VALID SUBMISSION - Set its parameters and save the entry
			
			// Set the 'simple' data parameters directly
			$receipt->payment_id = $payment_id;
			$receipt->date_created = strtotime(str_replace('/', '-', $this->input->post('date_created')));
			$receipt->content->es = $this->input->post('content_es');
			$receipt->content->en = $this->input->post('content_en');
			
			// Call the models save method
			$receipt->save_entry();
			
			// Send the user on their way to the listing page via an http redirect
			redirect($redirect_string, 'refresh');
		
		}
		
	}
	
	
	/* DELETE METHOD 
	------------------------------------------------------------------
	Description: Used with AJAX to delete a receipt document.
	Returns JSON
	----------------------------------------------------------------*/
	
	public function delete($item_id=0)
	{	
		
		// Set the defaults
		$status = '';
		$msg = '';
		
		// Delete the entry using the model
		$this->receipt->delete_entry($item_id);
		
		$status = 'success';
		$msg = 'Se ha borrado el recibo';
		
		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg, 'item_id' => $item_id));
		
	}
	
	
	
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/admin/receipts.php */