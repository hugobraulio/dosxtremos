<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homes extends Admin_Controller {
	
	// Create a property for this class
	// (used in checking for existing home codes in the check_code method of this class)
	var $home_item_id;
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Homes()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load the models we will use in this controller
		$this->load->model('home');
		
		// Initialize this property to 0
		$this->home_item_id = 0;

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page
	----------------------------------------------------------------*/
	
	public function index()
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Get the list of elements from the model and prep them for the view
		$data['homes'] = $this->home->list_entries()->result();
		foreach($data['homes'] as $home) {
			$home->view_url = site_url(array('admin','homes','view',$home->item_id));
			$home->edit_url = site_url(array('admin','homes','action','edit',$home->item_id));
			$home->delete_url = site_url(array('admin','homes','delete',$home->item_id));
		}
		
		// Add and list links
		$data['list_url'] = site_url(array('admin','homes','index'));
		$data['add_url'] = site_url(array('admin','homes','action','new'));
		$data['order_url'] = site_url(array('admin','homes','set_order'));
		
		// Load the view with the data
		$this->load->view('admin/homes_list', $data);
		
	}
	
	
	
	/* VIEW METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with data
	----------------------------------------------------------------*/
	
	public function view($item_id=0,$delete_item=FALSE)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Initialize a home object with its data
		$data['home'] = $this->home->initialize($item_id);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($data['home']->is_new === TRUE) {
			redirect('/admin/homes/index', 'refresh');
			exit();
		}
		
		// Add and prep any entry navigation links
		$data['list_url'] = site_url('admin/homes');
		$data['calendar_url'] = site_url(array('admin','homes','calendar',$item_id,date('Y')));
		$data['prices_url'] = site_url(array('admin','homes','prices',$item_id,date('Y')));
		$data['maintenance_url'] = site_url(array('admin','maintenances','index',$item_id,date('Y')));
		$data['comments_url'] = site_url(array('admin','comments','index',$item_id,date('Y')));
		$data['documents_url'] = site_url(array('admin','homes','documents',$item_id,date('Y')));
		$data['map_url'] = (($data['home']->latitude && $data['home']->longitude) ? site_url(array('admin','homes','map',$item_id,'es')) : NULL);
		$data['edit_url'] = site_url(array('admin','homes','action','edit',$item_id));
		$data['delete_url'] = site_url(array('admin','homes','delete',$item_id));
		$data['status_url'] = site_url(array('admin','homes','set_active',$item_id,($data['home']->is_active ? 0 : 1)));
		
		// Owner link
		$data['home']->owner->view_url = site_url(array('admin','owners','view',$data['home']->owner->item_id));
		
		// Add the delete flag
		$data['delete_item'] = $delete_item;
		
		// Load the view with the data
		$this->load->view('admin/homes_view', $data);
		
		
	}
	
	
	/* CALENDAR METHOD 
	------------------------------------------------------------------
	Description: Loads the items calendar page with data
	----------------------------------------------------------------*/
	
	public function calendar($item_id=0,$year=0,$month=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Initialize a home object with its data
		$data['home'] = $this->home->initialize($item_id);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($data['home']->is_new === TRUE) {
			redirect('/admin/homes/index', 'refresh');
			exit();
		}
		
		// Default the year and month to current year and month to january if no month is defined
        $year = ($year ? $year : date('Y'));
        $month = ($month ? $month : 1);
		
		// Get the calendar based on the year and month and prep for the view
		$data['calendar'] = $this->home->get_calendar($item_id,$year,$month);
		foreach($data['calendar']->days as $day) {
			
			// Format the week price and estimate price
			$day->week_estimate = number_format($day->week_estimate, 2, '.', '');
			$day->week_price = number_format($day->week_price, 2, '.', '');
			
			// Check for offers
			if($day->offers) {
				foreach($day->offers as $offer) {
					$offer->view_url = site_url(array('admin','bookings','view',$offer->item_id));
				}
				$day->status_id = 1;
			}
			
			// Check for bookings
			if($day->booking) {
				$day->booking->view_url = site_url(array('admin','bookings','view',$day->booking->item_id));
				$day->status_id = $day->booking->status_id;
			}
			
			// Add css class for days without prices
			$day->view_class = ($day->item_id ? '' : 'no-price');
			
			// Add the add links and week-start class if this day is a saturday
			if(date('N', $day->timestamp) == 6) {
				$day->view_class = trim($day->view_class . ' week_start');
				$day->is_saturday = TRUE;
			} else {
				$day->is_saturday = FALSE;
			}
			
			// Set the action urls for adding an offer or a booking
			if($day->status_id <= 1 && $day->item_id) {
				$day->add_offer_url = site_url(array('admin','bookings','action','new','offer',0,$item_id,$day->timestamp,strtotime('+6 days', $day->timestamp)));
				$day->add_booking_url = site_url(array('admin','bookings','action','new','booking',0,$item_id,$day->timestamp,strtotime('+6 days', $day->timestamp)));
			} else {
				$day->add_offer_url = NULL;
				$day->add_booking_url = NULL;
			}
			
		}
		
		// Add and prep any entry navigation links
		$data['list_url'] = site_url('admin/homes');
		$data['parent_url'] = site_url(array('admin','homes','view',$item_id));
		
		// Add month navigation urls
		$last_month = strtotime('-1 month', mktime(0,0,0,$month,1,$year));
		$next_month = strtotime('+1 month', mktime(0,0,0,$month,1,$year));
		$data['back_url'] = site_url(array('admin','homes','calendar',$item_id,timestamp_to_date($last_month, 'Y'),timestamp_to_date($last_month, 'm')));
		$data['forward_url'] = site_url(array('admin','homes','calendar',$item_id,timestamp_to_date($next_month, 'Y'),timestamp_to_date($next_month, 'm')));
		
		// Set the spanish locale for the month names
		setlocale(LC_TIME, "es_ES");
		$prev_year = mktime(0,0,0,12,1,($year-1));
		$next_year = mktime(0,0,0,1,1,($year+1));
		
		// Month Navigation Structure
		$data['month_navigation'] = new stdClass;
		
		// December of previous year
		$data['month_navigation']->prev_year_link = new stdClass;
		$data['month_navigation']->prev_year_link->name = timestamp_to_date($prev_year, 'Y') . ' ' . ucfirst(strftime('%B', $prev_year));
		$data['month_navigation']->prev_year_link->url = site_url(array('admin','homes','calendar',$item_id,timestamp_to_date($prev_year, 'Y'),timestamp_to_date($prev_year, 'm')));
		
		// January of next year
		$data['month_navigation']->next_year_link = new stdClass;
		$data['month_navigation']->next_year_link->name = timestamp_to_date($next_year, 'Y') . ' ' . ucfirst(strftime('%B', $next_year));
		$data['month_navigation']->next_year_link->url = site_url(array('admin','homes','calendar',$item_id,timestamp_to_date($next_year, 'Y'),timestamp_to_date($next_year, 'm')));
		
		// All months of the current year we are on
		$data['month_navigation']->this_year_links = Array();
		for($i=1; $i <= 12; $i++) {
			$month_date = mktime(0,0,0,$i,1,$year);
			$month_link = new stdClass;
			$month_link->name = $year . ' ' . ucfirst(strftime('%B', $month_date));
			$month_link->url = site_url(array('admin','homes','calendar',$item_id,$year,$i));
			array_push($data['month_navigation']->this_year_links, $month_link);
		}
		
		// Load the view with the data
		$this->load->view('admin/homes_calendar', $data);
		
	}
	
	/* PRICES METHOD 
	------------------------------------------------------------------
	Description: Loads the list of prices for the home including
	year paging
	----------------------------------------------------------------*/
	
	public function prices($item_id=0,$year=0,$estimate_saved=FALSE)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Initialize a home object with its data
		$data['home'] = $this->home->initialize($item_id);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($data['home']->is_new === TRUE) {
			redirect('/admin/homes/index', 'refresh');
			exit();
		}
		
		// Default the year to current year
        $year = ($year ? $year : date('Y'));
        
        // Get the anual estimate data
        $anual_estimate = $data['home']->get_anual_estimate($year);
        $data['anual_estimate'] = array(
        	'amount' => ($anual_estimate ? (number_format($anual_estimate, 2, '.', '')) : ''),
        	'action_url' => site_url(array('admin','homes','save_estimate',$item_id,$year)),
        	'estimate_saved' => $estimate_saved
        );
		
		// Load the timeslots model to get the price data
		$this->load->model('timeslot');
		
		$data['calendar'] = $this->timeslot->get_calendar($item_id,$year);
		
		// Loop though the weeks in the calendar to prep the data
		foreach($data['calendar']->weeks as $week) {
			$week->price = ($week->price ? (number_format($week->price, 2, '.', '') . ' &euro;') : 'Sin Precio');
			$week->estimate = (is_float($week->estimate) ? (number_format($week->estimate, 2, '.', '') . ' &euro;') : 'Sin Precio');
			$week->action_url = site_url(array('admin','timeslots','action',($week->has_prices ? 'edit' : 'new'),$item_id,$week->date_start,$week->date_end));
			$week->delete_url = site_url(array('admin','timeslots','delete',$item_id,$week->date_start,$week->date_end));
			$week->view_class = ($week->has_prices ? '' : 'no-price');
		}
		
		// Add and prep any entry navigation links
		$data['list_url'] = site_url('admin/homes');
		$data['parent_url'] = site_url(array('admin','homes','view',$item_id));
		
		// Add year navigation urls
		$data['back_url'] = site_url(array('admin','homes','prices',$item_id,($year - 1)));
		$data['forward_url'] = site_url(array('admin','homes','prices',$item_id,($year + 1)));
		
		// Load the view with the data
		$this->load->view('admin/homes_prices', $data);
		
	}
	
	
	/* DOCUMENTS METHOD 
	------------------------------------------------------------------
	Description: Loads the list of documents for the home including
	year paging
	----------------------------------------------------------------*/
	
	public function documents($item_id=0,$year=0,$tab='conditions')
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Initialize a home object with its data
		$data['home'] = $this->home->initialize($item_id);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($data['home']->is_new === TRUE) {
			redirect('/admin/homes/index', 'refresh');
			exit();
		}
		
		// Default the year to current year
        $year = ($year ? $year : date('Y'));
        $data['year'] = $year;
        $data['tab'] = $tab;
		
		// Get the list of conditions
		$data['conditions'] = $this->home->list_conditions($item_id,$year)->result();
		foreach($data['conditions'] as $condition) {
			$condition->date_created = mysqldatetime_to_date($condition->date_created, 'd/m/Y');
			$condition->booking_url = site_url(array('admin','bookings','view',$condition->booking_id));
			$condition->view_url_es = site_url(array('admin','conditions','view',$condition->item_id,'es'));
			$condition->view_url_en = site_url(array('admin','conditions','view',$condition->item_id,'en'));
		}
		
		// Get the list of receipts
		$data['receipts'] = $this->home->list_receipts($item_id,$year)->result();
		foreach($data['receipts'] as $receipt) {
			$receipt->date_created = mysqldatetime_to_date($receipt->date_created, 'd/m/Y');
			$receipt->booking_url = site_url(array('admin','bookings','view',$receipt->booking_id));
			$receipt->view_url_es = site_url(array('admin','receipts','view',$receipt->item_id,'es'));
			$receipt->view_url_en = site_url(array('admin','receipts','view',$receipt->item_id,'en'));
		}
		
		// Add and prep any entry navigation links
		$data['list_url'] = site_url('admin/homes');
		$data['parent_url'] = site_url(array('admin','homes','view',$item_id));
		
		// Add year navigation urls
		$data['back_url'] = site_url(array('admin','homes','documents',$item_id,($year - 1),$tab));
		$data['forward_url'] = site_url(array('admin','homes','documents',$item_id,($year + 1),$tab));
		
		// Load the view with the data
		$this->load->view('admin/homes_documents', $data);
		
	}
	
	
	/* MAP METHOD 
	------------------------------------------------------------------
	Description: Loads the items map page with data
	----------------------------------------------------------------*/
	
	public function map($item_id=0,$lang='es')
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Initialize a home object with its data
		$data['home'] = $this->home->initialize($item_id);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($data['home']->is_new === TRUE) {
			redirect('/admin/homes/index', 'refresh');
			exit();
		}
		
		// Add and prep any entry navigation links and data
		$data['lang'] = $lang;
		$data['lang_url_en'] = site_url(array('admin','homes','map',$item_id,'en'));
		$data['lang_url_es'] = site_url(array('admin','homes','map',$item_id,'es'));
		$data['list_url'] = site_url('admin/homes');
		$data['parent_url'] = site_url(array('admin','homes','view',$item_id));
		
		// Get the airport data for the dropdown
		$this->load->model('airport');
		$data['airports'] = $this->airport->list_entries()->result();
		
		// Get the payment types data for the dropdown
		$this->load->model('payment_type');
		$data['payment_types'] = $this->payment_type->list_entries()->result();
		
		// Load the view with the data
		$this->load->view('admin/homes_map',$data);
		
	}
	
	
	/* ACTION METHOD 
	------------------------------------------------------------------
	Description: Loads the items form page for 'new' or 'edit'
	----------------------------------------------------------------*/
	
	public function action($action='new',$item_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for cancels first
		if($this->input->post('submit') !== FALSE && $this->input->post('submit') == 'Cancelar') {
			redirect('/admin/homes/index', 'refresh');
		}
		
		// Set the home_item_id variable of this class to the home that is being edited or created
		$this->home_item_id = $item_id;
		
		// Load any necessary libraries and helpers
		$this->load->library('form_validation');
		
		// Set the form validation rules on required elements
		$this->form_validation->set_rules('name', 'Nombre', 'trim|required');
		$this->form_validation->set_rules('code', 'C&oacute;digo', 'trim|required|alpha|min_length[3]|max_length[3]|callback_code_check');
		$this->form_validation->set_rules('address', 'Direcci&iacute;n', 'trim|required');
		$this->form_validation->set_rules('city', 'Municipio', 'trim|required');
		$this->form_validation->set_rules('province', 'Provincia', 'trim|required');
		$this->form_validation->set_rules('postal_code', 'C&oacute;digo Postal', 'trim|required');
		$this->form_validation->set_rules('owner_id', 'Propietario', 'trim|required');
		$this->form_validation->set_rules('commission_percent', 'Comisi&oacute;n', 'trim|required');
		$this->form_validation->set_rules('capacity', 'Capacidad', 'trim|required');
		
		// Optional Elements (run in if statements to check for their presence - NOTE: CI requires this to return posted values)
		if($this->input->post('latitude') || $this->input->post('longitude')) {
			$this->form_validation->set_rules('latitude', 'Latitude', 'trim|required|decimal');
			$this->form_validation->set_rules('longitude', 'Longitude', 'trim|required|decimal');
		}
		if($this->input->post('notes')) {
			$this->form_validation->set_rules('notes', 'Notas Adicionales', 'trim|required');
		}
		
		// Set the error message delimeters
		$this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');
		
		// Initialize the home model with the data
		$home = $this->home->initialize($item_id);
		
		// Check for form submissions
		if ($this->form_validation->run() == FALSE) {
		
			// INVALID SUBMISSION OR NO SUBMISSION - Display the form
			
			// Load the dependancies (for select and check boxes)
			$this->load->model('owner');
			$this->load->helper('form');
			
			// Add the home data for the form
			$data['home'] = $home;
			
			// Add the navigation data
			$data['list_url'] = site_url(array('admin','homes','index'));
			$data['action'] = $action;
			$data['action_url'] = site_url(array('admin','homes','action',$action,$item_id));
			$data['action_title'] = ($action == 'edit' ? ('Editar ' . $home->name) : 'A&ntilde;adir Vivienda Nueva');
			
			// Add the form select and check box data
			$data['all_owners'] = $this->owner->list_entries()->result();
			$data['all_capacity'] = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);
			
			// Load the form view with the data
			$this->load->view('admin/homes_form', $data);
			
		
		} else {
		
			// VALID SUBMISSION - Set its parameters and save the entry
			
			// Set the 'simple' data parameters directly
			$home->name = $this->input->post('name');
			$home->code = $this->input->post('code');
			$home->address = $this->input->post('address');
			$home->city = $this->input->post('city');
			$home->province = $this->input->post('province');
			$home->postal_code = $this->input->post('postal_code');
			$home->latitude = $this->input->post('latitude');
			$home->longitude = $this->input->post('longitude');
			$home->commission_percent = $this->input->post('commission_percent');
			$home->notes = $this->input->post('notes');
			$home->capacity = $this->input->post('capacity');
			
			// Set the more 'complex' data types using class setters
			$home->set_owner($this->input->post('owner_id'));
			
			// Call the models save method
			$home->save_entry();
			
			// Send the user on their way to the listing page via an http redirect
			redirect('/admin/homes/index', 'refresh');
		
		}

		
	}
	
	
	/* SAVE ESTIMATE METHOD 
	------------------------------------------------------------------
	Description: Saves the estimate for a specific year and routes
	back to the prices method
	----------------------------------------------------------------*/
	
	function save_estimate($item_id=0,$year=0)
	{
		
		$estimate = $this->input->post('amount');
		$this->home->save_estimate($item_id, $year, $estimate);
		$saved = FALSE;
		
		// If there is an estimate (not blank or 0)
		if($estimate) {
			$saved = TRUE;
		}
		
		// Route the user
		$this->prices($item_id,$year,$saved);
		
	}
	
	
	/* SET PRICES METHOD 
	------------------------------------------------------------------
	Description: Sets the items timeslot price
	
	For AJAX Calls
	----------------------------------------------------------------*/
	
	public function set_price($item_id=0,$timeslot_id=0)
	{	
		
		// If form is submitted
		
			// Set the defaults
			$status = '';
			$msg = '';
			
			// Echo out JSON encoded response data
			echo json_encode(array('status' => $status, 'msg' => $msg));
		
		
		// If form is not submitted
		
			// Load the view with the data
			$this->load->view('admin/price_modal');
		
	}
	
	
	/* SET STATUS METHOD 
	------------------------------------------------------------------
	Description: Sets the items status (activated/deactivated).
	
	For AJAX Calls
	----------------------------------------------------------------*/
	
	public function set_status($item_id=0,$status='')
	{	
		
		// Set the defaults
		$status = '';
		$msg = '';
		
		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg));
		
	}
	
	
	/* SET ORDER METHOD 
	------------------------------------------------------------------
	Description: Sets the items order based on a comma delimited list
	of all item ids in the post scope
	
	For AJAX Calls
	----------------------------------------------------------------*/
	
	public function set_order()
	{	
		
		// Set the defaults
		$status = '';
		$msg = '';
		$uccess = FALSE;
		
		if($this->input->post('home_ids')) {
			$success = $this->home->reorder($this->input->post('home_ids'));
		}
		
		// Set the response data
		if($success) {
			$status = 'success';
			$msg = 'Se ha guardado correctamente el orden de las viviendas.';
		} else {
			$status = 'error';
			$msg = 'Hay un problema. No se ha podido reordenar las viviendas.';
		}
		
		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg));
		
	}
	
	
	/* SET ACTIVE METHOD 
	------------------------------------------------------------------
	Description: Sets the items status (activated/deactivated).
	
	For AJAX Calls
	----------------------------------------------------------------*/
	
	public function set_active($item_id=0,$is_active=0)
	{	
		
		// Set the defaults
		$status = '';
		$msg = '';
		
		// Call the function in the owner model to update the is_active status
		$this->home->save_active_state($item_id,$is_active);
		$status = 'success';
		$msg = 'La vivienda ha sido ' . ($is_active ? 'activado' : 'desactivado');
		
		// Echo out JSON encoded response data
		echo json_encode(array('status' => $status, 'msg' => $msg, 'item_id' => $item_id, 'is_active' => $is_active));
		
	}
	
	
	/* DELETE METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with a delete warning form
	or deletes the item on form submission.
	----------------------------------------------------------------*/
	
	public function delete($item_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for form confirmation
		if($this->input->post('submit')) {
			
			// Check the confirmation
			if ($this->input->post('submit') == 'Borrar') {
				
				// Delete the entry
				$this->home->delete_entry($item_id);
				
			}
			
			// Redirect the user to the list page via http redirect
			redirect('/admin/homes/index');
			
		} else {
			
			// Call the view method with a delete flag to display the form
			$this->view($item_id,TRUE);
			
		}
		
		
	}
	
	
	/* CODE CHECK CALLBACK FUNCTION
	------------------------------------------------------------------
	Description: Used in conjunction with the form validation methods
	in the action method to test if a code is not already taken
	----------------------------------------------------------------*/
	
	public function code_check($str)
	{
		
		// Call the database directly to check for existing record
		$this->db->select('item_id, code');
		$this->db->from('homes');
		$this->db->where('code', $str);
		$this->db->where('item_id !=', $this->home_item_id);
		$query_result = $this->db->get()->result();
		
		if ($query_result)
		{
			$error_msg = 'El c&oacute;digo ' . $str . ' ya esta siendo utilizado por otra vivienda';
			$this->form_validation->set_message('code_check', $error_msg);
			return FALSE;
		}
		else
		{
			return TRUE;
		}
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/admin/homes.php */