<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoices extends Admin_Controller {
	
	// Create a property for this class
	// (used in checking for existing reference codes in the check_code method of this class)
	var $invoice_item_id;
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Invoices()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load the models we will use in this controller
		$this->load->model('invoice');
		$this->load->model('owner');
		$this->load->model('system_config');
		
		// Initialize this property to 0
		$this->invoice_item_id = 0;

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page based on the parent id
	(owner) and the year
	----------------------------------------------------------------*/
	
	public function index($parent_id=0,$year=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Default the year to current
		if(!$year) {
			$year = date('Y');
		}
		
		// Get the data for the owner
		$data['owner'] = $this->owner->get_entry($parent_id)->row();
		$data['current_year'] = $year;
		
		// Get the list of invoices from the model and prep them for the view
		$data['invoices'] = $this->invoice->list_entries($parent_id,$year)->result();
		foreach($data['invoices'] as $invoice) {
			$invoice->trimester = 'T' . get_fiscal_trimester(mysqldatetime_to_timestamp($invoice->invoice_date));
			$invoice->invoice_date = mysqldatetime_to_date($invoice->invoice_date, 'd/m/Y');
			$invoice->total_amount = ($invoice->total_amount ? (number_format($invoice->total_amount, 2, '.', '') . ' &euro;') : '-');
			$invoice->view_url = site_url(array('admin','invoices','view',$invoice->item_id));
			$invoice->edit_url = site_url(array('admin','invoices','action','edit',$parent_id,$invoice->item_id));
			$invoice->delete_url = site_url(array('admin','invoices','delete',$parent_id,$year,$invoice->item_id));
		}
		
		// Add and list links
		$data['parent_url'] = site_url(array('admin','owners','view',$parent_id));
		$data['back_url'] = site_url(array('admin','invoices','index',$parent_id,($year - 1)));
		$data['forward_url'] = site_url(array('admin','invoices','index',$parent_id,($year + 1)));
		
		// New Invoices links based on year and trimester
		$data['add_t1_url'] = site_url(array('admin','invoices','action','new',$parent_id,0,$year,1));
		$data['add_t2_url'] = site_url(array('admin','invoices','action','new',$parent_id,0,$year,2));
		$data['add_t3_url'] = site_url(array('admin','invoices','action','new',$parent_id,0,$year,3));
		$data['add_t4_url'] = site_url(array('admin','invoices','action','new',$parent_id,0,$year,4));
		$data['add_all_url'] = site_url(array('admin','invoices','action','new',$parent_id));
		
		// Load the view with the data
		$this->load->view('admin/invoices_list', $data);
		
	}
	
	
	/* VIEW METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with data
	----------------------------------------------------------------*/
	
	public function view($item_id=0,$delete_item=FALSE)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Get the invoice data
		$data['invoice'] = $this->invoice->initialize($item_id);
		
		// If this is not a valid entry (URL Hack), send user back to list page
		if($data['invoice']->is_new === TRUE) {
			redirect('/admin/owners/index', 'refresh');
			exit();
		}
		
		// Prep the data for view
		$year = timestamp_to_date($data['invoice']->invoice_date, 'Y');
		$data['current_year'] = $year;
		$data['invoice']->invoice_date = timestamp_to_date($data['invoice']->invoice_date, 'd/m/Y');
		
		// Add and prep any entry navigation links
		$data['parent_url'] = site_url(array('admin','owners','view',$data['invoice']->owner->item_id));
		$data['list_url'] = site_url(array('admin','invoices','index',$data['invoice']->owner->item_id,$year));
		$data['print_url_es'] = site_url(array('admin','invoices','print_view',$item_id,'es'));
		$data['print_url_en'] = site_url(array('admin','invoices','print_view',$item_id,'en'));
		$data['edit_url'] = site_url(array('admin','invoices','action','edit',$data['invoice']->owner->item_id,$item_id));
		$data['delete_url'] = site_url(array('admin','invoices','delete',$data['invoice']->owner->item_id,$year,$item_id));
		
		// Add the delete flag
		$data['delete_item'] = $delete_item;
		
		// Load the view with the data
		$this->load->view('admin/invoices_view', $data);
		
	}
	
	
	/* PRINT METHOD 
	------------------------------------------------------------------
	Description: Loads the items print view page with data
	----------------------------------------------------------------*/
	
	public function print_view($item_id=0, $language_id='es')
	{	
		
		// Get the invoice data
		$data['invoice'] = $this->invoice->initialize($item_id);
		$data['system_config'] = $this->system_config->initialize(1);
		
		// Prep the data for view
		$data['language_id'] = $language_id;
		$data['invoice']->invoice_date = timestamp_to_date($data['invoice']->invoice_date, 'd/m/Y');
		
		// Load the view with the data
		$this->load->view('common/invoices_print', $data);
		
	}
	
	
	/* ACTION METHOD 
	------------------------------------------------------------------
	Description: Loads the items form page for 'new' or 'edit'
	----------------------------------------------------------------*/
	
	public function action($action='new',$parent_id=0,$item_id=0,$year=0,$trimester=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for cancels first
		if($this->input->post('submit') !== FALSE && $this->input->post('submit') == 'Cancelar') {
			$redirect_string = '/admin/invoices/index/' . $parent_id;
			redirect($redirect_string, 'refresh');
		}
		
		// Set the invoice_item_id variable of this class to the invoice that is being edited or created
		$this->invoice_item_id = $item_id;
		
		// Load any necessary libraries and helpers
		$this->load->library('form_validation');
		
		// Set the form validation rules on required elements
		$this->form_validation->set_rules('invoice_num', 'N&uacute;mero de Factura', 'trim|required|callback_reference_check');
		$this->form_validation->set_rules('invoice_date', 'Fecha Factura', 'trim|required');
		$this->form_validation->set_rules('invoice_description', 'Descripci&oacute;n', 'trim|required');
		
		$this->form_validation->set_rules('line_item_ids', 'Detalle', 'required');
		$this->form_validation->set_rules('line_descriptions', 'Descripciones', 'required');
		$this->form_validation->set_rules('line_net_amounts', 'Cantidades', 'required');
		$this->form_validation->set_rules('line_vat_percentages', 'Porcentages de IVA', 'required');
		$this->form_validation->set_rules('line_vat_amounts', 'Cantidades de IVA', 'required');
		$this->form_validation->set_rules('line_total_amounts', 'Sub-Totales', 'required');
		
		$this->form_validation->set_rules('net_amount', 'Base Imponible', 'trim|required');
		$this->form_validation->set_rules('vat_amount', 'Total IVA', 'trim|required');
		$this->form_validation->set_rules('total_amount', 'Total Factura', 'trim|required');
		
		// Set optional rules
		if($this->input->post('line_item_payment_ids')) {
			$this->form_validation->set_rules('line_payment_ids', 'Comisiones', 'required');
		}
		if($this->input->post('line_item_expense_ids')) {
			$this->form_validation->set_rules('line_expense_ids', 'Gastos', 'required');
		}
		
		// Set the error message delimeters
		$this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');
		
		// Initialize the invoice model with the data
		$invoice = $this->invoice->initialize($item_id,$parent_id,$year,$trimester);
		
		// Check for form submissions
		if ($this->form_validation->run() == FALSE) {
		
			// INVALID SUBMISSION OR NO SUBMISSION - Display the form
			
			// Load the dependancies (for select and check boxes)
			$this->load->helper('form');
			
			// Add the invoice data for the form and format it
			$data['invoice'] = $invoice;
			$data['invoice']->invoice_date = timestamp_to_date($invoice->invoice_date, 'd/m/Y');
			$data['invoice']->net_amount = number_format($invoice->net_amount, 2, '.', '');
			$data['invoice']->vat_amount = number_format($invoice->vat_amount, 2, '.', '');
			$data['invoice']->total_amount = number_format($invoice->total_amount, 2, '.', '');
			
			// Prep line items and get the comma delimited lists of values for hidden form fields
			$line_item_ids = Array();
			$line_descriptions = Array();
			$line_net_amounts = Array();
			$line_vat_percentages = Array();
			$line_vat_amounts = Array();
			$line_total_amounts = Array();
			$line_payment_ids = Array();
			$line_expense_ids = Array();
			foreach($data['invoice']->line_items as $line_item) {
				$line_item->net_amount = number_format($line_item->net_amount, 2, '.', '');
				$line_item->vat_amount = number_format($line_item->vat_amount, 2, '.', '');
				$line_item->total_amount = number_format($line_item->total_amount, 2, '.', '');
				array_push($line_item_ids, $line_item->item_id);
				array_push($line_descriptions, $line_item->description);
				array_push($line_net_amounts, $line_item->net_amount);
				array_push($line_vat_percentages, $line_item->vat_percent);
				array_push($line_vat_amounts, $line_item->vat_amount);
				array_push($line_total_amounts, $line_item->total_amount);
				array_push($line_payment_ids, $line_item->payment_id);
				array_push($line_expense_ids, $line_item->expense_id);
			}
			$data['invoice']->line_item_ids = implode('|', $line_item_ids);
			$data['invoice']->line_descriptions = implode('|', $line_descriptions);
			$data['invoice']->line_net_amounts = implode('|', $line_net_amounts);
			$data['invoice']->line_vat_percentages = implode('|', $line_vat_percentages);
			$data['invoice']->line_vat_amounts = implode('|', $line_vat_amounts);
			$data['invoice']->line_total_amounts = implode('|', $line_total_amounts);
			$data['invoice']->line_payment_ids = implode('|', $line_payment_ids);
			$data['invoice']->line_expense_ids = implode('|', $line_expense_ids);
			
			// Add the navigation data
			$data['parent_url'] = site_url(array('admin','owners','view',$parent_id));
			$data['list_url'] = site_url(array('admin','invoices','index',$parent_id));
			$data['action'] = $action;
			$data['action_url'] = site_url(array('admin','invoices','action',$action,$parent_id,$item_id,$year,$trimester));
			$data['action_title'] = ($action == 'edit' ? ('Editar Factura N&deg; : ' . $invoice->invoice_num) : 'Crear Factura Nueva');
			
			// Load the form view with the data
			$this->load->view('admin/invoices_form', $data);
			
		
		} else {
		
			// VALID SUBMISSION - Set its parameters and save the entry
			
			// Set the 'simple' data parameters directly
			$invoice->invoice_num = $this->input->post('invoice_num');
			$invoice->invoice_date = strtotime(str_replace('/', '-', $this->input->post('invoice_date')));
			$invoice->invoice_description = $this->input->post('invoice_description');
			$invoice->net_amount = $this->input->post('net_amount');
			$invoice->vat_amount = $this->input->post('vat_amount');
			$invoice->total_amount = $this->input->post('total_amount');
			
			// Set the more 'complex' data types using class setters
			$invoice->set_owner($parent_id);
			$invoice->set_line_items(
				$this->input->post('line_item_ids'),
				$this->input->post('line_descriptions'),
				$this->input->post('line_net_amounts'),
				$this->input->post('line_vat_percentages'),
				$this->input->post('line_vat_amounts'),
				$this->input->post('line_total_amounts'),
				$this->input->post('line_payment_ids'),
				$this->input->post('line_expense_ids')
			);
			
			// Call the models save method
			$invoice->save_entry();
			
			// Send the user on their way to the listing page via an http redirect
			$redirect_string = '/admin/invoices/index/' . $parent_id;
			redirect($redirect_string, 'refresh');
		
		}

		
	}
	
	
	/* DELETE METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with a delete warning form
	or deletes the item on form submission.
	----------------------------------------------------------------*/
	
	public function delete($parent_id=0,$year=0,$item_id=0)
	{	
		
		// Add the number of alerts
		$data['active_alerts'] = $this->active_alerts;
		
		// Check for form confirmation
		if($this->input->post('submit')) {
			
			// Check the confirmation
			if ($this->input->post('submit') == 'Borrar') {
				
				// Delete the entry
				$this->invoice->delete_entry($item_id);
				
			}
			
			// Redirect the user to the list page via http redirect
			$redirect_string = '/admin/invoices/index/' . $parent_id . '/' . $year;
			redirect($redirect_string);
			
		} else {
			
			// Call the view method with a delete flag to display the form
			$this->view($item_id,TRUE);
			
		}
		
	}
	
	
	/* CODE CHECK CALLBACK FUNCTION
	------------------------------------------------------------------
	Description: Used in conjunction with the form validation methods
	in the action method to test if a reference is not already taken
	----------------------------------------------------------------*/
	
	public function reference_check($str)
	{
		
		// Call the database directly to check for existing record
		$this->db->select('item_id, invoice_num');
		$this->db->from('invoices');
		$this->db->where('invoice_num', $str);
		$this->db->where('item_id !=', $this->invoice_item_id);
		$query_result = $this->db->get()->result();
		
		if ($query_result)
		{
			$error_msg = 'El n&uacute;mero de factura ' . $str . ' ya esta siendo utilizado por otro factura';
			$this->form_validation->set_message('reference_check', $error_msg);
			return FALSE;
		}
		else
		{
			return TRUE;
		}
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/admin/invoices.php */