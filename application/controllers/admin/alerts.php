<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alerts extends Admin_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Alerts()
	{

		// Inherit parent class methods and properties
		parent::__construct();

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Loads the items listing page as a modal and returns
	the resulted generated page via JSON.
	
	For AJAX Calls
	----------------------------------------------------------------*/
	
	public function index()
	{	
		// Get the alerts from the model
		$data['alerts'] = $this->alert->list_entries()->result();
		$data['num_alerts'] = count($data['alerts']);

		
		// Prep the data for the view
		foreach($data['alerts'] as $alert) {
			
			// Data formatting
			switch($alert->type_id) {
				case 'directions':
					$alert->type_name = 'Enviar Direcciones';
					break;
				case 'reminder':
					$alert->type_name = 'Llegada el ' . mysqldatetime_to_date($alert->booking_date_from, 'd/m/Y');
					break;
				case 'review':
					$alert->type_name = 'Enviar Cuestionario';
					break;
				case 'pay_1_delayed':
					$alert->type_name = 'Primer Pago Retrasado';
					break;
				case 'pay_2_pending':
					$alert->type_name = 'Segundo Pago Pendiente';
					break;
				case 'pay_2_delayed':
					$alert->type_name = 'Segundo Pago Retrasado';
					break;
				case 'pay_3_pending':
					$alert->type_name = 'Tercer Pago Pendiente';
					break;
				case 'pay_3_delayed':
					$alert->type_name = 'Tercer Pago Retrasado';
					break;
				case 'payment':
				default:
					$alert->type_name = 'Pago Pendiente';
					break;
			}
			$alert->display_date = mysqldatetime_to_date($alert->display_date, 'd/m/Y');
			$alert->payment_amount = ($alert->payment_amount ? number_format($alert->payment_amount, 2, '.', '') : NULL);
			$alert->payment_date_due = ($alert->payment_date_due ? mysqldatetime_to_date($alert->payment_date_due, 'd/m/Y') : NULL);
			
			// Navigation for each
			$alert->active_url = site_url(array('admin','alerts','set_active',$alert->item_id,0));
			$alert->booking_url = site_url(array('admin','bookings','view',$alert->booking_id));
			$alert->booking_email_url = 'mailto:' . $alert->booking_email;
			
		}
		
		// Load the view with the data
		$modal = $this->load->view('admin/alerts_list',$data,TRUE);
		
		// Echo back the rendered view
		echo $modal;
		
	}
	
	/* SET IS_ACTIVE METHOD 
	------------------------------------------------------------------
	Description: Sets the items active property (activated = 1 /
	deactivated = 0).
	
	For AJAX Calls
	----------------------------------------------------------------*/
	
	public function set_active($item_id=0,$is_active=0)
	{	
		
		// Set the defaults
		$status = '';
		$msg = '';
		
		// Initialize the alert, set its is_active property and save it
		$selected_alert = $this->alert->initialize($item_id);
		$selected_alert->is_active = $is_active;
		$selected_alert->save_entry();
		
		$status = 'success';
		$msg = 'The alert has been dismissed';
		
		// Echo out JSON encoded response data
		echo json_encode(array('item_id' => $item_id, 'is_active' => $is_active, 'status' => $status, 'msg' => $msg));
		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/admin/alerts.php */