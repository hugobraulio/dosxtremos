<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Timeslots extends Admin_Controller {
	
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function Timeslots()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load the models we will use in this controller
		$this->load->model('timespan');

	}
	
	
	/* DEFAULT METHOD 
	------------------------------------------------------------------
	Description: Dummy function to send user back to calendar view
	----------------------------------------------------------------*/
	
	public function index()
	{	
		
		// Send the user on their way to the listing page via an http redirect
		redirect('/admin/bookings/index', 'refresh');
		
	}
	
	
	
	
	public function view_week($home_id, $date_start, $date_end) {
		
		// Get the timespan to view
		$data['timespan'] = $this->timespan->get_week($home_id, $date_start, $date_end);
		
		// New Offer and Booking Links
		$data['add_offer_url'] = site_url(array('admin','bookings','action','new','offer',0,$home_id,$date_start,$date_end));
		$data['add_booking_url'] = site_url(array('admin','bookings','action','new','booking',0,$home_id,$date_start,$date_end));
		
		// Load the view with the data
		$modal = $this->load->view('admin/timeslots_week',$data,TRUE);
		
		// Echo back the rendered view
		echo $modal;
		
	}
	
	
	/* VIEW METHOD 
	------------------------------------------------------------------
	Description: Loads the items view page with data into a variable
	which is echoed out (Used for AJAX calls)
	----------------------------------------------------------------*/
	
	public function view($home_id=0, $date_start=0, $date_end=0)
	{	
		
		// Get the data for this timeslot
		$data['timespan'] = $this->timespan->initialize($home_id,$date_start,$date_end);
		
		// Prep the data for the view
		
		// Action title
		setlocale(LC_TIME, "es_ES");
		$data['action_title'] = 'Semana del ' . strftime('%d de %B', $data['timespan']->date_start) . ' al ' . strftime('%d de %B', $data['timespan']->date_end);
		
		// Prices
		$data['timespan']->price = ($data['timespan']->price ? number_format($data['timespan']->price, 2, '.', '') : '');
		$data['timespan']->estimate = ($data['timespan']->estimate ? number_format($data['timespan']->estimate, 2, '.', '') : '');
		
		// Timeslot Dates
		$data['timespan']->date_start = timestamp_to_date($data['timespan']->date_start, 'd/m/Y');
		$data['timespan']->date_end = timestamp_to_date($data['timespan']->date_end, 'd/m/Y');
		
		// Offers Navigation and Offered Dates
		foreach($data['timespan']->offers as $offer) {
			$offer->date_from = timestamp_to_date($offer->date_from, 'd/m/Y');
			$offer->date_to = timestamp_to_date($offer->date_to, 'd/m/Y');
			$offer->date_offered = timestamp_to_date($offer->date_offered, 'd/m/Y');
			$offer->view_url = site_url(array('admin','bookings','view',$offer->item_id));
			$offer->edit_url = site_url(array('admin','bookings','action','edit','offer',$offer->item_id));
			$offer->delete_url = site_url(array('admin','bookings','delete',$offer->item_id));
		}
		
		// Booking
		if($data['timespan']->booking) {
			
			// Booking Data and Navigation
			$data['timespan']->booking->date_from = timestamp_to_date($data['timespan']->booking->date_from, 'd/m/Y');
			$data['timespan']->booking->date_to = timestamp_to_date($data['timespan']->booking->date_to, 'd/m/Y');
			$data['timespan']->booking->date_offered = timestamp_to_date($data['timespan']->booking->date_offered, 'd/m/Y');
			$data['timespan']->booking->date_booked = timestamp_to_date($data['timespan']->booking->date_booked, 'd/m/Y');
			$data['timespan']->booking->view_url = site_url(array('admin','bookings','view',$data['timespan']->booking->item_id));
			$data['timespan']->booking->edit_url = site_url(array('admin','bookings','action','edit','booking',$data['timespan']->booking->item_id));
			$data['timespan']->booking->delete_url = site_url(array('admin','bookings','delete',$data['timespan']->booking->item_id));
			// Booking Payments
			foreach($data['timespan']->booking->payments as $payment) {
				$payment->date_due = ($payment->date_due ? timestamp_to_date($payment->date_due, 'd/m/Y') : '-');
				$payment->date_paid = ($payment->date_paid ? timestamp_to_date($payment->date_paid, 'd/m/Y') : '-');
			}
			
		}
		
		// Status label and class
		switch($data['timespan']->status->item_id) {
			case 1 :
				$data['timespan']->status->label = '<span class="label label-success">' . $data['timespan']->status->name . '</span>';
				$data['timespan']->status->class_name = 'offered';
				break;
			case 2 :
				$data['timespan']->status->label = '<span class="label label-warning">' . $data['timespan']->status->name . '</span>';
				$data['timespan']->status->class_name = 'pre-booked';
				break;
			case 3 :
				$data['timespan']->status->label = '<span class="label label-important">' . $data['timespan']->status->name . '</span>';
				$data['timespan']->status->class_name = 'booked';
				break;
			default :
				$data['timespan']->status->label = $data['timespan']->status->name;
				$data['timespan']->status->class_name = '';
		}
		
		// New Offer and Booking Links
		$data['add_offer_url'] = site_url(array('admin','bookings','action','new','offer',0,$home_id,$date_start,$date_end));
		$data['add_booking_url'] = site_url(array('admin','bookings','action','new','booking',0,$home_id,$date_start,$date_end));
		
		// Load the view with the data
		$modal = $this->load->view('admin/timeslots_view',$data,TRUE);
		
		// Echo back the rendered view
		echo $modal;
		
	}
	
	
	/* ACTION METHOD 
	------------------------------------------------------------------
	Description: Loads the itmes form page to edit or add an entry.
	Returns this view as HTML for using with Ajax modals. Saves the
	data if the form is submitted via Ajax and sends back a confirmation
	or if there is an error, re-sends the form view again marked with
	errors. Uses the parent id parameter and date parameter for new
	entries.
	----------------------------------------------------------------*/
	
	public function action($action='new', $home_id=0, $date_start=0, $date_end=0)
	{	
		
		// Load any necessary libraries and helpers
		$this->load->library('form_validation');
		
		// Set the form validation rules on required elements
		$this->form_validation->set_rules('price', 'Precio Fijado', 'trim|required|decimal|greater_than[0]');
		$this->form_validation->set_rules('estimate', 'Precio Estimado', 'trim|required|decimal');
		$this->form_validation->set_rules('home_id', 'Vivienda', 'trim|required|greater_than[0]');
		
		// Set the error message delimeters
		$this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');
		
		// Initialize the model with the data
		$timespan = $this->timespan->initialize($home_id, $date_start, $date_end);
		
		// Check for form submissions
		if ($this->form_validation->run() == FALSE) {
		
			// INVALID SUBMISSION OR NO SUBMISSION - Display the form
			
			// Load the dependancies (for select and check boxes)
			$this->load->helper('form');
			
			// Add the data for the form
			$data['timespan'] = $timespan;
			$data['timespan']->price = ($data['timespan']->price ? number_format($data['timespan']->price, 2, '.', '') : '');
			$data['timespan']->estimate = (is_float($data['timespan']->estimate) ? number_format($data['timespan']->estimate, 2, '.', '') : '');
			
			// Add the navigation data and additional variables
			$data['action'] = $action;
			$data['action_url'] = site_url(array('admin','timeslots','action',$action,$home_id,$date_start,$date_end));
			
			// Action title
			setlocale(LC_TIME, "es_ES");
			$data['action_title'] = ($action == 'edit' ? 'Editar precios para la semana del ' : 'A&ntilde;adir precios para la semana del ');
			$data['action_title'] = $data['action_title'] . strftime('%d de %B',$timespan->date_start) . ' al ' . strftime('%d de %B',$timespan->date_end);
			
			// Load the form with the data and set the status
			$modal = $this->load->view('admin/timeslots_form',$data,TRUE);
		
		} else {
		
			// VALID SUBMISSION - Set its parameters and save the entry
			
			// Set the 'simple' data parameters directly
			$timespan->price = $this->input->post('price');
			$timespan->estimate = $this->input->post('estimate');
			
			// Set the more 'complex' data types using class setters
			$timespan->set_home($this->input->post('home_id'));
			
			// Call the models save method
			$timespan->save_entry();
			
			// Send back a blank modal value
			$modal = '';
		
		}
		
		// Add a timestamp
		$timestamp = time();
		
		// Format the numbers to currency
		$timespan->price = ($timespan->price ? number_format($timespan->price, 2, '.', '') : '');
		$timespan->estimate = (is_float($timespan->estimate) ? number_format($timespan->estimate, 2, '.', '') : '');
		
		// Send back the item id, price, and rendered modals as JSON
		echo json_encode(array('home_id' => $timespan->home->item_id, 'date_start' => $timespan->date_start, 'date_end' => $timespan->date_end, 'price' => $timespan->price, 'estimate' => $timespan->estimate, 'modal' => $modal, 'timestamp' => $timestamp));
		
	}
	
	
	/* DELETE METHOD 
	------------------------------------------------------------------
	Description: Calls the delete method of the timeslot and returns
	values as JSON encoded string for AJAX calls.
	----------------------------------------------------------------*/
	
	public function delete($home_id=0, $date_start=0, $date_end=0)
	{	
		
		// Call the method that deletes the timeslots
		$this->timespan->delete_entry($home_id,$date_start,$date_end);
		
		// Send back some data via
		echo json_encode(array('home_id' => $home_id, 'date_start' => $date_start, 'date_end' => $date_end));

		
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/admin/bookings.php */