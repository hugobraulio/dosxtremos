
	<div class="modal hide fade" id="owners_bookings_commissions">
	
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3><?php echo ($title ? $title : lang('owners_total_commissions_and_costs') + 'DosXtremos'); ?></h3>
		</div>
		
		<div class="modal-body">
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th><?php echo lang('owners_description'); ?></th>
						<th width="80"><?php echo lang('owners_amount'); ?></th>
						<th width="80"><?php echo lang('owners_vat'); ?></th>
						<th width="80"><?php echo lang('owners_total'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($costs as $cost): ?>
						<tr>
							<td><?php echo $cost->description; ?></td>
							<td><?php echo $cost->amount_gross; ?></td>
							<td><?php echo $cost->amount_vat; ?></td>
							<td><?php echo $cost->amount_net; ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					<th><?php echo lang('owners_total'); ?></th>
					<td><strong><?php echo $total_gross; ?> €</strong></td>
					<td><strong><?php echo $total_vat; ?> €</strong></td>
					<td><strong><?php echo $total_net; ?> €</strong></td>
				</tfoot>
			</table>
			
		</div>
		
		<div class="modal-footer clearfix">
			<a href="#" class="btn pull-right" data-dismiss="modal"><?php echo lang('owners_close'); ?></a>
		</div>
	
	</div>