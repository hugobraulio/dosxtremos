<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/owners_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">
			
			<h1>Casa Almadraba</h1>
			
			<!-- Include the sub navigation -->
			<?php $this->load->view('includes/owners_subnav'); ?>

		</header>
		
		<div class="clearfix page-subheader">
			<h2 class="pull-left"><?php echo lang('owners_advanced_expenses'); ?> : <?php echo $maintenance->name; ?></h2>
			<nav class="btn-group pull-right">
				<a class="btn" href="<?php echo $list_url; ?>"><i class="icon-circle-arrow-up"></i> <?php echo lang('owners_back'); ?></a>
			</nav>
		</div>
		
		<dl class="dl-horizontal">
			<dt><?php echo lang('owners_reference'); ?></dt>
			<dd><?php echo $maintenance->reference_num; ?></dd>
			<dt><?php echo lang('owners_date_complete'); ?></dt>
			<dd><?php echo $maintenance->date_complete; ?></dd>
			<dt><?php echo lang('owners_provider'); ?></dt>
			<dd><?php echo $maintenance->provider; ?></dd>
			<dt><?php echo lang('owners_amount'); ?></dt>
			<dd><?php echo $maintenance->amount; ?> €</dd>
			<?php if($maintenance->notes): ?>
				<dt><?php echo lang('owners_just_comments'); ?></dt>
				<dd><?php echo $maintenance->notes; ?></dd>
			<?php endif; ?>
		</dl>
		
		<div class="page-subheader">
			<h3><?php echo lang('owners_payments_made'); ?></h3>
		</div>
		
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th><?php echo lang('owners_date'); ?></th>
					<th width="100"><?php echo lang('owners_amount'); ?></th>
					<th width="100"><?php echo lang('owners_invoice'); ?></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo $maintenance->date_payed; ?></td>
					<td><?php echo $maintenance->amount; ?> €</td>
					<td>
						<?php if($maintenance->download_url): ?>
							<i class="icon-download"></i> <a href="<?php echo $maintenance->download_url; ?>"><?php echo lang('owners_download'); ?></a>
						<?php else: ?>
							-
						<?php endif; ?>
					</td>
				</tr>
			</tbody>
		</table>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>