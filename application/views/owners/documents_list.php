<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/owners_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">
			
			<h1><?php echo $home->name; ?></h1>
			
			<!-- Include the sub navigation -->
			<?php $this->load->view('includes/owners_subnav'); ?>

		</header>
		
		<div class="clearfix page-subheader">
			<h2 class="pull-left"><?php echo lang('owners_documents'); ?> : <?php echo $current_year; ?></h2>
			<nav class="btn-group pull-right">
				<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
				<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
			</nav>
		</div>
		
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th><?php echo lang('owners_client'); ?></th>
					<th><?php echo lang('owners_booking'); ?></th>
					<th><?php echo lang('owners_file'); ?></th>
					<th width="100"><?php echo lang('owners_download'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($documents as $document): ?>
					<tr>
						<td><?php echo $document->name; ?></td>
						<td><?php echo $document->reference_num . ' (' . $document->date_from . ' - ' . $document->date_to . ')'; ?></td>
						<td><?php echo $document->filename; ?></td>
						<td><a href="<?php echo $document->download_url; ?>" class="btn"><i class="icon-download"></i> <?php echo lang('owners_download'); ?></a></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>