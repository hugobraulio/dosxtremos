<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/owners_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="clearfix page-header">
			
			<h2 class="pull-left"><?php echo lang('owners_invoices'); ?> : <?php echo $current_year; ?></h2>
			<nav class="btn-group pull-right">
				<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
				<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
			</nav>

		</header>
		
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th width="80"><?php echo lang('owners_invoice_number'); ?></th>
					<th width="80"><?php echo lang('owners_date'); ?></th>
					<th><?php echo lang('owners_description'); ?></th>
					<th width="80"><?php echo lang('owners_trimester'); ?></th>
					<th width="80"><?php echo lang('owners_amount'); ?></th>
					<th width="60"><?php echo lang('owners_print'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($invoices as $invoice): ?>
					<tr>
						<td><?php echo $invoice->invoice_num; ?></td>
						<td><?php echo $invoice->invoice_date; ?></td>
						<td><?php echo $invoice->invoice_description; ?></td>
						<td><?php echo 'T' . $invoice->trimester; ?></td>
						<td><?php echo $invoice->total_amount; ?> €</td>
						<td>
							<nav class="btn-group">
								<a class="btn" rel="tooltip" href="<?php echo $invoice->print_url; ?>" target="_blank" title="Imprimir"><i class="icon-print"></i></a>
							</nav>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>