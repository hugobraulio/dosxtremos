
<!--             	IMPORTANT          -->
<!-- 19/Apr/2018, Hugo: Not being used yet. We thought they wanted a pop-up with the commission deducted from the liquidation,
                        and showed in detail in the pop-up. But they want liquidation = payment so far -->

	<div class="modal hide fade" id="owners_bookings_liquidations">

		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3><?php echo ($title ? $title : lang('owners_total_liquidations')); ?></h3>
		</div>

		<div class="modal-body">

			<table class="table table-bordered">
				<thead>
					<tr>
						<th><?php echo lang('owners_description'); ?></th>
						<th width="80"><?php echo lang('owners_received'); ?></th>
						<th width="80"><?php echo lang('owners_commission'); ?></th>
						<th width="80"><?php echo lang('owners_total'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($liquidations as $liquidation): ?>
						<tr>
							<td><?php echo $liquidation->description; ?></td>
							<td><?php echo $liquidation->amount_gross; ?></td>
							<td><?php echo $liquidation->amount_commissions; ?></td>
							<td><?php echo $liquidation->amount_net; ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					<th><?php echo lang('owners_total'); ?></th>
					<td><strong><?php echo $total_gross; ?> €</strong></td>
					<td><strong><?php echo $total_commissions; ?> €</strong></td>
					<td><strong><?php echo $total_net; ?> €</strong></td>
				</tfoot>
			</table>

		</div>

		<div class="modal-footer clearfix">
			<a href="#" class="btn pull-right" data-dismiss="modal"><?php echo lang('owners_close'); ?></a>
		</div>

	</div>
