<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>

	<!-- Load Page Specific Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/owners_expenses_list.js"></script>

</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/owners_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">

		<header class="page-header">

			<h1><?php echo $home->name; ?></h1>

			<!-- Include the sub navigation -->
			<?php $this->load->view('includes/owners_subnav'); ?>

		</header>

		<div class="clearfix page-subheader">
			<h2 class="pull-left"><?php echo lang('owners_expenses'); ?> : <?php echo $current_year; ?></h2>
			<nav class="btn-group pull-right">
				<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
				<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
			</nav>
		</div>

		<!-- No longer necessary to include subheader -->
		<!-- <div class="page-subheader">
			<h3><?php echo lang('owners_commissions'); ?></h3>
		</div> -->

		<table class="table table-bordered table-striped" id="commissions">
			<thead>
				<tr>
					<th><?php echo lang('owners_arrival'); ?></th>
					<th><?php echo lang('owners_client'); ?></th>
					<th><?php echo lang('owners_agreed'); ?></th>
					<th colspan="4"><?php echo lang('owners_received'); ?></th>
					<th colspan="4"><?php echo lang('owners_commissions'); ?></th>
					<th colspan="4"><?php echo lang('owners_liquidated'); ?></th>
				</tr>
				<tr>
					<th colspan="3">&nbsp;</th>
					<th><?php echo lang('t1'); ?></th>
					<th><?php echo lang('t2'); ?></th>
					<th><?php echo lang('t3'); ?></th>
					<th><?php echo lang('t4'); ?></th>
					<th><?php echo lang('t1'); ?></th>
					<th><?php echo lang('t2'); ?></th>
					<th><?php echo lang('t3'); ?></th>
					<th><?php echo lang('t4'); ?></th>
					<th><?php echo lang('t1'); ?></th>
					<th><?php echo lang('t2'); ?></th>
					<th><?php echo lang('t3'); ?></th>
					<th><?php echo lang('t4'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($bookings as $booking): ?>
					<tr>
						<td><?php echo $booking->date_from; ?></td>
						<td><?php echo $booking->name; ?></td>
						<td><?php echo $booking->agreed_price; ?> €</td>
						<td><?php echo $booking->t1_payments; ?></td>
						<td><?php echo $booking->t2_payments; ?></td>
						<td><?php echo $booking->t3_payments; ?></td>
						<td><?php echo $booking->t4_payments; ?></td>
						<td><?php echo $booking->t1_commissions; ?></td>
						<td><?php echo $booking->t2_commissions; ?></td>
						<td><?php echo $booking->t3_commissions; ?></td>
						<td><?php echo $booking->t4_commissions; ?></td>
						<td><?php echo $booking->t1_liquidations; ?></td>
						<td><?php echo $booking->t2_liquidations; ?></td>
						<td><?php echo $booking->t3_liquidations; ?></td>
						<td><?php echo $booking->t4_liquidations; ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="7"><?php echo lang('owners_totals_by_trimester'); ?></th>
					<td><strong><?php echo $bookings_totals->t1_total; ?></strong></td>
					<td><strong><?php echo $bookings_totals->t2_total; ?></strong></td>
					<td><strong><?php echo $bookings_totals->t3_total; ?></strong></td>
					<td><strong><?php echo $bookings_totals->t4_total; ?></strong></td>
					<td><strong><?php echo $bookings_totals->t1_liquidations_total; ?></strong></td>
					<td><strong><?php echo $bookings_totals->t2_liquidations_total; ?></strong></td>
					<td><strong><?php echo $bookings_totals->t3_liquidations_total; ?></strong></td>
					<td><strong><?php echo $bookings_totals->t4_liquidations_total; ?></strong></td>
				</tr>
			</tfoot>
		</table>

		<br/>

		<div class="page-subheader">
			<h3><?php echo lang('owners_advanced_expenses'); ?></h3>
		</div>

		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th><?php echo lang('owners_date'); ?></th>
					<th><?php echo lang('owners_reference'); ?></th>
					<th><?php echo lang('owners_description'); ?></th>
					<th colspan="4"><?php echo lang('owners_just_expenses'); ?></th>
				</tr>
				<tr>
					<th colspan="3">&nbsp;</th>
					<th width="65"><?php echo lang('t1'); ?></th>
					<th width="65"><?php echo lang('t2'); ?></th>
					<th width="65"><?php echo lang('t3'); ?></th>
					<th width="65"><?php echo lang('t4'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($maintenances as $maintenance): ?>
				<tr>
					<td><?php echo $maintenance->date_payed; ?></td>
					<td><a href="<?php echo $maintenance->view_url; ?>"><?php echo $maintenance->reference_num; ?></a></td>
					<td><?php echo $maintenance->name; ?></td>
					<td><?php echo $maintenance->t1_payments; ?></td>
					<td><?php echo $maintenance->t2_payments; ?></td>
					<td><?php echo $maintenance->t3_payments; ?></td>
					<td><?php echo $maintenance->t4_payments; ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="3"><?php echo lang('owners_totals_by_trimester'); ?></th>
					<td><strong><?php echo $maintenance_totals->t1_total; ?></strong></td>
					<td><strong><?php echo $maintenance_totals->t2_total; ?></strong></td>
					<td><strong><?php echo $maintenance_totals->t3_total; ?></strong></td>
					<td><strong><?php echo $maintenance_totals->t4_total; ?></strong></td>
				</tr>
			</tfoot>
		</table>

		<br/>

		<div class="page-subheader">
			<h3><?php echo lang('owners_total_expenses'); ?> <small><?php echo lang('owners_commissions'); ?> + <?php echo lang('owners_advanced_expenses'); ?></small></h3>
		</header>

		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th width="65"><?php echo lang('t1'); ?></th>
					<th width="65"><?php echo lang('t2'); ?></th>
					<th width="65"><?php echo lang('t3'); ?></th>
					<th width="65"><?php echo lang('t4'); ?></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th><?php echo lang('owners_totals_by_trimester'); ?></th>
					<td><strong><?php echo $grand_totals->t1_total; ?></strong></td>
					<td><strong><?php echo $grand_totals->t2_total; ?></strong></td>
					<td><strong><?php echo $grand_totals->t3_total; ?></strong></td>
					<td><strong><?php echo $grand_totals->t4_total; ?></strong></td>
				</tr>
			</tbody>
		</table>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

	<div id="modals"></div>

</body>

</html>
