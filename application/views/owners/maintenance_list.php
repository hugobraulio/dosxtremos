<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/owners_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">
			
			<h1><?php echo $home->name; ?></h1>
			
			<!-- Include the sub navigation -->
			<?php $this->load->view('includes/owners_subnav'); ?>

		</header>
		
		<div class="clearfix page-subheader">
			<h2 class="pull-left"><?php echo lang('owners_advanced_expenses'); ?> : <?php echo $current_year; ?></h2>
			<nav class="btn-group pull-right">
				<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
				<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
			</nav>
		</div>
		
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th><?php echo lang('owners_description'); ?></th>
					<th width="140"><?php echo lang('owners_reference'); ?></th>
					<th width="100"><?php echo lang('owners_date'); ?></th>
					<th width="100"><?php echo lang('owners_amount'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($maintenances as $maintenance): ?>
					<tr>
						<td><a href="<?php echo $maintenance->view_url; ?>"><?php echo $maintenance->name; ?></a></td>
						<td><?php echo $maintenance->reference_num; ?></td>
						<td><?php echo $maintenance->date_payed; ?></td>
						<td><?php echo $maintenance->amount; ?> €</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="3"><?php echo lang('owners_total'); ?></th>
					<td><strong><?php echo $total; ?> €</strong></td>
				</tr>
			</tfoot>
		</table>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>