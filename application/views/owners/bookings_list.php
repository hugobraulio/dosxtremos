<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>

	<!-- Load Page Specific Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/owners_bookings_list.js"></script>

</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/owners_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">

		<header class="page-header">

			<h1><?php echo $home->name; ?></h1>

			<!-- Include the sub navigation -->
			<?php $this->load->view('includes/owners_subnav'); ?>

		</header>

		<div class="clearfix page-subheader">
			<h2 class="pull-left"><?php echo lang('owners_bookings'); ?> : <?php echo $current_year; ?></h2>
			<nav class="btn-group pull-right">
				<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
				<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
			</nav>
		</div>

		<!-- The liquidated column will not be shown until further notice -->
		<table class="table table-striped table-bordered" id="bookings">
			<thead>
				<tr>
					<th><?php echo lang('owners_date'); ?></th>
					<th><?php echo lang('owners_booking'); ?></th>
					<th><?php echo lang('owners_client'); ?></th>
					<th width="85"><?php echo lang('owners_agreed'); ?></th>
					<th width="85"><?php echo lang('owners_received'); ?> <!-- * --></th> <!-- Commented out the asterisk -->
					<!-- <th width="85"><?php echo lang('owners_liquidated'); ?></th> -->
					<th width="85"><?php echo lang('owners_due'); ?></th>
					<th width="85"><?php echo lang('owners_costs'); ?></th>
					<th width="85"><?php echo lang('owners_net'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($bookings as $booking): ?>
					<tr>
						<th><?php echo $booking->date_from . '<br/>' . $booking->date_to; ?></th>
						<td><?php echo $booking->reference_num; ?></td>
						<td><?php echo $booking->name; ?></td>
						<td><?php echo $booking->agreed_price; ?> €</td>
						<td>
							<?php if($booking->view_payments_url): ?>
								<a class="payments detail" href="<?php echo $booking->view_payments_url; ?>"><?php echo $booking->amount_received; ?> €</a>
							<?php else: ?>
								<?php echo $booking->amount_received; ?> €
							<?php endif; ?>
						</td>
						<!-- <td><?php echo $booking->amount_liquidated; ?> €</td> -->
						<td><?php echo $booking->amount_due; ?> €</td>
						<td>
							<?php if($booking->view_costs_url): ?>
								<a class="costs detail" href="<?php echo $booking->view_costs_url; ?>"><?php echo $booking->costs; ?> €</a>
							<?php else: ?>
								<?php echo $booking->costs; ?> €
							<?php endif; ?>
						</td>
						<td><?php echo $booking->net; ?> €</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="3"><?php echo lang('owners_current_totals_for'); ?> <?php echo $current_year; ?></th>
					<td><strong><?php echo $totals->agreed; ?> €</strong></td>
					<td><strong><?php echo $totals->received; ?> €</strong></td>
					<!-- <td><strong><?php echo $totals->liquidated; ?> €</strong></td> -->
					<td><strong><?php echo $totals->due; ?> €</strong></td>
					<td><strong><?php echo $totals->costs; ?> €</strong></td>
					<td><strong><?php echo $totals->net; ?> €</strong></td>
				</tr>
			</tfoot>
		</table>

		<!-- No longer necessary to inclue note about transfer date, etc -->
		<!-- <p><small>* <?php echo lang('owners_received_disclaimer'); ?></small><br/><br/></p> -->

		<!-- No longer necessary to show Estimates -->
		<!-- <div class="page-subheader">
			<h3><?php echo lang('owners_estimates'); ?></h3>
		</div>

		<table class="table table-bordered">
			<tbody>
				<tr>
					<th><?php echo lang('owners_initial_annual_estimate'); ?></th>
					<td width="85"><?php echo $estimates->initial; ?> €</td>
				</tr>
				<tr>
					<th><?php echo lang('owners_revised_annual_estimate'); ?></th>
					<td width="85"><?php echo $estimates->revised; ?> €</td>
				</tr>
			</tbody>
		</table> -->

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

	<div id="modals"></div>

</body>

</html>
