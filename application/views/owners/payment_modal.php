
	<div class="modal hide fade" id="payment_modal">
	
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3><?php echo lang('owners_payments_received'); ?></h3>
		</div>
		
		<div class="modal-body">
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th><?php echo lang('owners_percentage'); ?></th>
						<th width="90"><?php echo lang('owners_due'); ?></th>
						<th width="90"><?php echo lang('owners_received'); ?></th>
						<th width="90"><?php echo lang('owners_amount'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($payments as $payment): ?>
						<tr>
							<td><?php echo $payment->percentage; ?>%</td>
							<td><?php echo $payment->date_due; ?></td>
							<td><?php echo $payment->date_paid; ?></td>
							<td><?php echo $payment->amount; ?> €</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="3"><?php echo lang('owners_total'); ?></th>
						<th><?php echo $total; ?> €</th>
					</tr>
				</tfoot>
			</table>
			
		</div>
		
		<div class="modal-footer">
			<a href="#" class="btn pull-right" data-dismiss="modal"><?php echo lang('owners_close'); ?></a>
		</div>
	
	</div>