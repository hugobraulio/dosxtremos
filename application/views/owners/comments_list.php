<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/owners_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">
			
			<h1><?php echo $home->name; ?></h1>
			
			<!-- Include the sub navigation -->
			<?php $this->load->view('includes/owners_subnav'); ?>

		</header>
		
		<div class="clearfix page-subheader">
			<h2 class="pull-left"><?php echo lang('owners_comments'); ?> : <?php echo $current_year; ?></h2>
			<nav class="btn-group pull-right">
				<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
				<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
			</nav>
		</div>
		
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th><?php echo lang('owners_client'); ?></th>
					<th width="100"><?php echo lang('owners_booking'); ?></th>
					<th width="150"><?php echo lang('owners_dates_of_stay'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($comments as $comment): ?>
					<tr>
						<td><a href="<?php echo $comment->view_url; ?>"><?php echo $comment->name; ?></a></td>
						<td><?php echo $comment->reference_num; ?></td>
						<td><?php echo $comment->booking_dates; ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>