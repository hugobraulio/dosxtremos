<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/owners_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">
			
			<h1><?php echo $comment->booking->home->name; ?></h1>
			
			<!-- Include the sub navigation -->
			<?php $this->load->view('includes/owners_subnav'); ?>

		</header>
		
		<div class="clearfix page-subheader">
			<h2 class="pull-left"><?php echo lang('owners_comments'); ?> : <?php echo $comment->name; ?></h2>
			<nav class="btn-group pull-right">
				<a class="btn" href="<?php echo $list_url; ?>"><i class="icon-circle-arrow-up"></i> <?php echo lang('owners_back'); ?></a>
			</nav>
		</div>
		
		<dl class="dl-horizontal">
			<dt><?php echo lang('owners_booking'); ?></dt>
			<dd><?php echo $comment->booking->reference_num; ?></dd>
			<dt><?php echo lang('owners_dates_of_stay'); ?></dt>
			<dd><?php echo $comment->booking->date_from; ?> - <?php echo $comment->booking->date_to; ?></dd>
		</dl>
		
		<blockquote>
			<p><?php echo $comment->client_comment; ?></p>
			<small><?php echo $comment->name; ?></small>
		</blockquote>
		<?php if($comment->admin_comment): ?>
			<blockquote>
				<p><?php echo $comment->admin_comment; ?></p>
				<small><?php echo lang('owners_administrator'); ?></small>
			</blockquote>
		<?php endif; ?>
	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>