<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>

</head>

<body class="print">

	<!-- PAGE CONTENT -->
	<section class="container">

		<div class="branding">
			<img class="logo" src="<?php echo base_url(); ?>assets/img/application_logo_print2.png" width="300" height="70" alt="DosXtremos" />
		</div>

		<?php echo $content; ?>

		<footer>
			<br/><br/><br/>
			<h4>Andrew Platt</h4>
			<br/><br/>
			<hgroup>
				<h3>DosXtremos</h3>
				<h5>La Coruña - Cádiz</h5>
				<p>Tel: +34 619 096 140 | Web: <a href="http://www.dosxtremos.com">www.dosxtremos.com</a></p>
			</hgroup>
			<br/><br/><br/><br/><br/><br/><br/>

		</footer>

	</section>

</body>

</html>
