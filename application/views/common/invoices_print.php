<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body class="print">

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
				
				<div class="pull-left">
					<h1><?php echo ($language_id == 'en' ? 'Invoice' : 'Factura'); ?></h1>
					<dl class="dl-horizontal">
						<dt><?php echo ($language_id == 'en' ? 'Invoice Nº' : 'Nº Factura'); ?></dt>
						<dd><?php echo $invoice->invoice_num; ?></dd>
						<dt><?php echo ($language_id == 'en' ? 'Invoice Date' : 'Fecha Factura'); ?></dt>
						<dd><?php echo $invoice->invoice_date; ?></dd>
					</dl>
					
					<strong><?php echo $invoice->owner->name; ?></strong>
					<address>
						<?php echo $invoice->owner->address; ?><br/>
						<?php echo $invoice->owner->postal_code . ' ' . $invoice->owner->city . ' (' . $invoice->owner->province . ') '; ?><br/>
						<?php echo $invoice->owner->country; ?>
					</address>
					<strong><?php echo $invoice->owner->tax_id; ?></strong>
				</div>
				
				<div class="pull-right legal">
					<img class="logo" src="<?php echo base_url(); ?>assets/img/application_logo_print.png" width="300" height="70" alt="DosXtremos" />
					<address>
						<strong>Andrew Platt</strong><br/>
						C/ Almendro 12<br/>
						11140 Conil de La Frontera (Cádiz)<br/>
						España
					</address>
					<strong>X-0972510-R</strong>
				</div>
			
			</div>

		</header>
		
		<!-- REMOVED BY CLIENT REQUEST Line Items Table
		<table class="table table-striped table-bordered table-print">
			<thead>
				<tr>
					<th><?php echo ($language_id == 'en' ? 'Description' : 'Descripción'); ?></th>
					<th><?php echo ($language_id == 'en' ? 'Net' : 'Base'); ?></th>
					<th><?php echo ($language_id == 'en' ? 'VAT' : 'IVA'); ?></th>
					<th><?php echo ($language_id == 'en' ? 'Type' : 'Tipo'); ?></th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($invoice->line_items as $line_item): ?>
					<tr>
						<td><?php echo $line_item->description; ?></td>
						<td><?php echo $line_item->net_amount; ?> €</td>
						<td><?php echo $line_item->vat_amount; ?> €</td>
						<td><?php echo $line_item->vat_percent; ?>%</td>
						<td><?php echo $line_item->total_amount; ?> €</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<td><strong><?php echo ($language_id == 'en' ? 'Totals' : 'Totales'); ?></strong></td>
					<td><?php echo $invoice->net_amount; ?> €</td>
					<td><?php echo $invoice->vat_amount; ?> €</td>
					<td>&nbsp;</td>
					<td><strong><?php echo $invoice->total_amount; ?> €</strong></td>
				</tr>
			</tfoot>
		</table>
		 -->
		
		<table class="table table-striped table-bordered table-print">
			<thead>
				<tr>
					<th><?php echo ($language_id == 'en' ? 'Description' : 'Descripción'); ?></th>
					<th width="80"><?php echo ($language_id == 'en' ? 'Net' : 'Base'); ?></th>
					<th width="80"><?php echo ($language_id == 'en' ? 'VAT' : 'IVA'); ?></th>
					<th width="80">Total</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo $invoice->invoice_description; ?></td>
					<td><?php echo $invoice->net_amount; ?> €</td>
					<td><?php echo $invoice->vat_amount; ?> €</td>
					<td><strong><?php echo $invoice->total_amount; ?> €</strong></td>
				</tr>
			</tbody>
		</table>
		
		<div class="page-header clearfix">
		
			<div class="pull-right">
				<h4><?php echo ($language_id == 'en' ? 'Total Invoice' : 'Total Factura'); ?> :</h4>
				<h2><?php echo $invoice->total_amount; ?> €</h2>
				</dl>
			</div>
		
		</div>
		
		<div class="page-header">
			
			<h4><?php echo ($language_id == 'en' ? 'Bank Transfer Data' : 'Datos para Transferecias'); ?></h4>
			
			<dl class="dl-horizontal">
				<dt><?php echo ($language_id == 'en' ? 'Bank' : 'Banco'); ?></dt>
				<dd><?php echo $system_config->bank->name; ?></dd>
				<dt><?php echo ($language_id == 'en' ? 'Account Name' : 'Titular'); ?></dt>
				<dd><?php echo $system_config->bank->account->name; ?></dd>
				<dt><?php echo ($language_id == 'en' ? 'IBAN / BIC Code' : 'Nº de Cuenta'); ?></dt>
				<dd>
					<?php if($language_id == 'en'): ?>
						<?php echo $system_config->bank->iban . ' ' . $system_config->bank->account->number . ' / ' . $system_config->bank->bic; ?>
					<?php else: ?>
						<?php echo $system_config->bank->account->number; ?>
					<?php endif; ?>
				</dd>
			</dl>
		
		</div>

	</section>

</body>

</html>