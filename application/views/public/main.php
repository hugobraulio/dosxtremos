<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body class="login public">

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<h1 class="logo">DX Gestión</h1>
		
		<form class="login_form" name="admin_login" method="post" action="<?php echo $login_url; ?>">
			
			<?php if($lang == 'es'): ?>
			
				<div class="page-header">
					<h2>Login para Clientes</h2>
					<div class="pull-right">
						<a class="btn" href="<?php echo site_url('main/index/en'); ?>">English</a>
					</div>
				</div>
			
			<?php else: ?>
			
				<div class="page-header">
					<h2>Client Login</h2>
					<div class="pull-right">
						<a class="btn" href="<?php echo site_url('main/index/es'); ?>">Español</a>
					</div>
				</div>
			
			<?php endif; ?>
			
			<?php if(isset($failed) && $failed != ''): ?>
				<div class="alert alert-error">
				
					<?php if($lang == 'es'): ?>
					
						<h3>Nombre de usuario y/o contraseña inválida</h3>
						<p>Su nombre de usuario o contraseña es incorrecta. Por favor intente de nuevo o contacte nuestro administrador en <a href="mailto:info@dosxtremos.com">info@dosxtremos.com</a></p>
						
					<?php else: ?>
					
						<h3>Invalid Username and/or Password</h3>
						<p>Your username or password is incorrect. Please try again or contact the administrator at <a href="mailto:info@dosxtremos.com">info@dosxtremos.com</a></p>
						
					<?php endif; ?>
					
				</div>
			<?php endif; ?>
			
			<fieldset>
				<label for="username"><?php echo lang('public_username'); ?></label>
				<input type="text" class="span4" name="username" id="username" />
				<label for="password"><?php echo lang('public_password'); ?></label>
				<input type="password" class="span4" name="password" id="password" />
			</fieldset>
			
			<fieldset>
				<input type="submit" name="submit" class="btn btn-primary" id="submit" value="<?php echo lang('public_enter'); ?>" />
			</fieldset>
			
			<fieldset>
				<p><br/>&copy; Copyright  <?php echo date('Y'); ?> | <a href="http://www.dosxtremos.com">DosXtremos</a></p>
			</fieldset>
		
		</form>

	</section>

</body>

</html>