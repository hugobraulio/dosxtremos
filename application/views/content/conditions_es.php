
	<header class="page-header">
		<h1>Condiciones de Reserva</h1>
	</header>

	<div>
		<p><strong><?php echo $booking_data->client_name; ?></strong><br/>
		<?php echo $booking_data->client_address; ?><br/>
		<?php echo $booking_data->client_city; ?> <?php echo ($booking_data->client_province ? ('(' . $booking_data->client_province . ')') : '') ; ?><br/>
		<?php echo $booking_data->client_postal_code; ?> <?php echo $booking_data->client_country; ?></p>
		<p><strong>Fecha:</strong> <?php echo $document_data->date_created; ?></p>
	</div>

	<?php if($booking_data->is_admin): ?>
		<p><em>Hemos bloqueado las fechas solicitadas, <?php echo $booking_data->date_from; ?> al <?php echo $booking_data->date_to; ?> inclusive, a favor de D/Dª <?php echo $booking_data->client_name; ?>, en <?php echo $booking_data->home_name; ?> (vivienda ubicada en <?php echo $booking_data->home_address; ?>, <?php echo $booking_data->home_city; ?>, <?php echo $booking_data->home_province; ?>, <?php echo $booking_data->home_postal_code; ?> España). Esta reserva será mantenida durante cinco días hábiles en espera de señal/reserva del <?php echo $booking_data->first_payment_percent; ?>% del precio de la renta acordada en concepto de señal/reserva (ver detalles de pago a continuación). El pago de <span style="text-decoration: underline">esta señal/reserva constituye la aceptación de las condiciones de la reserva detalladas abajo</span>. Una vez recibido el pago de la seña/reserva le enviaremos el justificante de pago confirmatorio de la reserva realizada a su favor en las condiciones pactadas. Con esta confirmación entra en vigor su contrato con el propietario. Los términos de este contrato se basan en las siguientes condiciones:</em></p>
	<?php else: ?>
		<p><em>Hemos bloqueado las fechas solicitadas, <?php echo $booking_data->date_from; ?> al <?php echo $booking_data->date_to; ?> inclusive, a favor de D/Dª <?php echo $booking_data->client_name; ?>, en <?php echo $booking_data->home_name; ?> (vivienda ubicada en <?php echo $booking_data->home_address; ?>, <?php echo $booking_data->home_city; ?>, <?php echo $booking_data->home_province; ?>, <?php echo $booking_data->home_postal_code; ?> España).Esta reserva será mantenida durante cinco días hábiles en espera de señal/reserva del <?php echo $booking_data->first_payment_percent; ?>% del precio de la renta acordada en concepto de señal/reserva (ver detalles de pago a continuación). El pago de <span style="text-decoration: underline">esta señal/reserva constituye la aceptación de las condiciones de la reserva detalladas abajo</span>. Una vez recibido el pago de la seña/reserva le enviaremos el justificante de pago confirmatorio de la reserva realizada a su favor en las condiciones pactadas. Con esta confirmación entra en vigor su contrato con el propietario. Los términos de este contrato se basan en las siguientes condiciones:</em></p>
	<?php endif; ?>

	<!-- SPECIFIC CONDITIONS -->

	<header class="page-header">
		<h2>Condiciones Específicas</h2>
	</header>

	<h3>Renta</h3>
	<p>€<?php echo $booking_data->agreed_price; ?> por el período más arriba indicado</p>

	<h3>Pago</h3>
	<p><?php echo $booking_data->first_payment_percent; ?>% (€<?php echo $booking_data->first_payment_amount; ?>) de la renta acordada (€<?php echo $booking_data->agreed_price; ?>) deberá ser ingresada
	<?php if ($booking_data->payment_type_id==3){ // default value, no payment type selected?>
		bien mediante transferencia en la cuenta bancaria <?php echo ($booking_data->bank_nationality == 'es' ? 'espa&ntilde;ola' : 'inglesa'); ?> detallada a continuación o bien mediante pago directo de la reserva con tarjeta de crédito.
	<?php } elseif ($booking_data->payment_type_id==1) { //Bank Transfer ?>
		mediante transferencia en la cuenta bancaria <?php echo ($booking_data->bank_nationality == 'es' ? 'espa&ntilde;ola' : 'inglesa'); ?> detallada a continuación:
	<?php } elseif ($booking_data->payment_type_id==2) { //Credit Card ?>
		mediante pago directo de la reserva con tarjeta de crédito.
	<?php } ?>
	</p>

	<?php 
	if ($booking_data->payment_type_id <=1) { //Bank Transfer or no payment type defined
	?>
	<p>
		<strong>Banco:</strong> <?php echo $booking_data->bank_name; ?><br/>
		<strong>Titular:</strong> <?php echo $booking_data->bank_account_name; ?><br/>
		<strong>IBAN / BIC Code:</strong> <?php echo $booking_data->bank_iban . ' ' . $booking_data->bank_account_number . ' / ' . $booking_data->bank_bic; ?>
	</p>
	<? } ?>
	<p>El pago de esta cantidad supone la confirmación de la reseva en las condiciones establecidas a continuación durante la fecha arriba señalada.</p>

	<p>El <?php echo $booking_data->second_payment_percent; ?>% restante (€<?php echo $booking_data->second_payment_amount; ?>) deberá ser
	<?php if ($booking_data->payment_type_id==3){ // default value, no payment type selected?>
		ingresado en la misma cuenta bancaria al menos <?php echo $system_config->final_payment_months; ?> meses antes del inicio del periodo acordado o abonado mediante tarjeta de crédito. 
	<?php } elseif ($booking_data->payment_type_id==1) { //Bank Transfer ?>
		ingresado en la misma cuenta bancaria al menos <?php echo $system_config->final_payment_months; ?> meses antes del inicio del periodo acordado. 
	<?php } elseif ($booking_data->payment_type_id==2) { //Credit Card ?>
		abonado mediante tarjeta de crédito al menos <?php echo $system_config->final_payment_months; ?> meses antes del inicio del periodo acordado. 
	<?php } ?>
	En el caso de que no se cumpla con la fecha acordada para el segundo pago se considerará la reserva cancelada y consecuentemente la vivienda libre y a entera disposición de Dosxtremos.
	</p>

	<h3>Condiciones de Cancelación</h3>
	<p>Si se cancela con más de 4 meses de antelación a la fecha de llegada procede la devolución del 100%. Si se cancela entre 2 y 4 meses de antelación a la fecha de llegada procede la devolución del 50% de las cantidades pagadas. Si se cancela con menos de 2 meses de antelación a la fecha de llegada no procede devolución alguna.</p>

	<p>En el caso de que no se cumpla con la fecha acordada para el segundo pago se considerará la reserva cancelada y consecuentemente la vivienda libre y a entera disposición de Dosxtremos</p>

	<!-- PREVIOUS DYNAMIC CANCELATION CLAUSE
	<h3>Gastos de Cancelación</h3>
	<p>Si se cancela con más de <?php echo $system_config->cancellation_months; ?> meses de antelación a la fecha de llegada procede la devolución del <?php echo $system_config->cancellation_percent; ?>% del importe pagado. Si se cancela con menos de <?php echo $system_config->cancellation_months; ?> meses de antelación a la fecha de llegada no procede devolución alguna.</p> -->

	<h3>Servicios Incluidos</h3>
	<p>Electricidad, agua, gas, e internet están incluidos en el precio acordado. Se ofrecen toallas de playa, se ruega no llevar las toallas de baño a la playa.</p>

	<h3>Depósito de Garantía</h3>
	<p>Pedimos un depósito de €<?php echo $booking_data->down_payment; ?>. Este debe ser abonado mediante transferencia.</p>
	<p>La fianza menos las deducciones pertinentes por roturas, desperfectos o suciedad excesiva, se devolverá dentro del plazo de 3 días desde la fecha de partida.</p>

	<h3>Hora de Llegada y Partida</h3>
	<p>Para poder limpiar la casa correctamente les pedimos que no lleguen antes de las <?php echo $system_config->time_arrival; ?> y que abandonen la casa a las <?php echo $system_config->time_departure; ?>.</p>

	<!-- GENERAL CONDITIONS -->

	<header class="page-header">
		<h2>Otras Condiciones</h2>
	</header>

	<?php if($booking_data->is_admin): ?>
		<ol>
			<li>Esta relación contractual se configura como vivienda turística de conformidad con lo dispuesto en la Ley de Viviendas Turísticas de Andalucia, Decreto 28/2016 de 2 de Febrero.</li>
			<li>A la entrega de la vivienda los usuarios de la vivienda deberán exhibir su documento de identidad, en cumplicmiento del Decreto 28/2016 y firmar una copia del presente documento. La vivienda objeto de este contrato (<?php echo $booking_data->home_name; ?>) está ubicada en <?php echo $booking_data->home_address; ?>, <?php echo $booking_data->home_city; ?>, <?php echo $booking_data->home_province; ?>, <?php echo $booking_data->home_postal_code; ?> España. La persona usuaria recibirá todo lo acordado en perfecto estado para el uso a que se destina y en igual estado deberá devolverlo a la finalización del tiempo de vigencia del contrato.</li>
			<li>El retraso en el desalojo de la vivienda por parte de la persona usuaria dará lugar a que se devengue a favor de la propiedad, por cada día o fracción de retraso una penalización del doble de la renta diaria pactada. Dicha penalización tiene el carácter de condición esencial del contrato</li>
			<li>La vivienda sólo puede ser utilizada por un máximo de <?php echo $booking_data->num_occupants; ?> personas (sin acuerdo previo) y no se la utilizará para organizar fiestas u otras actividades que le puedan causar algún tipo de daño. En el caso de exceso de ocupación, se producirá una penalización consistente en un incremento en la renta proporcional al número de personas que excedan del de personas autorizadas. Se exigirá, en todo caso, el inmediato desalojo de las personas de más no autorizadas.</li>
			<li>No se admiten animales domésticos y queda expresamente prohibida cualquier forma de camping (tiendas de campaña, caravanas etc.) en la finca de la vivienda. La persona usuaria también deberá respetar y cumplir en todo momento las normas de buena vecindad.</li>
			<li>La persona usuaria deberá poner en conocimiento del agente, en el plazo más breve posible, cualquier reparación que vea como necesaria. Y aunque se pondrá todo el empeño razonable en llevar a cabo reparaciones rápidas, no se ofrece garantía alguna ni se procederá a la devolución de dinero alguno.</li>
			<li>Cualquier reserva que se haga facilitando una identidad u otros datos que no se correspondan a la realidad estará sujeta a la perdida de cualquier pago que se haya realizado, y no se permitirá la entrada en la propiedad.</li>
			<li>Si por algún motivo la propiedad no está disponible o no es adecuada como vivienda turística en la fecha reservada, todos los depósitos y rentas pagadas por la persona usuaria serán reembolsados en su totalidad, pero no dará lugar a mayor reclamación contra el agente o propietario. No se acepta responsabilidad alguna por cualquier pérdida, daño, enfermedad o lesión causada que pueda ocurrir durante las vacaciones a cualquier miembro del grupo de la persona usuaria o a sus posesiones.</li>
			<li>Ambas partes contratantes (la persona usuaria y el propietario) renuncian a cualquier fuero que pudieran ostentar y se someten expresamente, para dilucidar cualquier cuestión derivada de la aplicación y ejecución del presente contrato a los Juzgados y Tribunales de Chiclana de Frontera.</li>
		</ol>

	<?php else: ?>
		<ol>
			<li>Esta relación contractual se configura como vivienda turística de conformidad con lo dispuesto en la Ley de Viviendas Turísticas de Andalucia, Decreto 28/2016 de 2 de Febrero.</li>
			<li>A la entrega de la vivienda los usuarios de la vivienda deberán exhibir su documento de identidad, en cumplicmiento del Decreto 28/2016 y firmar una copia del presente documento. La vivienda objeto de este contrato (<?php echo $booking_data->home_name; ?>) está ubicada en <?php echo $booking_data->home_address; ?>, <?php echo $booking_data->home_city; ?>, <?php echo $booking_data->home_province; ?>, <?php echo $booking_data->home_postal_code; ?> España. La persona usuaria recibirá todo lo acordado en perfecto estado para el uso a que se destina y en igual estado deberá devolverlo a la finalización del tiempo de vigencia del contrato.</li>
			<li>El retraso en el desalojo de la vivienda por parte de la persona usuaria dará lugar a que se devengue a favor de la propiedad, por cada día o fracción de retraso una penalización del doble de la renta diaria pactada. Dicha penalización tiene el carácter de condición esencial del contrato</li>
			<li>La vivienda sólo puede ser utilizada por un máximo de <?php echo $booking_data->num_occupants; ?> personas (sin acuerdo previo) y no se la utilizará para organizar fiestas u otras actividades que le puedan causar algún tipo de daño. En el caso de exceso de ocupación, se producirá una penalización consistente en un incremento en la renta proporcional al número de personas que excedan del de personas autorizadas. Se exigirá, en todo caso, el inmediato desalojo de las personas de más no autorizadas.</li>
			<li>No se admiten animales domésticos y queda expresamente prohibida cualquier forma de camping (tiendas de campaña, caravanas etc.) en la finca de la vivienda. La persona usuaria también deberá respetar y cumplir en todo momento las normas de buena vecindad.</li>
			<li>La persona usuaria deberá poner en conocimiento del agente, en el plazo más breve posible, cualquier reparación que vea como necesaria. Y aunque se pondrá todo el empeño razonable en llevar a cabo reparaciones rápidas, no se ofrece garantía alguna ni se procederá a la devolución de dinero alguno.</li>
			<li>Cualquier reserva que se haga facilitando una identidad u otros datos que no se correspondan a la realidad estará sujeta a la perdida de cualquier pago que se haya realizado, y no se permitirá la entrada en la propiedad.</li>
			<li>Si por algún motivo la propiedad no está disponible o no es adecuada como vivienda turística en la fecha reservada, todos los depósitos y rentas pagadas por la persona usuaria serán reembolsados en su totalidad, pero no dará lugar a mayor reclamación contra el agente o propietario. No se acepta responsabilidad alguna por cualquier pérdida, daño, enfermedad o lesión causada que pueda ocurrir durante las vacaciones a cualquier miembro del grupo de la persona usuaria o a sus posesiones.</li>
			<li>Ambas partes contratantes (la persona usuaria y el propietario) renuncian a cualquier fuero que pudieran ostentar y se someten expresamente, para dilucidar cualquier cuestión derivada de la aplicación y ejecución del presente contrato a los Juzgados y Tribunales de Chiclana de Frontera.</li>
		</ol>
	<?php endif; ?>

	<p>Detalles sobre la recogida de las llaves e instrucciones para llegar a la casa serán enviadas cerca de la fecha de llegada, así como información sobre la zona y sus alrededores. Si mientras tanto, quiere alguna otra información sobre la casa o la zona así como actividades disponibles, por favor no dude en preguntarnos.</p>

	<p>Gracias por reservar <?php echo $booking_data->home_name; ?>. Haremos todo  lo posible para que tenga una estancia agradable.</p>

	<!-- LEGAL DISCLAIMER -->

	<header class="page-header">
		<h2>Aviso Legal</h2>
	</header>

	<?php if($booking_data->is_admin): ?>
		<small>
			<p>Dos Xtremos actúa como agente de <?php echo $booking_data->owner_name; ?> (el "Propietario"), y ha sido confiado por el Propietario para la gestión de su propiedad, <?php echo $booking_data->home_name; ?>, situada en <?php echo $booking_data->home_address; ?>, <?php echo $booking_data->home_city; ?>, <?php echo $booking_data->home_province; ?>, <?php echo $booking_data->home_postal_code; ?> España. El Agente es responsable de encontrar clientes para <?php echo $booking_data->home_name; ?>, pero las recomendaciones y opiniones dadas por el Agente tan solo son una referencia y aunque las descripciones han sido hechas de buena fe son subjetivas y el Agente no acepta responsabilidad alguna por ellas. Su acuerdo contractual para <?php echo $booking_data->home_name; ?> es con el Propietario y se basa en estas Condiciones de Reserva. Aunque toda la correspondencia, comunicación y pagos de la reserva y renta se realizan a través del Agente, el Agente no acepta responsabilidad alguna por los acuerdos directos con el Propietario.</p>

			<p>Las agencias, operadores de actividades o guías recomendadas en la página web de Dosxtremos C.B. (de ahora en adelante "la página web") y en toda la correspondencia entre el Agente y  la persona usuaria, y en cualquier escrito publicado por Dosxtremos C.B., actúa hasta donde tenemos conocimiento, profesional y responsablemente. Sin embargo, Dosxtremos C.B. no tiene control alguno sobre el contenido de terceras partes o sobre la gestión de las actividades recomendadas en la correspondencia entre Agente y persona usuaria, en la página web o en cualquier escrito publicado por Dosxtremos C.B. y no hacemos garantía alguna en relación a los servicios ofrecidos por cualquiera de las agencias, operadores de actividades  o guías aquí mencionados. No nos hacemos responsables de ninguna pérdida o daño que puede ocurrir por la utilización de cualquiera de las agencias, operadores de actividades o guías mencionados en la correspondencia entre Agente y persona usuaria, página web o escrito publicado por Dosxtremos C.B. o por llevar a cabo cualquiera de las actividades recomendadas. Hemos hecho todo el esfuerzo posible para asegurarnos de la exactitud de la información que se ofrece, pero no podemos aceptar responsabilidad alguna por los errores que en ella pueda haber.</p>

			<p>Dosxtremos C.B tan solo es responsable de la reserva de su vivienda turísitca y no venden un paquete. No opera como intermediarios o reciben comisión alguna de ninguno de los operadores, agencias o guías que mencionan en la página web, correspondencia entre el Agente y la persona usuaria o cualquier escrito publicado por Dosxtremos C.B.. Es su responsabilidad organizar seguro de viaje y transporte y asegurarse de que su pasaporte está en regla. Le recomendamos que contrate seguro de viaje para sus vacaciones.</p>
		</small>
	<?php else: ?>
		<small>
			<p>Dos Xtremos actúa como agente de <?php echo $booking_data->owner_name; ?> (el "Propietario"), y ha sido confiado por el Propietario para la gestión de su propiedad, <?php echo $booking_data->home_name; ?>, situada en <?php echo $booking_data->home_address; ?>, <?php echo $booking_data->home_city; ?>, <?php echo $booking_data->home_province; ?>, <?php echo $booking_data->home_postal_code; ?> España. El Agente es responsable de encontrar clientes para <?php echo $booking_data->home_name; ?>, pero las recomendaciones y opiniones dadas por el Agente tan solo son una referencia y aunque las descripciones han sido hechas de buena fe son subjetivas y el Agente no acepta responsabilidad alguna por ellas. Su acuerdo contractual para <?php echo $booking_data->home_name; ?> es con el Propietario y se basa en estas Condiciones de Reserva. Aunque toda la correspondencia y comunicación se realiza a través del Agente, el Agente no acepta responsabilidad alguna por los acuerdos directos con el Propietario.</p>

			<p>Las agencias, operadores de actividades o guías recomendadas en la página web de Dosxtremos C.B. (de ahora en adelante "la página web") y en toda la correspondencia entre el Agente y  la persona usuaria, y en cualquier escrito publicado por Dosxtremos C.B., actúa hasta donde tenemos conocimiento, profesional y responsablemente. Sin embargo, Dosxtremos C.B. no tiene control alguno sobre el contenido de terceras partes o sobre la gestión de las actividades recomendadas en la correspondencia entre Agente y persona usuaria, en la página web o en cualquier escrito publicado por Dosxtremos C.B. y no hacemos garantía alguna en relación a los servicios ofrecidos por cualquiera de las agencias, operadores de actividades  o guías aquí mencionados. No nos hacemos responsables de ninguna pérdida o daño que puede ocurrir por la utilización de cualquiera de las agencias, operadores de actividades o guías mencionados en la correspondencia entre Agente y persona usuaria, página web o escrito publicado por Dosxtremos C.B. o por llevar a cabo cualquiera de las actividades recomendadas. Hemos hecho todo el esfuerzo posible para asegurarnos de la exactitud de la información que se ofrece, pero no podemos aceptar responsabilidad alguna por los errores que en ella pueda haber.</p>

			<p>Dosxtremos C.B tan solo es responsable de la reserva de su vivienda turísitca y no venden un paquete. No opera como intermediarios o reciben comisión alguna de ninguno de los operadores, agencias o guías que mencionan en la página web, correspondencia entre el Agente y la persona usuaria o cualquier escrito publicado por Dosxtremos C.B.. Es su responsabilidad organizar seguro de viaje y transporte y asegurarse de que su pasaporte está en regla. Le recomendamos que contrate seguro de viaje para sus vacaciones.</p>

		</small>
	<?php endif; ?>
