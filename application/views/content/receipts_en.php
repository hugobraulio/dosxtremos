
	<header class="page-header">
		<h1><?php switch($payment_data->type) {
				case 'first':
					echo 'First Receipt';
					break;
				case 'last':
					echo 'Final Receipt';
					break;
				default:
					echo 'Receipt';
					break;
			} ?></h1>
	</header>

	<div>
		<p>
		Booking no: <strong><?php echo $payment_data->reference_num;?></strong><br/><br/>
		<strong><?php echo $payment_data->client_name; ?></strong><br/>
		<?php echo $payment_data->client_address; ?><br/>
		<?php echo $payment_data->client_city; ?> (<?php echo $payment_data->client_province; ?>)<br/>
		<?php echo $payment_data->client_postal_code; ?> <?php echo $payment_data->client_country; ?></p>
		<p><strong>Date:</strong> <?php echo $document_data->date_created; ?></p>
	</div>

	<!-- PREAMBLE -->

	<?php
	$leaving_date = date("d/m/Y",strtotime(strtr($payment_data->date_to, '/', '-'). "+ 1 day"));
	if($payment_data->is_admin): ?>
		<p>Andrew Anthony Platt (Spanish ID Nº X-0972510-R), Owner of "<?php echo $payment_data->home_name; ?>" located at <?php echo $payment_data->home_address; ?>, <?php echo $payment_data->home_city; ?>, <?php echo $payment_data->home_province; ?>, <?php echo $payment_data->home_postal_code; ?> Spain, has received the sum of €<?php echo $payment_data->amount; ?> from <?php echo $payment_data->client_name; ?> as <?php echo $payment_data->percentage; ?>% of the total agreed rent (€<?php echo $payment_data->agreed_price; ?>) for "<?php echo $payment_data->home_name; ?>" for the period, <?php echo $payment_data->date_from; ?> to <?php echo $leaving_date; ?>.</p>
	<?php else: ?>
		<p>Dosxtremos C.B. (Spanish VAT number - CIF E72311798) acting as Agent for <?php echo $payment_data->owner_name; ?>, Owner of the house located at <?php echo $payment_data->home_address; ?>, <?php echo $payment_data->home_city; ?>, <?php echo $payment_data->home_province; ?>, <?php echo $payment_data->home_postal_code; ?> Spain, ("<?php echo $payment_data->home_name; ?>"), confirms that it has received the amount of €<?php echo $payment_data->amount; ?> from <?php echo $payment_data->client_name; ?> as deposit/reservation of <?php echo $payment_data->percentage; ?>% of the total agreed rent (€<?php echo $payment_data->agreed_price; ?>) for "<?php echo $payment_data->home_name; ?>" for the period, <?php echo $payment_data->date_from; ?> to <?php echo $leaving_date; ?>.</p>
	<?php endif; ?>

	<!-- RECEIPT TYPE TEXTS (First, Last or Single Payment) -->

	<?php switch($payment_data->type): case 'first': ?>
			<!-- First Payment (Admin is Owner)-->
			<p>This receipt acts as confirmation of your booking, and with it your contractual agreement <?php echo ($payment_data->is_admin ? 'with me, the Owner' : 'with the Owner'); ?>, terms of which are based on the Booking Conditions sent to you, comes into force.</p>
			<?php break; ?>
		<?php case 'last': ?>
			<!-- Last Payment -->
			<p>This final receipt follows your first receipt <?php echo ($payment_data->previous_receipt->date_created ? ('(dated ' . $payment_data->previous_receipt->date_created . ')') : ''); ?>, which acted as confirmation of your booking and the commencement of your contractual agreement <?php echo ($payment_data->is_admin ? 'with me, the Owner' : 'with the Owner'); ?>, terms of which are based on the Booking Conditions sent to you.</p>
			<?php break; ?>
		<?php case 'single': ?>
			<!-- Single Payment -->
			<p>This receipt acts as confirmation of your booking and the commencement of your contractual agreement <?php echo ($payment_data->is_admin ? 'with me, the Owner' : 'with the Owner'); ?>, terms of which are based on the Booking Conditions sent to you.</p>
			<?php break; ?>
	<?php endswitch;?>

	<!-- LEGAL DISCLAIMER -->

	<header class="page-header">
		<h2>Legal Disclaimer</h2>
	</header>

	<?php if($payment_data->is_admin): ?>
		<small>
			<p>Dos Xtremos acts as Agent for <?php echo $payment_data->owner_name; ?> (the "Owner"), and has been entrusted by the Owner with the management of their property, <?php echo $payment_data->home_name; ?>, situated in <?php echo $payment_data->home_address; ?>, <?php echo $payment_data->home_city; ?>, <?php echo $payment_data->home_province; ?>, <?php echo $payment_data->home_postal_code; ?> Spain. The Agent is responsible for finding users for <?php echo $payment_data->home_name; ?>, but the recommendations and opinions given by the Agent are for guidance only, and while descriptions have been written in good faith they are subjective and the Agent accepts no liability for these. Your contractual agreement on <?php echo $payment_data->home_name; ?> is with the Owner and is based on the Booking Conditions sent to you. Despite the fact that all correspondence, communication and payments towards the rent/reservation are managed by the Agent, the Agent accepts no responsibility for your direct agreements with the Owner.</p>

			<p>The agencies, activity operators, and guides recommended on the Dosxtremos website (hereinafter "the website"), in all correspondence between the Agent and the user, and in any literature published by Dosxtremos C.B. are to the best of our knowledge run professionally and responsibly. However, Dosxtremos C.B. has no control over third party content or over the management of any of the activities recommended on the website, in any of the correspondence between the Agent and the user, or in any of the literature published by Dosxtremos C.B., and we make no warranty whatsoever in relation to any of the services being provided by any of the agencies, guides, or activity operators mentioned. We take no responsibility for any loss or injury that may occur to you or to any members of your party if you choose to use any of the agencies, guides, or activity operators recommended on the website, in any of the correspondence between the Agent and the user, or in any of the literature published by Dosxtremos C.B., or if you choose to practice any of the activities that we suggest. We have made every effort to ensure the accuracy of the information we provide, but we can accept no responsibility for any errors it may contain.</p>

			<p>Dosxtremos C.B. is only responsible for organising the reservation of you holiday property and it is not selling a package. Dosxtremos C.B. does not operate as intermediary for or receive any commissions from any of the agencies, guides, or activity operators mentioned on the website, in the correspondence between the Agent and the user, or in any literature published by Dosxtremos C.B. It is your responsibility to organise travel insurance and transport and to make sure that your passports are valid. We recommend that you take out travel insurance.</p>
		</small>
	<?php else: ?>
		<small>
			<p>Dos Xtremos acts as Agent for <?php echo $payment_data->owner_name; ?> (the "Owner"), and has been entrusted by the Owner with the management of their property, <?php echo $payment_data->home_name; ?>, situated in <?php echo $payment_data->home_address; ?>, <?php echo $payment_data->home_city; ?>, <?php echo $payment_data->home_province; ?>, <?php echo $payment_data->home_postal_code; ?> Spain. The Agent is responsible for finding users for <?php echo $payment_data->home_name; ?>, but the recommendations and opinions given by the Agent are for guidance only, and while descriptions have been written in good faith they are subjective and the Agent accepts no liability for these. Your contractual agreement on <?php echo $payment_data->home_name; ?> is with the Owner and is based on the Booking Conditions sent to you. Despite the fact that all correspondence, communication and payments towards the rent/reservation are managed by the Agent, the Agent accepts no responsibility for your direct agreements with the Owner.</p>

			<p>The agencies, activity operators, and guides recommended on the Dosxtremos website (hereinafter "the website"), in all correspondence between the Agent and the user, and in any literature published by Dosxtremos C.B. are to the best of our knowledge run professionally and responsibly. However, Dosxtremos C.B. has no control over third party content or over the management of any of the activities recommended on the website, in any of the correspondence between the Agent and the user, or in any of the literature published by Dosxtremos C.B., and we make no warranty whatsoever in relation to any of the services being provided by any of the agencies, guides, or activity operators mentioned. We take no responsibility for any loss or injury that may occur to you or to any members of your party if you choose to use any of the agencies, guides, or activity operators recommended on the website, in any of the correspondence between the Agent and the user, or in any of the literature published by Dosxtremos C.B., or if you choose to practice any of the activities that we suggest. We have made every effort to ensure the accuracy of the information we provide, but we can accept no responsibility for any errors it may contain.</p>

			<p>Dosxtremos C.B. is only responsible for organising the reservation of you holiday property and it is not selling a package. Dosxtremos C.B. does not operate as intermediary for or receive any commissions from any of the agencies, guides, or activity operators mentioned on the website, in the correspondence between the Agent and the user, or in any literature published by Dosxtremos C.B. It is your responsibility to organise travel insurance and transport and to make sure that your passports are valid. We recommend that you take out travel insurance.</p>
		</small>
	<?php endif; ?>
