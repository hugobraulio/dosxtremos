
	<header class="page-header">
		<h1><?php switch($payment_data->type) {
				case 'first':
					echo 'Primer Recibo';
					break;
				case 'last':
					echo 'Recibo Final';
					break;
				default:
					echo 'Recibo';
					break;
			} ?></h1>
	</header>

	<div>
		<p>
		Nº de reserva: <strong><?php echo $payment_data->reference_num;?></strong><br/><br/>
		<strong><?php echo $payment_data->client_name; ?></strong><br/><br/>
		<?php echo $payment_data->client_address; ?><br/>
		<?php echo $payment_data->client_city; ?> (<?php echo $payment_data->client_province; ?>)<br/>
		<?php echo $payment_data->client_postal_code; ?> <?php echo $payment_data->client_country; ?></p>
		<p><strong>Fecha:</strong> <?php echo $document_data->date_created; ?></p>
	</div>

	<!-- PREAMBLE -->

	<?php
	$leaving_date = date("d/m/Y",strtotime(strtr($payment_data->date_to, '/', '-'). "+ 1 day"));
	if($payment_data->is_admin): ?>
		<p>Andrew Anthony Platt, con DNI X-0972510-R, Propietario de "<?php echo $payment_data->home_name; ?>"vivienda sita en <?php echo $payment_data->home_address; ?>, <?php echo $payment_data->home_city; ?>,<?php echo $payment_data->home_province; ?>, <?php echo $payment_data->home_postal_code; ?> España,confirma que ha recibido la cantidad de €<?php echo $payment_data->amount; ?> de <?php echo $payment_data->client_name; ?>en concepto del <?php echo $payment_data->percentage; ?>% de la renta total acordada (€<?php echo $payment_data->agreed_price; ?>)para "<?php echo $payment_data->home_name; ?>" para el periodo del <?php echo $payment_data->date_from; ?> al <?php echo $leaving_date;?>.</p>
	<?php else: ?>
		<p>Dosxtremos CB, con CIF E72311798, actuando como mandatario de <?php echo $payment_data->owner_name; ?>, Propietario de la vivienda sita en <?php echo $payment_data->home_address; ?>, <?php echo $payment_data->home_city; ?>,<?php echo $payment_data->home_province; ?>, <?php echo $payment_data->home_postal_code; ?> España (<?php echo $payment_data->home_name; ?>), confirma que ha recibido la cantidad de €<?php echo $payment_data->amount; ?> de <?php echo $payment_data->client_name; ?> en concepto de señal/reserva del <?php echo $payment_data->percentage; ?>% de la renta total acordada (€<?php echo $payment_data->agreed_price; ?>) para "<?php echo $payment_data->home_name; ?>" para el periodo del <?php echo $payment_data->date_from; ?> al <?php echo $leaving_date; ?>.</p>
	<?php endif; ?>

	<!-- RECEIPT TYPE TEXTS (First, Last or Single Payment) -->

	<?php switch($payment_data->type): case 'first': ?>
			<!-- First Payment -->
			<p>Este recibo actúa como confirmación de su reserva, y con él entra en vigor su contrato con el Propietario, cuyos términos se basan en las Condiciones de Reserva que le hemos enviado.</p>
			<?php break; ?>
		<?php case 'last': ?>
			<!-- Last Payment -->
			<p>Este recibo final se emite a continuación de su primer recibo <?php echo ($payment_data->previous_receipt->date_created ? ('(con fecha de ' . $payment_data->previous_receipt->date_created . ')') : ''); ?>, que constituyó la confirmación de su reserva y con ello la entrada en vigor de su contrato con el Propietario, cuyos términos se basan en las Condiciones de Reserva que le hemos enviado.</p>
			<?php break; ?>
		<?php case 'single': ?>
			<!-- Single Payment -->
			<p>Este recibo actúa como confirmación de su reserva, y con él entra en vigor su contrato con el Propietario, cuyos términos se basan en las Condiciones de Reserva que le hemos enviado.</p>
			<?php break; ?>
	<?php endswitch;?>

	<!-- LEGAL DISCLAIMER -->

	<header class="page-header">
		<h2>Aviso Legal</h2>
	</header>

	<?php if($payment_data->is_admin): ?>
		<small>
			<p>Dosxtremos C.B. actúa como agente de <?php echo $payment_data->owner_name; ?> (el "Propietario"), y ha sido confiado por el Propietario para la gestión de su propiedad, <?php echo $payment_data->home_name; ?>, situada en <?php echo $payment_data->home_address; ?>, <?php echo $payment_data->home_city; ?>, <?php echo $payment_data->home_province; ?>, <?php echo $payment_data->home_postal_code; ?> España. El Agente es responsable de encontrar clientes para <?php echo $payment_data->home_name; ?>, pero las recomendaciones y opiniones dadas por el Agente tan solo son una referencia y aunque las descripciones han sido hechas de buena fe son subjetivas y el Agente no acepta responsabilidad alguna por ellas. Su acuerdo contractual para <?php echo $payment_data->home_name; ?> es con el Propietario y se basa en estas Condiciones de Reserva. Aunque toda la correspondencia, comunicación y pagos de la reserva y renta se realiza a través del Agente, el Agente no acepta responsabilidad alguna por los acuerdos directos con el Propietario.</p>

			<p>Las agencias, operadores de actividades o guías recomendadas en la página web de Dosxtremos C.B. (de ahora en adelante "la página web") y en toda la correspondencia entre el Agente y  la persona usuaria, y en cualquier escrito publicado por Dosxtremos C.B., actúa hasta donde tenemos conocimiento, profesional y responsablemente. Sin embargo, Dosxtremos C.B. no tiene control alguno sobre el contenido de terceras partes o sobre la gestión de las actividades recomendadas en la correspondencia entre Agente y persona usuaria, en la página web o en cualquier escrito publicado por Dosxtremos C.B. y no hacemos garantía alguna en relación a los servicios ofrecidos por cualquiera de las agencias, operadores de actividades  o guías aquí mencionados. No nos hacemos responsables de ninguna pérdida o daño que puede ocurrir por la utilización de cualquiera de las agencias, operadores de actividades o guías mencionados en la correspondencia entre Agente y persona usuaria, página web o escrito publicado por Dosxtremos C.B. o por llevar a cabo cualquiera de las actividades recomendadas. Hemos hecho todo el esfuerzo posible para asegurarnos de la exactitud de la información que se ofrece, pero no podemos aceptar responsabilidad alguna por los errores que en ella pueda haber.</p>

			<p>Dosxtremos C.B tan solo es responsable de la reserva de su vivienda turísitca y no venden un paquete. No opera como intermediarios o reciben comisión alguna de ninguno de los operadores, agencias o guías que mencionan en la página web, correspondencia entre el Agente y la persona usuaria o cualquier escrito publicado por Dosxtremos C.B.. Es su responsabilidad organizar seguro de viaje y transporte y asegurarse de que su pasaporte está en regla. Le recomendamos que contrate seguro de viaje para sus vacaciones.</p>
		</small>
	<?php else: ?>
		<small>
			<p>Dosxtremos C.B. actúa como agente de <?php echo $payment_data->owner_name; ?> (el "Propietario"), y ha sido confiado por el Propietario para la gestión de su propiedad, <?php echo $payment_data->home_name; ?>, situada en <?php echo $payment_data->home_address; ?>, <?php echo $payment_data->home_city; ?>, <?php echo $payment_data->home_province; ?>, <?php echo $payment_data->home_postal_code; ?> España. El Agente es responsable de encontrar clientes para <?php echo $payment_data->home_name; ?>, pero las recomendaciones y opiniones dadas por el Agente tan solo son una referencia y aunque las descripciones han sido hechas de buena fe son subjetivas y el Agente no acepta responsabilidad alguna por ellas. Su acuerdo contractual para <?php echo $payment_data->home_name; ?> es con el Propietario y se basa en estas Condiciones de Reserva. Aunque toda la correspondencia, comunicación y pagos de la reserva y renta se realiza a través del Agente, el Agente no acepta responsabilidad alguna por los acuerdos directos con el Propietario.</p>

			<p>Las agencias, operadores de actividades o guías recomendadas en la página web de Dosxtremos C.B. (de ahora en adelante "la página web") y en toda la correspondencia entre el Agente y  la persona usuaria, y en cualquier escrito publicado por Dosxtremos C.B., actúa hasta donde tenemos conocimiento, profesional y responsablemente. Sin embargo, Dosxtremos C.B. no tiene control alguno sobre el contenido de terceras partes o sobre la gestión de las actividades recomendadas en la correspondencia entre Agente y persona usuaria, en la página web o en cualquier escrito publicado por Dosxtremos C.B. y no hacemos garantía alguna en relación a los servicios ofrecidos por cualquiera de las agencias, operadores de actividades  o guías aquí mencionados. No nos hacemos responsables de ninguna pérdida o daño que puede ocurrir por la utilización de cualquiera de las agencias, operadores de actividades o guías mencionados en la correspondencia entre Agente y persona usuaria, página web o escrito publicado por Dosxtremos C.B. o por llevar a cabo cualquiera de las actividades recomendadas. Hemos hecho todo el esfuerzo posible para asegurarnos de la exactitud de la información que se ofrece, pero no podemos aceptar responsabilidad alguna por los errores que en ella pueda haber.</p>

			<p>Dosxtremos C.B tan solo es responsable de la reserva de su vivienda turísitca y no venden un paquete. No opera como intermediarios o reciben comisión alguna de ninguno de los operadores, agencias o guías que mencionan en la página web, correspondencia entre el Agente y la persona usuaria o cualquier escrito publicado por Dosxtremos C.B.. Es su responsabilidad organizar seguro de viaje y transporte y asegurarse de que su pasaporte está en regla. Le recomendamos que contrate seguro de viaje para sus vacaciones.</p>
		</small>
	<?php endif; ?>
