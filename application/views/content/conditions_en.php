
	<header class="page-header">
		<h1>Booking Conditions</h1>
	</header>

	<div>
		<p><strong><?php echo $booking_data->client_name; ?></strong><br/>
		<?php echo $booking_data->client_address; ?><br/>
		<?php echo $booking_data->client_city; ?> <?php echo ($booking_data->client_province ? ('(' . $booking_data->client_province . ')') : '') ; ?><br/>
		<?php echo $booking_data->client_postal_code; ?> <?php echo $booking_data->client_country; ?></p>
		<p><strong>Date:</strong> <?php echo $document_data->date_created; ?></p>
	</div>

	<?php if($booking_data->is_admin): ?>
		<p><em>We have blocked the dates, <?php echo $booking_data->date_from; ?> to <?php echo $booking_data->date_to; ?> inclusive, in favour of <?php echo $booking_data->client_name; ?>, at <?php echo $booking_data->home_name; ?> (property situated in <?php echo $booking_data->home_address; ?>, <?php echo $booking_data->home_city; ?>, <?php echo $booking_data->home_province; ?>, <?php echo $booking_data->home_postal_code; ?> Spain). This booking will be held for five working days in anticipation of receiving a deposit of <?php echo $booking_data->first_payment_percent; ?>% of the price of the agreed rent as a deposit (see payment details below). <span style="text-decoration: underline"> Payment of this deposit constitutes acceptance of the booking conditions detailed below</span>. Once we have received confirmation of this payment we will send you the appropriate receipt and confirmation for the booking made in your favour under the terms of the present agreement. With this confirmation, your contract with the owner comes into force. The terms of this contract are based on the following conditions:</em></p>
	<?php else: ?>
		<p><em>We have blocked the dates, <?php echo $booking_data->date_from; ?> to <?php echo $booking_data->date_to; ?> inclusive, in favour of <?php echo $booking_data->client_name; ?>, at <?php echo $booking_data->home_name; ?> (property situated in <?php echo $booking_data->home_address; ?>, <?php echo $booking_data->home_city; ?>, <?php echo $booking_data->home_province; ?>, <?php echo $booking_data->home_postal_code; ?> Spain). This booking will be held for five working days in anticipation of receiving a deposit of <?php echo $booking_data->first_payment_percent; ?>% of the price of the agreed rent as a deposit (see payment details below). <span style="text-decoration: underline"> Payment of this deposit constitutes acceptance of the booking conditions detailed below</span>. Once we have received confirmation of this payment we will send you the appropriate receipt and confirmation for the booking made in your favour under the terms of the present agreement. With this confirmation, your contract with the owner comes into force. The terms of this contract are based on the following conditions:</em></p>
	<?php endif; ?>

	<!-- SPECIFIC CONDITIONS -->

	<header class="page-header">
		<h2>Specific Conditions</h2>
	</header>
	<h3>Rent</h3>
	<p>€<?php echo $booking_data->agreed_price; ?> for the period indicated above.</p>
	<h3>Payment</h3>
	<p><?php echo $booking_data->first_payment_percent; ?>% (€<?php echo $booking_data->first_payment_amount; ?>) of the agreed rent (€<?php echo $booking_data->agreed_price; ?>) is to be paid
	<?php
	if ($booking_data->payment_type_id==3){ // default value, no payment type selected?>
		either via bank transference to the <?php echo ($booking_data->bank_nationality == 'es' ? 'Spanish' : 'English'); ?> bank account detailed below or via direct payment with credit card.</p>
	<?php } elseif ($booking_data->payment_type_id==1) { //Bank Transfer ?>
		via bank transference to the <?php echo ($booking_data->bank_nationality == 'es' ? 'Spanish' : 'English'); ?> bank account detailed below: </p>
	<?php } elseif ($booking_data->payment_type_id==2) { //Credit Card ?>
		via direct payment with credit card.
	<?php }
	
	if ($booking_data->payment_type_id <=1) { //Bank Transfer or no payment type defined
	?>
	
		<p>
			<strong>Bank:</strong> <?php echo $booking_data->bank_name; ?><br/>
			<strong>Account Name:</strong> <?php echo $booking_data->bank_account_name; ?><br/>
			<strong>IBAN / BIC Code:</strong> <?php echo $booking_data->bank_iban . ' ' . $booking_data->bank_account_number . ' / ' . $booking_data->bank_bic; ?>
		</p>
	
	<?php } ?>

	<p>This payment is sufficient to confirm the booking under the conditions established below for the aforementioned period.</p>

	<p>The remaining <?php echo $booking_data->second_payment_percent; ?>% (€<?php echo $booking_data->second_payment_amount; ?>) should be
	<?php if ($booking_data->payment_type_id==3){ // default value, no payment type selected?>
		transferred to the same bank account at least <?php echo $system_config->final_payment_months; ?> months prior to the arrival date or settled via credit card payment.
	<?php } elseif ($booking_data->payment_type_id==1) { //Bank Transfer ?>
		transferred to the same bank account at least <?php echo $system_config->final_payment_months; ?> months prior to the arrival date.
	<?php } elseif ($booking_data->payment_type_id==2) { //Credit Card ?>
		settled via credit card payment at least <?php echo $system_config->final_payment_months; ?> months prior to the arrival date.
	<?php } ?>
	Should the payment not have been made effective by the agreed date for the second payment the reservation will be considered void and the property will be free and at Dosxtremos' entire disposal.</p>

	<h3>Cancellation Policy</h3>
	<p>Should you wish to cancel more than 4 months ahead of the arrival date, 100% of any funds paid will be refunded. If you wish to cancel between 2 and 4 months ahead of the arrival date, 50% of any funds paid will be refunded. If you cancel less than 4 months ahead of the arrival date, no funds paid will be refunded.</p>

	<p>In such cases where the second payment is not made effective by the second payment agreed date, the reservation will be considered void and the property will be free and at Dosxtremos' entire disposal.</p>

	<!-- PREVIOUS DYNAMIC CANCELATION CLAUSE
	<h3>Cancellation Costs</h3>
	<p>If you cancel more than <?php echo $system_config->cancellation_months; ?> months ahead of the agreed arrival date, we will refund <?php echo $system_config->cancellation_percent; ?>% of any funds paid. If you cancel less than <?php echo $system_config->cancellation_months; ?> months ahead of the agreed arrival date, we will not refund any money you have sent us.</p> -->

	<h3>Services Included</h3>
	<p>Electricity, water, gas, and Internet are included in the agreed rent. Beach towels are provided and we ask that you do not take bathroom towels to the beach.</p>

	<h3>Breakages Deposit</h3>
	<p>A breakages deposit of €<?php echo $booking_data->down_payment; ?> will be asked for. This should be paid via bank trasnfer.</p>

	<p>This amount, minus any deductions for breakages, damage, or excessive dirtiness, will be refunded within 3 working days after departure.</p>

	<h3>Arrival and Departure Times</h3>
	<p>In order to ensure that the property is properly cleaned and prepared, we ask that you arrive no earlier than <?php echo $system_config->time_arrival; ?> and that you leave on departure day before <?php echo $system_config->time_departure; ?>.</p>

	<!-- GENERAL CONDITIONS -->

	<header class="page-header">
		<h2>Other Conditions</h2>
	</header>

	<?php //if($booking_data->is_admin): ?>
		<ol>
			<li>This contractual agreement covers holiday properties (viviendas turísticas) as stipulated in Andalusia's Holiday Property Law, Decree 28/2016 passed on February 2, 2016.</li>
			<li>At the beginning of the rental period, the users of the property must show identification (ID card or passport), as stated by Decree 28/2016, and also sign a copy of this document. The house referred to in this contract (<?php echo $booking_data->home_name; ?>) is situated in <?php echo $booking_data->home_address; ?>, <?php echo $booking_data->home_city; ?>, <?php echo $booking_data->home_province; ?>, <?php echo $booking_data->home_postal_code; ?> Spain. At the beginning of the rental period, the user will find the property fit for the use for which it is designed, and it is their responsibility to leave the property at the end of the rental period in the same condition. </li>
			<li>If the user fails to leave the property at the stipulated time, they will have to pay additional rent to the owner at double the agreed daily rent for each day or fraction of day beyond the stipulated departure day. This penalisation is an essential condition to this contract.</li>
			<li>The property may only be used by a maximum of <?php echo $booking_data->num_occupants; ?> people (unless agreed otherwise), and it may not be used for organising parties or other activities that may cause it damage.  In the event of over-occupation, the user will have to pay the owner a penalisation that is equal to the agreed rent increased proportionately to reflect the number of people exceeding the maximum capacity stipulated. And the user will be responsible for reducing the number of people occupying the property to the stated capacity of <?php echo $booking_data->num_occupants; ?> immediately.</li>
			<li>Pets are not allowed on the property unless specifically agreed otherwise, and neither is camping in any shape or form (tents, caravans, etc.). The user and his party will be expected to behave at all times in a manner sympathetic to the local community and the environment and to take every possible care to avoid creating nuisance to those living in the vicinity.</li>
			<li>The user must make the agent or owner aware, as soon as is reasonably possible, of any repairs to the property that they deem necessary. And whilst everything reasonable will be done to effect speedy repairs no guarantees are given and no refunds will be made. </li>
			<li>Any bookings obtained under false pretense will be subject to forfeiture of any payments made, and the user will not be permitted to enter the property.</li>
			<li>If for any reason the property is not available or has been rendered unsuitable for use as a holiday property on the date booked, all deposits and rent paid by the user will be refunded in full, but there shall be no further claim against the owner and/or the agent. No liability is accepted for any loss, damage, sickness or injury howsoever caused which may be sustained during the holiday to any member of the user's party or to their possessions.</li>
			<li>Both parties (the user and the owner) expressly forego any other jurisdiction, and any disputes relating to this contract that cannot be settled by other means will be referred for resolution by the Court of Chiclana-de-la-Frontera.</li>
		</ol>
		<!--
	<?php //else: ?>
		<p>This is considered a temporary rental as stipulated in Article 3, nº 2 of Spain's 1994 Urban Rental Law. The house referred to in these booking conditions (<?php echo $booking_data->home_name; ?>) is situated in <?php echo $booking_data->home_address; ?>, <?php echo $booking_data->home_city; ?>, <?php echo $booking_data->home_province; ?>, <?php echo $booking_data->home_postal_code; ?> Spain. At the beginning of the rental period, the tenant will find the property fit for the use for which it was designed and it is his responsibility to leave the property at the end of the rental period in the same condition. The property may only be used by a maximum of <?php echo $booking_data->num_occupants; ?> people (unless agreed otherwise), and it should not be used for organizing parties or other activities that may cause it damage. In the event of over-occupation, we reserve the right to demand suitable compensation for the period in which the property was over-occupied and that the number of people occupying the house is reduced to the stated capacity of <?php echo $booking_data->num_occupants; ?> immediately. Pets are not allowed, unless specifically agreed otherwise, and neither is camping in any shape or form (tents, caravans, etc). The tenant and his party will be expected to behave at all times in a manner sympathetic to the local community and the environment and to take every possible care to avoid creating nuisance to those living in the vicinity. The tenant must also make the agent or owner aware, as soon as is reasonably possible, of any repairs to the property that they deem necessary, and whilst everything reasonable will be done to effect speedy repairs no guarantees are given and no refunds will be made. In the event that the tenant has paid a deposit to cover potential breakages, this will be returned either on departure or within seven days of departure, minus any amount taken off for breakages. Any bookings obtained under false pretense will be subject to forfeiture of any payments made, and the party will not be permitted to check in. If for any reason the property is not available or has been rendered unsuitable for the holiday letting on the date booked, all deposits and rent paid by the tenant will be refunded in full, but there shall be no further claim against the owner and/or the agent. No liability is accepted for any loss, damage, sickness or injury howsoever caused which may be sustained during the holiday to any member of the tenant’s party or to their possessions</p>
	<?php //endif; ?>
  -->

	<p>Details on key collection and instructions for getting to the property will be sent closer to the time, as will information on the surrounding area. If, in the meantime, you would like any more information on the property, the area, or the activities available locally, please ask.</p>

	<p>Thank you for booking <?php echo $booking_data->home_name; ?>.We will do everything we can to make sure your holiday is a success!</p>

	<!-- LEGAL DISCLAIMER -->

	<header class="page-header">
		<h2>Legal Disclaimer</h2>
	</header>

	<?php if($booking_data->is_admin): ?>
		<small>
			<p>Dosxtremos C.B. acts as Agent for <?php echo $booking_data->owner_name; ?> (the "Owner"), and has been entrusted by the Owner with the management of their property, <?php echo $booking_data->home_name; ?>, situated in <?php echo $booking_data->home_address; ?>, <?php echo $booking_data->home_city; ?>, <?php echo $booking_data->home_province; ?>, <?php echo $booking_data->home_postal_code; ?> Spain. The Agent is responsible for finding users for <?php echo $booking_data->home_name; ?>, but the recommendations and opinions given by the Agent are for guidance only, and while descriptions have been written in good faith they are subjective and the Agent accepts no liability for these. Your contractual agreement on <?php echo $booking_data->home_name; ?> is with the Owner and is based on these Booking Conditions. Despite the fact that all correspondence, communication and payments are carried out via the Agent, the Agent accepts no responsibility for your direct agreements with the Owner.</p>

			<p>The agencies, activity operators, and guides recommended on the Dosxtremos website (hereinafter "the website"), in all correspondence between the Agent and the user, and in any literature published by Dosxtremos C.B. are to the best of our knowledge run professionally and responsibly. However, Dosxtremos C.B. has no control over third party content or over the management of any of the activities recommended on the website, in any of the correspondence between the Agent and the user, or in any of the literature published by Dosxtremos C.B., and we make no warranty whatsoever in relation to any of the services being provided by any of the agencies, guides, or activity operators mentioned. We take no responsibility for any loss or injury that may occur to you or to any members of your party if you choose to use any of the agencies, guides, or activity operators recommended on the website, in any of the correspondence between the Agent and the user, or in any of the literature published by Dosxtremos C.B., or if you choose to practice any of the activities that we suggest. We have made every effort to ensure the accuracy of the information we provide, but we can accept no responsibility for any errors it may contain.</p>

			<p>Dosxtremos C.B. is only responsible for organising the reservation of you holiday property and it is not selling a package. Dosxtremos C.B. does not operate as intermediary for or receive any commissions from any of the agencies, guides, or activity operators mentioned on the website, in the correspondence between the Agent and the user, or in any literature published by Dosxtremos C.B. It is your responsibility to organise travel insurance and transport and to make sure that your passports are valid. We recommend that you take out travel insurance.</p>
		</small>
	<?php else: ?>
		<small>
			<p>Dosxtremos C.B. acts as Agent for <?php echo $booking_data->owner_name; ?> (the "Owner"), and has been entrusted by the Owner with the management of their property, <?php echo $booking_data->home_name; ?>, situated in <?php echo $booking_data->home_address; ?>, <?php echo $booking_data->home_city; ?>, <?php echo $booking_data->home_province; ?>, <?php echo $booking_data->home_postal_code; ?> Spain. The Agent is responsible for finding users for <?php echo $booking_data->home_name; ?>, but the recommendations and opinions given by the Agent are for guidance only, and while descriptions have been written in good faith they are subjective and the Agent accepts no liability for these. Your contractual agreement on <?php echo $booking_data->home_name; ?> is with the Owner and is based on these Booking Conditions. Despite the fact that all correspondence and communication is carried out via the Agent, the Agent accepts no responsibility for your direct agreements with the Owner.</p>

			<p>The agencies, activity operators, and guides recommended on the Dosxtremos website (hereinafter "the website"), in all correspondence between the Agent and the user, and in any literature published by Dosxtremos C.B. are to the best of our knowledge run professionally and responsibly. However, Dosxtremos C.B. has no control over third party content or over the management of any of the activities recommended on the website, in any of the correspondence between the Agent and the user, or in any of the literature published by Dosxtremos C.B., and we make no warranty whatsoever in relation to any of the services being provided by any of the agencies, guides, or activity operators mentioned. We take no responsibility for any loss or injury that may occur to you or to any members of your party if you choose to use any of the agencies, guides, or activity operators recommended on the website, in any of the correspondence between the Agent and the user, or in any of the literature published by Dosxtremos C.B., or if you choose to practice any of the activities that we suggest. We have made every effort to ensure the accuracy of the information we provide, but we can accept no responsibility for any errors it may contain.</p>

			<p>Dosxtremos C.B. is only responsible for organising the reservation of you holiday property and it is not selling a package. Dosxtremos C.B. does not operate as intermediary for or receive any commissions from any of the agencies, guides, or activity operators mentioned on the website, in the correspondence between the Agent and the user, or in any literature published by Dosxtremos C.B. It is your responsibility to organise travel insurance and transport and to make sure that your passports are valid. We recommend that you take out travel insurance.</p>
		</small>
	<?php endif; ?>
