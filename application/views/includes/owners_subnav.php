
	<ul class="nav nav-pills">
		<li class="<?php echo ($current_view == 'bookings' ? 'active' : ''); ?>">
			<a href="<?php echo site_url(array('owners','bookings','index',$home_id)); ?>"><?php echo lang('owners_bookings'); ?></a>
		</li>
		<li class="<?php echo ($current_view == 'maintenance' ? 'active' : ''); ?>">
			<a href="<?php echo site_url(array('owners','maintenances','index',$home_id)); ?>"><?php echo lang('owners_advanced_expenses'); ?></a>
		</li>
		<li class="<?php echo ($current_view == 'comments' ? 'active' : ''); ?>">
			<a href="<?php echo site_url(array('owners','comments','index',$home_id)); ?>"><?php echo lang('owners_comments'); ?></a>
		</li>
		<li class="<?php echo ($current_view == 'expenses' ? 'active' : ''); ?>">
			<a href="<?php echo site_url(array('owners','expenses','index',$home_id)); ?>"><?php echo lang('owners_expenses'); ?></a>
		</li>
		<li class="<?php echo ($current_view == 'documents' ? 'active' : ''); ?>">
			<a href="<?php echo site_url(array('owners','documents','index',$home_id)); ?>"><?php echo lang('owners_documents'); ?></a>
		</li>
	</ul>