
	<meta charset="UTF-8">
	<title>DosXtremos App</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<!-- Styles -->
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/datepicker.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/timepicker.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/application.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/print.css" rel="stylesheet" media="print">
	
	<!-- Fav and touch icons -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.ico">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-57-precomposed.png">
	
	<!-- Javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.8.21.custom.min.js"></script>
	
	<!-- Set any constants -->
	<script type="text/javascript">
	
		var BASE_URL = '<?php echo base_url(); ?>';
		var SITE_URL = '<?php echo site_url(); ?>';
	
	</script>
	
	<!-- Load section common scripts -->
	
	<?php switch($this->uri->segment(1)) : case 'admin': ?>
	
		<!-- Admin Script(s) -->
		<script src="<?php echo base_url(); ?>assets/js/admin_common.js"></script>
		
		<!-- CKEditor Scripts -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckeditor/adapters/jquery.js"></script>
		
		<?php break; ?>
	
	<?php case 'owner': ?>
	
		<!-- Owner Script(s) -->
		
		<?php break; ?>
	
	<?php default: ?>
	
		<!-- Public (Default) Script(s) -->
	
	<?php endswitch; ?>
	
	<!-- IE CSS & JS fixes -->
	
	<!--[if lte IE 9]>
		<link href="<?php echo base_url(); ?>assets/css/ie9.css" rel="stylesheet">
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.placeholder.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('input, textarea').placeholder();
			});
		</script>
	<![endif]-->
	
	<!--[if lte IE 8]>
		<link href="<?php echo base_url(); ?>assets/css/ie8.css" rel="stylesheet">
		<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	