	<!-- PAGE HEADER -->
	<header class="navbar navbar-fixed-top">
		<nav class="navbar-inner">
			<div class="container">
				
				<!-- Branding -->
				<div class="logo pull-left">
					<img src="<?php echo base_url(); ?>assets/img/application_logo_nav.png" alt="DosXtremos Logo" width="188" height="40" />
				</div>
				
				<!-- Primnary Navigation -->
				<ul class="nav pull-right">
					<li class="<?php echo ($lang == 'es' ? 'active' : ''); ?>"><a href="<?php echo site_url('main/index/es'); ?>">Español</a></li>
					<li class="<?php echo ($lang == 'en' ? 'active' : ''); ?>"><a href="<?php echo site_url('main/index/en'); ?>">English</a></li>
				</ul>
				
			</div>
		</nav>
	</header>