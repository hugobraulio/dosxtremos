	<!-- PAGE HEADER -->
	<header class="navbar navbar-fixed-top">
		<nav class="navbar-inner">
			<div class="container">
				
				<!-- Branding -->
				<a class="logo pull-left" href="<?php echo site_url(array('admin','bookings','index',0,time())); ?>"><img src="<?php echo base_url(); ?>assets/img/application_logo_nav.png" alt="DosXtremos Logo" width="188" height="40" /></a>
				
				<!-- Primnary Navigation -->
				<ul class="nav">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo site_url(array('admin','index',0,time())); ?>">Bookings <b class="caret"></b></a>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
						  <li><a tabindex="-1" href="<?php echo site_url(array('admin','bookings','index',0,time())); ?>">Calendario de Bookings</a></li>
						  <li><a tabindex="-1" href="<?php echo site_url(array('admin','bookings','cancelled',0,time())); ?>">Cancelados</a></li>
						</ul>
					</li>
					<li><a href="<?php echo site_url('admin/homes'); ?>">Viviendas</a></li>
					<li><a href="<?php echo site_url('admin/owners'); ?>">Propietarios</a></li>
					<?php if($this->session->userdata('admin_role') > 1): ?>
						<li><a href="<?php echo site_url('admin/users'); ?>">Usuarios</a></li>
						<li><a href="<?php echo site_url('admin/system_configs'); ?>">Sistema</a></li>
					<?php endif; ?>
				</ul>
				
				<!-- Secondary Navigation -->
				<ul class="nav pull-right">
					<li><a id="alert_trigger" href="<?php echo site_url('admin/alerts/index'); ?>">Alertas <span class="badge<?php echo ($active_alerts > 0 ? ' badge-important' : ''); ?>"><?php echo $active_alerts; ?></span></a></li>
					<li class="divider-vertical"></li>
					<li><a href="<?php echo site_url('admin/sessions/logout'); ?>">Cerrar Sesión</a></li>
				</ul>
				
			</div>
		</nav>
	</header>