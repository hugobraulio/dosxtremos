	<!-- PAGE HEADER -->
	<header class="navbar navbar-fixed-top">
		<nav class="navbar-inner">
			<div class="container">
				
				<!-- Branding -->
				<a class="logo pull-left" href="<?php echo site_url('owners/bookings'); ?>"><img src="<?php echo base_url(); ?>assets/img/application_logo_nav.png" alt="DosXtremos Logo" width="188" height="40" /></a>
				
				<!-- Primnary Navigation -->
				<ul class="nav pull-left">
					<?php
						$home_ids = explode(',', $this->session->userdata('homes'));
						$home_names = explode(',', $this->session->userdata('home_names'));
					?>
					<?php for($i=0; $i < count($home_ids); $i++): ?>
						<li><a href="<?php echo site_url(array('owners','bookings','index',$home_ids[$i])); ?>"><?php echo $home_names[$i]; ?></a></li>
					<?php endfor; ?>
				</ul>
				
				<!-- Secondary Navigation -->
				<ul class="nav pull-right">
					<li><span class="salutation"><i class="icon-user icon-white"></i> &nbsp; <?php echo $this->session->userdata('name'); ?></span></li>
					<li><a href="<?php echo site_url('owners/invoices/index'); ?>"><?php echo lang('owners_invoices'); ?></a></li>
					<li class="divider-vertical"></li>
					<li><a href="<?php echo site_url('owners/sessions/logout'); ?>"><?php echo lang('owners_end_session'); ?></a></li>
				</ul>
				
			</div>
		</nav>
	</header>