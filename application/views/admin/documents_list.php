<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Viviendas <small>Documentos de Casa Almadraba : 2012</small></h1>
				
				<nav class="btn-group pull-right">
					<a class="btn" href="admin_homes_documents_list.php"><i class="icon-chevron-left"></i></a>
					<a class="btn" href="admin_homes_documents_list.php"><i class="icon-chevron-right"></i></a>
				</nav>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="admin_index.php">Inicio</a> <span class="divider">/</span></li>
				<li><a href="admin_homes_list.php">Viviendas</a> <span class="divider">/</span></li>
				<li><a href="admin_homes_view.php">Casa Almadraba</a> <span class="divider">/</span></li>
				<li class="active">Documentos</li>
				<li class="pull-right"><a href="admin_homes_view.php">Volver a detalle</a></li>
			</ul>

		</header>
		
		<ul class="nav nav-tabs" id="document_tabs">
			<li class="active"><a href="#contracts">Contratos</a></li>
			<li><a href="#conditions">Condiciones</a></li>
			<li><a href="#receipts">Recibos</a></li>
		</ul>
		
		<div class="tab-content">
		
			<!-- Contracts List Table -->
			<section class="tab-pane active" id="contracts">
				
				<header class="page-header">
					<h2>Contratos</h2>
				</header>
				
				<table class="table">
					<thead>
						<tr>
							<th width="110">Nº Booking</th>
							<th>Cliente</th>
							<th width="110">Fecha</th>
							<th width="35">Opciones</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><a href="admin_bookings_view.php">CAL-070812</a></td>
							<td>James Maxwell</td>
							<td>07/08/2012</td>
							<td>
								<nav class="btn-group">
									<a class="btn" href="admin_homes_documents_contract_print.php" target="_blank" title="Imprimir"><i class="icon-print"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td><a href="admin_bookings_view.php">CAL-022312</a></td>
							<td>Felipe Massa</td>
							<td>02/23/2012</td>
							<td>
								<nav class="btn-group">
									<a class="btn" href="admin_homes_documents_contract_print.php" target="_blank" title="Imprimir"><i class="icon-print"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td><a href="admin_bookings_view.php">CAL-070812</a></td>
							<td>Kimi Raikkonen</td>
							<td>07/08/2012</td>
							<td>
								<nav class="btn-group">
									<a class="btn" href="admin_homes_documents_contract_print.php" target="_blank" title="Imprimir"><i class="icon-print"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td><a href="admin_bookings_view.php">CAL-070812</a></td>
							<td>Rubens Barichello</td>
							<td>07/08/2012</td>
							<td>
								<nav class="btn-group">
									<a class="btn" href="admin_homes_documents_contract_print.php" target="_blank" title="Imprimir"><i class="icon-print"></i></a>
								</nav>
							</td>
						</tr>
					</tbody>
				</table>
				
			</section>
			
			<!-- Conditions List Table -->
			<section class="tab-pane" id="conditions">
				
				<header class="page-header">
					<h2>Condiciones</h2>
				</header>
				
				<table class="table">
					<thead>
						<tr>
							<th width="110">Nº Booking</th>
							<th>Cliente</th>
							<th width="110">Fecha</th>
							<th width="35">Opciones</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><a href="admin_bookings_view.php">CAL-070812</a></td>
							<td>James Maxwell</td>
							<td>07/08/2012</td>
							<td>
								<nav class="btn-group">
									<a class="btn" href="admin_homes_documents_conditions_print.php" target="_blank" title="Imprimir"><i class="icon-print"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td><a href="admin_bookings_view.php">CAL-022312</a></td>
							<td>Felipe Massa</td>
							<td>02/23/2012</td>
							<td>
								<nav class="btn-group">
									<a class="btn" href="admin_homes_documents_conditions_print.php" target="_blank" title="Imprimir"><i class="icon-print"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td><a href="admin_bookings_view.php">CAL-070812</a></td>
							<td>Kimi Raikkonen</td>
							<td>07/08/2012</td>
							<td>
								<nav class="btn-group">
									<a class="btn" href="admin_homes_documents_conditions_print.php" target="_blank" title="Imprimir"><i class="icon-print"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td><a href="admin_bookings_view.php">CAL-070812</a></td>
							<td>Rubens Barichello</td>
							<td>07/08/2012</td>
							<td>
								<nav class="btn-group">
									<a class="btn" href="admin_homes_documents_conditions_print.php" target="_blank" title="Imprimir"><i class="icon-print"></i></a>
								</nav>
							</td>
						</tr>
					</tbody>
				</table>
				
			</section>
			
			<!-- Receipts List Table -->
			<section class="tab-pane" id="receipts">
				
				<header class="page-header">
					<h2>Recibos</h2>
				</header>
				
				<table class="table">
					<thead>
						<tr>
							<th width="110">Nº Booking</th>
							<th>Cliente</th>
							<th width="110">Fecha</th>
							<th width="110">Tipo de Pago</th>
							<th width="35">Opciones</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><a href="admin_bookings_view.php">CAL-070812</a></td>
							<td>James Maxwell</td>
							<td>07/08/2012</td>
							<td>Primero</td>
							<td>
								<nav class="btn-group">
									<a class="btn" href="admin_homes_documents_receipt_print.php" target="_blank" title="Imprimir"><i class="icon-print"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td><a href="admin_bookings_view.php">CAL-022312</a></td>
							<td>Felipe Massa</td>
							<td>23/02/2012</td>
							<td>Segundo</td>
							<td>
								<nav class="btn-group">
									<a class="btn" href="admin_homes_documents_receipt_print.php" target="_blank" title="Imprimir"><i class="icon-print"></i></a>
								</nav>
							</td>
						</tr>
						<tr>
							<td><a href="admin_bookings_view.php">CAL-022312</a></td>
							<td>Felipe Massa</td>
							<td>08/01/2012</td>
							<td>Primero</td>
							<td>
								<nav class="btn-group">
									<a class="btn" href="admin_homes_documents_receipt_print.php" target="_blank" title="Imprimir"><i class="icon-print"></i></a>
								</nav>
							</td>
						</tr>
					</tbody>
				</table>
				
			</section>
			
		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>