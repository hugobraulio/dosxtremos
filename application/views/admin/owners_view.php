<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
	<!-- Load Page Specific Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/admin_owners_view.js"></script>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Propietarios <small><?php echo $owner->fname . ' ' . $owner->lname; ?></small></h1>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $list_url; ?>">Propietarios</a> <span class="divider">/</span></li>
				<li class="active"><?php echo $owner->fname . ' ' . $owner->lname; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver a la lista</a></li>
			</ul>

		</header>
		
		<?php if($delete_item): ?>
		
			<form class="alert alert-error" method="post" action="<?php echo $delete_url; ?>">
				<h4 class="alert-heading">¡Aviso!</h4>
				<p>¿Esta seguro que quiere seguir? Si proceda, borrará por completo este propietario del sistema.</p>
				<fieldset>
					<input type="submit" name="submit" class="btn btn-danger" value="Borrar" />
					<input type="submit" name="submit" class="btn" value="Cancelar" />
				</fieldset>
			</form>
		
		<?php endif; ?>
		
		<div class="row">
			
			<!-- Item Data -->
			<article class="span9">
				
				<h2>
					<?php echo $owner->fname . ' ' . $owner->lname; ?>
					<?php if($owner->is_active): ?>
						<span id="active_tag" class="label label-success">Activado</span>
					<?php else: ?>
						<span id="active_tag" class="label label-important">Desactivado</span>
					<?php endif; ?>
				</h2>
				<br/>
				
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab_owner" data-toggle="tab">Datos Propietario</a></li>
					<li><a href="#tab_access" data-toggle="tab">Datos Acceso</a></li>
					<li><a href="#tab_bank" data-toggle="tab">Datos Bancarios</a></li>
				</ul>
				
				<div class="tab-content">
				
					<div class="tab-pane row active" id="tab_owner">
					
						<dl class="dl-horizontal">
						
							<dt>Nombre</dt>
							<dd><?php echo $owner->fname . ' ' . $owner->lname; ?></dd>
							
							<dt>Dirección</dt>
							<dd>
								<address>
								<?php 
									echo $owner->address . '<br>';
									echo $owner->postal_code . ' ' . $owner->city . ' (' . $owner->province . ') ' . $owner->country;
								?> 
								</address>
							</dd>
							
							<dt>Email</dt>
							<dd><a href="mailto:<?php echo $owner->email; ?>"><?php echo $owner->email; ?></a></dd>
							
							<?php if($owner->telephone): ?>
								<dt>Teléfono Fijo</dt>
								<dd><?php echo $owner->telephone; ?></dd>
							<?php endif; ?>
							
							<?php if($owner->mobile): ?>
								<dt>Teléfono Móvil</dt>
								<dd><?php echo $owner->mobile; ?></dd>
							<?php endif; ?>
							
							<?php if($owner->fax): ?>
								<dt>Fax</dt>
								<dd><?php echo $owner->fax; ?></dd>
							<?php endif; ?>
							
							<dt>NIF/CIF/NIE</dt>
							<dd><?php echo $owner->tax_id; ?></dd>
							
							<?php if($owner->homes): ?>
								<dt>Vivienda(s)</dt>
								<dd>
									<ul>
										<?php foreach($owner->homes as $home): ?>
											<li><a href="<?php echo $home->view_url; ?>"><?php echo $home->name; ?></a></li>
										<?php endforeach; ?>
									</ul>
								</dd>
							<?php endif; ?>
							
							<?php if($owner->notes): ?>
								<dt>Notas Adicionales</dt>
								<dd><?php echo $owner->notes; ?></dd>
							<?php endif; ?>
							
						</dl>
					
					</div>
					
					<div class="tab-pane row" id="tab_access">
					
						<dl class="dl-horizontal">
						
							<dt>Usuario</dt>
							<dd><?php echo $owner->username; ?></dd>
							
							<dt>Contraseña</dt>
							<dd><?php echo $owner->password; ?></dd>
							
							<dt>Administrador</dt>
							<dd><?php echo ($owner->is_admin ? 'Sí' : 'No'); ?></dd>
							
							<dt>Idioma</dt>
							<dd><?php echo $owner->language->name; ?></dd>
							
						</dl>
					
					</div>
					
					<div class="tab-pane row" id="tab_bank">
					
						<dl class="dl-horizontal">
						
							<?php if($owner->bank->name): ?>
								<dt>Nombre</dt>
								<dd><?php echo $owner->bank->name; ?></dd>
							<?php endif; ?>
							
							<?php if($owner->bank->nationality && $owner->bank->name): ?>
								<dt>Nacionalidad</dt>
								<dd><?php echo ($owner->bank->nationality == 'es' ? 'Espa&ntilde;ola' : 'Ingl&eacute;s'); ?></dd>
							<?php endif; ?>
							
							<?php if($owner->bank->iban): ?>
								<dt>Código IBAN</dt>
								<dd><?php echo $owner->bank->iban; ?></dd>
							<?php endif; ?>
							
							<?php if($owner->bank->bic): ?>
								<dt>Código BIC</dt>
								<dd><?php echo $owner->bank->bic; ?></dd>
							<?php endif; ?>
							
							<?php if($owner->bank->account->name): ?>
								<dt>Nombre Cuenta</dt>
								<dd><?php echo $owner->bank->account->name; ?></dd>
							<?php endif; ?>
							
							<?php if($owner->bank->account->number): ?>
								<dt>Número Cuenta</dt>
								<dd><?php echo $owner->bank->account->number; ?></dd>
							<?php endif; ?>
							
						</dl>
					
					</div>
				
				</div>
				
			</article>
			
			<!-- Sidebar -->
			<aside class="span3">
				
				<!-- Item Sub-Navigation -->
				<nav class="well">
					<ul class="nav nav-list">
						<li class="nav-header">Secciones</li>
						<li><a href="<?php echo $expenses_url; ?>"><i class="icon-list-alt"></i> Liquidaciones</a></li>
						<li><a href="<?php echo $invoices_url; ?>"><i class="icon-file"></i> Facturas</a></li>
						<?php if($this->session->userdata('admin_role') > 1): ?>
							<li class="nav-header">Opciones</li>
							<li><a href="<?php echo $edit_url; ?>"><i class="icon-pencil"></i> Editar</a></li>
							<li>
								<a id="item_is_active_link" href="<?php echo $status_url; ?>">
									<?php if($owner->is_active): ?>
										<i class="icon-eye-close"></i> <span class="display_text">Desactivar</span>
									<?php else: ?>
										<i class="icon-eye-open"></i> <span class="display_text">Activar</span>
									<?php endif; ?>
								</a>
							</li>
							<li><a href="<?php echo $delete_url; ?>"><i class="icon-trash"></i> Borrar</a></li>
						<?php endif; ?>
					</ul>
				</nav>
			
			</aside>
			
		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>