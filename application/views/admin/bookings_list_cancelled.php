<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Bookings Cancelados : <?php echo $current_year; ?></h1>
				
				<nav class="btn-group pull-right">
					<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
					<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
				</nav>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo $home_url; ?>">Inicio</a> <span class="divider">/</span></li>
				<li class="active">Bookings Cancelados : 2012</li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver al Calendario</a></li>
			</ul>

		</header>
		
		<table class="table table-striped">
			<thead>
				<tr>
					<th width="120">Nº Referencia</th>
					<th>Nombre</th>
					<th width="160">Fechas del Booking</th>
					<th width="75">Opciones</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($bookings as $booking): ?>
					<tr>
						<td><a href="<?php echo $booking->view_url; ?>"><?php echo $booking->reference_num; ?></a></td>
						<td><?php echo $booking->name; ?></td>
						<td><?php echo $booking->date_from; ?> - <?php echo $booking->date_to; ?></td>
						<td>
							<nav class="btn-group">
								<a class="btn" rel="tooltip" href="<?php echo $booking->edit_url; ?>" title="Editar"><i class="icon-pencil"></i></a>
								<a class="btn" rel="tooltip" href="<?php echo $booking->delete_url; ?>" title="Borrar"><i class="icon-trash"></i></a>
							</nav>
						</td>
					</tr>				
				<?php endforeach; ?>
			</tbody>
		</table>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>