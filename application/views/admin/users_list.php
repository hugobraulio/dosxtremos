<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">
			
			<h1>Usuarios <small>Listado de Usuarios Administrativos</small></h1>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li class="active">Usuarios</li>
			</ul>

		</header>
		
		<table class="table">
			<thead>
				<tr>
					<th>Nombre / Apellido</th>
					<th width="170">Nivel</th>
					<th width="75">Opciones</th>
				</tr>
			</thead>
			<tbody>
			
				<?php foreach($users as $user): ?>
			
					<tr>
						<td><a href="<?php echo $user->view_url; ?>" title="Ver Detalle"><?php echo $user->name; ?></a></td>
						<td><?php echo $user->role_name; ?></td>
						<td>
							<nav class="btn-group">
								<a class="btn" rel="tooltip" href="<?php echo $user->edit_url; ?>" title="Editar"><i class="icon-pencil"></i></a>
								<a class="btn" rel="tooltip" href="<?php echo $user->delete_url; ?>" title="Borrar"><i class="icon-trash"></i></a>
							</nav>
						</td>
					</tr>
				
				<?php endforeach; ?>
				
			</tbody>
		</table>
		
		<div class="clearfix">
		
			<div class="btn-toolbar pull-left">
				<nav class="btn-group">
					<?php echo $pagination; ?>
				</nav>
				<nav class="btn-group">
					<a class="btn" href="<?php echo $add_url; ?>"><i class="icon-plus"></i> Añadir Usuario</a>
				</nav>
			</div>
		
		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>