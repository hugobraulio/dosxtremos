<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Viviendas <small>Documentos de <?php echo $home->name; ?> : <?php echo $year; ?></small></h1>
				
				<nav class="btn-group pull-right">
					<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
					<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
				</nav>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $list_url; ?>">Viviendas</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>"><?php echo $home->name; ?></a> <span class="divider">/</span></li>
				<li class="active">Documentos</li>
				<li class="pull-right"><a href="<?php echo $parent_url; ?>">Volver a detalle</a></li>
			</ul>

		</header>
		
		<ul class="nav nav-tabs" id="document_tabs">
			<li class="<?php echo ($tab == 'conditions' ? 'active' : ''); ?>"><a href="#conditions">Condiciones</a></li>
			<li class="<?php echo ($tab == 'receipts' ? 'active' : ''); ?>"><a href="#receipts">Recibos</a></li>
		</ul>
		
		<div class="tab-content">
			
			<!-- Conditions List Table -->
			<section class="tab-pane<?php echo ($tab == 'conditions' ? ' active' : ''); ?>" id="conditions">
				
				<header class="page-header">
					<h2>Condiciones</h2>
				</header>
				
				<table class="table">
					<thead>
						<tr>
							<th width="110">Nº Booking</th>
							<th>Cliente</th>
							<th width="110">Fecha</th>
							<th width="75">En Español</th>
							<th width="75">En Inglés</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($conditions as $condition): ?>
							<tr>
								<td><a href="<?php echo $condition->booking_url; ?>"><?php echo $condition->reference_num; ?></a></td>
								<td><?php echo $condition->client_name; ?></td>
								<td><?php echo $condition->date_created; ?></td>
								<td><a class="btn" href="<?php echo $condition->view_url_es; ?>" target="_blank" title="Imprimir"><i class="icon-print"></i></a></td>
								<td><a class="btn" href="<?php echo $condition->view_url_en; ?>" target="_blank" title="Imprimir"><i class="icon-print"></i></a></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				
			</section>
			
			<!-- Receipts List Table -->
			<section class="tab-pane<?php echo ($tab == 'receipts' ? ' active' : ''); ?>" id="receipts">
				
				<header class="page-header">
					<h2>Recibos</h2>
				</header>
				
				<table class="table">
					<thead>
						<tr>
							<th width="110">Nº Booking</th>
							<th>Cliente</th>
							<th width="110">Fecha</th>
							<th width="110">Tipo de Pago</th>
							<th width="110">Cantidad</th>
							<th width="75">En Español</th>
							<th width="75">En Inglés</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($receipts as $receipt): ?>
							<tr>
								<td><a href="<?php echo $receipt->booking_url; ?>"><?php echo $receipt->reference_num; ?></a></td>
								<td><?php echo $receipt->client_name; ?></td>
								<td><?php echo $receipt->date_created; ?></td>
								<td><?php echo $receipt->percentage; ?>%</td>
								<td><?php echo $receipt->amount; ?> €</td>
								<td><a class="btn" href="<?php echo $receipt->view_url_es; ?>" target="_blank" title="Imprimir"><i class="icon-print"></i></a></td>
								<td><a class="btn" href="<?php echo $receipt->view_url_en; ?>" target="_blank" title="Imprimir"><i class="icon-print"></i></a></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				
			</section>
			
		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>