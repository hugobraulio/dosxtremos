<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Viviendas <small>Calendario de <?php echo $home->name; ?> : <?php echo $calendar->month . ' ' .$calendar->year; ?></small></h1>
				
				<nav class="btn-group pull-right">
					<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
					<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $calendar->year . ' ' . $calendar->month; ?> &nbsp;<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo $month_navigation->prev_year_link->url; ?>"><?php echo $month_navigation->prev_year_link->name; ?></a></li>
						<li class="divider"></li>
						<?php foreach($month_navigation->this_year_links as $link): ?>
							<li><a href="<?php echo $link->url; ?>"><?php echo $link->name; ?></a></li>
						<?php endforeach; ?>
						<li class="divider"></li>
						<li><a href="<?php echo $month_navigation->next_year_link->url; ?>"><?php echo $month_navigation->next_year_link->name; ?></a></li>
					</ul>
				</nav>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $list_url; ?>">Viviendas</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>"><?php echo $home->name; ?></a> <span class="divider">/</span></li>
				<li class="active">Calendario</li>
				<li class="pull-right"><a href="<?php echo $parent_url; ?>">Volver al detalle</a></li>
			</ul>

		</header>
		
		<ul class="legend">
			<li class="available">Disponible</li>
			<li class="offered">Ofertada</li>
			<li class="pre-booked">Pre-Confirmado</li>
			<li class="booked">Confirmado</li>
		</ul>
		
		<table class="table calendar">
			<thead>
				<tr>
					<th>Día</th>
					<th width="75">Precio</th>
					<th width="75">Estimado</th>
					<th width="280">Booking</th>
					<th width="280">Ofertas</th>
				</tr>
			</thead>
			<tbody>
			
				<?php foreach($calendar->days as $day): ?>
				
					<tr class="<?php echo $day->view_class; ?>">
						<th><?php echo $day->name; ?></th>
						<td>
							<?php if(($day->is_saturday && $day->week_price != '0.00') || ($day->start_booking)): ?>
								<?php echo $day->week_price; ?> €
							<?php else: ?>
								&nbsp;
							<?php endif; ?>
						</td>
						<td>
							<?php if($day->is_saturday && $day->week_estimate != '0.00'): ?>
								<?php echo $day->week_estimate; ?> €
							<?php else: ?>
								&nbsp;
							<?php endif; ?>
						</td>
						<td>
							<?php if($day->booking): ?>
								<a href="<?php echo $day->booking->view_url; ?>" class="<?php echo $day->booking->status_permalink; ?>">
									<i class="icon-user"></i> <?php echo $day->booking->name; ?>
								</a>
							<?php else: ?>
								<?php if($day->is_saturday && $day->add_booking_url): ?>
									<a class="add_booking" href="<?php echo $day->add_booking_url; ?>"><i class="icon-plus"></i> Añadir Booking</a>
								<?php else: ?>
									<span class="placeholder">Sin Bookings</span>
								<?php endif; ?>
							<?php endif; ?>
						</td>
						<td>
							<?php if($day->is_saturday && $day->add_offer_url): ?>
								<a class="add_booking" href="<?php echo $day->add_offer_url; ?>"><i class="icon-plus"></i> Añadir Oferta</a>
							<?php endif; ?>
							<?php if($day->offers): ?>
								<ul class="offer_list clearfix">
								<?php foreach($day->offers as $offer): ?>
									<li>
										<a href="<?php echo $offer->view_url; ?>" class="<?php echo $offer->status_permalink; ?>">
											<i class="icon-user"></i> <?php echo $offer->name; ?>
										</a>
									</li>
								<?php endforeach; ?>
								</ul>
							<?php else: ?>
								<span class="placeholder">Sin Ofertas</span>
							<?php endif; ?>
						</td>
					</tr>
				
				<?php endforeach; ?>				
				
			</tbody>
		</table>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>