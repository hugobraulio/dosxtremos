<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
	<!-- Page Specific Script(s) -->
	<script src="<?php echo base_url(); ?>assets/js/admin_homes_prices.js" charset="utf-8"></script>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Viviendas <small>Precios de <?php echo $home->name; ?> : <?php echo $calendar->year; ?></small></h1>
				
				<nav class="btn-group pull-right">
					<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
					<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
				</nav>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $list_url; ?>">Viviendas</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>"><?php echo $home->name; ?></a> <span class="divider">/</span></li>
				<li class="active">Precios</li>
				<li class="pull-right"><a href="<?php echo $parent_url; ?>">Volver a detalle</a></li>
			</ul>

		</header>
		
		<div class="page-header">
			<div class="clearfix">
				<h3 class="pull-left">Precio Estimado para <?php echo $calendar->year; ?></h3>
				<form class="form-inline pull-right" name="home_estimate" method="post" action="<?php echo $anual_estimate['action_url']; ?>">
					<div class="input-append">
						<input type="text" name="amount" class="span2 currency" id="amount" placeholder="0.00" value="<?php echo $anual_estimate['amount']; ?>"><span class="add-on">€</span>
					</div>
					<input type="hidden" name="year" value="<?php echo $calendar->year; ?>" />
					<input type="submit" name="submit" class="btn btn-primary" id="save" value="Guardar">
				</form>
			</div>
			<?php if($anual_estimate['estimate_saved']): ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>Guardado:</strong> Se ha guardado correctamente el precio estimado de <?php echo $home->name; ?> para <?php echo $calendar->year; ?>.
				</div>
			<?php endif; ?>
		</div>
		
		<table class="table calendar">
			<thead>
				<tr>
					<th>Semana</th>
					<th width="160">Precio Fijado</th>
					<th width="160">Precio Estimado</th>
					<th width="75">Opciones</th>
				</tr>
			</thead>
			<tbody>
			
				<?php foreach($calendar->weeks as $week): ?>
			
					<tr class="<?php echo $week->view_class; ?>">
						<th><?php echo $week->name; ?></th>
						<td class="price"><?php echo $week->price; ?></td>
						<td class="estimate"><?php echo $week->estimate; ?></td>
						<td>
							<nav class="btn-group">
								<a class="btn date_action" href="<?php echo $week->action_url; ?>" title="<?php echo ($week->has_prices ? 'Editar' : 'Añadir'); ?>">
									<?php if($week->has_prices): ?>
										<i class="icon-pencil"></i>
									<?php else: ?>
										<i class="icon-plus"></i>
									<?php endif; ?>
								</a>
								<a class="btn date_delete<?php echo (!$week->has_prices ? ' disabled' : ''); ?>" href="<?php echo $week->delete_url; ?>" title="Borrar"><i class="icon-trash"></i></a>
							</nav>
						</td>
					</tr>
				
				<?php endforeach; ?>

			</tbody>
		</table>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>
	
	<div id="modals"></div>

</body>

</html>