	
	<div class="modal hide fade">
	
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h5><?php echo $timespan->home->name; ?></h5>
			<h3><?php echo $action_title; ?></h3>
		</div>
		
		<form name="timeslot_form" id="timeslot_form" method="post" action="<?php echo $action_url; ?>">
		
			<fieldset class="modal-body">
				
				<div class="row">
				
					<div class="span2 control-group<?php if(form_error('price')) { echo ' error'; } ?>">
						<label for="price">Precio Fijado</label>
						<div class="input-append">
							<input
								type="text"
								name="price"
								class="span2 currency"
								id="price"
								placeholder="0.00"
								value="<?php echo set_value('price',$timespan->price); ?>" /><span class="add-on">€</span>
						</div>
						<?php echo form_error('price'); ?>
					</div>
					
					<div class="span2 control-group<?php if(form_error('estimate')) { echo ' error'; } ?>">
						<label for="estimate">Precio Estimado</label>
						<div class="input-append">
							<input
								type="text"
								name="estimate"
								class="span2 currency"
								id="estimate"
								placeholder="0.00"
								value="<?php echo set_value('estimate',$timespan->estimate); ?>" /><span class="add-on">€</span>
						</div>
						<?php echo form_error('estimate'); ?>
					</div>
				
				</div>
				
			</fieldset>
			
			<fieldset class="modal-footer clearfix">
			
				<input type="hidden" name="home_id" id="home_id" value="<?php echo $timespan->home->item_id; ?>" />
				<input type="submit" name="submit" class="btn btn-primary pull-left" id="save" value="Guardar" />
				<a href="#" class="btn pull-right" data-dismiss="modal">Cerrar</a>
				
			</fieldset>
		
		</form>
		
	</div>

	