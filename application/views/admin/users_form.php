<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Usuarios <small><?php echo $action_title; ?></small></h1>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo site_url('admin/users'); ?>">Usuarios</a> <span class="divider">/</span></li>
				<li class="active"><?php echo $action_title; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver a la lista</a></li>
			</ul>

		</header>
		
		<!-- Item Form -->
		<form name="home_form" method="post" action="<?php echo $action_url; ?>">
			
			<div class="page-header">
				<h3>Datos del Usuario</h3>
			</div>
			
			<fieldset class="row">
				
				<div class="span4 control-group<?php if(form_error('fname')) { echo ' error'; } ?>">
					<label for="fname">Nombre *</label>
					<input
						type="text"
						name="fname"
						class="span4"
						id="fname"
						placeholder="Nombre del usuario"
						value="<?php echo set_value('fname',$user->fname); ?>"
					/>
					<?php echo form_error('fname'); ?>
				</div>
				
				<div class="span4 control-group<?php if(form_error('lname')) { echo ' error'; } ?>">
					<label for="lname">Apellido(s) *</label>
					<input
						type="text"
						name="lname"
						class="span4"
						id="lname"
						placeholder="Apellido(s) del usuario"
						value="<?php echo set_value('lname',$user->lname); ?>"
					/>
					<?php echo form_error('lname'); ?>
				</div>
				
				<div class="span4 control-group<?php if(form_error('email')) { echo ' error'; } ?>">
					<label for="email">Email *</label>
					<input
						type="email"
						name="email"
						class="span4"
						id="email"
						placeholder="example@example.com"
						value="<?php echo set_value('email',$user->email); ?>"
					/>
					<?php echo form_error('email'); ?>
				</div>
				
			</fieldset>
			
			<div class="page-header">
				<h3>Datos de Acceso</h3>
			</div>
			
			<fieldset class="row">
				
				<div class="span4 control-group<?php if(form_error('username')) { echo ' error'; } ?>">
					<label for="username">Nombre Usuario *</label>
					<input
						type="text"
						name="username"
						class="span4"
						id="username"
						placeholder="8-12 caractéres"
						value="<?php echo set_value('username',$user->username); ?>"
					/>
					<?php echo form_error('username'); ?>
				</div>
				
				<div class="span4 control-group<?php if(form_error('password')) { echo ' error'; } ?>">
					<label for="password">Contraseña *</label>
					<input
						type="text"
						name="password"
						class="span4"
						id="password"
						placeholder="8-12 caractéres"
						value="<?php echo set_value('password',$user->password); ?>"
					/>
					<?php echo form_error('password'); ?>
				</div>
				
				<div class="span4 control-group<?php if(form_error('role_id')) { echo ' error'; } ?>">
					<label for="role_id">Nivel *</label>
					<select
						name="role_id"
						class="span4"
						id="role_id">
						<option value="">Seleccione</option>
						<?php foreach($all_roles as $role): ?>
							<option
								value="<?php echo $role->item_id; ?>"
								<?php echo set_select('role_id',$role->item_id,($role->item_id == $user->role->item_id)); ?>>
								<?php echo $role->name; ?>
							</option>
						<?php endforeach; ?>
					</select>
					<?php echo form_error('role_id'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="form-actions">
				
				<input type="submit" name="submit" class="btn btn-primary" id="save" value="Guardar" />
				<input type="submit" name="submit" class="btn" id="cancel" value="Cancelar" />
				<span class="help-inline">Los campos marcados con un asterisco (*) son obligatórias.</span>
				
			</fieldset>
			
		</form>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>