<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Sistema <small>Configuraciones del Sistema</small></h1>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li class="active">Sistema</li>
			</ul>

		</header>
		
		<?php if($confirm): ?>
		
			<div class="alert alert-success alert-block">
				<a class="close" data-dismiss="alert" href="#">×</a>
				<h4 class="alert-heading">Guardado!</h4>
				Se han guardado las configuraciones del sistema correctamente.
			</div>
		
		<?php endif; ?>
		
		<!-- Item Form -->
		<form name="system_form" method="post" action="<?php echo $action_url; ?>">
			
			<div class="row">
			
				<fieldset class="span4">
					
					<header class="page-header">
						<h3>Porcentajes de Pagos</h3>
					</header>
					
					<p class="help-block">Porcentajes utilizados para calcular los pagos basados en el precio acordado.</p>
					
					<div class="row">
					
						<div class="span2 control-group<?php if(form_error('pay_percent_1')) { echo ' error'; } ?>">
							<label for="pay_percent_1">Primer Pago *</label>
							<div class="input-append">
								<input
									type="text"
									name="pay_percent_1"
									class="span1"
									id="pay_percent_1"
									placeholder="0"
									value="<?php echo set_value('pay_percent_1',$system_config->pay_percent_1); ?>" /><span class="add-on">%</span>
							</div>
							<?php echo form_error('pay_percent_1'); ?>
						</div>
						
						<div class="span2 control-group<?php if(form_error('pay_percent_2')) { echo ' error'; } ?>">
							<label for="percent_2">Segundo Pago *</label>
							<div class="input-append">
								<input
									type="text"
									name="pay_percent_2"
									class="span1"
									id="pay_percent_2"
									placeholder="0"
									value="<?php echo set_value('pay_percent_2',$system_config->pay_percent_2); ?>" /><span class="add-on">%</span>
							</div>
							<?php echo form_error('pay_percent_2'); ?>
						</div>
					
					</div>
					
				</fieldset>
				
				<fieldset class="span4">
					
					<header class="page-header">
						<h3>Comisiones DosXtremos</h3>
					</header>
					
					<p class="help-block">Porcentaje de comisión de DosXtremos por defecto para viviendas nuevas.</p>
					
					<div class="control-group<?php if(form_error('commission_percent')) { echo ' error'; } ?>">
						<label for="commission">Comisión *</label>
						<div class="input-append">
							<input
								type="text"
								name="commission_percent"
								class="span1"
								id="commission_percent"
								placeholder="0"
								value="<?php echo set_value('commission_percent',$system_config->commission_percent); ?>" /><span class="add-on">%</span>
						</div>
						<?php echo form_error('commission_percent'); ?>
					</div>
					
				</fieldset>
				
				<fieldset class="span4">
					
					<header class="page-header">
						<h3>Datos Fiscales</h3>
					</header>
					
					<p class="help-block">Datos utilizados en las facturas generadas por el sistema.</p>
					
					<div class="row">
					
						<div class="span2 control-group<?php if(form_error('tax_id')) { echo ' error'; } ?>">
							<label for="tax_id">CIF/NIF/NIE</label>
							<input
								type="text"
								name="tax_id"
								class="span2"
								id="tax_id"
								placeholder="A-0000000-A"
								value="<?php echo set_value('tax_id',$system_config->tax_id); ?>"
							/>
							<?php echo form_error('tax_id'); ?>
						</div>
						
						<div class="span2 control-group<?php if(form_error('vat')) { echo ' error'; } ?>">
							<label for="vat">IVA *</label>
							<div class="input-append">
								<input
									type="text"
									name="vat"
									class="span1"
									id="vat"
									placeholder="0"
									value="<?php echo set_value('vat',$system_config->vat); ?>" /><span class="add-on">%</span>
								<?php echo form_error('vat'); ?>
							</div>
						</div>
					
					</div>
					
				</fieldset>
			
			</div>
			
			<header class="page-header">
				<h3>Datos Bancarias <small>Los datos bancarios utilizados en los recibos y condiciones de reserva generados por el sistema.</small></h3>
			</header>
			
			<fieldset class="row">
			
				<div class="span4 control-group<?php if(form_error('bank_name')) { echo ' error'; } ?>">
					<label for="bank_name">Nombre del Banco *</label>
					<input
						type="text"
						name="bank_name"
						class="span4"
						id="bank_name"
						placeholder="Nombre del banco"
						value="<?php echo set_value('bank_name', $system_config->bank->name); ?>"
					/>
					<?php echo form_error('bank_name'); ?>
				</div>
				
				<div class="span4 control-group<?php if(form_error('bank_nationality')) { echo ' error'; } ?>">
					<label for="bank_nationality">Nacionalidad del Banco *</label>
					<select
						name="bank_nationality"
						class="span4"
						id="bank_nationality">
						<option value="">Seleccionar</option>
						<?php foreach($all_nationalities as $key => $value): ?>
							<option
								value="<?php echo $key; ?>"
								<?php echo set_select('bank_nationality',$key,($key == $system_config->bank->nationality)); ?>>
								<?php echo $value; ?>
							</option>
						<?php endforeach; ?>
					</select>
					<?php echo form_error('bank_nationality'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('bank_iban')) { echo ' error'; } ?>">
					<label for="bank_iban">Código IBAN *</label>
					<input
						type="text"
						name="bank_iban"
						class="span2"
						id="bank_iban"
						placeholder="AA00"
						value="<?php echo set_value('bank_iban', $system_config->bank->iban); ?>"
					/>
					<?php echo form_error('bank_iban'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('bank_bic')) { echo ' error'; } ?>">
					<label for="bank_bic">Código BIC *</label>
					<input
						type="text"
						name="bank_bic"
						class="span2"
						id="bank_bic"
						placeholder="AAAAAAAAAAA"
						value="<?php echo set_value('bank_bic', $system_config->bank->bic); ?>"
					/>
					<?php echo form_error('bank_bic'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="row">
			
				<div class="span4 control-group<?php if(form_error('bank_account_name')) { echo ' error'; } ?>">
					<label for="bank_account_name">Nombre del Titular de la Cuenta *</label>
					<input
						type="text"
						name="bank_account_name"
						class="span4"
						id="bank_account_name"
						placeholder="Nombre y Appellido(s) del Titular"
						value="<?php echo set_value('bank_account_name', $system_config->bank->account->name); ?>"
					/>
					<?php echo form_error('bank_account_name'); ?>
				</div>
				
				<div class="span4 control-group<?php if(form_error('bank_account_number')) { echo ' error'; } ?>">
					<label for="bank_account_number">Número de la Cuenta *</label>
					<input
						type="text"
						name="bank_account_number"
						class="span4"
						id="bank_account_number"
						placeholder="0000 0000 0000 0000 0000"
						value="<?php echo set_value('bank_account_number', $system_config->bank->account->number); ?>"
					/>
					<?php echo form_error('bank_account_number'); ?>
				</div>
			
			</fieldset>
			
			
			<fieldset class="form-actions">
				<input type="submit" name="submit" class="btn btn-primary" id="save" value="Guardar Cambios" />
				<span class="help-inline">Los campos marcados con un asterisco (*) son obligatorios.</span>
			</fieldset>

			
		</form>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>