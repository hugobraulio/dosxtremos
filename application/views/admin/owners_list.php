<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">
			
			<h1>Propietarios <small>Listado de Propietarios</small></h1>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li class="active">Propietarios</li>
			</ul>

		</header>
		
		<table class="table">
			<thead>
				<tr>
					<th>Propietario</th>
					<th width="170">Estado</th>
					<th width="75">Opciones</th>
				</tr>
			</thead>
			<tbody>
				
				<?php foreach($owners as $owner): ?>
				
					<tr>
						<td>
							<a href="<?php echo $owner->view_url; ?>" title="Ver Detalle"><?php echo $owner->name; ?></a>
							<?php if($owner->is_admin): ?>
								<i class="icon-star"></i>
							<?php endif; ?>
						</td>
						<td>
							<?php if($owner->is_active): ?>
								<span class="label label-success">Activado</span>
							<?php else : ?>
								<span class="label label-important">Desctivado</span>
							<?php endif; ?>
						</td>
						<td>
							<nav class="btn-group">
								<a class="btn<?php echo ($this->session->userdata('admin_role') == 1 ? ' disabled' : ''); ?>" rel="tooltip" href="<?php echo $owner->edit_url; ?>" title="Editar"><i class="icon-pencil"></i></a>
								<a class="btn<?php echo ($this->session->userdata('admin_role') == 1 ? ' disabled' : ''); ?>" rel="tooltip" href="<?php echo $owner->delete_url; ?>" title="Borrar"><i class="icon-trash"></i></a>
							</nav>
						</td>
					</tr>
				
				<?php endforeach; ?>
				
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3"><i class="icon-star"></i> = También Usuario Administrativo</td>
				</tr>
			</tfoot>
		</table>
		
		<nav class="btn-group">
			<?php echo $pagination; ?>
			<a class="btn<?php echo ($this->session->userdata('admin_role') == 1 ? ' disabled' : ''); ?>" href="<?php echo $add_url; ?>"><i class="icon-plus"></i> Añadir Propietario</a>
		</nav>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>