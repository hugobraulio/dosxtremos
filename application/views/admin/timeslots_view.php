
	<div class="modal hide fade<?php echo ($timespan->status->item_id ? (' ' . $timespan->status->class_name) : ''); ?>">
	
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h5><?php echo $timespan->home->name; ?></h5>
			<h3><?php echo $action_title; ?></h3>
		</div>
		
		<div class="modal-body">

			<dl class="dl-horizontal">
				<dt>Estado</dt>
				<dd><?php echo $timespan->status->label; ?></dd>
				<dt>Precio</dt>
				<dd><?php echo $timespan->price; ?> &euro;</dd>
				<dt>Estimado</dt>
				<dd><?php echo $timespan->estimate; ?> &euro;</dd>
			</dl>
			
			<!-- Tabs of Data (Only for timespans with offers or bookings) -->
			<?php if($timespan->status->item_id): ?>
			
				<ul class="nav nav-tabs">
				
					<?php if($timespan->booking): ?>
						<li><a href="#tab_booking" data-toggle="tab">Booking</a></li>
					<?php endif; ?>
					
					<?php if($timespan->booking && $timespan->booking->payments): ?>
						<li><a href="#tab_payments" data-toggle="tab">Pagos</a></li>
					<?php endif; ?>
					
					<?php if($timespan->offers): ?>
						<li><a href="#tab_offers" data-toggle="tab">Ofertas</a></li>
					<?php endif; ?>
					
				</ul>
				
				<div class="tab-content">
				
					<!-- Booking (Pre-booked or Booked) -->
					<?php if($timespan->booking): ?>
					
						<div class="tab-pane row" id="tab_booking">
						
							<dl class="dl-horizontal span4">
								<dt>Nombre</dt>
								<dd><a href="mailto:<?php echo $timespan->booking->email; ?>"><?php echo $timespan->booking->fname . ' ' . $timespan->booking->lname; ?> <i class="icon icon-envelope"></i></a></dd>
								<dt>Teléfono</dt>
								<dd><?php echo $timespan->booking->mobile; ?></dd>
								<dt>Nº Referencia</dt>
								<dd><?php echo $timespan->booking->reference_num; ?></dd>
								<dt>Acordado</dt>
								<dd><?php echo $timespan->booking->agreed_price; ?> &euro;</dd>
							</dl>
							<dl class="dl-horizontal span4">
								<dt>Nº Adultos</dt>
								<dd><?php echo $timespan->booking->adults; ?></dd>
								<dt>Nº Niños</dt>
								<dd><?php echo $timespan->booking->children; ?></dd>
							</dl>
							
							<nav class="btn-group span6">
								<a class="btn" href="<?php echo $timespan->booking->view_url; ?>"><i class="icon icon-file"></i> Detalle</a>
								<a class="btn" href="<?php echo $timespan->booking->edit_url; ?>"><i class="icon icon-pencil"></i> Editar</a>
								<a class="btn" href="<?php echo $timespan->booking->delete_url; ?>"><i class="icon icon-trash"></i> Borrar</a>
							</nav>
						
						</div>
					
					<?php endif; ?>
					
					<!-- Payments -->
					<?php if($timespan->booking && $timespan->booking->payments): ?>
					
						<div class="tab-pane" id="tab_payments">
				
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Tipo</th>
										<th width="90">Cantidad</th>
										<th width="90">Pendiente</th>
										<th width="90">Pagado</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($timespan->booking->payments as $payment): ?>
										<tr>
											<td>Pago (<?php echo $payment->percentage; ?>%)</td>
											<td><?php echo $payment->amount; ?> &euro;</td>
											<td><?php echo $payment->date_due; ?></td>
											<td><?php echo $payment->date_paid; ?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						
						</div>
					
					<?php endif; ?>
					
					<!-- Offers -->
					<?php if($timespan->offers): ?>
					
						<div class="tab-pane" id="tab_offers">
				
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Nombre</th>
										<th width="150">Estancia</th>
										<th width="90">Ofertada</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($timespan->offers as $offer): ?>
										<tr>
											<td><a href="<?php echo $offer->view_url; ?>"><?php echo $offer->name; ?></a></td>
											<td><?php echo $offer->date_from . ' - ' . $offer->date_to; ?></td>
											<td><?php echo $offer->date_offered; ?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						
						</div>
					
					<?php endif; ?>
				
				</div>
			
			<?php endif; ?>
			
		</div>
		
		<div class="modal-footer clearfix">
			
			<?php if($timespan->status->item_id <= 1): ?>
				<div class="btn-group pull-left">
					<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">Añadir <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo $add_offer_url; ?>">Oferta</a></li>
						<li><a href="<?php echo $add_booking_url; ?>">Booking</a></li>
					</ul>
				</div>
			<?php endif; ?>
			
			<a href="#" class="btn pull-right" data-dismiss="modal">Cerrar</a>
			
		</div>
		
	</div>
	