<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Propietarios <small><?php echo $action_title; ?></small></h1>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="admin_index.php">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo site_url('admin/owners'); ?>">Propietarios</a> <span class="divider">/</span></li>
				<li class="active"><?php echo $action_title; ?></li>
				<li class="pull-right"><a href="<?php echo site_url('admin/owners'); ?>">Volver a la lista</a></li>
			</ul>

		</header>
		
		<!-- Item Form -->
		<form name="owner_form" method="post" action="<?php echo $action_url; ?>">
			
			<div class="page-header">
				<h3>Datos del Propietario</h3>
			</div>
			
			<fieldset class="row">
				
				<div class="span5 control-group<?php if(form_error('fname')) { echo ' error'; } ?>">
					<label for="fname">Nombre *</label>
					<input
						type="text"
						name="fname"
						class="span5"
						id="fname"
						placeholder="Nombre del propietario"
						value="<?php echo set_value('fname',$owner->fname); ?>"
					/>
					<?php echo form_error('fname'); ?>
				</div>
				
				<div class="span5 control-group<?php if(form_error('lname')) { echo ' error'; } ?>">
					<label for="lname">Apellido(s) *</label>
					<input
						type="text"
						name="lname"
						class="span5"
						id="lname"
						placeholder="Apellido(s) del propietario"
						value="<?php echo set_value('lname',$owner->lname); ?>"
					/>
					<?php echo form_error('lname'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('tax_id')) { echo ' error'; } ?>">
					<label for="tax_id">NIF / NIE / CIF *</label>
					<input
						type="text"
						name="tax_id"
						class="span2"
						id="tax_id"
						placeholder="A-0000000-A"
						value="<?php echo set_value('tax_id',$owner->tax_id); ?>"
					/>
					<?php echo form_error('tax_id'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="row">
				
				<div class="span5 control-group<?php if(form_error('address')) { echo ' error'; } ?>">
					<label for="address">Dirección *</label>
					<input
						type="text"
						name="address"
						class="span5"
						id="address"
						placeholder="Dirección de Calle"
						value="<?php echo set_value('address',$owner->address); ?>"
					/>
					<?php echo form_error('address'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('city')) { echo ' error'; } ?>">
					<label for="city">Municipio *</label>
					<input
						type="text"
						name="city"
						class="span2"
						id="city"
						placeholder="Ciudad / Pueblo"
						value="<?php echo set_value('city',$owner->city); ?>"
					/>
					<?php echo form_error('city'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('province')) { echo ' error'; } ?>">
					<label for="province">Provincia *</label>
					<input
						type="text"
						name="province"
						class="span2"
						id="province"
						placeholder="Provincia"
						value="<?php echo set_value('province',$owner->province); ?>"
					/>
					<?php echo form_error('province'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('postal_code')) { echo ' error'; } ?>">
					<label for="postal_code">Código Postal *</label>
					<input
						type="text"
						name="postal_code"
						class="span2"
						id="postal_code"
						placeholder="00000"
						value="<?php echo set_value('postal_code',$owner->postal_code); ?>"
					/>
					<?php echo form_error('postal_code'); ?>
				</div>
				
				<div class="span1 control-group<?php if(form_error('country')) { echo ' error'; } ?>">
					<label for="country">País *</label>
					<input
						type="text"
						name="country"
						class="span1"
						id="country"
						placeholder="AA"
						value="<?php echo set_value('country',$owner->country); ?>"
					/>
					<?php echo form_error('country'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="row">
				
				<div class="span3 control-group<?php if(form_error('email')) { echo ' error'; } ?>">
					<label for="email">Email *</label>
					<input
						type="email"
						name="email"
						class="span3"
						id="email"
						placeholder="example@example.com"
						value="<?php echo set_value('email',$owner->email); ?>"
					/>
					<?php echo form_error('email'); ?>
				</div>
				
				<div class="span3 control-group<?php if(form_error('telephone')) { echo ' error'; } ?>">
					<label for="telephone">Teléfono Fijo *</label>
					<input
						type="tel"
						name="telephone"
						class="span3"
						id="telephone"
						placeholder="000 000 000"
						value="<?php echo set_value('telephone',$owner->telephone); ?>"
					/>
					<?php echo form_error('telephone'); ?>
				</div>
				
				<div class="span3 control-group<?php if(form_error('mobile')) { echo ' error'; } ?>">
					<label for="mobile">Teléfono Móvil</label>
					<input
						type="tel"
						name="mobile"
						class="span3"
						id="mobile"
						placeholder="000 000 000"
						value="<?php echo set_value('mobile',$owner->mobile); ?>"
					/>
					<?php echo form_error('mobile'); ?>
				</div>
				
				<div class="span3 control-group<?php if(form_error('fax')) { echo ' error'; } ?>">
					<label for="fax">Fax</label>
					<input
						type="tel"
						name="fax"
						class="span3"
						id="fax"
						placeholder="000 000 000"
						value="<?php echo set_value('fax',$owner->fax); ?>"
					/>
					<?php echo form_error('fax'); ?>
				</div>
				
			</fieldset>
			
			<div class="page-header">
				<h3>Datos de Acceso</h3>
			</div>
			
			<fieldset class="row">
				
				<div class="span3 control-group<?php if(form_error('username')) { echo ' error'; } ?>">
					<label for="username">Nombre Usuario *</label>
					<input
						type="text"
						name="username"
						class="span3"
						id="username"
						placeholder="8-12 caractéres"
						value="<?php echo set_value('username',$owner->username); ?>"
					/>
					<?php echo form_error('username'); ?>
				</div>
				
				<div class="span3 control-group<?php if(form_error('password')) { echo ' error'; } ?>">
					<label for="password">Contraseña *</label>
					<input
						type="text"
						name="password"
						class="span3"
						id="password"
						placeholder="8-12 caractéres"
						value="<?php echo set_value('password',$owner->password); ?>"
					/>
					<?php echo form_error('password'); ?>
				</div>
				
				<div class="span3 control-group<?php if(form_error('language_id')) { echo ' error'; } ?>">
					<label for="language_id">Idioma *</label>
					<select
						name="language_id"
						class="span3"
						id="language_id">
						<option value="">Seleccione</option>
						<?php foreach($all_languages as $language): ?>
							<option
								value="<?php echo $language->item_id; ?>"
								<?php echo set_select('language_id',$language->item_id,($language->item_id == $owner->language->item_id)); ?>>
								<?php echo $language->name; ?>
							</option>
						<?php endforeach; ?>
					</select>
					<?php echo form_error('language_id'); ?>
				</div>
				
				<div class="span3 control-group<?php if(form_error('is_admin')) { echo ' error'; } ?>">
					<label for="is_admin">Usuario Administrativo</label>
					<input
						type="checkbox"
						name="is_admin"
						id="is_admin"
						value="1"
						<?php echo set_checkbox('is_admin', '1', ($owner->is_admin == 1)); ?>
					/>
					<?php echo form_error('is_admin'); ?>
				</div>
				
			</fieldset>
			
			<header class="page-header">
				<h3>Datos Bancarios</h3>
			</header>
			
			<fieldset class="row">
			
				<div class="span4 control-group<?php if(form_error('bank_name')) { echo ' error'; } ?>">
					<label for="bank_name">Nombre del Banco *</label>
					<input
						type="text"
						name="bank_name"
						class="span4"
						id="bank_name"
						placeholder="Nombre del banco"
						value="<?php echo set_value('bank_name', $owner->bank->name); ?>"
					/>
					<?php echo form_error('bank_name'); ?>
				</div>
				
				<div class="span4 control-group<?php if(form_error('bank_nationality')) { echo ' error'; } ?>">
					<label for="bank_nationality">Nacionalidad del Banco *</label>
					<select
						name="bank_nationality"
						class="span4"
						id="bank_nationality">
						<option value="">Seleccionar</option>
						<?php foreach($all_nationalities as $key => $value): ?>
							<option
								value="<?php echo $key; ?>"
								<?php echo set_select('bank_nationality',$key,($key == $owner->bank->nationality)); ?>>
								<?php echo $value; ?>
							</option>
						<?php endforeach; ?>
					</select>
					<?php echo form_error('bank_nationality'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('bank_iban')) { echo ' error'; } ?>">
					<label for="bank_iban">Código IBAN *</label>
					<input
						type="text"
						name="bank_iban"
						class="span2"
						id="bank_iban"
						placeholder="AA00"
						value="<?php echo set_value('bank_iban', $owner->bank->iban); ?>"
					/>
					<?php echo form_error('bank_iban'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('bank_bic')) { echo ' error'; } ?>">
					<label for="bank_bic">Código BIC *</label>
					<input
						type="text"
						name="bank_bic"
						class="span2"
						id="bank_bic"
						placeholder="AAAAAAAAAAA"
						value="<?php echo set_value('bank_bic', $owner->bank->bic); ?>"
					/>
					<?php echo form_error('bank_bic'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="row">
			
				<div class="span4 control-group<?php if(form_error('bank_account_name')) { echo ' error'; } ?>">
					<label for="bank_account_name">Nombre del Titular de la Cuenta *</label>
					<input
						type="text"
						name="bank_account_name"
						class="span4"
						id="bank_account_name"
						placeholder="Nombre y Appellido(s) del Titular"
						value="<?php echo set_value('bank_account_name', $owner->bank->account->name); ?>"
					/>
					<?php echo form_error('bank_account_name'); ?>
				</div>
				
				<div class="span4 control-group<?php if(form_error('bank_account_number')) { echo ' error'; } ?>">
					<label for="bank_account_number">Número de la Cuenta *</label>
					<input
						type="text"
						name="bank_account_number"
						class="span4"
						id="bank_account_number"
						placeholder="0000 0000 0000 0000 0000"
						value="<?php echo set_value('bank_account_number', $owner->bank->account->number); ?>"
					/>
					<?php echo form_error('bank_account_number'); ?>
				</div>
			
			</fieldset>
			
			<div class="page-header">
				<h3>Adicional</h3>
			</div>
			
			<fieldset class="row">
				
				<div class="span12 control-group<?php if(form_error('notes')) { echo ' error'; } ?>">
					<label for="notes">Notas Adicionales</label>
					<textarea
						name="notes"
						class="span12"
						id="notes"
						rows="12"
						placeholder="Incluye notas adicionales sobre la vivienda aquí"><?php echo set_value('notes',$owner->notes); ?></textarea>
					<?php echo form_error('notes'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="form-actions">
				
				<input type="submit" name="submit" class="btn btn-primary" id="save" value="Guardar" />
				<input type="submit" name="submit" class="btn" id="cancel" value="Cancelar" />
				<span class="help-inline">Los campos marcados con un asterisco (*) son obligatórias.</span>
				
			</fieldset>
			
		</form>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>