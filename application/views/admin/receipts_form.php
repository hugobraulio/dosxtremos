<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>

</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">

		<header class="page-header">

			<div class="clearfix">

				<h1 class="pull-left">Bookings <small><?php echo $action_title; ?></small></h1>

			</div>

			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>">Booking Nº <?php echo $receipt->reference_num; ?></a> <span class="divider">/</span></li>
				<li><a href="<?php echo $list_url; ?>">Pagos y Recibos</a> <span class="divider">/</span></li>
				<li class="active"><?php echo $action_title; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver al detalle</a></li>
			</ul>

		</header>

		<!-- Item Form -->
		<form name="home_form" method="post" action="<?php echo $action_url; ?>">

			<div class="page-header">
				<h3>Contenido del Documento <small>Multilingüe</small></h3>
			</div>

			<fieldset class="row">

				<div class="span12">

					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_es" data-toggle="tab">Español</a></li>
						<li><a href="#tab_en" data-toggle="tab">Inglés</a></li>
					</ul>

					<div class="tab-content">

						<div class="tab-pane active" id="tab_es">

							<!-- Spanish Content -->
							<div class="control-group<?php if(form_error('content_es')) { echo ' error'; } ?>">
								<label for="content_es">Recibo en Español *</label>
								<textarea
									name="content_es"
									class="span12 wysiwyg"
									id="content_es"
									rows="12"
									placeholder="Incluye el contenido del recibo en español"><?php echo set_value('content_es', $receipt->content->es); ?></textarea>
									<?php echo form_error('content_es'); ?>
							</div>

						</div>

						<div class="tab-pane" id="tab_en">

							<!-- English Content -->
							<div class="control-group<?php if(form_error('content_en')) { echo ' error'; } ?>">
								<label for="content_en">Recibo en Inglés *</label>
								<textarea
									name="content_en"
									class="span12 wysiwyg"
									id="content_en"
									rows="12"
									placeholder="Incluye el contenido del recibo en inglés"><?php echo set_value('content_en', $receipt->content->en); ?></textarea>
									<?php echo form_error('content_en'); ?>
							</div>

						</div>

					</div>

				</div>

			</fieldset>

			<fieldset class="form-actions">

				<!-- Date Created as a hidden field for now -->
				<input type="hidden" name="date_created" id="date_created" value="<?php echo set_value('date_created', $receipt->date_created); ?>" />

				<!-- Submission Controls -->
				<input type="submit" name="submit" class="btn btn-primary" id="save" value="Guardar" />
				<input type="submit" name="submit" class="btn" id="cancel" value="Cancelar" />
				<span class="help-inline">Los campos marcados con un asterisco (*) son obligatorios.</span>
				
			</fieldset>

		</form>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>
