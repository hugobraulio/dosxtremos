<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Viviendas <small>Gasto Adelantado de <?php echo $maintenance->home->name; ?></small></h1>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo site_url('admin/homes'); ?>">Viviendas</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>"><?php echo $maintenance->home->name; ?></a> <span class="divider">/</span></li>
				<li><a href="<?php echo $list_url; ?>">Gastos Adelantados <?php echo $current_year; ?></a> <span class="divider">/</span></li>
				<li><?php echo $maintenance->name; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver a la lista</a></li>
			</ul>

		</header>
		
		<?php if($delete_item): ?>
		
			<form class="alert alert-error" method="post" action="<?php echo $delete_url; ?>">
				<h4 class="alert-heading">¡Aviso!</h4>
				<p>¿Esta seguro que quiere seguir? Si proceda, borrará por completo este gasto adelantado.</p>
				<fieldset>
					<input type="submit" name="submit" class="btn btn-danger" value="Borrar" />
					<input type="submit" name="submit" class="btn" value="Cancelar" />
				</fieldset>
			</form>
		
		<?php endif; ?>
		
		<div class="row">
			
			<!-- Item Data -->
			<article class="span9">
				
				<h2><?php echo $maintenance->name; ?></h2>
				
				<dl class="dl-horizontal">
				
					<dt>Nº Referencia</dt>
					<dd><?php echo $maintenance->reference_num; ?></dd>
					
					<dt>Fecha Realizada</dt>
					<dd><?php echo $maintenance->date_complete; ?></dd>
					
					<dt>Fecha Pagada</dt>
					<dd><?php echo $maintenance->date_payed; ?></dd>
					
					<dt>Proveedor</dt>
					<dd><?php echo $maintenance->provider; ?></dd>
					
					<dt>Pagado Por</dt>
					<dd><?php echo $maintenance->payed_by; ?></dd>
					
					<dt>Importe</dt>
					<dd><?php echo $maintenance->amount; ?> €</dd>
					
					<dt>Trimestre</dt>
					<dd>T<?php echo $maintenance->trimester; ?></dd>
					
					<?php if($maintenance->notes): ?>
						<dt>Notas Adicionales</dt>
						<dd><?php echo $maintenance->notes; ?></dd>
					<?php endif; ?>
					
				</dl>
				
			</article>
			
			<!-- Sidebar -->
			<aside class="span3">
				
				<!-- Item Sub-Navigation -->
				<nav class="well">
					<ul class="nav nav-list">
						<?php if($download_url): ?>
							<li class="nav-header">Archivo Adjunta</li>
							<li><a href="<?php echo $download_url; ?>"><i class="icon-download"></i> Descargar</a></li>
						<?php endif; ?>
						<li class="nav-header">Opciones</li>
						<li><a href="<?php echo $edit_url; ?>"><i class="icon-pencil"></i> Editar</a></li>
						<li><a href="<?php echo $delete_url; ?>"><i class="icon-trash"></i> Borrar</a></li>
					</ul>
				</nav>
			
			</aside>
			
		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>