	
	<div class="modal hide fade" id="owners_bookings_commissions">
	
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3><?php echo ($title ? $title : 'Comisiones y Gastos de DosXtremos'); ?></h3>
		</div>
		
		<div class="modal-body">
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Descripción</th>
						<th width="80">Importe</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($costs as $cost): ?>
						<tr>
							<td><?php echo $cost->description; ?></td>
							<td><?php echo $cost->amount_net; ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					<th>Total</th>
					<td><strong><?php echo $total_net; ?> €</strong></td>
				</tfoot>
			</table>
			
		</div>
		
		<div class="modal-footer clearfix">
			<a href="#" class="btn pull-right" data-dismiss="modal">Cerrar</a>
		</div>
	
	</div>