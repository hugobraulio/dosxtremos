<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>

	<!-- Load Page Specific Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/ajaxfileupload.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/admin_bookings_view.js"></script>

</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">

		<header class="page-header">

			<div class="clearfix">

				<h1 class="pull-left">Bookings <small>Nº <?php echo $booking->reference_num; ?></small></h1>

			</div>

			<ul class="breadcrumb">
				<li><a href="<?php echo $home_url; ?>">Inicio</a> <span class="divider">/</span></li>
				<li class="active">Booking Nº <?php echo $booking->reference_num; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver al calendario</a></li>
			</ul>

		</header>

		<?php if($delete_item): ?>

			<form class="alert alert-error" method="post" action="<?php echo $delete_url; ?>">
				<h4 class="alert-heading">¡Aviso!</h4>
				<p>¿Está seguro que quiere seguir? Si procede borrará por completo este booking.</p>
				<fieldset>
					<input type="submit" name="submit" class="btn btn-danger" value="Borrar" />
					<input type="submit" name="submit" class="btn" value="Cancelar" />
				</fieldset>
			</form>

		<?php endif; ?>

		<div class="row">

			<!-- Item Data -->
			<article class="span10">

				<h2><?php echo $booking->fname . ' ' . $booking->lname; ?></h2>
				<br/>

				<ul class="nav nav-tabs">
					<li<?php echo ($active_tab == '' ? ' class="active"' : ''); ?>><a href="#tab_booking" data-toggle="tab">Datos del Booking</a></li>
					<?php if($booking->payments): ?>
						<li<?php echo ($active_tab == 'payments' ? ' class="active"' : ''); ?>><a href="#tab_payments" data-toggle="tab">Pagos y Recibos</a></li>
					<?php endif; ?>
					<?php if($booking->comments || $booking->status->item_id > 2): ?>
						<li<?php echo ($active_tab == 'comments' ? ' class="active"' : ''); ?>><a href="#tab_comments" data-toggle="tab">Comentarios</a></li>
					<?php endif; ?>
					<li<?php echo ($active_tab == 'documents' ? ' class="active"' : ''); ?>><a href="#tab_documents" data-toggle="tab">Documentos</a></li>
				</ul>

				<div class="tab-content">

					<div class="tab-pane row<?php echo ($active_tab == '' ? ' active' : ''); ?>" id="tab_booking">

						<dl class="dl-horizontal">

							<?php if($booking->tax_id): ?>
								<dt>Nº DNI/Passaporte</dt>
								<dd><?php echo $booking->tax_id; ?></dd>
							<?php endif; ?>

							<dt>Email</dt>
							<dd><a href="mailto:<?php echo $booking->email; ?>"><?php echo $booking->email; ?></a></dd>

							<?php if($booking->mobile): ?>
								<dt>Teléfono Móvil</dt>
								<dd><?php echo $booking->mobile; ?></dd>
							<?php endif; ?>

							<?php if($booking->full_address): ?>
								<dt>Dirección</dt>
								<dd><address><?php echo $booking->full_address; ?></address></dd>
							<?php endif; ?>

							<dt>Nº Referencia</dt>
							<dd><?php echo $booking->reference_num; ?></dd>

							<dt>Estado</dt>
							<dd>
								<?php if(!$booking->is_active): ?>
									<span class="label">Cancelado</span>
								<?php else: ?>
									<span class="<?php echo $booking->status->class_name; ?>"><?php echo $booking->status->name; ?></span>
								<?php endif; ?>
							</dd>

							<?php if($booking->date_offered): ?>
								<dt>Fecha Ofertada</dt>
								<dd><?php echo $booking->date_offered; ?></dd>
							<?php endif; ?>

							<?php if($booking->date_booked): ?>
								<dt>Fecha del Booking</dt>
								<dd><?php echo $booking->date_booked; ?></dd>
							<?php endif; ?>

							<dt>Vivienda</dt>
							<dd><a href="<?php echo $booking->home->view_url; ?>"><?php echo $booking->home->name; ?></a></dd>

							<dt>Fechas Estancia</dt>
							<dd><?php echo $booking->date_from . ' - ' . $booking->date_to; ?></dd>

							<?php if($booking->arrival_time): ?>
								<dt>Hora de Llegada</dt>
								<dd><?php echo $booking->arrival_time; ?></dd>
							<?php endif; ?>

							<?php if($booking->airport->item_id): ?>
								<dt>Aeropuerto de Llegada</dt>
								<dd><?php echo $booking->airport->name; ?></dd>
							<?php endif; ?>

							<dt>Nº Adultos</dt>
							<dd><?php echo $booking->adults; ?></dd>

							<dt>Nº Niños</dt>
							<dd><?php echo $booking->children; ?></dd>

							<dt>Cunas</dt>
							<dd><?php echo $booking->cribs; ?></dd>

							<?php if($booking->agreed_price): ?>
								<dt>Precio Acordado</dt>
								<dd><?php echo $booking->agreed_price; ?> €</dd>
							<?php endif; ?>

							<?php if($booking->internet_cost): ?>
								<dt>Gastos Internet</dt>
								<dd><?php echo $booking->internet_cost; ?> €</dd>
							<?php endif; ?>

							<?php if($booking->other_cost): ?>
								<dt>Otros Gastos</dt>
								<dd><?php echo $booking->other_cost; ?> €</dd>
							<?php endif; ?>

							<?php if($booking->down_payment): ?>
								<dt>Cantidad de Fianza</dt>
								<dd><?php echo $booking->down_payment; ?> €</dd>
							<?php endif; ?>
							
							<?php if($booking->payment_type->item_id): ?>
								<dt>Tipo de Pago</dt>
								<dd><?php echo $booking->payment_type->name; ?></dd>
							<?php endif; ?>

							<?php if($booking->notes): ?>
								<dt>Notas Adicionales</dt>
								<dd><?php echo $booking->notes; ?></dd>
							<?php endif; ?>

							<?php if($booking->booking_source): ?>
								<dt>Origen del Booking</dt>
								<dd><?php echo $booking->booking_source; ?></dd>
							<?php endif; ?>

						</dl>

					</div>

					<?php if($booking->payments): ?>

						<div class="tab-pane<?php echo ($active_tab == 'payments' ? ' active' : ''); ?>" id="tab_payments">

							<table class="table table-bordered table-striped" id="payments">
								<thead>
									<tr>
										<th>Tipo</th>
										<th width="70">Cantidad</th>
										<th width="70">Pendiente</th>
										<th width="70">Pagado</th>
										<th width="108">Gestionar Pagos</th>
										<th width="50">Recibo Español</th>
										<th width="50">Recibo Inglés</th>
										<th width="70">Liquidado</th>
										<th width="90">Gestionar Liquidaciones</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($booking->payments as $payment): ?>
										<tr>
											<td>Pago (<?php echo $payment->percentage; ?>%)</td>
											<td><?php echo $payment->amount; ?> €</td>
											<td><?php echo $payment->date_due; ?></td>
											<td class="date_paid"><?php echo $payment->date_paid; ?></td>
											<td>
												<nav class="btn-group">
													<a href="<?php echo $payment->pay_url; ?>" class="btn add_payment<?php echo ($payment->receipt_id ? ' disabled' : ''); ?>" title="Añadir Pago y Generar Recibo"><i class="icon-plus"></i></a>
													<a href="<?php echo $payment->edit_url; ?>" class="btn edit_receipt<?php echo (!$payment->receipt_id ? ' disabled' : ''); ?>" title="Editar Recibo"><i class="icon-pencil"></i></a>
													<a href="<?php echo $payment->unpay_url; ?>" class="btn delete_payment<?php echo (!$payment->receipt_id ? ' disabled' : ''); ?>" title="Borrar Recibo"><i class="icon-trash"></i></a>
												</nav>
											</td>
											<td><a href="<?php echo $payment->view_url_es; ?>" lang="es" target="_blank" class="btn<?php echo (!$payment->receipt_id ? ' disabled' : ''); ?>" title="Imprimir recibo en español"><i class="icon-print"></a></td>
											<td><a href="<?php echo $payment->view_url_en; ?>" lang="en" target="_blank" class="btn<?php echo (!$payment->receipt_id ? ' disabled' : ''); ?>" title="Imprimir recibo en inglés"><i class="icon-print"></a></td>
											<td class="date_liquidated"><?php echo $payment->date_liquidated; ?></td>
                      <td>
	                      <nav class="btn-group">
												<?php
													$liquidation_done = ($payment->date_liquidated != null && $payment->date_liquidated != "-" && $payment->date_liquidated != '');
												?>
	                          <a href="<?php echo $payment->liquidate_url; ?>" class="btn add_liquidation<?php echo ((!$payment->receipt_id or $liquidation_done) ? ' disabled' : ''); ?>" title="Añadir Liquidación"><i class="icon-plus"></i></a>
	                          <a href="<?php echo $payment->delete_liquidation_url; ?>" class="btn delete_liquidation<?php echo ((!$payment->receipt_id or !$liquidation_done) ? ' disabled' : ''); ?>" title="Borrar Liquidación"><i class="icon-trash"></i></a>
	                      </nav>

	                  </td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>

						</div>

					<?php endif; ?>

					<?php if($booking->comments || $booking->status->item_id > 2): ?>

						<div class="tab-pane<?php echo ($active_tab == 'comments' ? ' active' : ''); ?>" id="tab_comments">

							<?php if($booking->comments): ?>

								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Cliente</th>
											<th width="110">Fecha</th>
											<th width="75">Opciones</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($booking->comments as $comment): ?>
										<tr>
											<td><a href="<?php echo $comment->view_url; ?>"><?php echo $comment->name; ?></a></td>
											<td><?php echo $comment->date_comment; ?></td>
											<td>
												<nav class="btn-group">
													<a class="btn" rel="tooltip" href="<?php echo $comment->edit_url; ?>" title="Editar"><i class="icon-pencil"></i></a>
													<a class="btn" rel="tooltip" href="<?php echo $comment->delete_url; ?>" title="Borrar"><i class="icon-trash"></i></a>
												</nav>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>

							<?php endif; ?>

							<a href="<?php echo $add_comment_url; ?>" class="btn"><i class="icon-plus"></i> Añadir Comentario</a>

						</div>

					<?php endif; ?>

					<div class="tab-pane<?php echo ($active_tab == 'documents' ? ' active' : ''); ?>" id="tab_documents">

						<?php if($booking->conditions->item_id): ?>

							<div class="page-subheader">
								<h4>Documentos Automáticos</h4>
							</div>

							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Documento</th>
										<th width="72">En Español</th>
										<th width="72">En Inglés</th>
										<th width="72">Opciones</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><?php echo $booking->conditions->name; ?></td>
										<td><a href="<?php echo $booking->conditions->view_url_es; ?>" target="_blank" class="btn" title="Imprimir versión en español"><i class="icon-print"></a></td>
										<td><a href="<?php echo $booking->conditions->view_url_en; ?>" target="_blank" class="btn" title="Imprimir versión en inglés"><i class="icon-print"></a></td>
										<td><a href="<?php echo $booking->conditions->edit_url; ?>" class="btn" title="Editar Condiciones"><i class="icon-pencil"></i></a></td>
									</tr>
								</tbody>
							</table>

						<?php endif; ?>

						<div class="page-subheader">
								<h4>Documentos Adjuntas</h4>
							</div>

						<table class="table table-bordered table-striped" id="document_list">
							<thead>
								<tr>
									<th>Documento</th>
									<th width="72">Opciones</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($booking->documents as $document): ?>
								<tr>
									<td><?php echo $document->filename; ?></td>
									<td>
										<nav class="btn-group">
											<a href="<?php echo $document->download_url; ?>" class="btn download" title="Descargar el documento"><i class="icon-download"></i></a>
											<a href="<?php echo $document->delete_url; ?>" class="btn delete" title="Borrar el documento"><i class="icon-trash"></i></a>
										</nav>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>

						<form name="document_upload_form" id="document_form" enctype="multipart/form-data" method="post" action="<?php echo $add_document_url; ?>">
							<fieldset class="row">
								<div class="span9 file_upload">
									<div class="well">
										<label for="document_file">Solo PDF, JPG, o PNG. Tamaño máximo 2mb por archivo.</label>
										<div class="control-group">
											<input type="file" name="document_file" class="span7" id="document_file" />
											<button type="button" name="document_upload" class="btn" id="document_upload"><i class="icon-upload"></i> Subir Archivo</button>
										</div>
									</div>
								</div>
							</fieldset>
						</form>

					</div>

				</div>

			</article>

			<!-- Sidebar -->
			<aside class="span2">

				<!-- Item Sub-Navigation -->
				<nav class="well">
					<ul class="nav nav-list">
						<?php if($booking->is_active): ?>
							<li class="nav-header">Convertir</li>
							<?php if($convert_to_offer_url): ?>
								<li><a href="<?php echo $convert_to_offer_url; ?>"><i class="icon-refresh"></i> Oferta</a></li>
							<?php endif; ?>
							<?php if($convert_to_prebooking_url): ?>
								<li><a href="<?php echo $convert_to_prebooking_url; ?>"><i class="icon-refresh"></i> Pre-Confirmada</a></li>
							<?php endif; ?>
							<?php if($convert_to_booking_url): ?>
								<li><a href="<?php echo $convert_to_booking_url; ?>"><i class="icon-refresh"></i> Confirmada</a></li>
							<?php endif; ?>
						<?php endif; ?>
						<li class="nav-header">Opciones</li>
						<li><a href="<?php echo $edit_url; ?>"><i class="icon-pencil"></i> Editar</a></li>
						<li><a href="<?php echo $delete_url; ?>"><i class="icon-trash"></i> Borrar</a></li>
					</ul>
				</nav>

			</aside>

		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>
