<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
	<!-- Page Specific Script(s) -->
	<script src="<?php echo base_url(); ?>assets/js/admin_homes_list.js"></script>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">
			
			<h1>Viviendas <small>Listado de Viviendas</small></h1>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li class="active">Viviendas</li>
			</ul>

		</header>
		
		<div id="response_message" class="alert alert-success alert-block hide">
			<a class="close" href="#">×</a>
			<h4 class="alert-heading"></h4>
			<p class="alert-body"></p>
		</div>
		
		<table id="homes_list" class="table sortable">
			<thead>
				<tr>
					<th>Vivienda</th>
					<th width="170">Estado</th>
					<th width="110">Opciones</th>
				</tr>
			</thead>
			<tbody>
			
				<?php foreach($homes as $home): ?>
			
					<tr id="<?php echo 'home_' . $home->item_id; ?>">
						<td><a href="<?php echo $home->view_url; ?>" title="Ver Detalle"><?php echo $home->name; ?></a></td>
						<td>
							<?php if($home->is_active): ?>
								<span class="label label-success">Activado</span>
							<?php else : ?>
								<span class="label label-important">Desctivado</span>
							<?php endif; ?>
						</td>
						<td>
							<nav class="btn-group">
								<a class="btn order" href="#" title="Cambiar Orden"><i class="icon-move"></i></a>
								<a class="btn<?php echo ($this->session->userdata('admin_role') == 1 ? ' disabled' : ''); ?>" href="<?php echo $home->edit_url; ?>" title="Editar"><i class="icon-pencil"></i></a>
								<a class="btn<?php echo ($this->session->userdata('admin_role') == 1 ? ' disabled' : ''); ?>" href="<?php echo $home->delete_url; ?>" title="Borrar"><i class="icon-trash"></i></a>
							</nav>
						</td>
					</tr>
				
				<?php endforeach; ?>
				
			</tbody>
		</table>
		
		<div class="btn-toolbar">
			<nav class="btn-group">
				<a class="btn<?php echo ($this->session->userdata('admin_role') == 1 ? ' disabled' : ''); ?>" href="<?php echo $add_url; ?>"><i class="icon-plus"></i> Añadir Vivienda</a>
				<a class="btn" id="save_order" href="<?php echo $order_url; ?>"><i class="icon-list"></i> Guardar Orden</a>
			</nav>
		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>