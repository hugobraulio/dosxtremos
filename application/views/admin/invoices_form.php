<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
	<!-- Load Page Specific SCripts -->
	<script src="<?php echo base_url(); ?>assets/js/admin_invoices_form.js"></script>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Propietarios <small><?php echo $action_title; ?></small></h1>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo site_url('admin/owners'); ?>">Propietarios</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>"><?php echo $invoice->owner->name; ?></a> <span class="divider">/</span></li>
				<li><a href="<?php echo $list_url; ?>">Facturas</a> <span class="divider">/</span></li>
				<li class="active"><?php echo $action_title; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver a la lista</a></li>
			</ul>

		</header>
		
		<!-- Item Form -->
		<form name="owner_form" method="post" action="<?php echo $action_url; ?>">
			
			<div class="page-header">
				<h3>Datos de la Factura</h3>
			</div>
			
			<fieldset class="row">
			
				<div class="span8">
					<dl class="dl-horizontal">
						<dt>Propietario</dt>
						<dd><?php echo $invoice->owner->name; ?></dd>
						<dt>NIF/CIF/NIE</dt>
						<dd><?php echo $invoice->owner->tax_id; ?></dd>
						<dt>Dirección</dt>
						<dd>
							<address>
							<?php echo $invoice->owner->address; ?><br>
							<?php echo $invoice->owner->postal_code . ' ' . $invoice->owner->city . ' (' . $invoice->owner->province . ') ' . $invoice->owner->country; ?>
							</address>
						</dd>
					</dl>
				</div>
				
				<div class="span2 control-group<?php if(form_error('invoice_num')) { echo ' error'; } ?>">
					<label for="invoice_num">Nº Factura *</label>
					<input
						type="text"
						name="invoice_num"
						class="span2"
						id="invoice_num"
						placeholder="000000"
						value="<?php echo set_value('invoice_num', $invoice->invoice_num); ?>"
					/>
					<?php echo form_error('invoice_num'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('invoice_date')) { echo ' error'; } ?>">
					<label for="invoice_date">Fecha Factura *</label>
					<input
						type="text"
						name="invoice_date"
						class="span2 datepicker"
						id="invoice_date"
						placeholder="dd/mm/aaaa"
						value="<?php echo set_value('invoice_date', $invoice->invoice_date); ?>"
					/>
					<?php echo form_error('invoice_date'); ?>
				</div>
			
			</fieldset>
			
			<fieldset class="row">
				
				<div class="span12 control-group<?php if(form_error('invoice_description')) { echo ' error'; } ?>">
					<label for="invoice_description">Descripción *</label>
					<textarea
						rows="4"
						name="invoice_description"
						class="span12"
						id="invoice_description"
						placeholder="Incluye una descripción de la factura"
					><?php echo set_value('invoice_description', $invoice->invoice_description); ?></textarea>
					<?php echo form_error('invoice_description'); ?>
				</div>
				
			</fieldset>
			
			<div class="page-header">
				<h3>Comisiones y Gastos incluidos en la factura</h3>
			</div>
			
			<table class="table table-striped line_items" id="line_items">
				<thead>
					<tr>
						<th>Descripción</th>
						<th width="70">Precio</th>
						<th width="70">Tipo IVA</th>
						<th width="70">IVA</th>
						<th width="70">Total</th>
						<th width="60">Borrar</th>
					</tr>
				</thead>
				<tbody>
				
					<?php foreach($invoice->line_items as $line_item): ?>
						<tr
							data-item_id="<?php echo $line_item->item_id; ?>"
							data-net_amount="<?php echo $line_item->net_amount; ?>"
							data-vat_percent="<?php echo $line_item->vat_percent; ?>"
							data-vat_amount="<?php echo $line_item->vat_amount; ?>"
							data-total_amount="<?php echo $line_item->total_amount; ?>"
							data-payment_id="<?php echo $line_item->payment_id; ?>"
							data-expense_id="<?php echo $line_item->expense_id; ?>"
						>
							<td>
								<input
									type="text"
									name="line_item_0"
									id="line_item_0"
									class="span6 description"
									placeholder="Incluye una descripción breve"
									value="<?php echo $line_item->description; ?>"
								/>
							</td>
							<td><?php echo $line_item->net_amount; ?> €</td>
							<td><?php echo $line_item->vat_percent; ?> %</td>
							<td><?php echo $line_item->vat_amount; ?> €</td>
							<td><?php echo $line_item->total_amount; ?> €</td>
							<td>
								<nav class="btn-group">
									<a class="btn remove_row" href="#" title="Borrar"><i class="icon-trash"></i></a>
								</nav>
							</td>
						</tr>
					<?php endforeach; ?>
					
				</tbody>
				<tfoot>
					<tr>
						<th>Totales</th>
						<td><span id="net_totals"><?php echo $invoice->net_amount; ?></span> €</td>
						<td>&nbsp;</td>
						<td><span id="vat_totals"></span><?php echo $invoice->vat_amount; ?> €</td>
						<td><strong><span id="invoice_totals"><?php echo $invoice->total_amount; ?></span> €</strong></td>
						<td>&nbsp;</td>
					</tr>
				</tfoot>
			</table>
			
			<input type="hidden" name="net_amount" id="net_amount" value="<?php echo $invoice->net_amount; ?>" />
			<input type="hidden" name="vat_amount" id="vat_amount" value="<?php echo $invoice->vat_amount; ?>" />
			<input type="hidden" name="total_amount" id="total_amount" value="<?php echo $invoice->total_amount; ?>" />
			
			<input type="hidden" name="line_item_ids" id="line_item_ids" value="<?php echo $invoice->line_item_ids; ?>" />
			<input type="hidden" name="line_descriptions" id="line_descriptions" value="<?php echo $invoice->line_descriptions; ?>" />
			<input type="hidden" name="line_net_amounts" id="line_net_amounts" value="<?php echo $invoice->line_net_amounts; ?>" />
			<input type="hidden" name="line_vat_percentages" id="line_vat_percentages" value="<?php echo $invoice->line_vat_percentages; ?>" />
			<input type="hidden" name="line_vat_amounts" id="line_vat_amounts" value="<?php echo $invoice->line_vat_amounts; ?>" />
			<input type="hidden" name="line_total_amounts" id="line_total_amounts" value="<?php echo $invoice->line_total_amounts; ?>" />
			<input type="hidden" name="line_payment_ids" id="line_payment_ids" value="<?php echo $invoice->line_payment_ids; ?>" />
			<input type="hidden" name="line_expense_ids" id="line_expense_ids" value="<?php echo $invoice->line_expense_ids; ?>" />
			
			<fieldset class="form-actions">
				
				<input type="submit" name="submit" class="btn btn-primary" id="save" value="Guardar" />
				<input type="submit" name="submit" class="btn" id="cancel" value="Cancelar" />
				<span class="help-inline">Los campos marcados con un asterisco (*) son obligatórias.</span>
				
			</fieldset>
			
		</form>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>