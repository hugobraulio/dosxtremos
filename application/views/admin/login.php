<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body class="login">

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<form class="login_form" name="admin_login" method="post" action="<?php echo site_url('/admin/sessions/authenticate'); ?>">
		
			<div class="page-header">
				<h1>DX Gestión<br/><small>Área Administrativa</small></h1>
			</div>
			
			<?php if(isset($failed) && $failed != ''): ?>
				<div class="alert alert-error">
					<h3>Error</h3>
					<p>Su nombre de usuario y/o clave son inválidos. Por favor intente de nuevo o contacte el administrador del sistema.</p>
				</div>
			<?php endif; ?>
			
			<fieldset>
				<label for="username">Nombre de Usuario</label>
				<input type="text" class="span4" name="username" id="username" placeholder="Su nombre de usuario" />
				<label for="password">Contraseña</label>
				<input type="password" class="span4" name="password" id="password" placeholder="Su Contraseña" />
			</fieldset>
			
			<fieldset>
				<input type="submit" name="submit" class="btn btn-primary" id="submit" value="Entrar" />
			</fieldset>
		
		</form>

	</section>
	

</body>

</html>