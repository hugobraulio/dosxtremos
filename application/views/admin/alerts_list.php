
	<div class="modal hide fade" id="alert_modal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h2><span id="alert_num"><?php echo $num_alerts; ?></span> Alertas</h2>
		</div>
		<div class="modal-body">
			
			<?php foreach($alerts as $alert): ?>
			
				<div class="alert alert-error">
					<a class="close" data-dismiss="alert" href="<?php echo $alert->active_url; ?>">×</a>
					<h3 class="alert-heading"><?php echo $alert->type_name; ?></h3>
					<dl class="dl-horizontal">
						<dt>Booking</dt>
						<dd><a href="<?php echo $alert->booking_url; ?>"><?php echo $alert->booking_reference_num; ?></a></dd>
						<?php if($alert->payment_date_due): ?>
							<dt>Fecha</dt>
							<dd><?php echo $alert->payment_date_due; ?></dd>
						<?php endif; ?>
						<dt>Cliente</dt>
						<dd><?php echo $alert->booking_name; ?></dd>
						<dt>Teléfono</dt>
						<dd><?php echo $alert->booking_mobile; ?></dd>
						<dt>Email</dt>
						<dd><a href="<?php echo $alert->booking_email_url; ?>"><?php echo $alert->booking_email; ?></a></dd>
					</dl>
				</div>
			
			<?php endforeach; ?>
			
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Cerrar</a>
		</div>
	</div>