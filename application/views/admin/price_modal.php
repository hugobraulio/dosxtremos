
	<div class="modal hide fade" id="homes_calendar_modal_edit">
	
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Editar precios para 30 Dic.</h3>
		</div>
		
		<form name="home_date_price" method="post" action="#">
		
			<fieldset class="modal-body">
				
				<div class="row">
				
					<div class="span2 control-group">
						<label for="asking_price">Precio Fijado</label>
						<div class="input-append">
							<input type="text" name="asking_price" class="span2 currency" id="asking_price" placeholder="0.00" value="" /><span class="add-on">€</span>
						</div>
					</div>
					
					<div class="span2 control-group">
						<label for="estimated_price">Precio Estimado</label>
						<div class="input-append">
							<input type="text" name="estimated_price" class="span2 currency" id="estimated_price" placeholder="0.00" value="" /><span class="add-on">€</span>
						</div>
					</div>
				
				</div>
				
			</fieldset>
			
			<fieldset class="modal-footer clearfix">
			
				<input type="submit" name="submit" class="btn btn-primary pull-left" id="save" value="Guardar" />
				<a href="#" class="btn pull-right" data-dismiss="modal">Cerrar</a>
				
			</fieldset>
		
		</form>
		
	</div>