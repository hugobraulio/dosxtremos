
	<div class="modal hide fade <?php echo $timespan->status->permalink; ?>">
	
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h5><?php echo $timespan->home->name; ?></h5>
			<h3><?php echo $timespan->name; ?></h3>
		</div>
		
		<div class="modal-body">

			<dl class="dl-horizontal">
				<?php if($timespan->agreed_price): ?>
					<dt>Acordado</dt>
					<dd>
						<?php echo $timespan->agreed_price; ?> &euro;
						<?php if($timespan->booked_dates): ?>para booking del <?php echo $timespan->booked_dates; ?><?php endif; ?>
					</dd>
				<?php else: ?>
					<dt>Precio</dt>
					<dd><?php echo $timespan->price; ?> &euro;</dd>
				<?php endif; ?>
				<dt>Estimado</dt>
				<dd><?php echo $timespan->estimate; ?> &euro;</dd>
			</dl>
			
			<!-- Timespan Days Table -->
			<table class="table calendar table-striped table-bordered">
				<thead>
					<tr>
						<th>Día</th>
						<th width="250">Bookings</th>
						<th width="250">Ofertas</th>
					</tr>
				</thead>
				<tbody>
					
					<?php foreach($timespan->days as $day): ?>
					
						<tr>
							<td><?php echo $day->name; ?></td>
							<td>
								<?php if($day->booking): ?>
									<a class="<?php echo $day->booking->status->permalink; ?>" href="<?php echo $day->booking->view_url; ?>">
										<i class="icon-user"></i> <?php echo $day->booking->name; ?>
									</a>
								<?php else: ?>
									<span class="placeholder">Sin Booking</span>
								<?php endif; ?>
							</td>
							<td>
								<?php if($day->offers): ?>
									<ul class="offer_list clearfix">
										<?php foreach($day->offers as $offer): ?>
											<li>
												<a class="<?php echo $offer->status->permalink; ?>" href="<?php echo $offer->view_url; ?>">
													<i class="icon-user"></i> <?php echo $offer->name; ?>
												</a>
											</li>
										<?php endforeach; ?>
									</ul>
								<?php else: ?>
									<span class="placeholder">Sin Ofertas</span>
								<?php endif; ?>
							</td>
						</tr>
					
					<?php endforeach; ?>
					
				</tbody>
			</table>
			
		</div>
		
		<div class="modal-footer clearfix">
			
			<?php if($timespan->status->item_id <= 1): ?>
				<div class="btn-group pull-left">
					<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">Añadir <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo $add_offer_url; ?>">Oferta</a></li>
						<li><a href="<?php echo $add_booking_url; ?>">Booking</a></li>
					</ul>
				</div>
			<?php endif; ?>
			
			<a href="#" class="btn pull-right" data-dismiss="modal">Cerrar</a>
			
		</div>
		
	</div>
	