<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Booking <small>Comentario de <?php echo $comment->name; ?></small></h1>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>">Booking Nº <?php echo $comment->booking->reference_num; ?></a> <span class="divider">/</span></li>
				<li><a href="<?php echo $list_url; ?>">Comentarios</a> <span class="divider">/</span></li>
				<li>Comentario de <?php echo $comment->name; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver a la lista</a></li>
			</ul>

		</header>
		
		<?php if($delete_item): ?>
		
			<form class="alert alert-error" method="post" action="<?php echo $delete_url; ?>">
				<h4 class="alert-heading">¡Aviso!</h4>
				<p>¿Esta seguro que quiere seguir? Si proceda, borrará por completo este comentario</p>
				<fieldset>
					<input type="submit" name="submit" class="btn btn-danger" value="Borrar" />
					<input type="submit" name="submit" class="btn" value="Cancelar" />
				</fieldset>
			</form>
		
		<?php endif; ?>
		
		<div class="row">
			
			<!-- Item Data -->
			<article class="span9">
				
				<h2><?php echo $comment->name; ?></h2>
				
				<dl class="dl-horizontal">
					<dt>Booking</dt>
					<dd><a href="<?php echo $parent_url; ?>">Nº <?php echo $comment->booking->reference_num; ?></a> (<?php echo $comment->booking->date_from; ?> - <?php echo $comment->booking->date_to; ?>)</dd>
					<dt>Fecha</dt>
					<dd><?php echo $comment->date_comment; ?></dd>
					<dt>Comentario</dt>
					<dd><?php echo $comment->client_comment; ?></dt>
					<?php if($comment->admin_comment): ?>
						<dt>Administrador</dt>
						<dd><?php echo $comment->admin_comment; ?></dd>
					<?php endif; ?>
				</dl>
				
			</article>
			
			<!-- Sidebar -->
			<aside class="span3">
				
				<!-- Item Sub-Navigation -->
				<nav class="well">
					<ul class="nav nav-list">
						<li class="nav-header">Opciones</li>
						<li><a href="<?php echo $edit_url; ?>"><i class="icon-pencil"></i> Editar</a></li>
						<li><a href="<?php echo $delete_url; ?>"><i class="icon-trash"></i> Borrar</a></li>
					</ul>
				</nav>
			
			</aside>
			
		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>