<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">
			
			<h1>Viviendas <small>Añadir Comentario Nuevo</small></h1>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>">Booking Nº <?php echo $comment->booking->reference_num; ?></a> <span class="divider">/</span></li>
				<li><a href="<?php echo $list_url; ?>">Comentarios</a> <span class="divider">/</span></li>
				<li class="active"><?php echo $action_title; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver a la lista</a></li>
			</ul>

		</header>
		
		<!-- Item Form -->
		<form name="comment_form" method="post" action="<?php echo $action_url; ?>">
			
			<div class="page-header">
				<h3>Datos del Comentario</h3>
			</div>
			
			<fieldset class="row">
				
				<div class="span10 control-group<?php if(form_error('name')) { echo ' error'; } ?>">
					<label for="name">Nombre *</label>
					<input
						type="text"
						name="name"
						class="span10"
						id="name"
						placeholder="El nombre del cliente"
						value="<?php echo set_value('name', $comment->name); ?>"
					/>
					<?php echo form_error('name'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('date_comment')) { echo ' error'; } ?>">
					<label for="date_complete">Fecha del Comentario *</label>
					<input
						type="text"
						name="date_comment"
						class="span2 datepicker"
						id="date_comment"
						placeholder="dd/mm/aaaa"
						value="<?php echo set_value('date_comment', $comment->date_comment); ?>"
					/>
					<?php echo form_error('date_comment'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="row">
				
				<div class="span12 control-group<?php if(form_error('client_comment')) { echo ' error'; } ?>">
					<label for="client_comment">Comentario del Cliente *</label>
					<textarea
						name="client_comment"
						class="span12"
						id="client_comment"
						rows="12"
						placeholder="Incluye el comentario del cliente aquí"><?php echo set_value('client_comment', $comment->client_comment); ?></textarea>
						<?php echo form_error('client_comment'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="row">
				
				<div class="span12 control-group<?php if(form_error('admin_comment')) { echo ' error'; } ?>">
					<label for="admin_comment">Comentario del Administrador</label>
					<textarea
						name="admin_comment"
						class="span12"
						id="admin_comment"
						rows="12"
						placeholder="Opcionalmente, incluye un comentario del administrador aquí"><?php echo set_value('admin_comment', $comment->admin_comment); ?></textarea>
						<?php echo form_error('admin_comment'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="form-actions">
				
				<input type="submit" name="submit" class="btn btn-primary" id="save" value="Guardar" />
				<input type="submit" name="submit" class="btn" id="cancel" value="Cancelar" />
				<span class="help-inline">Los campos marcados con un asterisco (*) son obligatórias.</span>
				
			</fieldset>
			
		</form>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>