<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
	<!-- Load Page Specific Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/admin_homes_view.js"></script>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Viviendas <small><?php echo $home->name; ?></small></h1>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo site_url('admin/homes'); ?>">Viviendas</a> <span class="divider">/</span></li>
				<li class="active"><?php echo $home->name; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver a la lista</a></li>
			</ul>

		</header>
		
		<?php if($delete_item): ?>
		
			<form class="alert alert-error" method="post" action="<?php echo $delete_url; ?>">
				<h4 class="alert-heading">¡Aviso!</h4>
				<p>¿Esta seguro que quiere seguir? Si proceda, borrará por completo esta vivienda del sistema incluyendo todos los datos asociados a la vivienda como bookings, tareas de mantenimiento y comentarios.</p>
				<fieldset>
					<input type="submit" name="submit" class="btn btn-danger" value="Borrar" />
					<input type="submit" name="submit" class="btn" value="Cancelar" />
				</fieldset>
			</form>
		
		<?php endif; ?>
		
		<div class="row">
			
			<!-- Item Data -->
			<article class="span9">
				
				<h2>
					<?php echo $home->name; ?>
					<?php if($home->is_active): ?>
						<span id="active_tag" class="label label-success">Activado</span>
					<?php else: ?>
						<span id="active_tag" class="label label-important">Desactivado</span>
					<?php endif; ?>
				</h2>
				
				<dl class="dl-horizontal">
				
					<dt>Nombre</dt>
					<dd><?php echo $home->name; ?></dd>
					
					<dt>Dirección</dt>
					<dd>
						<address>
						<?php 
							echo $home->address . '<br>';
							echo $home->postal_code . ' ' . $home->city . ' (' . $home->province . ') ES';
						?> 
						</address>
					</dd>
					
					<dt>Código Referencia</dt>
					<dd><?php echo $home->code; ?></dd>
					
					<?php if($home->owner->item_id): ?>
						<dt>Propietario</dt>
						<dd><a href="<?php echo $home->owner->view_url; ?>"><?php echo $home->owner->name; ?></a></dd>
					<?php endif; ?>
					
					<dt>Comisión</dt>
					<dd><?php echo $home->commission_percent; ?>%</dd>
					
					<dt>Capacidad</dt>
					<dd><?php echo $home->capacity; ?> personas</dd>
					
					<?php if($home->notes): ?>
						<dt>Notas Adicionales</dt>
						<dd><?php echo $home->notes; ?></dd>
					<?php endif; ?>
				</dl>
				
			</article>
			
			<!-- Sidebar -->
			<aside class="span3">
				
				<!-- Item Sub-Navigation -->
				<nav class="well">
					<ul class="nav nav-list">
						<li class="nav-header">Secciones</li>
						<li><a href="<?php echo $calendar_url; ?>"><i class="icon-calendar"></i> Calendario</a></li>
						<li><a href="<?php echo $prices_url; ?>"><i class="icon-tag"></i> Precios</a></li>
						<li><a href="<?php echo $maintenance_url; ?>"><i class="icon-wrench"></i> Gastos Adelantados</a></li>
						<li><a href="<?php echo $comments_url; ?>"><i class="icon-comment"></i> Comentarios</a></li>
						<li><a href="<?php echo $documents_url; ?>"><i class="icon-file"></i> Documentos</a></li>
						<?php if($map_url): ?>
							<li><a href="<?php echo $map_url; ?>"><i class="icon-map-marker"></i> Mapas</a></li>
						<?php endif; ?>
						<?php if($this->session->userdata('admin_role') > 1): ?>
							<li class="nav-header">Opciones</li>
							<li><a href="<?php echo $edit_url; ?>"><i class="icon-pencil"></i> Editar</a></li>
							<li>
								<a id="item_is_active_link" href="<?php echo $status_url; ?>">
									<?php if($home->is_active): ?>
										<i class="icon-eye-close"></i> <span class="display_text">Desactivar</span>
									<?php else: ?>
										<i class="icon-eye-open"></i> <span class="display_text">Activar</span>
									<?php endif; ?>
								</a>
							</li>
							<li><a href="<?php echo $delete_url; ?>"><i class="icon-trash"></i> Borrar</a></li>
						<?php endif; ?>
					</ul>
				</nav>
			
			</aside>
			
		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>