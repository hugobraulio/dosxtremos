<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Usuarios <small><?php echo $user->fname . ' ' . $user->lname; ?></small></h1>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo site_url('admin/users'); ?>">Usuarios</a> <span class="divider">/</span></li>
				<li class="active"><?php echo $user->fname . ' ' . $user->lname; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver a la lista</a></li>
			</ul>

		</header>
		
		<?php if($delete_item): ?>
		
			<form class="alert alert-error" method="post" action="<?php echo $delete_url; ?>">
				<h4 class="alert-heading">¡Aviso!</h4>
				<p>¿Esta seguro que quiere seguir? Si proceda, borrará por completo este usuario del sistema y no podrá acceder a la área administrativa.</p>
				<fieldset>
					<input type="submit" name="submit" class="btn btn-danger" value="Borrar" />
					<input type="submit" name="submit" class="btn" value="Cancelar" />
				</fieldset>
			</form>
		
		<?php endif; ?>
		
		<div class="row">
			
			<!-- Item Data -->
			<article class="span9">
				
				<h2><?php echo $user->fname . ' ' . $user->lname; ?></h2>
				
				<dl class="dl-horizontal">
					
					<dt>Email</dt>
					<dd><a href="mailto:<?php echo $user->email; ?>"><?php echo $user->email; ?></a></dd>
					
					<dt>Username</dt>
					<dd><?php echo $user->username; ?></dd>
					
					<dt>Password</dt>
					<dd><?php echo $user->password; ?></dd>
					
					<dt>Nivel</dt>
					<dd><?php echo $user->role->name; ?></dd>
					
				</dl>
				
			</article>
			
			<!-- Sidebar -->
			<aside class="span3">
				
				<!-- Item Sub-Navigation -->
				<nav class="well">
					<ul class="nav nav-list">
						<li class="nav-header">Opciones</li>
						<li><a href="<?php echo $edit_url; ?>"><i class="icon-pencil"></i> Editar</a></li>
						<li><a href="<?php echo $delete_url; ?>"><i class="icon-trash"></i> Borrar</a></li>
					</ul>
				</nav>
			
			</aside>
			
		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>