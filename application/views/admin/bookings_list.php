<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>

	<!-- Page Specific Script(s) -->
	<script src="<?php echo base_url(); ?>assets/js/admin_bookings_list.js" charset="utf-8"></script>

</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">

		<header class="page-header clearfix">

			<h1 class="pull-left">Calendario de Bookings : <?php echo $calendar->year; ?></h1>

			<nav class="btn-group pull-right">
				<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
				<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
			</nav>

		</header>

		<!-- Include the calendar legend -->
		<?php $this->load->view('includes/admin_calendar_legend'); ?>

		<div id="scroll-table">

			<div class="fixed-column">
				<table class="ghost-header" id="dates-header">
					<thead>
						<tr>
							<th>
								<nav class="btn-group">
									<a class="btn btn-mini scroll-left" href="#"><i class="icon-arrow-left"></i></a>
									<a class="btn btn-mini scroll-right" href="#"><i class="icon-arrow-right"></i></a>
								</nav>
							</th>
						</tr>
					</thead>
				</table>
				<table>
					<thead>
						<tr>
							<th class="nav_head">
								<nav class="btn-group">
									<a class="btn scroll-left" href="#"><i class="icon-arrow-left"></i></a>
									<a class="btn scroll-right" href="#"><i class="icon-arrow-right"></i></a>
								</nav>
							</th>
						</tr>
					</thead>
					<tbody>
						<!-- Output the weeks names -->
						<?php foreach($calendar->weeks as $week): ?>
							<tr>
								<th><?php echo $week->name; ?></th>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>

			<div class="scroll-pane">
				<table class="ghost-header" id="homes-header">
					<thead>
						<tr>
							<!-- Output the homes names -->
							<?php foreach($calendar->homes as $home): ?>
								<th><?php echo $home->name; ?></th>
							<?php endforeach; ?>
						</tr>
					</thead>
				</table>
				<table>
					<thead>
						<tr>
							<!-- Output the homes names -->
							<?php foreach($calendar->homes as $home): ?>
								<th><a href="<?php echo $home->calendar_url; ?>"><?php echo $home->name; ?></a></th>
							<?php endforeach; ?>
						</tr>
					</thead>
					<tbody>
						<!-- Output the timeslots -->
						<?php foreach($calendar->weeks as $week): ?>
							<tr>
								<?php foreach($week->timeslots as $timeslot): ?>
									<td>
										<a
											data-date_start="<?php echo $timeslot->date_start; ?>"
											data-date_end="<?php echo $timeslot->date_end; ?>"
											data-home_id="<?php echo $timeslot->home->item_id; ?>"
											class="<?php echo $timeslot->view_class; ?>"
											href="<?php echo $timeslot->url; ?>">
											<?php if($timeslot->view_price == '-'): ?>
												&darr;
											<?php else: ?>
												<?php echo ($timeslot->view_price ? ($timeslot->view_price . ' &euro;') : 'Añadir Precio'); ?>
											<?php endif; ?>
										</a>
									</td>
								<?php endforeach; ?>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>

		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

	<div id="modals"></div>

</body>

</html>
