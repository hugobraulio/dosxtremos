<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
	<!-- Google Maps API -->
	<script type="text/javascript"
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5CekoylMLZBWSUqLShvrJ8FgJIv_vRSM&sensor=false&language=<?php echo $lang; ?>">
	</script>
	
	<!-- Page Script to Run Maps -->
	<script type="text/javascript">
	
		$(document).ready(function() {
			
			// Set the home location data in JS (from PHP)
			var homeLat = '<?php echo $home->latitude; ?>';
			var homeLon = '<?php echo $home->longitude; ?>';
			
			// Print Button
			var $print_button = $('#print_route').hide();
			
			// Set the map parameters
			var mapOptions = {
				center: new google.maps.LatLng(homeLat, homeLon),
				zoom: 13,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
        
			// Initialize the map and directions panel
			var map = new google.maps.Map(document.getElementById('google_map'), mapOptions);
			var directionsService = new google.maps.DirectionsService();
			var directionsDisplay = new google.maps.DirectionsRenderer();
			directionsDisplay.setMap(map);
			directionsDisplay.setPanel(document.getElementById('google_route'));
			
			// Add the home location marker
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(homeLat, homeLon),
				map: map,
				title:"<?php echo $home->name; ?>"
			});
			
			// Submission of map data
			$('#submit').click(function(e) {
				
				e.preventDefault();
				
				// Get the point of departure lat and lon 
				var departLatLon = $('#departure').val();
				
				// If it's valid, run the directions display
				if(departLatLon) {
					
					// Setup the request object
					var request = {
						origin: departLatLon,
						destination: (homeLat + ', ' + homeLon),
						travelMode: google.maps.TravelMode.DRIVING
					};
					
					// Make the request to the directions service
					directionsService.route(request, function(response, status) {
					   
						// Set the map and directions
						if (status == google.maps.DirectionsStatus.OK) {
							directionsDisplay.setDirections(response);
							$print_button.show();
						}
						
					});
					
				}
				
			});
			
			// Print Button Action
			$print_button.click(function(e){
				e.preventDefault();
				window.print();
			});
			
		});
	
	</script>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header no_print">

			<h1>Viviendas <small>Mapas <?php echo $home->name; ?></small></h1>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $list_url; ?>">Viviendas</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>"><?php echo $home->name; ?></a> <span class="divider">/</span></li>
				<li class="active">Mapas</li>
				<li class="pull-right"><a href="<?php echo $parent_url; ?>">Volver a detalle</a></li>
			</ul>

		</header>
		
		<div class="clearfix">
			
			<ul class="nav nav-tabs no_print">
				<li class="<?php echo ($lang == 'es' ? 'active' : ''); ?>"><a href="<?php echo $lang_url_es; ?>">Español</a></li>
				<li class="<?php echo ($lang == 'en' ? 'active' : ''); ?>"><a href="<?php echo $lang_url_en; ?>">Inglés</a></li>
			</ul>
			
			<div class="page-header">
				<h3>Parámetros de Ruta para llegar a <?php echo $home->name; ?> en <?php echo ($lang == 'en' ? 'Inglés' : 'Español'); ?></h3>
			</div>
			
			<?php if($home->latitude && $home->longitude): ?>
				<form class="form-inline pull-left" name="route_options" method="post" action="#">
					<fieldset>
						<select class="span3" name="departure" id="departure">
							<option value="">Desde (Aeropuertos)</option>
							<?php foreach($airports as $airport): ?>
								<option value="<?php echo $airport->latitude . ', ' . $airport->longitude; ?>"><?php echo $airport->name; ?></option>
							<?php endforeach; ?>
						</select>
						<input class="btn btn-primary" type="submit" name="submit" id="submit" value="Trazar Ruta" />
					</fieldset>
				</form>
				<nav class="btn-group pull-right">
					<a class="btn" id="print_route" href="#"><i class="icon-print"></i> Imprimir</a>
				</nav>
			<?php endif; ?>
			
		</div>
		
		<div id="google_map"></div>
		
		<div id="google_route"></div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>