<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Viviendas <small>Gastos Adelantados de <?php echo $home->name . ' : ' . $current_year; ?></small></h1>
				
				<nav class="btn-group pull-right">
					<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
					<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
				</nav>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo site_url('admin/homes'); ?>">Viviendas</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>"><?php echo $home->name; ?></a> <span class="divider">/</span></li>
				<li class="active">Gastos Adelantados <?php echo $current_year; ?></li>
				<li class="pull-right"><a href="<?php echo $parent_url; ?>">Volver a detalle</a></li>
			</ul>

		</header>
		
		<table class="table">
			<thead>
				<tr>
					<th width="120">Nº Referencia</th>
					<th>Descripción</th>
					<th width="110">Fecha Pagada</th>
					<th width="80">Importe</th>
					<th width="80">Trimestre</th>
					<th width="75">Opciones</th>
				</tr>
			</thead>
			<tbody>
			
				<?php foreach($maintenances as $maintenance): ?>
			
					<tr>
						<td><?php echo $maintenance->reference_num; ?></td>
						<td><a href="<?php echo $maintenance->view_url; ?>"><?php echo $maintenance->name; ?></a></td>
						<td><?php echo $maintenance->date_payed; ?></td>
						<td><?php echo $maintenance->amount; ?> €</td>
						<td>T<?php echo $maintenance->trimester; ?></td>
						<td>
							<nav class="btn-group">
								<a class="btn" rel="tooltip" href="<?php echo $maintenance->edit_url; ?>" title="Editar"><i class="icon-pencil"></i></a>
								<a class="btn" rel="tooltip" href="<?php echo $maintenance->delete_url; ?>" title="Borrar"><i class="icon-trash"></i></a>
							</nav>
						</td>
					</tr>
				
				<?php endforeach; ?>
				
			</tbody>
		</table>
		
		<div class="btn-toolbar">
			<nav class="btn-group">
				<a class="btn" href="<?php echo $add_url; ?>"><i class="icon-plus"></i> Añadir Gasto Adelantado</a>
			</nav>
		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>