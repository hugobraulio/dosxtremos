<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>

	<!-- Load Page Specific Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/admin_owners_expenses.js"></script>

</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">

		<header class="page-header">

			<div class="clearfix">

				<h1 class="pull-left">Propietarios <small>Liquidaciones de <?php echo $owner->fname . ' ' . $owner->lname . ' : ' . $year; ?></small></h1>

				<nav class="btn-group pull-right">
					<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
					<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
				</nav>

			</div>

			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $list_url; ?>">Propietarios</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>"><?php echo $owner->fname . ' ' . $owner->lname; ?></a> <span class="divider">/</span></li>
				<li class="active">Liquidaciones : <?php echo $year; ?></li>
				<li class="pull-right"><a href="<?php echo $parent_url; ?>">Volver a detalle</a></li>
			</ul>

		</header>

		<ul class="nav nav-tabs" id="document_tabs">
			<?php foreach($owner->homes as $home): ?>
				<li class="<?php echo ($home->sort_order == 0 ? 'active' : ''); ?>"><a href="#home_<?php echo $home->item_id; ?>"><?php echo $home->name; ?></a></li>
			<?php endforeach; ?>
		</ul>

		<div class="tab-content">

			<?php foreach($owner->homes as $home): ?>

				<section class="tab-pane<?php echo ($home->sort_order == 0 ? ' active' : ''); ?>" id="home_<?php echo $home->item_id; ?>">

					<!-- No longer necessary to include header -->
					<!-- <header class="page-header">
						<h2>Comisiones <small><?php echo $home->name; ?></small></h2>
					</header> -->

					<div class="table_holder">
						<table class="table table-bordered table-striped" id="commissions">
							<thead>
								<tr>
									<th>Llegada</th>
									<th>Nombre & Reserva</th>
									<th style="width:70px">Alquiler Acordado</th>
									<th colspan="4">Alquiler Pagado</th>
									<th colspan="4">Comisión inc IVA</th>
									<th colspan="4">Alquiler liquidado por DX</th>
								</tr>
								<tr>
									<th colspan="3">&nbsp;</th>
									<th>T1</th>
									<th>T2</th>
									<th>T3</th>
									<th>T4</th>
									<th>T1</th>
									<th>T2</th>
									<th>T3</th>
									<th>T4</th>
									<th>T1</th>
									<th>T2</th>
									<th>T3</th>
									<th>T4</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($home->bookings as $booking): ?>
									<tr>
										<td><?php echo $booking->date_from; ?></td>
										<td><a href="<?php echo $booking->view_url; ?>"><?php echo $booking->name; ?><br/>(<?php echo $booking->reference_num; ?>)</a></td>
										<td><?php echo $booking->agreed_price; ?> €</td>
										<td><?php echo $booking->t1_payments; ?></td>
										<td><?php echo $booking->t2_payments; ?></td>
										<td><?php echo $booking->t3_payments; ?></td>
										<td><?php echo $booking->t4_payments; ?></td>
										<td><?php echo $booking->t1_commissions; ?></td>
										<td><?php echo $booking->t2_commissions; ?></td>
										<td><?php echo $booking->t3_commissions; ?></td>
										<td><?php echo $booking->t4_commissions; ?></td>
										<td><?php echo $booking->t1_liquidations; ?></td>
										<td><?php echo $booking->t2_liquidations; ?></td>
										<td><?php echo $booking->t3_liquidations; ?></td>
										<td><?php echo $booking->t4_liquidations; ?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<th colspan="7">Totales por Trimestre</th>
									<td><strong><?php echo $home->bookings_totals->t1_total; ?></strong></td>
									<td><strong><?php echo $home->bookings_totals->t2_total; ?></strong></td>
									<td><strong><?php echo $home->bookings_totals->t3_total; ?></strong></td>
									<td><strong><?php echo $home->bookings_totals->t4_total; ?></strong></td>
									<td><strong><?php echo $home->bookings_totals->t1_liquidations_total; ?></strong></td>
									<td><strong><?php echo $home->bookings_totals->t2_liquidations_total; ?></strong></td>
									<td><strong><?php echo $home->bookings_totals->t3_liquidations_total; ?></strong></td>
									<td><strong><?php echo $home->bookings_totals->t4_liquidations_total; ?></strong></td>
								</tr>
							</tfoot>
						</table>
					</div>


					<br/>

					<header class="page-header">
						<h2>Gastos Adelantados <small><?php echo $home->name; ?></small></h2>
					</header>


					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Fecha</th>
								<th width="126">Referencia</th>
								<th>Descripción</th>
								<th>Proveedor</th>
								<th colspan="4">Pagado</th>
							</tr>
							<tr>
								<th colspan="4">&nbsp;</th>
								<th width="65">T1</th>
								<th width="65">T2</th>
								<th width="65">T3</th>
								<th width="65">T4</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($home->maintenances as $maintenance): ?>
								<tr>
									<td><?php echo $maintenance->date_payed; ?></td>
									<td><a href="<?php echo $maintenance->view_url; ?>"><?php echo $maintenance->reference_num; ?></a></td>
									<td><?php echo $maintenance->name; ?></td>
									<td><?php echo $maintenance->provider; ?></td>
									<td><?php echo $maintenance->t1_payments; ?></td>
									<td><?php echo $maintenance->t2_payments; ?></td>
									<td><?php echo $maintenance->t3_payments; ?></td>
									<td><?php echo $maintenance->t4_payments; ?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<th colspan="4">Totales por Trimestre</th>
								<td><strong><?php echo $home->maintenance_totals->t1_total; ?></strong></td>
								<td><strong><?php echo $home->maintenance_totals->t2_total; ?></strong></td>
								<td><strong><?php echo $home->maintenance_totals->t3_total; ?></strong></td>
								<td><strong><?php echo $home->maintenance_totals->t4_total; ?></strong></td>
							</tr>
						</tfoot>
					</table>

					<br/>

					<header class="page-header">
						<h2>Total Liquidaciones <small><?php echo $home->name; ?></small></h2>
					</header>

					<table class="table table-bordered">
						<thead>
							<tr>
								<th colspan="4">&nbsp;</th>
								<th width="65">T1</th>
								<th width="65">T2</th>
								<th width="65">T3</th>
								<th width="65">T4</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th colspan="4">Totales por Trimestre</th>
								<td><strong><?php echo $home->grand_totals->t1_total; ?></strong></td>
								<td><strong><?php echo $home->grand_totals->t2_total; ?></strong></td>
								<td><strong><?php echo $home->grand_totals->t3_total; ?></strong></td>
								<td><strong><?php echo $home->grand_totals->t4_total; ?></strong></td>
							</tr>
						</tfoot>
					</table>

				</section>

			<?php endforeach; ?>

		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

	<div id="modals"></div>

</body>

</html>
