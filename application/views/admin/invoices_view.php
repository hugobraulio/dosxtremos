<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Propietarios <small>Factura Nº <?php echo $invoice->invoice_num . ' de ' . $invoice->owner->name; ?></small></h1>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo site_url('admin/owners'); ?>">Propietarios</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>"><?php echo $invoice->owner->name; ?></a> <span class="divider">/</span></li>
				<li><a href="<?php echo $list_url; ?>">Facturas : <?php echo $current_year; ?></a> <span class="divider">/</span></li>
				<li>Factura Nº <?php echo $invoice->invoice_num; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver a la Lista</a></li>
			</ul>

		</header>
		
		<?php if($delete_item): ?>
		
			<form class="alert alert-error" method="post" action="<?php echo $delete_url; ?>">
				<h4 class="alert-heading">¡Aviso!</h4>
				<p>¿Esta seguro que quiere seguir? Si proceda, borrará por completo este factura</p>
				<fieldset>
					<input type="submit" name="submit" class="btn btn-danger" value="Borrar" />
					<input type="submit" name="submit" class="btn" value="Cancelar" />
				</fieldset>
			</form>
		
		<?php endif; ?>
		
		<div class="row">
			
			<!-- Item Data -->
			<article class="span9">
				
				<h2>Factura Nº <?php echo $invoice->invoice_num; ?></h2>
				
				<dl class="dl-horizontal">
					<dt>Fecha</dt>
					<dd><?php echo $invoice->invoice_date; ?></dd>
					<dt>Descripción</dt>
					<dd><?php echo $invoice->invoice_description; ?></dd>
					<dt>Nombre</dt>
					<dd><?php echo $invoice->owner->name; ?></dd>
					<dt>NIF/CIF/NIE</dt>
					<dd><?php echo $invoice->owner->tax_id; ?></dd>
					<dt>Dirección</dt>
					<dd>
						<address>
						<?php echo $invoice->owner->address; ?><br>
						<?php echo $invoice->owner->postal_code . ' ' . $invoice->owner->city . ' (' . $invoice->owner->province . ') ' . $invoice->owner->country; ?>
						</address>
					</dd>
				</dl>
				
				<div class="page-header">
					<h3>Comisiones y Gastos incluidos en la factura</h3>
				</div>
				
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Descripción</th>
							<th width="100">Precio</th>
							<th width="100">IVA</th>
							<th width="100">Total</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($invoice->line_items as $line_item): ?>
							<tr>
								<td><?php echo $line_item->description; ?></td>
								<td><?php echo $line_item->net_amount; ?> €</td>
								<td><?php echo $line_item->vat_amount; ?> € (<?php echo $line_item->vat_percent; ?>%)</td>
								<td><?php echo $line_item->total_amount; ?> €</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
					<tfoot>
						<tr>
							<th>Totales</th>
							<td><strong><?php echo $invoice->net_amount; ?> €</strong></td>
							<td><strong><?php echo $invoice->vat_amount; ?> €</strong></td>
							<td><strong><?php echo $invoice->total_amount; ?> €</strong></td>
						</tr>
					</tfoot>
				</table>
				
			</article>
			
			<!-- Sidebar -->
			<aside class="span3">
				
				<!-- Item Sub-Navigation -->
				<nav class="well">
					<ul class="nav nav-list">
						<li class="nav-header">Imprimir</li>
						<li><a href="<?php echo $print_url_es; ?>" target="_blank"><i class="icon-print"></i> Español</a></li>
						<li><a href="<?php echo $print_url_en; ?>" target="_blank"><i class="icon-print"></i> Inglés</a></li>
						<li class="nav-header">Opciones</li>
						<li><a href="<?php echo $edit_url; ?>"><i class="icon-pencil"></i> Editar</a></li>
						<li><a href="<?php echo $delete_url; ?>"><i class="icon-trash"></i> Borrar</a></li>
					</ul>
				</nav>
			
			</aside>
			
		</div>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>