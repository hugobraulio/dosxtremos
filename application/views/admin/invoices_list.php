<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Propietarios <small>Facturas de <?php echo $owner->fname . ' ' . $owner->lname . ' : ' . $current_year; ?></small></h1>
				
				<nav class="btn-group pull-right">
					<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
					<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
				</nav>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo site_url('admin/owners'); ?>">Propietarios</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>"><?php echo $owner->fname . ' ' . $owner->lname; ?></a> <span class="divider">/</span></li>
				<li class="active">Facturas : <?php echo $current_year; ?></li>
				<li class="pull-right"><a href="<?php echo $parent_url; ?>">Volver a detalle</a></li>
			</ul>

		</header>
		
		
		<table class="table">
			<thead>
				<tr>
					<th width="80">Nº Factura</th>
					<th width="80">Fecha</th>
					<th>Descripción</th>
					<th width="80">Trimestre</th>
					<th width="80">Cantidad</th>
					<th width="75">Opciones</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($invoices as $invoice): ?>
					<tr>
						<td><a href="<?php echo $invoice->view_url; ?>"><?php echo $invoice->invoice_num; ?></a></td>
						<td><?php echo $invoice->invoice_date; ?></td>
						<td><?php echo $invoice->invoice_description; ?></td>
						<td><?php echo $invoice->trimester; ?></td>
						<td><?php echo $invoice->total_amount; ?></td>
						<td>
							<nav class="btn-group">
								<a class="btn" rel="tooltip" href="<?php echo $invoice->edit_url; ?>" title="Editar"><i class="icon-pencil"></i></a>
								<a class="btn" rel="tooltip" href="<?php echo $invoice->delete_url; ?>" title="Borrar"><i class="icon-trash"></i></a>
							</nav>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		
		<!-- <div class="btn-toolbar">
			<nav class="btn-group">
				<a class="btn<?php echo ($add_url ? '' : ' disabled'); ?>" href="<?php echo $add_url; ?>"><i class="icon-plus"></i> Añadir Factura</a>
			</nav>
		</div> -->
		
		<div class="btn-group">
			<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-plus"></i> Crear Factura &nbsp;<span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li><a href="<?php echo $add_t1_url; ?>"><?php echo $current_year; ?> / T1</a></li>
				<li><a href="<?php echo $add_t2_url; ?>"><?php echo $current_year; ?> / T2</a></li>
				<li><a href="<?php echo $add_t3_url; ?>"><?php echo $current_year; ?> / T3</a></li>
				<li><a href="<?php echo $add_t4_url; ?>"><?php echo $current_year; ?> / T4</a></li>
				<li><a href="<?php echo $add_all_url; ?>">Todo sin facturar</a></li>
			</ul>
		</div>
				

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>