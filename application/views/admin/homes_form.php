<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Viviendas <small><?php echo $action_title; ?></small></h1>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo site_url('admin/homes'); ?>">Viviendas</a> <span class="divider">/</span></li>
				<li class="active"><?php echo $action_title; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver a la lista</a></li>
			</ul>

		</header>
		
		<!-- Item Form -->
		<form name="home_form" method="post" action="<?php echo $action_url; ?>">
			
			<div class="page-header">
				<h3>Datos de la Vivienda</h3>
			</div>
			
			<fieldset class="row">
				
				<div class="span9 control-group<?php if(form_error('name')) { echo ' error'; } ?>">
					<label for="name">Nombre *</label>
					<input
						type="text"
						name="name"
						class="span9"
						id="name"
						placeholder="Nombre de la Vivienda"
						value="<?php echo set_value('name',$home->name); ?>"
					/>
					<?php echo form_error('name'); ?>
				</div>
				
				<div class="span3 control-group<?php if(form_error('code')) { echo ' error'; } ?>">
					<label for="code">Código * <span class="help-inline"><em>(Utiliza 3 Letras)</em></span></label>
					<input
						type="text"
						name="code"
						class="span3"
						id="code"
						placeholder="AAA"
						value="<?php echo set_value('code',$home->code); ?>"
					/>
					<?php echo form_error('code'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="row">
				
				<div class="span6 control-group<?php if(form_error('address')) { echo ' error'; } ?>">
					<label for="address">Dirección *</label>
					<input
						type="text"
						name="address"
						class="span6"
						id="address"
						placeholder="Dirección de Calle"
						value="<?php echo set_value('address',$home->address); ?>"
					/>
					<?php echo form_error('address'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('city')) { echo ' error'; } ?>">
					<label for="city">Municipio *</label>
					<input
						type="text"
						name="city"
						class="span2"
						id="city"
						placeholder="Ciudad / Pueblo"
						value="<?php echo set_value('city',$home->city); ?>"
					/>
					<?php echo form_error('city'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('province')) { echo ' error'; } ?>">
					<label for="province">Provincia *</label>
					<input
						type="text"
						name="province"
						class="span2"
						id="province"
						placeholder="Provincia"
						value="<?php echo set_value('province',$home->province); ?>"
					/>
					<?php echo form_error('province'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('postal_code')) { echo ' error'; } ?>">
					<label for="postal_code">Código Postal *</label>
					<input
						type="text"
						name="postal_code"
						class="span2"
						id="postal_code"
						placeholder="00000"
						value="<?php echo set_value('postal_code',$home->postal_code); ?>"
					/>
					<?php echo form_error('postal_code'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="row">
				
				<div class="span3 control-group<?php if(form_error('latitude')) { echo ' error'; } ?>">
					<label for="latitude">Latitude</label>
					<input
						type="text"
						name="latitude"
						class="span3"
						id="latitude"
						placeholder="0.000000"
						value="<?php echo set_value('latitude',$home->latitude); ?>"
					/>
					<?php echo form_error('latitude'); ?>
				</div>
				
				<div class="span3 control-group<?php if(form_error('longitude')) { echo ' error'; } ?>">
					<label for="longitude">Longitude</label>
					<input
						type="text"
						name="longitude"
						class="span3"
						id="longitude"
						placeholder="0.000000"
						value="<?php echo set_value('longitude',$home->longitude); ?>"
					/>
					<?php echo form_error('longitude'); ?>
				</div>
				
				<div class="span3 control-group<?php if(form_error('owner_id')) { echo ' error'; } ?>">
					<label for="owner_id">Propietario *</label>
					<select
						name="owner_id"
						class="span3"
						id="owner_id">
						<option value="">Seleccionar</option>
						<?php foreach($all_owners as $owner): ?>
							<option
								value="<?php echo $owner->item_id; ?>"
								<?php echo set_select('owner_id',$owner->item_id,($owner->item_id == $home->owner->item_id)); ?>>
								<?php echo $owner->name; ?>
							</option>
						<?php endforeach; ?>
					</select>
					<?php echo form_error('owner_id'); ?>
				</div>
				
				<div class="span1 control-group<?php if(form_error('capacity')) { echo ' error'; } ?>">
					<label for="capacity">Cap. *</label>
					<select
						name="capacity"
						id="capacity"
						class="span1">
						<?php foreach($all_capacity as $capacity): ?>
							<option
								value="<?php echo $capacity; ?>"
								<?php echo set_select('capacity',$capacity,($capacity == $home->capacity)); ?>>
								<?php echo $capacity; ?>
							</option>
						<?php endforeach; ?>
					</select>
					<?php echo form_error('capacity'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('commission_percent')) { echo ' error'; } ?>">
					<label for="postal_code">Comisión *</label>
					<div class="input-append">
						<input
							type="text"
							name="commission_percent"
							class="span1 percentage"
							id="commission_percent"
							placeholder="00"
							value="<?php echo set_value('commission_percent',$home->commission_percent); ?>"
						/><span class="add-on">%</span>
					</div>
					<?php echo form_error('commission_percent'); ?>
				</div>
				
			</fieldset>
			
			<div class="page-header">
				<h3>Adicional</h3>
			</div>
			
			<fieldset class="row">
				
				<div class="span12 control-group<?php if(form_error('notes')) { echo ' error'; } ?>">
					<label for="notes">Notas Adicionales</label>
					<textarea
						name="notes"
						class="span12"
						id="notes"
						rows="12"
						placeholder="Incluye notas adicionales sobre la vivienda aquí"><?php echo set_value('notes',$home->notes); ?></textarea>
					<?php echo form_error('notes'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="form-actions">
				
				<input type="submit" name="submit" class="btn btn-primary" id="save" value="Guardar" />
				<input type="submit" name="submit" class="btn" id="cancel" value="Cancelar" />
				<span class="help-inline">Los campos marcados con un asterisco (*) son obligatórias.</span>
				
			</fieldset>
			
		</form>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>