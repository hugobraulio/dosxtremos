<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
	<!-- Load Page Specific Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/ajaxfileupload.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/admin_maintenance_form.js"></script>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">
			
			<h1>Viviendas <small><?php echo $action_title; ?></small></h1>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo site_url('admin/homes'); ?>">Viviendas</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>">Casa Almadraba</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $list_url; ?>">Gastos Adelantados</a> <span class="divider">/</span></li>
				<li class="active"><?php echo $action_title; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver a la lista</a></li>
			</ul>

		</header>
		
		<!-- Item Form -->
		<form name="maintenance_form" enctype="multipart/form-data" method="post" action="<?php echo $action_url; ?>">
			
			<div class="page-header">
				<h3>Datos del Gasto</h3>
			</div>
			
			<fieldset class="row">
				
				<div class="span10 control-group<?php if(form_error('name')) { echo ' error'; } ?>">
					<label for="name">Descripción *</label>
					<input
						type="text"
						name="name"
						class="span10"
						id="name"
						placeholder="Nombre de la tarea de mantenimiento"
						value="<?php echo set_value('name', $maintenance->name); ?>"
					/>
					<?php echo form_error('name'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('reference_num')) { echo ' error'; } ?>">
					<label for="reference_num">Nº Referencia *</label>
					<input
						type="text"
						name="reference_num"
						class="span2"
						id="reference_num"
						placeholder="AAA-000000"
						value="<?php echo set_value('reference_num', $maintenance->reference_num); ?>"
					/>
					<?php echo form_error('reference_num'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="row">
				
				<div class="span8 control-group<?php if(form_error('provider')) { echo ' error'; } ?>">
					<label for="provider">Proveedor *</label>
					<input
						type="text"
						name="provider"
						class="span8"
						id="provider"
						placeholder="El nombre del proveedor"
						value="<?php echo set_value('provider', $maintenance->provider); ?>"
					/>
					<?php echo form_error('provider'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('date_complete')) { echo ' error'; } ?>">
					<label for="date_complete">Fecha Realizada *</label>
					<input
						type="text"
						name="date_complete"
						class="span2 datepicker"
						id="date_complete"
						placeholder="dd/mm/aaaa"
						value="<?php echo set_value('date_complete', $maintenance->date_complete); ?>"
					/>
					<?php echo form_error('date_complete'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('date_payed')) { echo ' error'; } ?>">
					<label for="date_payed">Fecha Pagada *</label>
					<input
						type="text"
						name="date_payed"
						class="span2 datepicker"
						id="date_payed"
						placeholder="dd/mm/aaaa"
						value="<?php echo set_value('date_payed', $maintenance->date_payed); ?>"
					/>
					<?php echo form_error('date_payed'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="row">
				
				<div class="span8 control-group<?php if(form_error('payed_by')) { echo ' error'; } ?>">
					<label for="payed_by">Pagado por *</label>
					<input
						type="text"
						name="payed_by"
						class="span8"
						id="payed_by"
						placeholder="El nombre del pagador"
						value="<?php echo set_value('payed_by', $maintenance->payed_by); ?>"
					/>
					<?php echo form_error('payed_by'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('amount')) { echo ' error'; } ?>">
					<label for="amount">Importe *</label>
					<div class="controls">
						<div class="input-append">
							<input
								type="text"
								name="amount"
								class="span2 currency"
								id="amount"
								placeholder="0.00"
								value="<?php echo set_value('amount', $maintenance->amount); ?>" /><span class="add-on">€</span>
						</div>
						<?php echo form_error('amount'); ?>
					</div>
				</div>
				
				<div class="span2 control-group<?php if(form_error('trimester')) { echo ' error'; } ?>">
					<label for="trimester">Trimestre</label>
					<select name="trimester" class="span2" id="trimester">
						<option value="">Seleccionar</option>
						<?php foreach($all_trimesters as $trimester): ?>
							<option
								value="<?php echo $trimester->item_id; ?>"
								<?php echo set_select('trimester',$trimester->item_id,($trimester->item_id == $maintenance->trimester)); ?>>
								<?php echo $trimester->name; ?>
							</option>
						<?php endforeach; ?>
					</select>
					<?php echo form_error('trimester'); ?>
				</div>
				
			</fieldset>
			
			<div class="page-header">
				<h3>Adicional</h3>
			</div>
			
			<fieldset class="row">
				
				<div class="span12 file_upload">
					<label for="receipt_file">Archivo Adjunta</label>
					<div class="well" id="receipt_current">
						<?php if($maintenance->receipt_file): ?>
							<p class="help-block"><strong>Archivo Actual :</strong> <span class="filename"><?php echo $maintenance->receipt_file; ?></span> <a href="#" class="remove_file">[Borrar]</a></p>
						<?php endif; ?>
						<div class="control-group">
							<input type="file" name="receipt_file" class="span10" id="receipt_file" />
							<button type="button" name="receipt_upload" class="btn" id="receipt_upload"><i class="icon-upload"></i> Subir Archivo</button>
						</div>
						<input type="hidden" name="receipt_filename" id="receipt_filename" value="<?php echo $maintenance->receipt_file; ?>" />
					</div>
				</div>
				
			</fieldset>
			
			<fieldset class="row">
				
				<div class="span12 control-group<?php if(form_error('notes')) { echo ' error'; } ?>">
					<label for="notes">Notas Adicionales</label>
					<textarea
						name="notes"
						class="span12"
						id="notes"
						rows="12"
						placeholder="Incluye notas adicionales sobre la tarea de mantenimiento aquí"><?php echo set_value('notes', $maintenance->notes); ?></textarea>
					<?php echo form_error('notes'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="form-actions">
				
				<input type="submit" name="submit" class="btn btn-primary" id="save" value="Guardar" />
				<input type="submit" name="submit" class="btn" id="cancel" value="Cancelar" />
				<span class="help-inline">Los campos marcados con un asterisco (*) son obligatórias.</span>
				
			</fieldset>
			
		</form>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>