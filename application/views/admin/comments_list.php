<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Viviendas <small>Comentarios de <?php echo $home->name . ' : ' . $current_year; ?></small></h1>
				
				<nav class="btn-group pull-right">
					<a class="btn" href="<?php echo $back_url; ?>"><i class="icon-chevron-left"></i></a>
					<a class="btn" href="<?php echo $forward_url; ?>"><i class="icon-chevron-right"></i></a>
				</nav>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin'); ?>">Inicio</a> <span class="divider">/</span></li>
				<li><a href="<?php echo site_url('admin/homes'); ?>">Viviendas</a> <span class="divider">/</span></li>
				<li><a href="<?php echo $parent_url; ?>"><?php echo $home->name; ?></a> <span class="divider">/</span></li>
				<li class="active">Comentarios <?php echo $current_year; ?></li>
				<li class="pull-right"><a href="<?php echo $parent_url; ?>">Volver a detalle</a></li>
			</ul>

		</header>
		
		<table class="table">
			<thead>
				<tr>
					<th width="110">Nº Booking</th>
					<th>Comentario de</th>
					<th width="110">Fecha</th>
					<th width="75">Opciones</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($comments as $comment): ?>
				<tr>
					<td><?php echo $comment->reference_num; ?></td>
					<td><a href="<?php echo $comment->view_url; ?>"><?php echo $comment->name; ?></a></td>
					<td><?php echo $comment->date_comment; ?></td>
					<td>
						<nav class="btn-group">
							<a class="btn" rel="tooltip" href="<?php echo $comment->edit_url; ?>" title="Editar"><i class="icon-pencil"></i></a>
							<a class="btn" rel="tooltip" href="<?php echo $comment->delete_url; ?>" title="Borrar"><i class="icon-trash"></i></a>
						</nav>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>