<!DOCTYPE html>
<html lang="es">

<head>

	<!-- Include the document header -->
	<?php $this->load->view('includes/document_header'); ?>
	
	<!-- Load Page Specific SCripts -->
	<script src="<?php echo base_url(); ?>assets/js/admin_bookings_form.js"></script>
		
</head>

<body>

	<!-- Include the admin header -->
	<?php $this->load->view('includes/admin_header'); ?>

	<!-- PAGE CONTENT -->
	<section class="container">
		
		<header class="page-header">

			<div class="clearfix">
			
				<h1 class="pull-left">Bookings <small><?php echo $action_title; ?></small></h1>
			
			</div>
			
			<ul class="breadcrumb">
				<li><a href="<?php echo $home_url; ?>">Inicio</a> <span class="divider">/</span></li>
				<li class="active"><?php echo $action_title; ?></li>
				<li class="pull-right"><a href="<?php echo $list_url; ?>">Volver al calendario</a></li>
			</ul>

		</header>
		
		<!-- Item Form -->
		<form name="home_form" method="post" action="<?php echo $action_url; ?>">
			
			<div class="page-header">
				<h3>Datos del Cliente</h3>
			</div>
			
			<fieldset class="row">
				
				<div class="span4 control-group<?php if(form_error('fname')) { echo ' error'; } ?>">
					<label for="fname">Nombre<?php if(in_array('fname', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="text"
						name="fname"
						class="span4"
						id="fname"
						placeholder="Nombre del cliente"
						value="<?php echo set_value('fname',$booking->fname); ?>"
					/>
					<?php echo form_error('fname'); ?>
				</div>
				
				<div class="span4 control-group<?php if(form_error('lname')) { echo ' error'; } ?>">
					<label for="lname">Apellido(s)<?php if(in_array('lname', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="text"
						name="lname"
						class="span4"
						id="lname"
						placeholder="Apellido(s) del cliente"
						value="<?php echo set_value('lname',$booking->lname); ?>"
					/>
					<?php echo form_error('lname'); ?>
				</div>
				
				<div class="span4 control-group<?php if(form_error('email')) { echo ' error'; } ?>">
					<label for="email">Email<?php if(in_array('email', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="email"
						name="email"
						class="span4"
						id="email"
						placeholder="ejemplo@direccion.com"
						value="<?php echo set_value('email',$booking->email); ?>"
					/>
					<?php echo form_error('email'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="row">
				
				<div class="span3 control-group<?php if(form_error('address')) { echo ' error'; } ?>">
					<label for="address">Dirección<?php if(in_array('address', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="text"
						name="address"
						class="span3"
						id="address"
						placeholder="Dirección de Calle"
						value="<?php echo set_value('address',$booking->address); ?>"
					/>
					<?php echo form_error('address'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('city')) { echo ' error'; } ?>">
					<label for="city">Municipio<?php if(in_array('city', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="text"
						name="city"
						class="span2"
						id="city"
						placeholder="Ciudad / Pueblo"
						value="<?php echo set_value('city',$booking->city); ?>"
					/>
					<?php echo form_error('city'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('province')) { echo ' error'; } ?>">
					<label for="province">Provincia<?php if(in_array('province', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="text"
						name="province"
						class="span2"
						id="province"
						placeholder="Provincia"
						value="<?php echo set_value('province',$booking->province); ?>"
					/>
					<?php echo form_error('province'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('postal_code')) { echo ' error'; } ?>">
					<label for="postal_code">Código Postal<?php if(in_array('postal_code', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="text"
						name="postal_code"
						class="span2"
						id="postal_code"
						placeholder="00000"
						value="<?php echo set_value('postal_code',$booking->postal_code); ?>"
					/>
					<?php echo form_error('postal_code'); ?>
				</div>
				
				<div class="span1 control-group<?php if(form_error('country')) { echo ' error'; } ?>">
					<label for="country">País<?php if(in_array('country', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="text"
						name="country"
						class="span1"
						id="country"
						placeholder="AA"
						value="<?php echo set_value('country',$booking->country); ?>"
					/>
					<?php echo form_error('country'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('mobile')) { echo ' error'; } ?>">
					<label for="mobile">Tel. Móvil<?php if(in_array('mobile', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="text"
						name="mobile"
						class="span2"
						id="mobile"
						placeholder="+34 000 000 000"
						value="<?php echo set_value('mobile',$booking->mobile); ?>"
					/>
					<?php echo form_error('mobile'); ?>
				</div>
				
			</fieldset>
			
			<div class="page-header">
				<h3>Datos del Booking</h3>
			</div>
			
			<fieldset class="row">
				
				<div class="span2 control-group<?php if(form_error('is_active')) { echo ' error'; } ?>">
					<label for="is_active">Estado *</label>
					<select
						name="is_active"
						id="is_active"
						class="span2">
						<option value="1" <?php echo set_select('is_active','1',($booking->is_active == 1)); ?>>Activo</option>
						<option value="0" <?php echo set_select('is_active','0',($booking->is_active == 0)); ?>>Cancelado</option>
					</select>
					<?php echo form_error('reference_num'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('reference_num')) { echo ' error'; } ?>">
					<label for="reference_num">Nº Referencia<?php if(in_array('reference_num', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="text"
						name="reference_num"
						class="span2 reference_num"
						id="reference_num"
						placeholder="AAA-000000-000"
						value="<?php echo set_value('reference_num',$booking->reference_num); ?>"
					/>
					<?php echo form_error('reference_num'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('date_from')) { echo ' error'; } ?>">
					<label for="date_from">Desde<?php if(in_array('date_from', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="text"
						name="date_from"
						class="span2 datepicker"
						id="date_from"
						placeholder="dd/mm/aaaa"
						value="<?php echo set_value('date_from',$booking->date_from); ?>"
					/>
					<?php echo form_error('date_from'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('date_to')) { echo ' error'; } ?>">
					<label for="date_to">Hasta<?php if(in_array('date_to', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="text"
						name="date_to"
						class="span2 datepicker"
						id="date_to"
						placeholder="dd/mm/aaaa"
						value="<?php echo set_value('date_to',$booking->date_to); ?>"
					/>
					<?php echo form_error('date_to'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('date_offered')) { echo ' error'; } ?>">
					<label for="date_to">Fecha de la Oferta<?php if(in_array('date_offered', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="text"
						name="date_offered"
						class="span2 datepicker"
						id="date_offered"
						placeholder="dd/mm/aaaa"
						value="<?php echo set_value('date_offered',$booking->date_offered); ?>"
					/>
					<?php echo form_error('date_offered'); ?>
				</div>
				
				<?php if($booking->date_booked): ?>
					<div class="span2 control-group<?php if(form_error('date_booked')) { echo ' error'; } ?>">
						<label for="date_booked">Fecha del Booking<?php if(in_array('date_booked', $required_fields)): ?> *<?php endif; ?></label>
						<input
							type="text"
							name="date_booked"
							class="span2 datepicker"
							id="date_booked"
							placeholder="dd/mm/aaaa"
							value="<?php echo set_value('date_booked',$booking->date_booked); ?>"
						/>
						<?php echo form_error('date_booked'); ?>
					</div>
				<?php endif; ?>
				
			</fieldset>
			
			<fieldset class="row">
				
				<div class="span1 control-group<?php if(form_error('adults')) { echo ' error'; } ?>">
					<label for="adults">Adultos<?php if(in_array('adults', $required_fields)): ?> *<?php endif; ?></label>
					<select
						name="adults"
						id="adults"
						class="span1">
						<?php foreach($all_adults as $adult): ?>
							<option
								value="<?php echo $adult; ?>"
								<?php echo set_select('adults',$adult,($adult == $booking->adults)); ?>>
								<?php echo $adult; ?>
							</option>
						<?php endforeach; ?>
					</select>
					<?php echo form_error('adults'); ?>
				</div>
				
				<div class="span1 control-group<?php if(form_error('children')) { echo ' error'; } ?>">
					<label for="children">Niños<?php if(in_array('children', $required_fields)): ?> *<?php endif; ?></label>
					<select
						name="children"
						id="children"
						class="span1">
						<?php foreach($all_children as $children): ?>
							<option
								value="<?php echo $children; ?>"
								<?php echo set_select('children',$children,($children == $booking->children)); ?>>
								<?php echo $children; ?>
							</option>
						<?php endforeach; ?>
					</select>
					<?php echo form_error('children'); ?>
				</div>
				
				<div class="span1 control-group<?php if(form_error('cribs')) { echo ' error'; } ?>">
					<label for="cribs">Cunas<?php if(in_array('cribs', $required_fields)): ?> *<?php endif; ?></label>
					<select
						name="cribs"
						id="cribs"
						class="span1">
						<?php foreach($all_cribs as $cribs): ?>
							<option
								value="<?php echo $cribs; ?>"
								<?php echo set_select('cribs',$cribs,($cribs == $booking->cribs)); ?>>
								<?php echo $cribs; ?>
							</option>
						<?php endforeach; ?>
					</select>
					<?php echo form_error('cribs'); ?>
				</div>
				
				<div class="span2 control-group<?php if(form_error('arrival_time')) { echo ' error'; } ?>">
					<label for="arrival_time">Hora de Llegada<?php if(in_array('arrival_time', $required_fields)): ?> *<?php endif; ?></label>
					<input
						type="text"
						name="arrival_time"
						class="span2 timepicker"
						id="arrival_time"
						placeholder="00:00"
						value="<?php echo set_value('arrival_time',$booking->arrival_time); ?>"
					/>
					<?php echo form_error('arrival_time'); ?>
				</div>
				
				<div class="span3 control-group<?php if(form_error('airport_id')) { echo ' error'; } ?>">
					<label for="airport_id">Aeropuerto de Llegada<?php if(in_array('airport_id', $required_fields)): ?> *<?php endif; ?></label>
					<select
						name="airport_id"
						class="span3"
						id="airport_id">
						<option value="">Seleccione</option>
						<?php foreach($all_airports as $airport): ?>
							<option
								value="<?php echo $airport->item_id; ?>"
								<?php echo set_select('airport_id',$airport->item_id,($airport->item_id == $booking->airport->item_id)); ?>>
								<?php echo $airport->name; ?>
							</option>
						<?php endforeach; ?>
					</select>
					<?php echo form_error('airport_id'); ?>
				</div>
			
			</fieldset>
			
			<fieldset class="row">
				
				<div class="span12 control-group<?php if(form_error('booking_source')) { echo ' error'; } ?>">
					<label for="booking_source">Origen del Booking<?php if(in_array('booking_source', $required_fields)): ?> *<?php endif; ?></label>
					<textarea
						name="booking_source"
						class="span12"
						id="booking_source"
						rows="6"
						placeholder="Incluye notas sobre la procedencia del booking"><?php echo set_value('booking_source',$booking->booking_source); ?></textarea>
					<?php echo form_error('booking_source'); ?>
				</div>
				
			</fieldset>
			
			<div class="page-header">
				<h3><?php if($booking->payments): ?>Pagos<?php else: ?>Precio<?php endif; ?></h3>
			</div>
			
				<fieldset class="row" id="payments">
					
					<div class="span3">
						<div class="control-group<?php if(form_error('agreed_price')) { echo ' error'; } ?>">
							<label for="agreed_price">Precio Acordado<?php if(in_array('agreed_price', $required_fields)): ?> *<?php endif; ?></label>
							<div class="input-append">
								<input
									type="text"
									name="agreed_price"
									class="span2 currency"
									id="agreed_price"
									placeholder="0.00"
									value="<?php echo set_value('agreed_price',$booking->agreed_price); ?>"
								/><span class="add-on">€</span>
							</div>
							<?php echo form_error('agreed_price'); ?>
							<?php if($booking->payments): ?>
								<br/>
								<button class="btn" id="add_row" title="Añadir otro pago a la tabla"><i class="icon-plus"></i> Añadir Pago</button>
							<?php endif; ?>
						</div>
					
						<br/><br/>
						
						<div class="control-group<?php if(form_error('payment_type_id')) { echo ' error'; } ?>">
							<label for="payment_type_id">Tipo de Pago<?php if(in_array('payment_type_id', $required_fields)): ?> *<?php endif; ?></label>
							<select
								name="payment_type_id"
								class="span3"
								id="payment_type_id">
								<option value="">Seleccione</option>
								<?php foreach($all_payment_types as $payment_type): ?>
									<option
										value="<?php echo $payment_type->item_id; ?>"
										<?php echo set_select('payment_type_id',$payment_type->item_id,($payment_type->item_id == $booking->payment_type->item_id)); ?>>
										<?php echo $payment_type->name; ?>
									</option>
								<?php endforeach; ?>
							</select>
							<?php echo form_error('payment_type_id'); ?>
						</div>
					</div>
					
					<div class="span9">
						
						<?php if($booking->payments): ?>
						
							<table class="table">
								<thead>
									<tr>
										<th>Porcentaje *</th>
										<th width="100">Cantidad</th>
										<th width="100">Comisión *</th>
										<th width="130">Fecha Pendiente *</th>
										<th width="45">Borrar</th>
									</tr>
								</thead>
								<tbody>
									
									<?php $counter = 1; ?>
									<?php foreach($booking->payments as $payment): ?>
										<tr
											data-item_id="<?php echo $payment->item_id; ?>"
											data-amount="<?php echo $payment->amount; ?>"
											data-commission="<?php echo $payment->commission_percent; ?>"
											data-sort_order="<?php echo $payment->sort_order; ?>"
										>
											<td>
												<div class="input-append">
													<input
														type="text"
														name="<?php echo 'payment_' . $counter . '_percentage'; ?>"
														class="span1 percentage"
														id="<?php echo 'payment_' . $counter . '_percentage'; ?>"
														placeholder="0"
														value="<?php echo set_value(('payment_'.$counter.'_percent'),$payment->percentage); ?>"
													/><span class="add-on">%</span>
												</div>
											</td>
											<td><span class="amount" id="<?php echo 'payment_' . $counter . '_amount'; ?>"><?php echo $payment->amount; ?></span> €</td>
											<td>
												<div class="input-append">
													<input
														type="text"
														name="<?php echo 'payment_' . $counter . '_commission'; ?>"
														class="span1 commission"
														id="<?php echo 'payment_' . $counter . '_commission'; ?>"
														placeholder="0"
														value="<?php echo set_value(('payment_'.$counter.'_commission'),$payment->commission_percent); ?>"
													/><span class="add-on">%</span>
												</div>
											</td>
											<td>
												<input
													type="text"
													name="<?php echo 'payment_' . $counter . '_date_due'; ?>"
													class="span2 datepicker date_due"
													id="<?php echo 'payment_' . $counter . '_date_due'; ?>"
													placeholder="dd/mm/aaaa"
													value="<?php echo set_value(('payment_'.$counter.'_date_due'),$payment->date_due); ?>"
												/>
											</td>
											<td><a href="#" class="btn remove_row" title="Borrar pago de la tabla"><i class="icon-trash"></i></a></td>
										</tr>
										<?php $counter++; ?>
									<?php endforeach; ?>
									
								</tbody>
							</table>
						
						<?php endif; ?>
						
						<input type="hidden" name="home_commission" id="home_commission" value="<?php echo $booking->home->commission_percent; ?>" />
						<input type="hidden" name="payment_ids" id="payment_ids" value="<?php echo $booking->payment_ids; ?>" />
						<input type="hidden" name="payment_percentages" id="payment_percentages" value="<?php echo $booking->payment_percentages; ?>" />
						<input type="hidden" name="payment_amounts" id="payment_amounts" value="<?php echo $booking->payment_amounts; ?>" />
						<input type="hidden" name="payment_dates_due" id="payment_dates_due" value="<?php echo $booking->payment_dates_due; ?>" />
						<input type="hidden" name="payment_commissions" id="payment_commissions" value="<?php echo $booking->payment_commissions; ?>" />
						<input type="hidden" name="payment_sort_orders" id="payment_sort_orders" value="<?php echo $booking->payment_sort_orders; ?>" />
					</div>
				
				</fieldset>
			
			<div class="page-header">
				<h3>Fianza y Gastos</h3>
			</div>
			
			<fieldset class="row" id="expenses">
			
				<div class="span3 control-group<?php if(form_error('down_payment')) { echo ' error'; } ?>">
					<label for="down_payment">Fianza<?php if(in_array('down_payment', $required_fields)): ?> *<?php endif; ?></label>
					<div class="input-append">
						<input
							type="text"
							name="down_payment"
							class="span2 currency"
							id="down_payment"
							placeholder="0.00"
							value="<?php echo set_value('down_payment',$booking->down_payment); ?>"
						/><span class="add-on">€</span>
					</div>
					<?php echo form_error('down_payment'); ?>
				</div>
				
				<div class="span9">
				
					<table class="table">
						<thead>
							<tr>
								<th width="400">Descripción</th>
								<th>Cantidad</th>
								<th width="45">Borrar</th>
							</tr>
						</thead>
						<tbody>
							<?php $counter = 1; ?>
							<?php foreach($booking->expenses as $expense): ?>
								<tr
									data-item_id="<?php echo $expense->item_id; ?>"
									data-line_item_id="<?php echo $expense->line_item_id; ?>"
								>
									<td>
										<input
											type="text"
											name="<?php echo 'expense_' . $counter . '_description'; ?>"
											class="span6 description"
											id="<?php echo 'expense_' . $counter . '_description'; ?>"
											placeholder="Escribe una descripción breve del gasto"
											value="<?php echo $expense->description; ?>"
										/>
									</td>
									<td>
										<div class="input-append">
											<input
												type="text"
												name="<?php echo 'expense_' . $counter . '_amount'; ?>"
												class="span1 currency amount"
												id="<?php echo 'expense_' . $counter . '_amount'; ?>"
												placeholder="0.00"
												value="<?php echo $expense->amount; ?>"
											/><span class="add-on">€</span>
										</div>
									</td>
									<td><a href="#" class="btn remove_expense_row" title="Borrar gasto de la tabla"><i class="icon-trash"></i></a></td>
								</tr>
								<?php $counter++; ?>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3"><button class="btn" id="add_expense_row" title="Añadir otro gasto a la tabla"><i class="icon-plus"></i> Añadir Gasto</button></td>
							</tr>
						</tfoot>
					</table>
					
					<input type="hidden" name="expense_item_ids" id="expense_item_ids" value="<?php echo $booking->expense_ids; ?>" />
					<input type="hidden" name="expense_descriptions" id="expense_descriptions" value="<?php echo $booking->expense_descriptions; ?>" />
					<input type="hidden" name="expense_amounts" id="expense_amounts" value="<?php echo $booking->expense_amounts; ?>" />
					<input type="hidden" name="expense_line_item_ids" id="expense_line_item_ids" value="<?php echo $booking->expense_line_item_ids; ?>" />
				
				</div>
				
			</fieldset>
			
			<div class="page-header">
				<h3>Adicional</h3>
			</div>
			
			<fieldset class="row">
				
				<div class="span12 control-group<?php if(form_error('notes')) { echo ' error'; } ?>">
					<label for="notes">Notas Adicionales<?php if(in_array('notes', $required_fields)): ?> *<?php endif; ?></label>
					<textarea
						name="notes"
						class="span12"
						id="notes"
						rows="12"
						placeholder="Incluye notas adicionales sobre el booking aquí"><?php echo set_value('notes',$booking->notes); ?></textarea>
					<?php echo form_error('notes'); ?>
				</div>
				
			</fieldset>
			
			<fieldset class="form-actions">
				
				<input type="submit" name="submit" class="btn btn-primary" id="save" value="Guardar" />
				<input type="submit" name="submit" class="btn" id="cancel" value="Cancelar" />
				<span class="help-inline">Los campos marcados con un asterisco (*) son obligatórias.</span>
				
			</fieldset>
			
		</form>

	</section>

	<!-- Include the document header -->
	<?php $this->load->view('includes/site_footer'); ?>

</body>

</html>