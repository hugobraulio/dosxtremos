<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {
	
	var $active_alerts;
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function __construct()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// Load the CI native sessions library to keep track of user sessions
		$this->load->library('session');
		
		// If user is not logged in, send them to the login method of the admin sessions controller
		if (!$this->session->userdata('admin_loggedin')) {
		   redirect('/admin/sessions/login/', 'refresh');
		   exit();
		}
		
		// Load the necessary models
		$this->load->model('alert');
		$this->load->model('booking');
		
		//(Hugo V, jun18, the alerts were not working) Create the alerts for today
		$this->booking->create_alerts();
		
		// Set the number of current active alerts
		$this->active_alerts = $this->alert->get_num_alerts();

	}
	
	
}

/* End of file Admin_Controller.php */
/* Location: ./application/libraries/Admin_Controller.php */