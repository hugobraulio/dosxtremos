<?php if (!defined('BASEPATH')) exit('No direct script access allowed.');

class MY_Form_validation extends CI_Form_validation {

	function __construct()
    {
        parent::__construct();
    }
	
	// valid_time()
	// Ensures a string is formatted correctly as a time
	function valid_time($value) {
		
		list($hh, $mm) = explode(':', $value);
		
		if (!is_numeric($hh) || !is_numeric($mm)) {
			return FALSE;
		} else if ((int) $hh > 24 || (int) $mm > 59) {
			return FALSE;
		} else if (mktime((int) $hh, (int) $mm) === FALSE) {
			return FALSE;
		} else {
			return TRUE;
		}
	
	}
	
	// valid_date()
	// Ensures a string is formatted correctly as a date (EU version)
	function valid_date($value) {
		
		if (preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $value, $matches)) {
			if (!checkdate($matches[2], $matches[1], $matches[3])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			return FALSE;
		}
	
	}
  
  
  
}