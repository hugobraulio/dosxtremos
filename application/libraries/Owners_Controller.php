<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Owners_Controller extends MY_Controller {
	
	/* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Sets global models and data used in the controller.
	----------------------------------------------------------------*/
	
	function __construct()
	{

		// Inherit parent class methods and properties
		parent::__construct();
		
		// If user is not logged in, send them to  the login method of the owners sessions controller
		if (!$this->session->userdata('loggedin')) {
		   
		   redirect('/owners/sessions/login/', 'refresh');
		   exit();
		   
		}
		
		// Load the language file from the users data and language helper
		$this->lang->load('owners', ($this->session->userdata('language_id') == 'es' ? 'spanish' : 'english'));
		$this->load->helper('language');

	}
	
	
}

/* End of file Owners_Controller.php */
/* Location: ./application/libraries/Owners_Controller.php */