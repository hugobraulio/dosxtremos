<?php

class Home extends CI_Model {
    
    var $is_new;
    var $item_id;
    var $code;
    var $name;
    var $address;
    var $city;
    var $province;
    var $postal_code;
    var $latitude;
    var $longitude;
    var $commission_percent;
    var $notes;
    var $capacity;
    var $sort_order;
    var $owner;
    var $estimates;


    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->is_active = 1;
        $this->code = '';
        $this->name = '';
        $this->address = '';
        $this->city = '';
        $this->province = '';
        $this->postal_code = '';
        $this->latitude = '';
        $this->longitude = '';
        $this->commission_percent = 0;
        $this->notes = '';
        $this->capacity = 0;
        
        // Compound properties
        
        $this->owner = new stdClass;
        $this->owner->item_id = 0;
        $this->owner->name = '';
        
        $this->estimates = array();
        
        // Set the order (default)
        $this->set_order();
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0)
    {
        
        // Check for an item_id is present
		if ($item_id > 0) {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
			
			// Check for returned data
			if ($query->num_rows() == 1) {
			
				// Populate this instances primary properties
				$row = $query->row();
				
				// Insert the 'simple' properties
				$this->is_new = FALSE;
				$this->item_id = $row->item_id;
				$this->is_active = $row->is_active;
				$this->code = $row->code;
		        $this->name = $row->name;
		        $this->address = $row->address;
		        $this->city = $row->city;
		        $this->province = $row->province;
		        $this->postal_code = $row->postal_code;
		        $this->latitude = $row->latitude;
		        $this->longitude = $row->longitude;
		        $this->commission_percent = $row->commission_percent;
		        $this->notes = $row->notes;
		        $this->capacity = $row->capacity;
		        $this->sort_order = $row->sort_order;
		        
		        // Set the compund properties with methods
		        $this->set_owner($row->owner_id);
		        $this->set_estimates($row->item_id);
			
			}
		
		} else {
			
			// Default the commission percent using the config model
			$ci =& get_instance();
			$ci->load->model('system_config');
			$config = $ci->system_config->initialize(1);
			
			$this->commission_percent = $config->commission_percent;
			
		}
		
		// Return this instance
		return $this;
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB with optional owner id
	filter
	----------------------------------------------------------------*/
    
    function list_entries($owner_id=0)
    {
        
        // Setup the basic query using CI Active Records Class
		$this->db->select('
			homes.item_id,
			homes.is_active,
			homes.code,
			homes.name,
			owners.item_id AS owner_id,
			CONCAT(owners.fname, " ", owners.lname) AS owner_name
		', FALSE);
		$this->db->from('homes');
		$this->db->join('owners', 'homes.owner_id = owners.item_id', 'left');
		
		// Check for owner id filter
		if($owner_id) {
			$this->db->where('homes.owner_id', $owner_id);
		}
		
		// Set the order
		$this->db->order_by('homes.sort_order');
		
		// Run the query and return the results
		$query = $this->db->get();
		return $query;
        
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('homes', array('item_id' => $item_id));
		return $query;
        
    }
    
    
    /* LIST CONDITIONS METHOD 
	------------------------------------------------------------------
	Description: Returns query of conditions attached to homes
	based on booking and year
	----------------------------------------------------------------*/
	
    function list_conditions($item_id=0, $year=0)
    {
        
        // Default the year and set the timespan to use in the query
        $year = ($year ? $year : date('Y'));
		$date_start = timestamp_to_mysqldatetime(mktime(0,0,0,1,1,$year),FALSE);
		$date_end = timestamp_to_mysqldatetime(mktime(0,0,0,12,31,$year),FALSE);
		
		// Now lets get all of the conditions using CI active records
		$this->db->select('
			conditions.item_id,
			conditions.date_created,
			bookings.item_id AS booking_id,
			bookings.reference_num,
			CONCAT(bookings.fname, " ", bookings.lname) AS client_name
		', FALSE);
		$this->db->from('conditions');
		$this->db->join('bookings', 'conditions.booking_id = bookings.item_id', 'inner');
		$this->db->where('bookings.home_id', $item_id);
		$this->db->where('conditions.date_created >=', $date_start);
		$this->db->where('conditions.date_created <=', $date_end);
		$this->db->order_by('conditions.date_created', 'asc');
		$query = $this->db->get();
		
		return $query;
        
    }
    
    
    /* GET RECEIPTS METHOD 
	------------------------------------------------------------------
	Description: Returns query of receipts attached to homes
	based on booking and year
	----------------------------------------------------------------*/
	
    function list_receipts($item_id=0, $year=0)
    {
        
        // Default the year and set the timespan to use in the query
        $year = ($year ? $year : date('Y'));
		$date_start = timestamp_to_mysqldatetime(mktime(0,0,0,1,1,$year),FALSE);
		$date_end = timestamp_to_mysqldatetime(mktime(0,0,0,12,31,$year),FALSE);
		
		// Now lets get all of the conditions using CI active records
		$this->db->select('
			receipts.item_id,
			receipts.date_created,
			payments.percentage,
			payments.amount,
			bookings.item_id AS booking_id,
			bookings.reference_num,
			CONCAT(bookings.fname, " ", bookings.lname) AS client_name
		', FALSE);
		$this->db->from('receipts');
		$this->db->join('payments', 'receipts.payment_id = payments.item_id', 'inner');
		$this->db->join('bookings', 'payments.booking_id = bookings.item_id', 'inner');
		$this->db->where('bookings.home_id', $item_id);
		$this->db->where('receipts.date_created >=', $date_start);
		$this->db->where('receipts.date_created <=', $date_end);
		$this->db->order_by('receipts.date_created', 'asc');
		$query = $this->db->get();
		
		return $query;
        
    }
    
    
    /* GET CALENDAR METHOD 
	------------------------------------------------------------------
	Description: Returns an array of calendar dates including objects
	for timeslots and any current offers and bookings. Includes an
	optional set of parameters for the year and month
	----------------------------------------------------------------*/
    
    function get_calendar($item_id, $year=0, $month=0)
    {
        
        // Default the year and month to current year and month if no month is defined
        $year = ($year ? $year : date('Y'));
        $month = ($month ? $month : date('m'));
        
		// Get the first and last days of the timespan
		$calendar_start = timestamp_to_mysqldatetime(mktime(0,0,0,$month,1,$year),FALSE);
		$calendar_end = timestamp_to_mysqldatetime(mktime(0,0,0,$month,date('t',mysqldatetime_to_timestamp($calendar_start)),$year),FALSE);
		
		// Get all of the timeslots for the timespan
		$this->db->from('timeslots');
		$this->db->where('home_id', $item_id);
		$this->db->where('date_slot >=', $calendar_start);
		$this->db->where('date_slot <=', $calendar_end);
		$timeslots = $this->db->get()->result();
		
		// Get all of the current offers for the timespan
		$this->db->select('
			bookings.item_id,
			CONCAT(bookings.fname, " ", bookings.lname) AS name,
			bookings.date_from,
			bookings.date_to,
			bookings.date_offered,
			bookings.date_booked,
			bookings.agreed_price,
			statuses.item_id AS status_id,
			statuses.name AS status_name,
			statuses.permalink AS status_permalink
		', FALSE);
		$this->db->distinct();
		$this->db->from('bookings');
		$this->db->join('booking_timeslot', 'booking_timeslot.booking_id = bookings.item_id', 'inner');
		$this->db->join('timeslots', 'booking_timeslot.timeslot_id = timeslots.item_id', 'inner');
		$this->db->join('statuses', 'bookings.status_id = statuses.item_id', 'inner');
		$this->db->where('bookings.home_id', $item_id);
		$this->db->where('bookings.is_active', 1);
		$this->db->where('timeslots.date_slot >=', $calendar_start);
		$this->db->where('timeslots.date_slot <=', $calendar_end);
		$bookings = $this->db->get()->result();
		
		// Set the spanish locale for the month names
		setlocale(LC_TIME, "es_ES");
		
		// Now create the calendar we will return

		$current_day = mysqldatetime_to_timestamp($calendar_start);
		$calendar_end = mysqldatetime_to_timestamp($calendar_end);
		
		$calendar = new stdClass;
		$calendar->year = $year;
		$calendar->month = ($month ? ucfirst(strftime('%B', $current_day)) : '');
		$calendar->days = Array();
		
		while($current_day <= $calendar_end) {
			
			// Setup the basic structure for the day
			$day = new stdClass;
			$day->item_id = 0;
			$day->home_id = $item_id;
			$day->timestamp = $current_day;
			$day->name = utf8_encode(strftime('%d %A', $current_day));
			$day->status_id = 0;
			$day->week_price = 0;
			$day->week_estimate = 0;
			$day->start_booking = FALSE;
			$day->price = 0;
			$day->estimate = 0;
			$day->offers = Array();
			$day->booking = NULL;
			
			// Find a timeslot for the day
			foreach($timeslots as $timeslot) {
				if($day->timestamp >= mysqldatetime_to_timestamp($timeslot->date_slot) && $day->timestamp <= mysqldatetime_to_timestamp($timeslot->date_slot)) {
					$day->item_id = $timeslot->item_id;
					$day->week_price = round(($timeslot->price * 7), 0);
					$day->week_estimate = round(($timeslot->estimate * 7), 0);
					$day->price = $timeslot->price;
					$day->estimate = $timeslot->estimate;
					break;
				}
			}
			
			// Find all bookings for the day
			foreach($bookings as $booking) {
				if($day->timestamp >= mysqldatetime_to_timestamp($booking->date_from) && $day->timestamp <= mysqldatetime_to_timestamp($booking->date_to)) {
					
					// Run a switch on the bookings status
					switch($booking->status_id) {
						case 1:
							array_push($day->offers, $booking);
							break;
						default:
							$day->booking = $booking;
							$day->week_price = ($day->timestamp == mysqldatetime_to_timestamp($booking->date_from) ? $booking->agreed_price : 0);
							$day->start_booking = ($day->timestamp == mysqldatetime_to_timestamp($booking->date_from) ? TRUE : FALSE);
					}
					
				}
			}
			
			// Add the day to the calendar
			array_push($calendar->days, $day);
			
			// Increment the current day
			$current_day = strtotime('+1 day', $current_day);
			
		}
		
		// Return the calendar object
		return $calendar;
        
    }
    
    
    /* GET BOOKING AMOUNTS
	------------------------------------------------------------------
	Description: Returns a query of bookings with amounts due and paid,
	as well as the sum of payment commissions and booking expenses for
	a specific home based on year. Used in Owners 'Bookings' section.
	----------------------------------------------------------------*/
	
	function get_booking_amounts($home_id=0, $year=0)
	{
	
		// Default the year value
		$year = ($year ? $year : date('Y'));
		
		// Get the first and last days of the timespan
		$report_start = timestamp_to_mysqldatetime(mktime(0,0,0,1,1,$year),FALSE);
		$report_end = timestamp_to_mysqldatetime(mktime(0,0,0,12,31,$year),FALSE);
		
		// Use a custom SQL query to get the data we need (CI Active Record can't grok this)
		$sql = "
			SELECT
				b.item_id,
				b.is_active,
				b.reference_num,
				b.date_from,
				b.date_to,
				CONCAT(b.fname, ' ', b.lname) AS name,
				b.agreed_price,
				pr.amount_received,
				pl.amount_liquidated,
				pd.amount_due,
				pc.amount_commission,
				e.amount_expense
			FROM bookings b
			LEFT JOIN (
				SELECT
					booking_id,
					SUM(amount) AS amount_received
				FROM payments
				WHERE date_paid IS NOT NULL
				GROUP BY booking_id
			) pr ON b.item_id = pr.booking_id
			LEFT JOIN (
				SELECT
					booking_id,
					SUM(amount*((100-commission_percent)/100)-(amount*payments.commission_percent/100*21/100)) AS amount_liquidated
				FROM payments
				WHERE date_liquidated IS NOT NULL
				GROUP BY booking_id
			) pl ON b.item_id = pl.booking_id
			LEFT JOIN (
				SELECT
					booking_id,
					SUM(amount) AS amount_due
				FROM payments
				WHERE date_paid IS NULL
				GROUP BY booking_id
			) pd ON b.item_id = pd.booking_id
			LEFT JOIN (
				SELECT
					booking_id,
					SUM((amount * (commission_percent/100))) AS amount_commission
				FROM payments
				GROUP BY booking_id
			) pc ON b.item_id = pc.booking_id
			LEFT JOIN (
				SELECT
					booking_id,
					SUM(amount) AS amount_expense
				FROM expenses
				GROUP BY booking_id 
			) e ON b.item_id = e.booking_id
			WHERE b.home_id = ?
				AND b.status_id > 1
				AND b.date_from >= ?
				AND b.date_from <= ?
			GROUP BY b.item_id
		";
		
		$query = $this->db->query($sql, array($home_id,$report_start,$report_end));
		
		return $query;
		
	}
	
	
	/* GET ANUAL ESTIMATE 
	------------------------------------------------------------------
	Description: Gets the anual estimate value based on year
	----------------------------------------------------------------*/
	
	function get_anual_estimate($year)
	{
		
		$ret_estimate = 0;
		
		// Loop through anual estimates array to get the correct value
		foreach($this->estimates as $estimate) {
			if($estimate->year == $year) {
				$ret_estimate = $estimate->estimate;
			}
		}
		
		return $ret_estimate;
		
	}
	
	
	/* GET BOOKING ESTIMATES 
	------------------------------------------------------------------
	Description: Gets the intial estimate and revised estimate based
	on estimates and actual bookings made so far.
	Estimate = All estimates of the year (sum) - 25% commission
	Revised Estimate = All Agreed Prices from bookings of the year -
	25% commission and expenses from bookings + rest of estimates
	from the year without bookings.
	----------------------------------------------------------------*/
	
	function get_booking_estimates($home_id, $year)
	{
		
		// Default the year value and get the current year
		$year = ($year ? $year : date('Y'));
		$current_year = timestamp_to_date(now(), 'Y');
		
		// Get the first and last days of the timespan
		$year_start = timestamp_to_mysqldatetime(mktime(0,0,0,1,1,$year),FALSE);
		$year_end = timestamp_to_mysqldatetime(mktime(0,0,0,12,31,$year),FALSE);
		
		// Create the structure we will return
		$estimates = new stdClass;
		$estimates->initial = 0;
		$estimates->revised = 0;
		
		/*
		
		NOTE: This is the previous method to get the initial estimate

		// Get the initial estimates sum for the year
		$this->db->select('
			ROUND(SUM(estimate), 2) AS total_estimates
		', FALSE);
		$this->db->from('timeslots');
		$this->db->where('home_id', $home_id);
		$this->db->where('date_slot >=', $year_start);
		$this->db->where('date_slot <=', $year_end);
		$this->db->limit(1);
		$initial_estimates = $this->db->get()->row();
		
		*/
		
		// Get the initial estimate from the DB for this year (NEW METHOD)
		
		$this->db->select('estimate AS total_estimates', FALSE);
		$this->db->from('home_estimates');
		$this->db->where('home_id', $home_id);
		$this->db->where('year', $year);
		$this->db->limit(1);
		$initial_estimates = $this->db->get()->row();
		
		// Set the initial estimate value
		if($initial_estimates) {
			$estimates->initial = $initial_estimates->total_estimates;
		}
		
		// Get the sum of all agreed prices (payments that will come in) within the year
		$this->db->select('
			SUM(booking_timeslot.agreed_price) AS total_agreed
		', FALSE);
		$this->db->from('bookings');
		$this->db->join('booking_timeslot', 'booking_timeslot.booking_id = bookings.item_id', 'inner');
		$this->db->join('timeslots', 'booking_timeslot.timeslot_id = timeslots.item_id', 'inner');
		$this->db->where('bookings.home_id', $home_id);
		$this->db->where('bookings.is_active', 1);
		$this->db->where('bookings.status_id >', 1);
		$this->db->where('timeslots.date_slot >=', $year_start);
		$this->db->where('timeslots.date_slot <=', $year_end);
		$this->db->limit(1);
		$agreed_prices = $this->db->get()->row();
		
		// Add the net payments to the revised estimate
		if($agreed_prices) {
			$estimates->revised += $agreed_prices->total_agreed;
		}
		
		// Get any timeslot estimates not tied to bookings (for future or current years only)
		if($year >= $current_year) {
		
			$remaining_estimates_start = ($year > $current_year ? $year_start : timestamp_to_mysqldatetime(now(), FALSE));
			$remaining_estimates_end = $year_end;
			
			$remaining_sql = "
				SELECT
					SUM(timeslots.estimate) AS total_estimates
				FROM timeslots
				LEFT JOIN booking_timeslot
					ON timeslots.item_id = booking_timeslot.timeslot_id
				LEFT JOIN bookings
					ON booking_timeslot.booking_id = bookings.item_id
				WHERE timeslots.home_id = ?
				AND timeslots.date_slot >= ?
				AND timeslots.date_slot <= ?
				AND ((booking_timeslot.timeslot_id IS NULL) OR (bookings.is_active = 0) OR (bookings.status_id = 1))
			";
			
			$remaining_estimates = $this->db->query($remaining_sql, array($home_id,$remaining_estimates_start,$remaining_estimates_end))->row();
			
			// Add the remaining estimates
			if($remaining_estimates) {
				$estimates->revised += $remaining_estimates->total_estimates;
			}
		
		}
		
		return $estimates;
		
	}
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        // Gather the data for the query into an array
		$data = array(
        	'owner_id' => $this->owner->item_id,
        	'is_active' => $this->is_active,
        	'code' => trim($this->code),
        	'name' => trim($this->name),
        	'address' => trim($this->address),
        	'city' => trim($this->city),
        	'province' => trim($this->province),
        	'postal_code' => trim($this->postal_code),
        	'latitude' => ($this->latitude ? $this->latitude : NULL),
        	'longitude' => ($this->longitude ? $this->longitude : NULL),
        	'commission_percent' => $this->commission_percent,
        	'notes' => (trim($this->notes) ? trim($this->notes) : NULL),
        	'capacity' => $this->capacity,
        	'sort_order' => $this->sort_order
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('homes', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('homes', $data);
			
		}
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database. Relies on DB
	foreign constraints to delete child elements (timeslots, bookings, etc.)
	----------------------------------------------------------------*/
	
	function delete_entry($item_id)
	{
	
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('homes');
		
	
	}
	
	
	/* SAVE ACTIVE STATE 
	------------------------------------------------------------------
	Description: Saves the is_active state of an entry directly
	(without using the full model init approach). Cascades the is_active
	state down to any bookings related to the home. Requires the item id
	of the home and the is_active state to set (1=TRUE, 0=FALSE)
	----------------------------------------------------------------*/
	
	function save_active_state($item_id, $is_active)
	{
	
		// Update the home status
		$this->db->where('item_id', $item_id);
		$this->db->update('homes', array('is_active' => $is_active));
		
		/* TO CASCADE ACTIVE STATES UN-COMMENT THIS AREA
		
		// Update the bookings related to homes
		$this->db->where('home_id', $item_id);
		$this->db->update('bookings', array('is_active' => $is_active));
		
		*/
	
	}
	
	
	/* SAVE ESTIMATE 
	------------------------------------------------------------------
	Description: Saves an anual estimate directly
	(without using the full model init approach). 
	----------------------------------------------------------------*/
	
	function save_estimate($item_id, $year, $estimate)
	{
	
		// Delete existing estimate
		$this->db->where('home_id', $item_id);
		$this->db->where('year', $year);
		$this->db->delete('home_estimates');
		
		// Insert the new estimate (if it exists)
		if($estimate) {
			$data = array(
				'home_id' => $item_id,
				'year' => $year,
				'estimate' => $estimate
			);
			$this->db->insert('home_estimates', $data);
		}
	
	}
	
	
	/* SET OWNER METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the owner of this entity.
	----------------------------------------------------------------*/
	
	function set_owner($owner_id)
	{
	
		// Get the owner data via a query
		$this->db->select('item_id, CONCAT(fname, " ", lname) AS name', FALSE);
		$this->db->from('owners');
		$this->db->where('item_id', $owner_id);
		$query = $this->db->get();
		
		// Set the owner data if a owner was found
		if($query->num_rows() == 1) {
			$this->owner = $query->row();
		}
	
	}
	
	
	/* SET ESTIMATES METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the annual estimates of this entity.
	----------------------------------------------------------------*/
	
	function set_estimates($item_id)
	{
	
		// Get the estimates data via a query
		$this->db->select('item_id, year, estimate');
		$this->db->from('home_estimates');
		$this->db->where('home_id', $item_id);
		$this->db->order_by('year', 'asc');
		$query = $this->db->get();
		
		// Set the data if it was found
		if($query->num_rows() > 0) {
			$this->estimates = $query->result();
		}
	
	}
	
	
	/* SET ORDER METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the order of this entity.
	----------------------------------------------------------------*/
	
	function set_order($order=0)
	{
	
		// Check for new items
		if($this->is_new) {
			
			// Get the new order (last + 1) from the db
			$this->db->select('item_id, sort_order',FALSE);
			$this->db->from('homes');
			$this->db->order_by('sort_order','desc');
			$this->db->limit(1);
			$row = $this->db->get()->row();
			
			$order = $row->sort_order + 1;
			
		}
		
		// Set the order
		$this->sort_order = $order;
	
	}
	
	
	/* REORDER METHOD
	------------------------------------------------------------------
	Description: Takes in an array or list of home ids in the order
	they need to be re-ordered to and saves the order value in all homes.
	----------------------------------------------------------------*/
	
	function reorder($home_ids='')
	{
		
		// Set the defaults
		$success = TRUE;
		
		// Convert the string to an array
		if(!is_array($home_ids)) {
			$home_ids = explode(',', $home_ids);
		}
		
		// Loop though the array of ids to update the sort order for each row
		foreach($home_ids as $key => $value) {
			$this->db->where('item_id', $value);
			$this->db->update('homes', array('sort_order' => $key));
			if($this->db->_error_message()) {
				$success = FALSE;
			}
		}
		
		// Return
		return $success;
	
	}
	
	
	/* BOOKING REPORT METHOD
	------------------------------------------------------------------
	Description: Returns a commissions report based on a home ID and a
	year which includes all booking transactions and costs
	----------------------------------------------------------------*/
	
	function booking_report($item_id=0, $year=0)
	{
		
		// Default the year and set the timespan to use in the query
        $year = ($year ? $year : date('Y'));
		$year_start = timestamp_to_mysqldatetime(mktime(0,0,0,1,1,$year),FALSE);
		$year_end = timestamp_to_mysqldatetime(mktime(0,0,0,12,31,$year),FALSE);
		
		// Get all the pre-confirmed and confirmed bookings
		$this->db->select('
			
			bookings.item_id,
			bookings.reference_num,
			CONCAT(bookings.fname, " ", bookings.lname) AS name,
			bookings.date_from,
			bookings.date_to,
			bookings.agreed_price,
			
			SUM(case when payments.trimester_paid=1 then (payments.amount * (payments.commission_percent/100)) else NULL end) AS t1_commissions,
			SUM(case when payments.trimester_paid=2 then (payments.amount * (payments.commission_percent/100)) else NULL end) AS t2_commissions,
			SUM(case when payments.trimester_paid=3 then (payments.amount * (payments.commission_percent/100)) else NULL end) AS t3_commissions,
			SUM(case when payments.trimester_paid=4 then (payments.amount * (payments.commission_percent/100)) else NULL end) AS t4_commissions,

			SUM(case when payments.trimester_paid=1 then payments.amount else NULL end) AS t1_payments,
			SUM(case when payments.trimester_paid=2 then payments.amount else NULL end) AS t2_payments,
			SUM(case when payments.trimester_paid=3 then payments.amount else NULL end) AS t3_payments,
			SUM(case when payments.trimester_paid=4 then payments.amount else NULL end) AS t4_payments,
			
			SUM(case when payments.trimester_liquidated=1 then payments.amount else NULL end) AS t1_liquidations,
			SUM(case when payments.trimester_liquidated=2 then payments.amount else NULL end) AS t2_liquidations,
			SUM(case when payments.trimester_liquidated=3 then payments.amount else NULL end) AS t3_liquidations,
			SUM(case when payments.trimester_liquidated=4 then payments.amount else NULL end) AS t4_liquidations
			
		', FALSE);
		$this->db->distinct();
		$this->db->from('bookings');
		$this->db->join('payments', 'payments.booking_id = bookings.item_id', 'inner');
		$this->db->where('bookings.home_id', $item_id);
		$this->db->where('payments.date_paid >=', $year_start);
		$this->db->where('payments.date_paid <=', $year_end);
		$this->db->where('bookings.status_id >=', 2);
		$this->db->group_by('bookings.item_id');
		$this->db->order_by('bookings.date_from', 'asc');
		$query = $this->db->get();
		
		// Return
		return $query;
	
	}
	
	
	/* EXPENSES REPORT METHOD
	------------------------------------------------------------------
	Description: Returns an expenses report based on a home ID and a
	year which includes all booking additional expenses
	----------------------------------------------------------------*/
	
	function expense_report($item_id=0, $year=0)
	{
		
		// Default the year and set the timespan to use in the query
        $year = ($year ? $year : date('Y'));
		$year_start = timestamp_to_mysqldatetime(mktime(0,0,0,1,1,$year),FALSE);
		$year_end = timestamp_to_mysqldatetime(mktime(0,0,0,12,31,$year),FALSE);
		
		// Get all the pre-confirmed and confirmed bookings
		$this->db->select('
			
			bookings.item_id,
			bookings.reference_num,
			CONCAT(bookings.fname, " ", bookings.lname) AS name,
			bookings.date_from,
			bookings.date_to,
			SUM(expenses.amount) AS total_amount
		', FALSE);
		$this->db->distinct();
		$this->db->from('bookings');
		$this->db->join('expenses', 'expenses.booking_id = bookings.item_id', 'inner');
		$this->db->where('bookings.home_id', $item_id);
		$this->db->where('bookings.date_from >=', $year_start);
		$this->db->where('bookings.date_from <=', $year_end);
		$this->db->where('bookings.status_id >=', 2);
		$this->db->group_by('bookings.item_id');
		$this->db->order_by('bookings.date_from', 'asc');
		$query = $this->db->get();
		
		// Return
		return $query;
	
	}
    

}