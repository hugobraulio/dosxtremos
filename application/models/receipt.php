<?php

class Receipt extends CI_Model {
    
    var $is_new;
    var $item_id;
    var $payment_id;
    var $booking_id;
    var $reference_num;
    var $date_created;
    var $content;

    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->payment_id = 0;
        $this->booking_id = 0;
        $this->reference_num = '';
        $this->date_created = now();
        
        // Multilingual content
        $this->content = new stdClass;
        $this->content->en = '';
        $this->content->es = '';
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0,$payment_id=0)
    {
        
        // Check for an item_id or payment id is present
		
		if(($item_id == 0) && ($payment_id > 0)) {
		
			// Get the entry data query from the 'get_entry_by_booking' method
			$query = $this->get_entry_by_payment($payment_id);
		
		} else {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
		
		}
		
		// Check for returned data
		if ($query->num_rows() > 0) {
		
			// Populate this instances primary properties
			$row = $query->row();
			
			// Insert the 'simple' properties
			$this->is_new = FALSE;
			$this->item_id = $row->item_id;
			$this->payment_id = $row->payment_id;
			$this->booking_id = $row->booking_id;
			$this->reference_num = $row->reference_num;
	        $this->date_created = mysqldatetime_to_timestamp($row->date_created);
	        $this->content->en = $row->content_en;
	        $this->content->es = $row->content_es;
		
		}
		
		return $this;
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB
	----------------------------------------------------------------*/
    
    function list_entries($payment_id=0)
    {
        
        // Setup the basic query using CI Active Records Class
        $this->db->from('receipts');
        
        if($payment_id) {
	        $this->db->where('payment_id', $payment_id);
        }
        
        $query = $this->db->get();
		return $query;
        
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $this->db->select('
        	receipts.item_id,
        	receipts.date_created,
        	receipts.content_en,
        	receipts.content_es,
        	payments.item_id AS payment_id,
        	bookings.item_id AS booking_id,
        	bookings.reference_num
        ');
        $this->db->from('receipts');
        $this->db->join('payments', 'receipts.payment_id = payments.item_id', 'inner');
        $this->db->join('bookings', 'payments.booking_id = bookings.item_id', 'inner');
        $this->db->where('receipts.item_id', $item_id);
        $query = $this->db->get();
        
		return $query;
        
    }
    
    
    
    /* GET ENTRY BY BOOKING METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's booking id
	----------------------------------------------------------------*/
	
    function get_entry_by_payment($payment_id)
    {
        
        // Use CI Active Records Class to create the query
        $this->db->select('
        	receipts.item_id,
        	receipts.date_created,
        	receipts.content_en,
        	receipts.content_es,
        	payments.item_id AS payment_id,
        	bookings.item_id AS booking_id,
        	bookings.reference_num
        ');
        $this->db->from('receipts');
        $this->db->join('payments', 'receipts.payment_id = payments.item_id', 'inner');
        $this->db->join('bookings', 'payments.booking_id = bookings.item_id', 'inner');
        $this->db->where('payments.item_id', $payment_id);
        $this->db->order_by('receipts.date_created desc');
        $query = $this->db->get();
		return $query;
        
    }
    
    
    
    /* GENERATE METHOD 
	------------------------------------------------------------------
	Description: Method used to generate the contents of this document.
	Used for new entries and requires the booking id to generate the data 
	----------------------------------------------------------------*/
	
    function generate_content($payment_id=0)
    {
        
        // Use CI active records to get the data we will need based on the booking id
        $this->db->select('
        	payments.item_id,
        	payments.percentage,
        	payments.amount,
        	payments.sort_order,
        	bookings.item_id AS booking_id,
        	bookings.date_from,
        	bookings.date_to,
        	CONCAT(bookings.fname, " ", bookings.lname) AS client_name,
        	bookings.address AS client_address,
        	bookings.city AS client_city,
        	bookings.province AS client_province,
        	bookings.postal_code AS client_postal_code,
        	bookings.country AS client_country,
        	bookings.agreed_price,
			bookings.reference_num,
        	homes.name AS home_name,
        	homes.address AS home_address,
        	homes.city AS home_city,
        	homes.province AS home_province,
        	homes.postal_code AS home_postal_code,
        	owners.is_admin,
        	CONCAT(owners.fname, " ", owners.lname) AS owner_name
        ', FALSE);
        $this->db->from('payments');
        $this->db->join('bookings', 'payments.booking_id = bookings.item_id', 'inner');
        $this->db->join('homes', 'bookings.home_id = homes.item_id', 'inner');
        $this->db->join('owners', 'homes.owner_id = owners.item_id', 'inner');
        $this->db->where('payments.item_id', $payment_id);
        $query = $this->db->get();
        
        // If a row was found procceed
        if($query->num_rows() == 1) {
	        
	        // Get the row of data returned and format the dates
	        $data['payment_data'] = $query->row();
	        $data['payment_data']->date_from = mysqldatetime_to_date($data['payment_data']->date_from, 'd/m/Y');
	        $data['payment_data']->date_to = mysqldatetime_to_date($data['payment_data']->date_to, 'd/m/Y');
	        $data['payment_data']->previous_receipt = new stdClass;
	        $data['payment_data']->previous_receipt->item_id = 0;
	        $data['payment_data']->previous_receipt->date_created = '';
	        
	        // Get the first payment
	        $this->db->from('payments');
	        $this->db->where('booking_id', $data['payment_data']->booking_id);
	        $this->db->order_by('sort_order', 'asc');
	        $this->db->limit(1);
	        $first_payment = $this->db->get()->row();
	        
	        // Get the last payment
	        $this->db->from('payments');
	        $this->db->where('booking_id', $data['payment_data']->booking_id);
	        $this->db->order_by('sort_order', 'desc');
	        $this->db->limit(1);
	        $last_payment = $this->db->get()->row();
	        
	        // Now set the type of payment in the payment data structure based on the first and last payments pulled
	        if(($data['payment_data']->item_id == $first_payment->item_id) && ($data['payment_data']->item_id == $last_payment->item_id)) {
		        $data['payment_data']->type = 'single';
		    } else if(($data['payment_data']->item_id == $first_payment->item_id) && ($data['payment_data']->item_id != $last_payment->item_id)) {
		    	$data['payment_data']->type = 'first';
		    } else if(($data['payment_data']->item_id != $first_payment->item_id) && ($data['payment_data']->item_id == $last_payment->item_id)) {
		    	$data['payment_data']->type = 'last';
		    } else {
		    	$data['payment_data']->type = 'middle';
		    }
		    
		    // If the payment is the last or a middle, get the receipt date of the previous one
		    if($data['payment_data']->type == 'last' || $data['payment_data']->type == 'middle') {
			    
			    // Get the previous date from receipts
			    $this->db->select('
			    	receipts.item_id,
			    	receipts.date_created
			    ', FALSE);
			    $this->db->from('receipts');
			    $this->db->join('payments', 'receipts.payment_id = payments.item_id', 'inner');
			    $this->db->where('payments.booking_id', $data['payment_data']->booking_id);
			    $this->db->where('payments.sort_order', ($data['payment_data']->sort_order - 1));
			    $query = $this->db->get();
			    
			    if($query->num_rows() == 1) {
				    $previous_receipt = $query->row();
				    $data['payment_data']->previous_receipt->item_id = $previous_receipt->item_id;
				    $data['payment_data']->previous_receipt->date_created = mysqldatetime_to_date($previous_receipt->date_created, 'd/m/Y');
			    }
			    
		    }
	        
	        // Add the document data
	        $data['document_data'] = new stdClass;
	        $data['document_data']->date_created = timestamp_to_date($this->date_created, 'd/m/Y');
	        
	        // Finally, use view files to render the data and then set the resulting HTML output as the content
	        $this->content->en = $this->load->view('content/receipts_en',$data,TRUE);
	        $this->content->es = $this->load->view('content/receipts_es',$data,TRUE);
	        
        }
        
        
    }
    
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        // Gather the data for the query into an array
		$data = array(
        	'payment_id' => $this->payment_id,
        	'date_created' => timestamp_to_mysqldatetime($this->date_created, FALSE),
        	'content_en' => $this->content->en,
        	'content_es' => $this->content->es
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('receipts', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('receipts', $data);
			
		}
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database.
	----------------------------------------------------------------*/
	
	function delete_entry($item_id=0)
	{
	
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('receipts');
		
	
	}
    

}