<?php

class Booking extends CI_Model {
    
    var $is_new;
    var $item_id;
    var $is_active;
    var $reference_num;
    var $date_from;
    var $date_to;
    var $date_offered;
    var $date_booked;
    var $arrival_time;
    var $tax_id;
    var $fname;
    var $lname;
    var $email;
    var $address;
    var $city;
    var $province;
    var $postal_code;
    var $country;
    var $mobile;
    var $adults;
    var $children;
    var $cribs;
    var $agreed_price;
    var $down_payment;
    var $internet_cost;
    var $other_cost;
    var $notes;
    var $booking_source;
	var $alert_pay1_delayed;
	var $alert_pay2_pending;
	var $alert_pay2_delayed;
	var $alert_pay3_pending;
	var $alert_pay3_delayed;
    
    var $status;
    var $home;
    var $airport;
    var $payments;
	var $payment_type;
    var $expenses;
    var $conditions;
    var $comments;
    var $documents;


    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->is_active = 1;
        $this->reference_num = '';
        $this->date_from = '';
        $this->date_to = '';
        $this->date_offered = now();
        $this->date_booked = '';
        $this->arrival_time = '';
        $this->tax_id = '';
        $this->fname = '';
        $this->lname = '';
        $this->email = '';
        $this->address = '';
        $this->city = '';
        $this->province = '';
        $this->postal_code = '';
        $this->country = '';
        $this->mobile = '';
        $this->adults = 0;
        $this->children = 0;
        $this->cribs = 0;
        $this->agreed_price = 0;
        $this->down_payment = 0;
        $this->notes = '';
        $this->booking_source = '';
		$this->alert_pay1_delayed = 0;
		$this->alert_pay2_pending = 0;
		$this->alert_pay2_delayed = 0;
		$this->alert_pay3_pending = 0;
		$this->alert_pay3_delayed = 0;
        $this->payments = Array();
        $this->expenses = Array();
        $this->documents = Array();
        $this->comments = Array();
        $this->documents = Array();
        
        // Compound properties
        
        $this->status = new stdClass;
        $this->status->item_id = 0;
        $this->status->name = '';
        $this->status->permalink = '';
        
        $this->home = new stdClass;
        $this->home->item_id = 0;
        $this->home->name = '';
        $this->home->code = '';
        $this->home->commission_percent = 0;
        
        $this->airport = new stdClass;
        $this->airport->item_id = 0;
        $this->airport->name = '';
        $this->airport->latitude = '';
        $this->airport->longitude = '';
        
        $this->conditions = new stdClass;
        $this->conditions->item_id = 0;
        $this->conditions->name = '';
        $this->conditions->date_created = '';
		
		$this->payment_type = new stdClass;
        $this->payment_type->item_id = 0;
        $this->payment_type->name = '';
        $this->payment_type->date_created = '';
		
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0)
    {
        
        // Check for an item_id is present
		if ($item_id > 0) {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
			
			// Check for returned data
			if ($query->num_rows() == 1) {
			
				// Populate this instances primary properties
				$row = $query->row();
				
				// Insert the 'simple' properties
				$this->is_new = FALSE;
				$this->item_id = $row->item_id;
				$this->is_active = $row->is_active;
				$this->reference_num = $row->reference_num;
				$this->date_from = mysqldatetime_to_timestamp($row->date_from);
				$this->date_to = mysqldatetime_to_timestamp($row->date_to);
				$this->date_offered = mysqldatetime_to_timestamp($row->date_offered);
				$this->date_booked = ($row->date_booked ? mysqldatetime_to_timestamp($row->date_booked) : '');
				$this->arrival_time = ($row->arrival_time ? strtotime($row->arrival_time) : '');
				$this->tax_id = $row->tax_id;
				$this->fname = $row->fname;
				$this->lname = $row->lname;
				$this->email = $row->email;
				$this->address = $row->address;
				$this->city = $row->city;
				$this->province = $row->province;
				$this->postal_code = $row->postal_code;
				$this->country = $row->country;
				$this->mobile = $row->mobile;
				$this->adults = $row->adults;
				$this->children = $row->children;
				$this->cribs = $row->cribs;
				$this->agreed_price = $row->agreed_price;
				$this->down_payment = $row->down_payment;
				$this->notes = $row->notes;
				$this->booking_source = $row->booking_source;
				$this->alert_pay1_delayed = $row->alert_pay1_delayed;
				$this->alert_pay2_pending = $row->alert_pay2_pending;
				$this->alert_pay2_delayed = $row->alert_pay2_delayed;
				$this->alert_pay3_pending = $row->alert_pay3_pending;
				$this->alert_pay3_delayed = $row->alert_pay3_delayed;
		        
		        // Set the compund properties with methods
		        $this->set_status($row->status_id);
		        $this->set_home($row->home_id);
		        $this->set_airport($row->airport_id);
		        $this->set_payments($row->item_id);
				$this->set_payment_type($row->payment_type_id);
		        $this->get_expenses($row->item_id);
		        $this->set_conditions($row->item_id);
		        $this->set_comments($row->item_id);
		        $this->set_documents($row->item_id);
			
			}
		
		}
		
		// Return this instance
		return $this;
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB with optional home,
	status ids (multiple), and year filters
	----------------------------------------------------------------*/
    
    function list_entries($home_id=0,$year=0,$is_active=1)
    {
        
        // Setup the basic query using CI Active Records Class
		$this->db->select('
			bookings.item_id,
			bookings.status_id,
			bookings.reference_num,
			CONCAT(bookings.fname, " ", bookings.lname) AS name,
			bookings.agreed_price,
			bookings.date_from,
			bookings.date_to,
			statuses.permalink AS status_permalink,
			SUM(payments.amount) AS amount_received
		', FALSE);
		$this->db->from('bookings');
		$this->db->join('statuses', 'bookings.status_id = statuses.item_id', 'inner');
		$this->db->join('payments', 'bookings.item_id = payments.booking_id', 'left');
		$this->db->where('bookings.is_active', $is_active);
		
		// Check for home id filter
		if($home_id) {
			$this->db->where('bookings.home_id', $home_id);
		}
		
		// Check for year filter
		if($year) {
			$start_year = timestamp_to_mysqldatetime(mktime(0,0,0,1,1,$year));
			$end_year = timestamp_to_mysqldatetime(mktime(0,0,0,12,31,$year));
			$this->db->having('bookings.date_from >=', $start_year);
			$this->db->having('bookings.date_to <=', $end_year);
		}
		
		// Makes sure to only get payed payments
		// $this->db->having('payments.date_paid !=', NULL);
		
		// Set the order
		$this->db->order_by('bookings.date_from');
		
		// Group the results
		$this->db->group_by('bookings.item_id');
		
		// Run the query and return the results
		$query = $this->db->get();
		return $query;
        
    }
	
	/* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB with optional home,
	status ids (multiple), and year filters
	----------------------------------------------------------------*/
    
    function list_bookings_not_paid()
    {
        
        // Setup the basic query using CI Active Records Class
		$this->db->select('
			bookings.item_id as booking_id,
			bookings.status_id,
			bookings.alert_pay1_delayed,
			bookings.alert_pay2_pending,
			bookings.alert_pay2_delayed,
			bookings.alert_pay3_pending,
			bookings.alert_pay3_delayed,
			payments.item_id as payment_id,
			payments.date_due,
			payments.date_paid,
			payments.sort_order,
			statuses.permalink AS status_permalink,
		', FALSE);
		$this->db->from('bookings');
		$this->db->join('statuses', 'bookings.status_id = statuses.item_id', 'inner');
		$this->db->join('payments', 'bookings.item_id = payments.booking_id', 'left');
		$this->db->where('bookings.is_active', 1);
		//$this->db->where('bookings.alert_pay1_delayed', 0);
		//$this->db->where('bookings.alert_pay2_pending', 0);
		//$this->db->where('bookings.alert_pay2_delayed', 0);
		$this->db->where('statuses.item_id >=',2); //only prebookings and bookings. Not offers (id=1)
		$this->db->where('payments.date_paid IS NULL');
		$this->db->where('payments.amount > ',0);
		$this->db->where('payments.date_due <= CURDATE() + INTERVAL 8 DAY');
		$this->db->where('payments.date_due >=','2018-01-01'); //limit the bd query as it's the first time.
		
		// Set the order
		$this->db->order_by('bookings.date_from');
		
		// Run the query and return the results
		$query = $this->db->get();
		return $query;
        
    }
	
    
    /* GET CALENDAR METHOD 
	------------------------------------------------------------------
	Description: Returns an array structure with the calendar bookings
	and timeslots as objects for the full year for all homes, by week.
	----------------------------------------------------------------*/
	
    function get_calendar($year=0)
    {
	    
	    // Get the first and last days of the year (-/+ a week for weeks that fall between years)
        $start_year = timestamp_to_mysqldatetime(strtotime('-1 week', mktime(0,0,0,1,1,$year)), FALSE);
        $end_year = timestamp_to_mysqldatetime(strtotime('+1 week', mktime(0,0,0,12,31,$year)), FALSE);
        
        // Get all of the homes
		$this->db->select('item_id, name');
		$this->db->from('homes');
		$this->db->where('is_active', 1);
		$this->db->order_by('sort_order', 'asc');
		$homes = $this->db->get()->result();
		
		// Use a custom SQL query to get all timeslot values by week
        $sql = "
        	SELECT
        		ts.home_id,
        		ROUND(SUM(ts.price), 2) AS price,
        		UNIX_TIMESTAMP(MIN(ts.date_slot)) AS week_start,
        		UNIX_TIMESTAMP(MAX(ts.date_slot)) AS week_end,
        		(case when (WEEKDAY(ts.date_slot) <= 4) then DATE(ts.date_slot + INTERVAL (4 - WEEKDAY(ts.date_slot)) DAY) else DATE(ts.date_slot + INTERVAL (4 + 7 - WEEKDAY(ts.date_slot)) DAY) end) AS week,
        		COUNT(bko.item_id) AS offers,
        		COUNT(bkp.item_id) AS prebooked,
        		COUNT(bkb.item_id) AS booked,
        		MAX(bk.agreed_price) AS agreed_price,
        		MIN(bk.date_from) AS booking_date
        		
        	FROM timeslots ts
        	
        	LEFT JOIN (
        		SELECT
        			booking_timeslot.timeslot_id,
        			bookings.item_id
        		FROM bookings INNER JOIN booking_timeslot ON bookings.item_id = booking_timeslot.booking_id
        		WHERE bookings.status_id = 1
        		AND bookings.is_active = 1
        		GROUP BY timeslot_id
        	) bko ON ts.item_id = bko.timeslot_id
        	
        	LEFT JOIN (
        		SELECT
        			booking_timeslot.timeslot_id,
        			bookings.item_id
        		FROM bookings INNER JOIN booking_timeslot ON bookings.item_id = booking_timeslot.booking_id
        		WHERE bookings.status_id = 2
        		AND bookings.is_active = 1
        		GROUP BY timeslot_id
        	) bkp ON ts.item_id = bkp.timeslot_id
        	
        	LEFT JOIN (
        		SELECT
        			booking_timeslot.timeslot_id,
        			bookings.item_id
        		FROM bookings INNER JOIN booking_timeslot ON bookings.item_id = booking_timeslot.booking_id
        		WHERE bookings.status_id = 3
        		AND bookings.is_active = 1
        		GROUP BY timeslot_id
        	) bkb ON ts.item_id = bkb.timeslot_id
        	
        	LEFT JOIN (
        		SELECT
        			booking_timeslot.timeslot_id,
        			bookings.item_id,
        			bookings.agreed_price,
        			bookings.date_from
        		FROM bookings INNER JOIN booking_timeslot ON bookings.item_id = booking_timeslot.booking_id
        		WHERE bookings.status_id > 1
        		AND bookings.is_active = 1
        		GROUP BY timeslot_id
        	) bk ON ts.item_id = bk.timeslot_id
  
        	WHERE ts.date_slot >= ?
        	AND ts.date_slot <= ?
        	GROUP BY ts.home_id, week
        	ORDER BY ts.home_id, ts.date_slot
        "; 
        
        // Run the query
        $timeslots = $this->db->query($sql, array($start_year,$end_year))->result();
		
		// Get the first saturday and following friday of the year
		$firstDayOfYear = mktime(0, 0, 0, 1, 1, $year);
		$nextSaturday     = strtotime('saturday', $firstDayOfYear);
		$nextFriday   = strtotime('friday', $nextSaturday);
		
		// Setup the calendar object we will return
		$calendar = new stdClass;
		$calendar->year = $year;
		$calendar->homes = $homes;
		$calendar->weeks = Array();
		
		// Set the spanish locale for the month names
		setlocale(LC_TIME, "es_ES");
		
		// Loop though the year
		while (date('Y', $nextSaturday) == $year) {
		
			// Create the date row as an object
			$date_row = new stdClass;
			$date_row->timestamp = $nextSaturday;
			$date_row->name = ucfirst(strftime('%B %d', $nextSaturday));
			$date_row->timeslots = Array();
			
			// Loop through homes
			foreach($homes as $home) {
				
				$week_slot = new stdClass;
				$week_slot->has_prices = FALSE;
				$week_slot->home = $home;
				$week_slot->date_start = $nextSaturday;
				$week_slot->date_end = $nextFriday;
				$week_slot->price = 0;
				$week_slot->offers = 0;
				$week_slot->prebooked = 0;
				$week_slot->booked = 0;
				$week_slot->agreed_price = 0;
				$week_slot->booking_date = 0;
				
				foreach($timeslots as $timeslot) {
					
					if( ($timeslot->home_id == $week_slot->home->item_id) && (mysqldatetime_to_timestamp($timeslot->week) == $week_slot->date_end) ) {
						
						$week_slot->has_prices = TRUE;
						$week_slot->price = $timeslot->price;
						$week_slot->offers = $timeslot->offers;
						$week_slot->prebooked = $timeslot->prebooked;
						$week_slot->booked = $timeslot->booked;
						$week_slot->agreed_price = $timeslot->agreed_price;
						$week_slot->booking_date = mysqldatetime_to_timestamp($timeslot->booking_date);
						
					}
					
				}
				
				// Round the price and estimate down to the nearest euro
				$week_slot->price = round($week_slot->price, 0);
				
				array_push($date_row->timeslots, $week_slot);
				
			}
			
			// Append the date row to the calendar
			array_push($calendar->weeks, $date_row);
			
			// Increment the friday and thursday
			$nextSaturday = strtotime('+1 week', $nextSaturday);
			$nextFriday = strtotime('+1 week', $nextFriday);
		
		}
        
        return $calendar;
        	    
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('bookings', array('item_id' => $item_id));
		return $query;
        
    }
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        
        // Gather the data for the query into an array
		$data = array(
        	'status_id' => $this->status->item_id,
        	'home_id' => $this->home->item_id,
        	'is_active' => ($this->is_active ? 1 : 0),
        	'reference_num' => trim($this->reference_num),
        	'date_from' => timestamp_to_mysqldatetime($this->date_from, FALSE),
        	'date_to' => timestamp_to_mysqldatetime($this->date_to, FALSE),
        	'date_offered' => timestamp_to_mysqldatetime($this->date_offered),
        	'date_booked' => ($this->date_booked ? timestamp_to_mysqldatetime($this->date_booked) : NULL),
        	'airport_id' => $this->airport->item_id,
        	'arrival_time' => (trim($this->arrival_time) ? trim($this->arrival_time) : NULL),
        	'tax_id' => (trim($this->tax_id) ? trim($this->tax_id) : NULL),
        	'fname' => trim($this->fname),
        	'lname' => trim($this->lname),
        	'email' => trim($this->email),
        	'address' => (trim($this->address) ? trim($this->address) : NULL),
        	'city' => (trim($this->city) ? trim($this->city) : NULL),
        	'province' => (trim($this->province) ? trim($this->province) : NULL),
        	'postal_code' => (trim($this->postal_code) ? trim($this->postal_code) : NULL),
        	'country' => (trim($this->country) ? trim($this->country) : NULL),
        	'mobile' => (trim($this->mobile) ? trim($this->mobile) : NULL),
        	'adults' => $this->adults,
        	'children' => $this->children,
        	'cribs' => $this->cribs,
        	'agreed_price' => $this->agreed_price,
        	'down_payment' => ($this->down_payment ? $this->down_payment : NULL),
			'payment_type_id' => $this->payment_type->item_id,
        	'notes' => (trim($this->notes) ? trim($this->notes) : NULL),
        	'booking_source' => $this->booking_source,
			'alert_pay1_delayed' => $this->alert_pay1_delayed,
			'alert_pay2_pending' => $this->alert_pay2_pending,
			'alert_pay2_delayed' => $this->alert_pay2_delayed,
			'alert_pay3_pending' => $this->alert_pay3_pending,
			'alert_pay3_delayed' => $this->alert_pay3_delayed
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('bookings', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('bookings', $data);
			
			// Clear out any existing booking timeslots
			$this->db->where('booking_id', $this->item_id);
			$this->db->delete('booking_timeslot');
			
			// Clear out any existing expenses
			$this->db->where('booking_id', $this->item_id);
			$this->db->delete('expenses');
			
		}
		
		// Set the timeslots based on the date_from and date_to of the booking
		$this->db->from('timeslots');
		$this->db->where('home_id', $this->home->item_id);
		$this->db->where('date_slot >=', timestamp_to_mysqldatetime($this->date_from, FALSE));
		$this->db->where('date_slot <=', timestamp_to_mysqldatetime($this->date_to, FALSE));
		$timeslot_query = $this->db->get();
		$number_days = $timeslot_query->num_rows();
		$timeslots = $timeslot_query->result();
		
		// If there are timeslots insert records into the booking_timeslot table (many-to-many)
		if($timeslots) {
			
			$booking_timeslots = Array();
			
			foreach($timeslots as $timeslot) {
				
				$timeslot->date_slot = mysqldatetime_to_timestamp($timeslot->date_slot);
				
				// Compare the timeslot to the booking to get the 'type' to include
				if($timeslot->date_slot == $this->date_from) {
					$type = 'start';
				} else if($timeslot->date_slot == $this->date_to) {
					$type = 'end';
				} else {
					$type = 'mid';
				}
				
				// Per day agreed price
				$per_day_price = $this->agreed_price / $number_days;
				
				// Add the data to the array
				$tmp_timeslot = array(
					'booking_id' => $this->item_id,
					'timeslot_id' => $timeslot->item_id,
					'type' => $type,
					'agreed_price' => $per_day_price
				);
				array_push($booking_timeslots, $tmp_timeslot);
				
			}
			
			// Run the batch insert
			$this->db->insert_batch('booking_timeslot', $booking_timeslots);
			
		}
		
		// Save the expenses if there are any
		if($this->expenses) {
			
			$expenses_insert_array = Array();
			
			foreach($this->expenses as $expense) {
				$tmp_expense = array(
					'booking_id' => $this->item_id,
					'description' => $expense->description,
					'amount' => $expense->amount,
					'line_item_id' => ($expense->line_item_id ? $expense->line_item_id : NULL)
				);
				array_push($expenses_insert_array, $tmp_expense);
			}
			
			$this->db->insert_batch('expenses', $expenses_insert_array);
			
		}
		
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database. Uses the DBs
	relational tables to remove associated elements where necessary.
	----------------------------------------------------------------*/
	
	function delete_entry($item_id)
	{
	
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('bookings');
	
	}
	
	
	/* SET STATUS METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the status of this entity.
	----------------------------------------------------------------*/
	
	function set_status($status_id)
	{
	
		// Get the status data via a query (include option for strings)
		if(is_numeric($status_id)) {
			$query = $this->db->get_where('statuses', array('item_id' => $status_id));
		} else {
			$query = $this->db->get_where('statuses', array('permalink' => $status_id));
		}
		
		// Set the status data if a status was found
		if($query->num_rows() == 1) {
			
			// Set the status
			$this->status = $query->row();
			
		}
	
	}
	
	
	/* SET HOME METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the home of this entity.
	----------------------------------------------------------------*/
	
	function set_home($home_id)
	{
	
		// Get the home data via a query
		$this->db->select('
			item_id,
			name,
			code,
			commission_percent
		');
		$this->db->from('homes');
		$this->db->where('item_id', $home_id);
		$this->db->limit(1);
		$query = $this->db->get();
		
		// Set the timeslot data if timeslots were found
		if($query->num_rows() == 1) {
			$this->home = $query->row();
		}
	
	}
	
	
	/* SET AIRPORT METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the airport of this entity.
	----------------------------------------------------------------*/
	
	function set_airport($airport_id)
	{
	
		// Get the home data via a query
		$this->db->from('airports');
		$this->db->where('item_id', $airport_id);
		$this->db->limit(1);
		$query = $this->db->get();
		
		// Set the timeslot data if timeslots were found
		if($query->num_rows() == 1) {
			$this->airport = $query->row();
		}
	
	}
	
	/* SET PAYMENT TYPE
	------------------------------------------------------------------
	Description: A 'setter' method to set the payment type of this entity.
	----------------------------------------------------------------*/
	
	function set_payment_type($payment_type_id)
	{
	
		// Get the home data via a query
		$this->db->from('payment_types');
		$this->db->where('item_id', $payment_type_id);
		$this->db->limit(1);
		$query = $this->db->get();
		
		// Set the timeslot data if timeslots were found
		if($query->num_rows() == 1) {
			$this->payment_type = $query->row();
		}
	
	}
	
	
	/* SET DEFAULT REFERENCE NUMBER METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the default reference number
	of this booking based on the home code, date, and incrementer
	----------------------------------------------------------------*/
	
	function set_default_reference_num($date=0)
	{
	
		// Create the first part of the number with the current home and date from properties
		$default_code = $this->home->code . '-' . timestamp_to_date($date, 'dmy') . '-';
		
		// Query the DB to find other bookings whos reference number starts with that string
		$this->db->select('COUNT(item_id) AS total_matches', FALSE);
		$this->db->from('bookings');
		$this->db->like('reference_num', $default_code, 'after');
		$this->db->group_by('item_id');
		$row = $this->db->get()->row();
		
		// Increment and pad the number returned and add it to the string
		$default_code .= ($row ? str_pad(($row->total_matches + 1), 3, '0', STR_PAD_LEFT) : '001');
		$this->reference_num = $default_code;
	
	}
	
	
	/* SET PAYMENTS METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the payments of this entity.
	----------------------------------------------------------------*/
	
	function set_payments($item_id)
	{
	
		// Get the payments data via the payment model
		$ci =& get_instance();
		$ci->load->model('payment');
		$query = $ci->payment->list_entries($item_id);
		
		// Set the payment data if payments were found
		if($query->num_rows() > 0) {
			$this->payments = $query->result();
			foreach($this->payments AS $payment) {
				$payment->date_due = mysqldatetime_to_timestamp($payment->date_due);
				$payment->date_paid = ($payment->date_paid ? mysqldatetime_to_timestamp($payment->date_paid) : NULL);
				$payment->date_liquidated = ($payment->date_liquidated ? mysqldatetime_to_timestamp($payment->date_liquidated) : NULL);
			}
		}
	
	}
	
	
	/* SET DEFAULT PAYMENTS METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the two default payments of
	this entity based on the system config.
	----------------------------------------------------------------*/
	
	function set_default_payments()
	{
	
		// Get the config data via the config model (DB id number 1)
		$ci =& get_instance();
		$ci->load->model('system_config');
		$config = $ci->system_config->initialize(1);
			
		// First Payment
		$first_payment = new stdClass;
		$first_payment->item_id = 0;
		$first_payment->booking_id = $this->item_id;
		$first_payment->percentage = $config->pay_percent_1;
		$first_payment->amount = number_format((($config->pay_percent_1 / 100) * $this->agreed_price), 2, '.', '') ;
		$first_payment->date_due = strtotime('+7 days', now());
		$first_payment->date_paid = NULL;
		$first_payment->date_liquidated = NULL;
		$first_payment->commission_percent = $this->home->commission_percent;
		$first_payment->sort_order = 0;
		array_push($this->payments, $first_payment);
		
		// Second Payment
		$second_payment = new stdClass;
		$second_payment->item_id = 0;
		$second_payment->booking_id = $this->item_id;
		$second_payment->percentage = $config->pay_percent_2;
		$second_payment->amount = number_format((($config->pay_percent_2 / 100) * $this->agreed_price), 2, '.', '') ;
		$second_payment->date_due = strtotime('-3 months', $this->date_from);
		$second_payment->date_paid = NULL;
		$second_payment->date_liquidated = NULL;
		$second_payment->commission_percent = $this->home->commission_percent;
		$second_payment->sort_order = 1;
		array_push($this->payments, $second_payment);
	
	}
	
	
	/* SAVE PAYMENTS METHOD
	------------------------------------------------------------------
	Description: A method to save the payments of this entity into
	the database.
	----------------------------------------------------------------*/
	
	function save_payments($item_ids='', $percentages='', $amounts='', $dates_due='', $commission_percents='', $sort_orders='')
	{
		
		// Get the payment data via the payment model
		$ci =& get_instance();
		$ci->load->model('payment');
		
		$item_ids = explode(',', $item_ids);
		$percentages = explode(',', $percentages);
		$amounts = explode(',', $amounts);
		$dates_due = explode(',', $dates_due);
		$commission_percents = explode(',', $commission_percents);
		$sort_orders = explode(',', $sort_orders);
		
		$saved_payments = Array();
		
		// Now loop through the arrays and instantiate and save each
		if($item_ids) {
			
			for($i=0; $i < count($item_ids); $i++) {
				
				$payment = $ci->payment->initialize($item_ids[$i]);
				$payment->percentage = $percentages[$i];
				$payment->amount = $amounts[$i];
				$new_payment = strtotime(str_replace('/', '-', $dates_due[$i]));
				if ($payment->date_due != $new_payment) {
					// If the date changes, delete the alerts if exists and unmark from the DB the alert created,
					// so that the alerts can be created again
					$cj =& get_instance();
					$cj->load->model('alert');
					
					// Delete alerts if exists of payment
					$alerts_deleted = $cj->alert->delete_alerts($item_ids[$i]);
					
					// Unmark the alerts from booking so that they can be created again
					$this->unmark_alerts($this->item_id, $payment->sort_order);
				}
				$payment->date_due = $new_payment;
				$payment->commission_percent = $commission_percents[$i];
				$payment->sort_order = $sort_orders[$i];
				$payment->set_booking($this->item_id);
				$payment->is_new = ($item_ids[$i] == 0) ? true : false;
				$payment->save_entry();
				
				array_push($saved_payments, $payment->item_id);				
			}
			
		}
		
		// Now delete any payments that do not appear in the saved_payments array
		$this->db->where('booking_id', $this->item_id);
		if($saved_payments) {
			$this->db->where_not_in('item_id', $saved_payments);
		}
		$this->db->delete('payments');
	
	}
	
	
	/* GET EXPENSES METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the expenses of this entity.
	----------------------------------------------------------------*/
	
	function get_expenses($item_id)
	{
	
		// Get the payments data via the payment model
		$ci =& get_instance();
		$ci->load->model('expense');
		
		// Set the expenses dat with the results of the query
		$this->expenses = $ci->expense->list_entries($item_id)->result();
	
	}
	
	
	/* SET EXPENSES METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the expenses of this entity.
	----------------------------------------------------------------*/
	
	function set_expenses($item_ids='', $descriptions='', $amounts='', $line_item_ids='')
	{
	
		// Loop through the input to insert the expenses into this models expenses array
		$item_ids = ($item_ids != '' ? explode('|', $item_ids) : NULL);
		$descriptions = ($descriptions != '' ? explode('|', $descriptions) : NULL);
		$amounts = ($amounts != '' ? explode('|', $amounts) : NULL);
		$line_item_ids = ($line_item_ids != '' ? explode('|', $line_item_ids) : NULL);
		
		// Clear the current expenses array
		$this->expenses = Array();
		
		// Loop through arrays to add data
		if($item_ids) {
			for($i=0; $i < count($item_ids); $i++) {
				$expense = new stdClass;
				$expense->item_id = $item_ids[$i];
				$expense->description = $descriptions[$i];
				$expense->amount = $amounts[$i];
				$expense->line_item_id = $line_item_ids[$i];
				array_push($this->expenses, $expense);
			}
		}
	
	}
	
	
	/* SAVE ALERTS METHOD
	------------------------------------------------------------------
	Description: A method to save the alerts of this entity into
	the database.
	----------------------------------------------------------------*/
	
	function save_alerts()
	{
		
		/*// 1. Get all alerts for this booking
		$old_alerts = $this->db->get_where('alerts', array('booking_id' => $this->item_id))->result();
		
		// 2. Creat new booking alerts
		$new_alerts = Array();
		
		// If offer is prebooked or booked
		if($this->status->item_id > 1) {
			
			$directions_alert = array(
				'booking_id' => $this->item_id,
				'payment_id' => 0,
				'type_id' => 'directions',
				'is_active' => 1,
				'display_date' => timestamp_to_mysqldatetime(strtotime('-2 Weeks', $this->date_from), FALSE)
			);
			array_push($new_alerts, $directions_alert);
			
			// Reminder (2 days before arrival)
			$reminder_alert = array(
				'booking_id' => $this->item_id,
				'payment_id' => 0,
				'type_id' => 'reminder',
				'is_active' => 1,
				'display_date' => timestamp_to_mysqldatetime(strtotime('-2 Days', $this->date_from), FALSE)
			);
			array_push($new_alerts, $reminder_alert);
			
			// Review (1 week after departure)
			$review_alert = array(
				'booking_id' => $this->item_id,
				'payment_id' => 0,
				'type_id' => 'review',
				'is_active' => 1,
				'display_date' => timestamp_to_mysqldatetime(strtotime('+1 Week', $this->date_to), FALSE)
			);
			array_push($new_alerts, $review_alert);
			
			// 3. Compare old and new booking based alerts to see if there are any deactivated alerts of this type or date changes
			foreach($old_alerts as $old_alert) {
				for($i = 0; $i < count($new_alerts); ++$i) {
					// Match found on type
					if($new_alerts[$i]['type_id'] == $old_alert->type_id) {
						// Set new alert is_active to old alert is_active
						$new_alerts[$i]['is_active'] = $old_alert->is_active;
						// Dates have changed, reset the alert
						if($new_alerts[$i]['display_date'] != $old_alert->display_date) {
							$new_alerts[$i]['is_active'] = 1;
						}
						break;
					}
				}
			}
			
			// 4. Set new alerts for the payments (set payments first from the database to make sure we have the current values)
			
			$this->set_payments($this->item_id);
			
			if($this->payments) {
				
				// Create the new alerts based on payments
				foreach($this->payments as $payment) {
					$payment_alert = array(
						'booking_id' => $this->item_id,
						'payment_id' => $payment->item_id,
						'type_id' => 'payment',
						'is_active' => 1,
						'display_date' => ($payment->sort_order == 0 ? timestamp_to_mysqldatetime($payment->date_due, FALSE) : timestamp_to_mysqldatetime(strtotime('-1 Months', $payment->date_due), FALSE))
					);
					array_push($new_alerts, $payment_alert);
				}
				
				// Compare old and new payment based alerts to see if there are any deactivated alerts of this type or due date changes
				foreach($old_alerts as $old_alert) {
					for($x = 0; $x < count($new_alerts); ++$x) {
						// Match found on payment id
						if($new_alerts[$x]['payment_id'] == $old_alert->payment_id) {
							// Set new alert is_active to old alert is_active
							$new_alerts[$x]['is_active'] = $old_alert->is_active;
							// Dates have changed, reset the alert
							if($new_alerts[$x]['display_date'] != $old_alert->display_date) {
								$new_alerts[$x]['is_active'] = 1;
							}
							break;
						}
					}
				}
				
			}
		
		}
		// Delete the existing alerts from the DB and add the new ones
		$this->db->where('booking_id', $this->item_id);
		$this->db->delete('alerts');
		
		if($new_alerts) {
			$this->db->insert_batch('alerts', $new_alerts);
		}*/
		
	}
	
	
	/* SET DOCUMENTS METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the documents of this entity.
	----------------------------------------------------------------*/
	
	function set_conditions($item_id)
	{
	
		// Get the conditions via CI active records
		$this->db->select('item_id, date_created');
		$this->db->from('conditions');
		$this->db->where('booking_id', $item_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0) {
			
			$this->conditions = $query->row();
			$this->conditions->name = 'Condiciones de Reserva';
			$this->conditions->date_created = mysqldatetime_to_timestamp($this->conditions->date_created);
			
		}
	
	}
	
	
	/* SET COMMENTS METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the comments of this entity.
	----------------------------------------------------------------*/
	
	function set_comments($item_id)
	{
	
		// Get the comments for this booking
		$this->db->select('item_id, date_comment, name');
		$this->db->from('comments');
		$this->db->where('booking_id', $item_id);
		$this->comments = $this->db->get()->result();
		
		// Format the dates into timestamps
		foreach($this->comments as $comment) {
			$comment->date_comment = mysqldatetime_to_timestamp($comment->date_comment);
		}
	
	}
	
	
	/* SET DOCUMENTS METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the documents of this entity.
	----------------------------------------------------------------*/
	
	function set_documents($item_id)
	{
	
		// Get the documents for this booking
		$this->documents = $this->db->get_where('documents', array('booking_id' => $item_id))->result();

	
	}
	
		/* CREATE ALERTS FOR THE DAY 
	------------------------------------------------------------------
	Description: Create the alerts for the day
	----------------------------------------------------------------*/
	
	function create_alerts()
	{
		if ($this->alert->check_alerts_not_created()) {
						
			$bookings = $this->list_bookings_not_paid()->result();
			
			// Creat new booking alerts
			$new_alerts = Array();
			
			foreach($bookings as $booking) {
				$date_due = strtotime($booking->date_due);
				$date_due_5_days = strtotime($booking->date_due. "+ 5 day");
				$date_due_minus_7_days = strtotime($booking->date_due. "- 7 day");
				$date_today = strtotime("today");
				if ($booking->sort_order == 0 && $date_due_5_days <= $date_today && $booking->alert_pay1_delayed == 0) {
					$data_alert = array(
						'booking_id' => $booking->booking_id,
						'payment_id' => $booking->payment_id,
						'type_id' => 'pay_1_delayed',
						'is_active' => 1,
						'display_date' => date("Y-m-d")
					);
					array_push($new_alerts, $data_alert);
					$this->db->where("item_id",$booking->booking_id);
					$this->db->update("bookings", array('alert_pay1_delayed' => 1));
				}
				if ($booking->sort_order == 1 && $date_due_5_days <= $date_today && $booking->alert_pay2_delayed == 0) {
					$data_alert = array(
						'booking_id' => $booking->booking_id,
						'payment_id' => $booking->payment_id,
						'type_id' => 'pay_2_delayed',
						'is_active' => 1,
						'display_date' => date("Y-m-d")
					);
					array_push($new_alerts, $data_alert);
					$this->db->where("item_id",$booking->booking_id);
					$this->db->update("bookings", array('alert_pay2_delayed' => 1));
				}
				if ($booking->sort_order == 1 && $date_due_minus_7_days <= $date_today && $booking->alert_pay2_pending == 0) {
					$data_alert = array(
						'booking_id' => $booking->booking_id,
						'payment_id' => $booking->payment_id,
						'type_id' => 'pay_2_pending',
						'is_active' => 1,
						'display_date' => date("Y-m-d")
					);
					array_push($new_alerts, $data_alert);
					$this->db->where("item_id",$booking->booking_id);
					$this->db->update("bookings", array('alert_pay2_pending' => 1));
				}
				if ($booking->sort_order == 2 && $date_due_5_days <= $date_today && $booking->alert_pay3_delayed == 0) {
					$data_alert = array(
						'booking_id' => $booking->booking_id,
						'payment_id' => $booking->payment_id,
						'type_id' => 'pay_3_delayed',
						'is_active' => 1,
						'display_date' => date("Y-m-d")
					);
					array_push($new_alerts, $data_alert);
					$this->db->where("item_id",$booking->booking_id);
					$this->db->update("bookings", array('alert_pay3_delayed' => 1));
				}
				if ($booking->sort_order == 2 && $date_due_minus_7_days <= $date_today && $booking->alert_pay3_pending == 0) {
					$data_alert = array(
						'booking_id' => $booking->booking_id,
						'payment_id' => $booking->payment_id,
						'type_id' => 'pay_3_pending',
						'is_active' => 1,
						'display_date' => date("Y-m-d")
					);
					array_push($new_alerts, $data_alert);
					$this->db->where("item_id",$booking->booking_id);
					$this->db->update("bookings", array('alert_pay3_pending' => 1));
				}
			}
			if ($new_alerts) {
				$this->db->insert_batch('alerts', $new_alerts);
			}
			
			$this->alert->set_alerts_created();
		}
	}
	
	/*------------------------------------------------------------------
	Description: Create the alerts for the day
	----------------------------------------------------------------*/
	
	function unmark_alerts($booking_id, $payment_number)
	{
		//payment number: 0 is payment number 1, 1 is payment number 2.
		//in the db is called "sort_order"
		$this->initialize($booking_id);
		if ($payment_number == 0) {
			$this->alert_pay1_delayed = 0;
		}
		elseif ($payment_number == 1) {
			$this->alert_pay2_pending = 0;
			$this->alert_pay2_delayed = 0;
		}
		elseif ($payment_number == 2) {
			$this->alert_pay3_pending = 0;
			$this->alert_pay3_delayed = 0;
		}
		$this->save_entry();
		
		$this->alert->unset_alerts_created();
		
	}

}