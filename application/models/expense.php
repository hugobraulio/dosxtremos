<?php

class Expense extends CI_Model {
    
    var $is_new;
    var $item_id;
    var $description;
    var $amount;
    var $booking;
    var $line_item;

    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->booking_id = 0;
        $this->description = '';
        $this->amount = 0;
        $this->line_item_id = 0;
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0)
    {
        
        // Check for an item_id is present
		if ($item_id) {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
			
			// Check for returned data
			if ($query->num_rows() == 1) {
			
				// Populate this instances primary properties
				$row = $query->row();
				
				// Insert the 'simple' properties
				$this->is_new = FALSE;
				$this->item_id = $row->item_id;
				$this->booking_id = $row->booking_id;
				$this->description = $row->description;
				$this->amount = $row->amount;
				$this->line_item_id = $row->line_item_id;
			
			}
		
		}
		
		return $this;
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB. Takes a parameter to filter
	on a specific booking
	----------------------------------------------------------------*/
    
    function list_entries($booking_id=0)
    {
        
        // Setup the basic query using CI Active Records Class
        $this->db->from('expenses');
        
        if($booking_id) {
	        $this->db->where('booking_id', $booking_id);
        }
        $query = $this->db->get();
		return $query;
        
    }
    
    
    /* LIST ENTRIES BY TRIMESTER METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB for a sepcific trimester
	with optional booking id or payment due parameters
	----------------------------------------------------------------*/
    
    function list_entries_by_trimester($booking_id=0,$trimester=0,$year=0)
    {
        
        // Setup the basic query using CI Active Records Class
        $this->db->select('
        	expenses.item_id,
        	expenses.description,
        	expenses.amount,
        	expenses.line_item_id,
        	bookings.item_id AS booking_id,
        	bookings.reference_num,
        	bookings.date_from,
        	bookings.date_to
        ', FALSE);
		$this->db->from('expenses');
		$this->db->join('bookings', 'expenses.booking_id = bookings.item_id', 'inner');
		
		// Check for a booking id
		if ($booking_id) {
			$this->db->where('bookings.item_id', $booking_id);
		}
		
		// Check for a trimester and year
		if($trimester && $year) {
			$timespan = get_fiscal_trimester_timespan($trimester,$year);
			$this->db->where('bookings.date_to >=', (timestamp_to_mysqldatetime($timespan->start, FALSE)));
			$this->db->where('bookings.date_to <=', (timestamp_to_mysqldatetime($timespan->end, FALSE)));
		}
		
		// Set the order
		$this->db->order_by('expenses.item_id', 'asc');
		
		// Run the query and return the results
		$query = $this->db->get();
		return $query;
        
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('expenses', array('item_id' => $item_id));
		return $query;
        
    }
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        // Gather the data for the query into an array
		$data = array(
        	'booking_id' => $this->booking_id,
        	'description' => trim($this->description),
        	'amount' => $this->amount,
        	'line_item_id' => ($this->line_item_id ? $this->line_item_id : NULL)
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('expenses', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('expenses', $data);
			
		}
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database.
	----------------------------------------------------------------*/
	
	function delete_entry($item_id)
	{
	
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('expenses');
		
	
	}
    

}