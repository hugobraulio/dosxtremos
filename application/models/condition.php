<?php

class Condition extends CI_Model {
    
    var $is_new;
    var $item_id;
    var $booking_id;
    var $reference_num;
    var $date_created;
    var $content;

    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->booking_id = 0;
        $this->reference_num = '';
        $this->date_created = now();
        
        // Multilingual content
        $this->content = new stdClass;
        $this->content->en = '';
        $this->content->es = '';
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0,$booking_id=0)
    {
        
        // Check for an item_id or booking id is present
		
		if(($item_id == 0) && ($booking_id > 0)) {
		
			// Get the entry data query from the 'get_entry_by_booking' method
			$query = $this->get_entry_by_booking($booking_id);
		
		} else {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
		
		}
		
		// Check for returned data
		if ($query->num_rows() > 0) {
		
			// Populate this instances primary properties
			$row = $query->row();
			
			// Insert the 'simple' properties
			$this->is_new = FALSE;
			$this->item_id = $row->item_id;
			$this->booking_id = $row->booking_id;
			$this->reference_num = $row->reference_num;
	        $this->date_created = mysqldatetime_to_timestamp($row->date_created);
	        $this->content->en = $row->content_en;
	        $this->content->es = $row->content_es;
		
		}
		
		return $this;
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB
	----------------------------------------------------------------*/
    
    function list_entries($booking_id=0)
    {
        
        // Setup the basic query using CI Active Records Class
        $this->db->from('conditions');
        if($booking_id) {
	        $this->db->where('bookings_id', $booking_id);
        }
        $query = $this->db->get();
		return $query;
        
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $this->db->select('
        	conditions.item_id,
        	conditions.date_created,
        	conditions.content_en,
        	conditions.content_es,
        	bookings.item_id AS booking_id,
        	bookings.reference_num
        ');
        $this->db->from('conditions');
        $this->db->join('bookings', 'conditions.booking_id = bookings.item_id', 'inner');
        $this->db->where('conditions.item_id', $item_id);
        $query = $this->db->get();
        
		return $query;
        
    }
    
    
    
    /* GET ENTRY BY BOOKING METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's booking id
	----------------------------------------------------------------*/
	
    function get_entry_by_booking($booking_id)
    {
        
        // Use CI Active Records Class to create the query
        $this->db->select('
        	conditions.item_id,
        	conditions.date_created,
        	conditions.content_en,
        	conditions.content_es,
        	bookings.item_id AS booking_id,
        	bookings.reference_num
        ');
        $this->db->from('conditions');
        $this->db->join('bookings', 'conditions.booking_id = bookings.item_id', 'inner');
        $this->db->where('bookings.item_id', $booking_id);
        $this->db->order_by('conditions.date_created desc');
        $query = $this->db->get();
		return $query;
        
    }
    
    
    
    /* GENERATE METHOD 
	------------------------------------------------------------------
	Description: Method used to generate the contents of this document.
	Used for new entries and requires the booking id to generate the data 
	----------------------------------------------------------------*/
	
    function generate_content($booking_id=0)
    {
        
        // Use CI active records to get the data we will need based on the booking id
        $this->db->select('
        	bookings.date_from,
        	bookings.date_to,
        	CONCAT(bookings.fname, " ", bookings.lname) AS client_name,
        	bookings.address AS client_address,
        	bookings.city AS client_city,
        	bookings.province AS client_province,
        	bookings.postal_code AS client_postal_code,
        	bookings.country AS client_country,
        	bookings.down_payment,
        	bookings.agreed_price,
			bookings.payment_type_id,
        	homes.name AS home_name,
        	homes.address AS home_address,
        	homes.city AS home_city,
        	homes.province AS home_province,
        	homes.postal_code AS home_postal_code,
        	homes.capacity AS num_occupants,
        	owners.is_admin,
        	CONCAT(owners.fname, " ", owners.lname) AS owner_name,
        	owners.bank_name,
        	owners.bank_nationality,
        	owners.bank_iban,
        	owners.bank_bic,
        	owners.bank_account_name,
        	owners.bank_account_number
        ', FALSE);
        $this->db->from('bookings');
        $this->db->join('homes', 'bookings.home_id = homes.item_id', 'inner');
        $this->db->join('owners', 'homes.owner_id = owners.item_id', 'inner');
        $this->db->where('bookings.item_id', $booking_id);
        $query = $this->db->get();
        
        // If a row was found procceed
        if($query->num_rows() == 1) {
	        
	        // Get the row of data returned and format the dates
	        $data['booking_data'] = $query->row();
	        $data['booking_data']->date_from = mysqldatetime_to_date($data['booking_data']->date_from, 'd/m/Y');
	        $data['booking_data']->date_to = mysqldatetime_to_date($data['booking_data']->date_to, 'd/m/Y');
			
			// Get the payment type
			//$data['booking_data']->payment_type = 
	        
	        // Get the payments (in order)
	        $this->db->from('payments');
	        $this->db->where('booking_id', $booking_id);
	        $this->db->order_by('sort_order asc');
	        $payments = $this->db->get()->result();
	        
	        // If payments were found procceed
	        if($payments) {
	        	
	        	// Pull out the first payment and set its data in the booking_data structure
	        	$first_payment = array_shift($payments);
	        	$data['booking_data']->first_payment_percent = $first_payment->percentage;
	        	$data['booking_data']->first_payment_amount = $first_payment->amount;
		        
		        // Prep the structure for the second payment
		        $data['booking_data']->second_payment_percent = 0;
		        $data['booking_data']->second_payment_amount = 0;
		        
		        // If there are any payments left in the payments array, loop though them and add their values to the structure
		        if($payments) {
		        	foreach($payments as $payment) {
			    	    $data['booking_data']->second_payment_percent += $payment->percentage;
			    	    $data['booking_data']->second_payment_amount += $payment->amount;
		        	}
		        }
		        
		        // Add the document data
		        $data['document_data'] = new stdClass;
		        $data['document_data']->date_created = timestamp_to_date($this->date_created, 'd/m/Y');
		        
		        // Get the system defaults from the config
		        $this->load->model('system_config');
		        $data['system_config'] = $this->system_config->initialize(1);
		        
		        // Add in these default config rules for bookings (this could come from the config table in the future)
		        $data['system_config']->final_payment_months = 2;
		        $data['system_config']->cancellation_months = 6;
		        $data['system_config']->cancellation_percent = 100;
		        $data['system_config']->time_arrival = '16:00';
		        $data['system_config']->time_departure = '10:00';
		        
		        // Finally, use view files to render the data and then set the resulting HTML output as the content
		        $this->content->en = $this->load->view('content/conditions_en',$data,TRUE);
		        $this->content->es = $this->load->view('content/conditions_es',$data,TRUE);
		        
	        }
	        
        }
        
        
    }
    
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        // Gather the data for the query into an array
		$data = array(
        	'booking_id' => $this->booking_id,
        	'date_created' => timestamp_to_mysqldatetime($this->date_created, FALSE),
        	'content_en' => $this->content->en,
        	'content_es' => $this->content->es
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('conditions', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('conditions', $data);
			
		}
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database.
	----------------------------------------------------------------*/
	
	function delete_entry($item_id)
	{
	
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('conditions');
		
	
	}
    

}