<?php

class Document extends CI_Model {
    
    var $is_new;
    var $item_id;
    var $filename;
    var $booking;

    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->filename = '';
        
        // Compound properties
        $this->booking = new stdClass;
        $this->booking->item_id = 0;
        $this->booking->reference_num = '';
        $this->booking->name = '';
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0)
    {
        
        // Check for an item_id is present
		if ($item_id) {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
			
			// Check for returned data
			if ($query->num_rows() == 1) {
			
				// Populate this instances primary properties
				$row = $query->row();
				
				// Insert the 'simple' properties
				$this->is_new = FALSE;
				$this->item_id = $row->item_id;
		        $this->filename = $row->filename;
		        
		        // Set the complex variables with setters
		        $this->set_booking($row->booking_id);
			
			}
		
		}
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB
	----------------------------------------------------------------*/
    
    function list_entries($booking_id=0)
    {
        
        // Setup the basic query using CI Active Records Class
        $this->db->from('documents');
        if($booking_id) {
        	$this->db->where('booking_id', $booking_id);
        }
        $query = $this->db->get();
		return $query;
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB
	----------------------------------------------------------------*/
    
    function list_entries_by_home($home_id=0, $year=0)
    {
        
        // Setup the basic query using CI Active Records Class
        $this->db->select('
        	documents.item_id,
        	documents.filename,
        	bookings.item_id AS booking_id,
        	bookings.reference_num,
        	CONCAT(bookings.fname, " ", bookings.lname) AS name,
        	bookings.date_from,
        	bookings.date_to
        ', FALSE);
        $this->db->from('documents');
        $this->db->join('bookings', 'documents.booking_id = bookings.item_id', 'inner');
        $this->db->where('bookings.is_active', 1);
        $this->db->where('bookings.status_id >', 1);
        // Check for home filter
        if($home_id) {
        	$this->db->where('bookings.home_id', $home_id);
        }
        // Check for year filter
		if($year) {
			$start_year = timestamp_to_mysqldatetime(mktime(0,0,0,1,1,$year), FALSE);
			$end_year = timestamp_to_mysqldatetime(mktime(0,0,0,12,31,$year), FALSE);
			$this->db->where('bookings.date_from >=', $start_year);
			$this->db->where('bookings.date_from <=', $end_year);
		}
		$this->db->order_by('bookings.date_from', 'asc');
        $query = $this->db->get();
		return $query;
        	
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('documents', array('item_id' => $item_id));
		return $query;
        
    }
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        // Gather the data for the query into an array
		$data = array(
        	'booking_id' => $this->booking->item_id,
        	'filename' => $this->filename
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('documents', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('documents', $data);
			
		}
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database and its file.
	----------------------------------------------------------------*/
	
	function delete_entry()
	{
		
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $this->item_id);
		$this->db->delete('documents');
		
		// Delete the file
		if(is_file('./uploads/booking_documents/'.$this->filename)) {
    		unlink('./uploads/booking_documents/'.$this->filename);
    	}
	
	}
	
	
	/* SET BOOKING
	------------------------------------------------------------------
	Description: Sets the elements booking data
	----------------------------------------------------------------*/
	
	function set_booking($booking_id=0)
	{
		
		// Get the data via CI active records
		$this->db->select('
			item_id,
			reference_num,
			CONCAT(fname, " ", lname) AS name
		', FALSE);
		$this->db->from('bookings');
		$this->db->where('item_id', $booking_id);
		$query = $this->db->get();
		
		if($query->num_rows() == 1) {
			$this->booking = $query->row();
		}
	
	}
    

}