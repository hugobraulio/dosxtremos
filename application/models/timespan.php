<?php

class Timespan extends CI_Model {
    
    var $status;
    var $home;
    var $date_start;
    var $date_end;
    var $price;
    var $estimate;
    var $timeslots;
    var $offers;
    var $booking;


    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->date_start = '';
        $this->date_end = '';
        $this->price = 0;
        $this->estimate = 0;
        
        // Compound properties
        
        $this->status = new stdClass;
        $this->status->item_id = 0;
        $this->status->name = 'Disponible';
        
        $this->home = new stdClass;
        $this->home->item_id = 0;
        $this->home->name = '';
        $this->timeslots = Array();
        $this->offers = Array();
        $this->booking = NULL;
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($home_id=0, $date_start='', $date_end='')
    {
        
        // Check for parameters
        if($home_id && $date_start && $date_end) {
	        
	        // Set the data in this instance
	        $this->date_start = $date_start;
	        $this->date_end = $date_end;
	        
	        // Set the compound elements in this instance with it's methods
	        $this->set_home($home_id);
	        
	        // TIMESLOTS //
	        
	        // Get any existing timeslots that fall within this timespans range
	        $this->db->from('timeslots');
			$this->db->where('home_id', $home_id);
			$this->db->where('date_slot >=', timestamp_to_mysqldatetime($date_start, FALSE));
			$this->db->where('date_slot <=', timestamp_to_mysqldatetime($date_end, FALSE));
			$timeslots = $this->db->get()->result();
			
			// Loop through the days in the timespan
			$current_day = $date_start;
			while($current_day <= $date_end) {
				
				$tmp_slot = new stdClass;
				$tmp_slot->item_id = 0;
				$tmp_slot->home_id = $home_id;
				$tmp_slot->date_slot = $current_day;
				$tmp_slot->price = 0;
				$tmp_slot->estimate = 0;
				
				// Loop through the timeslots pulled from the db
				foreach($timeslots as $timeslot) {
					
					if(mysqldatetime_to_timestamp($timeslot->date_slot) == $current_day) {
						
						// Set the data for the timeslot
						$tmp_slot->item_id = $timeslot->item_id;
						$tmp_slot->price = $timeslot->price;
						$tmp_slot->estimate = $timeslot->estimate;
						
						// Increment the price and estimate values
						$this->price += $timeslot->price;
						$this->estimate += $timeslot->estimate;
						
					}
					
				}
				
				// Append the timeslot to this timespans array
				array_push($this->timeslots, $tmp_slot);
				
				// add on another day to the current day
				$current_day = strtotime('+1 day', $current_day);
				
			}
			
			// Round the price and estimate to the euro
			$this->price = round($this->price, 0);
			$this->estimate = round($this->estimate, 0);
			
			// BOOKINGS //
			
			// Get any bookings that fall within this timespans range
			$this->db->select('
				bookings.item_id,
				CONCAT(bookings.fname, " ", bookings.lname) AS name,
				bookings.status_id,
				bookings.date_from,
				bookings.date_to,
				bookings.date_offered,
				bookings.date_booked,
				COUNT(timeslots.item_id) AS days
			', FALSE);
			$this->db->distinct();
			$this->db->from('bookings');
			$this->db->join('booking_timeslot', 'booking_timeslot.booking_id = bookings.item_id', 'inner');
			$this->db->join('timeslots', 'booking_timeslot.timeslot_id = timeslots.item_id', 'inner');
			$this->db->where('bookings.is_active', 1);
			$this->db->where('timeslots.home_id', $home_id);
			$this->db->where('timeslots.date_slot >=', timestamp_to_mysqldatetime($date_start, FALSE));
			$this->db->where('timeslots.date_slot <=', timestamp_to_mysqldatetime($date_end, FALSE));
			$this->db->group_by('bookings.item_id');
			$bookings = $this->db->get()->result();
			
			// Loop through the bookings to add them and set the status of this timespan
			foreach($bookings as $booking) {
				
				// Turn the dates into timestamps
				$booking->date_from = mysqldatetime_to_timestamp($booking->date_from);
				$booking->date_to = mysqldatetime_to_timestamp($booking->date_to);
				$booking->date_offered = mysqldatetime_to_timestamp($booking->date_offered);
				
				if($booking->status_id > 1) {
					
					// Initialize it as a booking in this instance
					$ci =& get_instance();
					$ci->load->model('booking', 'booking_model');
					$this->booking = $ci->booking_model->initialize($booking->item_id);
					
				} else {
					
					// Add the booking as an offer
					array_push($this->offers, $booking);
					
				}
				
			}
			
			// STATUS //
			
			// Set the offers
			if(count($this->offers) > 0 && !$this->booking) {
				$this->set_status(1);
			} else if($this->booking) {
				$this->set_status($this->booking->status->item_id);
			}
	        
        }
		
		// Return this instance
		return $this;
        
    }
    
    
    /* GET WEEK 
	------------------------------------------------------------------
	Description: Gets a weeks worth of timeslots with their respective
	bookings and offers. Requires a home_id, date_start, and date_end
	----------------------------------------------------------------*/
	
	function get_week($home_id, $date_start, $date_end)
    {
    	
    	// Set the local for string to time functions
    	setlocale(LC_TIME, "es_ES");
    	
    	// Setup the structure we will return
    	$week = new stdClass;
    	$week->date_start = $date_start;
    	$week->date_end = $date_end;
    	$week->name = 'Semana del ' . strftime('%d de %B', $date_start) . ' al ' . strftime('%d de %B', $date_end);
    	$week->status = new stdClass;
    	$week->status->item_id = 0;
    	$week->status->name = 'Disponible';
    	$week->status->permalink = 'available';
    	$week->price = 0;
    	$week->estimate = 0;
    	$week->agreed_price = 0;
    	$week->booked_dates = NULL;
    	$week->home = new stdClass;
    	$week->home->item_id = 0;
    	$week->home->name = '';
    	$week->days = Array();
    	
    	// Get the days data via CI Active Record Query
    	$this->db->select('
    		timeslots.item_id,
    		timeslots.date_slot,
    		timeslots.price,
    		timeslots.estimate,
    		GROUP_CONCAT(case when bookings.is_active = 1 then bookings.item_id else NULL end) AS bookings_item_id,
    		GROUP_CONCAT(case when bookings.is_active = 1 then bookings.status_id else NULL end) AS bookings_status_id,
    		GROUP_CONCAT(case when bookings.is_active = 1 then CONCAT(bookings.fname, " ", bookings.lname) else NULL end) AS bookings_name,
    		GROUP_CONCAT(case when bookings.is_active = 1 then bookings.date_from else NULL end) AS bookings_date_from,
    		GROUP_CONCAT(case when bookings.is_active = 1 then bookings.date_to else NULL end) AS bookings_date_to,
    		GROUP_CONCAT(case when bookings.is_active = 1 then bookings.agreed_price else NULL end) AS bookings_agreed_price
    	', FALSE);
    	$this->db->from('timeslots');
    	$this->db->join('booking_timeslot', 'timeslots.item_id = booking_timeslot.timeslot_id', 'left');
    	$this->db->join('bookings', 'booking_timeslot.booking_id = bookings.item_id', 'left');
    	$this->db->where('timeslots.home_id', $home_id);
    	$this->db->where('timeslots.date_slot >=', timestamp_to_mysqldatetime($date_start, FALSE));
		$this->db->where('timeslots.date_slot <=', timestamp_to_mysqldatetime($date_end, FALSE));
		$this->db->order_by('timeslots.date_slot', 'asc');
		$this->db->group_by('timeslots.item_id');
		$days_in_week = $this->db->get()->result();
		
		// Set the home data (use a query)
		$this->db->select('
			item_id,
			name
		');
		$this->db->from('homes');
		$this->db->where('item_id', $home_id);
		$home_data = $this->db->get()->row();
		if($home_data) {
			$week->home->item_id = $home_data->item_id;
			$week->home->name = $home_data->name;
		}
		
		// Populate the structure
		foreach($days_in_week as $week_day) {
			
			$day = new stdClass;
			$day->item_id = $week_day->item_id;
			$day->date = mysqldatetime_to_timestamp($week_day->date_slot);
			$day->status = new stdClass;
			$day->status->item_id = 0;
			$day->status->name = 'Disponible';
			$day->status->permalink = 'available';
			$day->name = utf8_encode(ucfirst(strftime('%A %d', mysqldatetime_to_timestamp($week_day->date_slot))));
			$day->price = $week_day->price;
			$day->estimate = $week_day->estimate;
			$day->booking = NULL;
			$day->offers = Array();
			
			// Increment the weeks price and estimate
			$week->price += $week_day->price;
			$week->estimate += $week_day->estimate;
			
			// Check for bookings on this day
			if($week_day->bookings_item_id) {
				
				// Convert the concatinated booking data to arrays
				$booking_item_ids = explode(',', $week_day->bookings_item_id);
				$booking_status_ids = explode(',', $week_day->bookings_status_id);
				$booking_names = explode(',', $week_day->bookings_name);
				$booking_dates_from = explode(',', $week_day->bookings_date_from);
				$booking_dates_to = explode(',', $week_day->bookings_date_to);
				$booking_agreed_prices = explode(',', $week_day->bookings_agreed_price);
				
				foreach($booking_item_ids as $key => $value) {
					
					$tmp_booking = new stdClass;
					$tmp_booking->item_id = $value;
					$tmp_booking->name = $booking_names[$key];
					$tmp_booking->date_from = mysqldatetime_to_timestamp($booking_dates_from[$key]);
					$tmp_booking->date_to = mysqldatetime_to_timestamp($booking_dates_to[$key]);
					$tmp_booking->agreed_price = $booking_agreed_prices[$key];
					$tmp_booking->status = new stdClass;
					$tmp_booking->status->item_id = $booking_status_ids[$key];
					switch($booking_status_ids[$key]) {
						case 1 :
							$tmp_booking->status->name = 'Oferta';
							$tmp_booking->status->permalink = 'offer';
							break;
						case 2 :
							$tmp_booking->status->name = 'Pre-Confirmada';
							$tmp_booking->status->permalink = 'prebooking';
							break;
						case 3 :
							$tmp_booking->status->name = 'Confirmada';
							$tmp_booking->status->permalink = 'booking';
							break;
					}
					$tmp_booking->view_url = site_url(array('admin','bookings','view',$value));
					
					if($tmp_booking->status->item_id > 1) {
						
						// Add it to the booking structure
						$day->booking = $tmp_booking;
						
						// Set the status data
						if($tmp_booking->status->item_id == 2) {
							$week->status->item_id = 2;
							$week->status->name = 'Pre-Confirmada';
							$week->status->permalink = 'prebooking';
						} else {
							$week->status->item_id = 3;
							$week->status->name = 'Confirmada';
							$week->status->permalink = 'booking';
						}
						
						// Set the agreed price
						$week->agreed_price = $tmp_booking->agreed_price;
						$week->booked_dates = timestamp_to_date($tmp_booking->date_from, 'd/m/Y') . ' - ' . timestamp_to_date($tmp_booking->date_to, 'd/m/Y');
						
					} else {
						
						// Append it to the offers array
						array_push($day->offers, $tmp_booking);
						
						// Set the status data
						if($week->status->item_id < 2) {
							$week->status->item_id = 1;
							$week->status->name = 'Ofertada';
							$week->status->permalink = 'offer';
						}
						
					}
					
				}
				
			}
			
			// Append the day to the week
			array_push($week->days, $day);
			
		}
		
		// Set values in week (main object)
		
		// 2. Round the price values to the nearest Euro
		$week->price = number_format(round($week->price, 0), 2, '.', '');
		$week->estimate = number_format(round($week->estimate, 0), 2, '.', '');
		
		
		return $week;
    
    }
    
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and calls the
	timeslot model to insert or update each timeslot
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        // Divide the prices by the number of timeslots
        $price = $this->price / count($this->timeslots);
        $estimate = $this->estimate / count($this->timeslots);
        
        // Create two arrays, one for new timeslots and one for existing timeslots
        $timeslot_inserts = Array();
        $timeslot_updates = Array();
        foreach($this->timeslots as $timeslot) {
        
	        if($timeslot->item_id) {
	        
		        $tmp_slot = array(
		        	'item_id' => $timeslot->item_id,
		        	'home_id' => $this->home->item_id,
		        	'date_slot' => timestamp_to_mysqldatetime($timeslot->date_slot, FALSE),
		        	'price' => $price,
		        	'estimate' => $estimate
		        );
		        
		        array_push($timeslot_updates, $tmp_slot);
		        
	        } else {
		        
		    	$tmp_slot = array(
		    		'home_id' => $this->home->item_id,
		    		'date_slot' => timestamp_to_mysqldatetime($timeslot->date_slot, FALSE),
		    		'price' => $price,
		        	'estimate' => $estimate
		    	);
		    	
		    	array_push($timeslot_inserts, $tmp_slot);
		        
	        }
	        
        }
        
        // Check for inserts and updates and use CI's batch functionality
        
        if($timeslot_inserts) {
	        $this->db->insert_batch('timeslots', $timeslot_inserts);
        }
        
        if($timeslot_updates) {
	        $this->db->update_batch('timeslots', $timeslot_updates, 'item_id');
        }
        
        // Round the price and estimate to the euro
		$this->price = round($this->price, 0);
		$this->estimate = round($this->estimate, 0);

        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes the timeslots for a specific home and between
	a specific range
	----------------------------------------------------------------*/
	
	function delete_entry($home_id,$date_start,$date_end)
	{
	
		// Delete entry via CI Active Record Class
		$this->db->where('home_id', $home_id);
		$this->db->where('date_slot >=', timestamp_to_mysqldatetime($date_start, FALSE));
		$this->db->where('date_slot <=', timestamp_to_mysqldatetime($date_end, FALSE));
		$this->db->delete('timeslots');
		
	}
	
	
	/* SET HOME METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the home of this entity.
	----------------------------------------------------------------*/
	
	function set_home($home_id)
	{
	
		// Get the home data via a query
		$this->db->select('item_id, name');
		$this->db->from('homes');
		$this->db->where('item_id', $home_id);
		$query = $this->db->get();
		
		// Set the owner data if a owner was found
		if($query->num_rows() == 1) {
			$this->home = $query->row();
		}
	
	}
	
	
	/* SET STATUS METHOD
	------------------------------------------------------------------
	Description: A simple 'setter' method to set the status values of
	this entity.
	----------------------------------------------------------------*/
	
	function set_status($status_id=0)
	{
		
		// Get an instance of the status model
		$ci =& get_instance();
		$ci->load->model('status', 'status_model');
		$query = $this->status_model->get_entry($status_id);
		
		// Set the status data if a status was found
		if($query->num_rows() == 1) {
			$this->status = $query->row();
		}
		
	}
    

}