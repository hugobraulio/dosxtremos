<?php

class Maintenance extends CI_Model {
    
    var $is_new;
    var $item_id;
    var $reference_num;
    var $name;
    var $provider;
    var $date_complete;
    var $date_payed;
    var $payed_by;
    var $amount;
    var $trimester;
    var $receipt_file;
    var $notes;
    var $home;


    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->reference_num = '';
        $this->name = '';
        $this->provider = '';
        $this->date_complete = '';
        $this->date_payed = '';
        $this->payed_by = '';
        $this->amount = '';
        $this->trimester = '';
        $this->receipt_file = '';
        $this->notes = '';
        
        // Compound properties
        $this->home = new stdClass;
        $this->home->item_id = 0;
        $this->home->code = '';
        $this->home->name = '';
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0)
    {
        
        // Check for an item_id is present
		if ($item_id > 0) {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
			
			// Check for returned data
			if ($query->num_rows() == 1) {
			
				// Populate this instances primary properties
				$row = $query->row();
				
				// Insert the 'simple' properties
				$this->is_new = FALSE;
				$this->item_id = $row->item_id;
				$this->reference_num = $row->reference_num;
				$this->name = $row->name;
		        $this->provider = $row->provider;
		        $this->date_complete = mysqldatetime_to_timestamp($row->date_complete);
		        $this->date_payed = mysqldatetime_to_timestamp($row->date_payed);
		        $this->payed_by = $row->payed_by;
		        $this->amount = $row->amount;
		        $this->trimester = $row->trimester;
		        $this->receipt_file = $row->receipt_file;
		        $this->notes = $row->notes;
		        
		        // Set the compund properties with methods
		        $this->set_home($row->home_id);
			
			}
		
		}
		
		// Return this instance
		return $this;
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB with optional owner id
	filter
	----------------------------------------------------------------*/
    
    function list_entries($home_id=0,$year=0)
    {
        
        // Setup the basic query using CI Active Records Class
		$this->db->select('
			item_id,
			reference_num,
			name,
			provider,
			date_payed,
			amount,
			trimester
		', FALSE);
		$this->db->from('maintenance');
		
		// Check for home id filter
		if($home_id) {
			$this->db->where('home_id', $home_id);
		}
		
		// Check for year filter
		if($year) {
			
			$start_year = timestamp_to_mysqldatetime(mktime(0,0,0,1,1,$year), FALSE);
			$end_year = timestamp_to_mysqldatetime(mktime(0,0,0,12,31,$year), FALSE);
			
			$this->db->where('date_payed >=', $start_year);
			$this->db->where('date_payed <=', $end_year);
			
		}
		
		// Set the order
		$this->db->order_by('date_payed');
		
		// Run the query and return the results
		$query = $this->db->get();
		return $query;
        
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('maintenance', array('item_id' => $item_id));
		return $query;
        
    }
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        
        // Gather the data for the query into an array
		$data = array(
        	'home_id' => $this->home->item_id,
        	'reference_num' => trim($this->reference_num),
        	'name' => trim($this->name),
        	'provider' => trim($this->provider),
        	'date_complete' => timestamp_to_mysqldatetime($this->date_complete),
        	'date_payed' => timestamp_to_mysqldatetime($this->date_payed),
        	'payed_by' => trim($this->payed_by),
        	'amount' => $this->amount,
        	'trimester' => $this->trimester,
        	'receipt_file' => ($this->receipt_file ? $this->receipt_file : NULL),
        	'notes' => (trim($this->notes) ? trim($this->notes) : NULL)
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('maintenance', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('maintenance', $data);
			
		}
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database.
	----------------------------------------------------------------*/
	
	function delete_entry($item_id)
	{
	
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('maintenance');
		
	
	}
	
	
	/* SET HOME METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the home of this entity.
	----------------------------------------------------------------*/
	
	function set_home($home_id)
	{
	
		// Get the home data via a query
		$this->db->select('item_id, code, name');
		$this->db->from('homes');
		$this->db->where('item_id', $home_id);
		$query = $this->db->get();
		
		// Set the owner data if a owner was found
		if($query->num_rows() == 1) {
			$this->home = $query->row();
		}
	
	}
    

}