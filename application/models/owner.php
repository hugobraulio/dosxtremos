<?php

class Owner extends CI_Model {
    
    var $is_new;
    var $item_id;
    var $is_active;
    var $is_admin;
    var $username;
    var $password;
    var $tax_id;
    var $fname;
    var $lname;
    var $email;
    var $telephone;
    var $mobile;
    var $fax;
    var $address;
    var $city;
    var $province;
    var $postal_code;
    var $country;
    var $notes;
    var $language;
    var $bank;
    var $homes;


    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
    
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->is_active = 1;
        $this->is_admin = 0;
        $this->username = '';
        $this->password = '';
        $this->tax_id = '';
        $this->fname = '';
        $this->lname = '';
        $this->email = '';
        $this->telephone = '';
        $this->mobile = '';
        $this->fax = '';
        $this->address = '';
        $this->city = '';
        $this->province = '';
        $this->postal_code = '';
        $this->country = '';
        $this->notes = '';
        
        // Compound properties
        
        $this->language = new stdClass;
        $this->language->item_id = '';
        $this->language->name = '';
        
        $this->bank = new stdClass;
        $this->bank->name = '';
        $this->bank->nationality = 'es';
        $this->bank->iban = '';
        $this->bank->bic = '';
        $this->bank->account = new stdClass;
        $this->bank->account->name = '';
        $this->bank->account->number = '';
        
        $this->homes = Array();
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0)
    {
        
        // Check for an item_id is present
		if ($item_id > 0) {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
			
			// Check for returned data
			if ($query->num_rows() == 1) {
			
				// Populate this instances primary properties
				$row = $query->row();
				
				// Insert the 'simple' properties
				$this->is_new = FALSE;
				$this->item_id = $row->item_id;
				$this->is_active = $row->is_active;
				$this->is_admin = $row->is_admin;
				$this->username = $row->username;
				$this->password = $row->password;
				$this->tax_id = $row->tax_id;
				$this->fname = $row->fname;
				$this->lname = $row->lname;
				$this->email = $row->email;
				$this->telephone = $row->telephone;
				$this->mobile = $row->mobile;
				$this->fax = $row->fax;
		        $this->address = $row->address;
		        $this->city = $row->city;
		        $this->province = $row->province;
		        $this->postal_code = $row->postal_code;
		        $this->country = $row->country;
		        $this->notes = $row->notes;
		        $this->bank->name = $row->bank_name;
				$this->bank->nationality = $row->bank_nationality;
				$this->bank->iban = $row->bank_iban;
				$this->bank->bic = $row->bank_bic;
				$this->bank->account->name = $row->bank_account_name;
				$this->bank->account->number = $row->bank_account_number;
		        
		        // Set the compund properties with methods
		        $this->set_language($row->language_id);
		        $this->set_homes($row->item_id);
			
			}
		
		}
		
		// Return this instance
		return $this;
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB with optional limit &
	offset for recordset paging
	----------------------------------------------------------------*/
    
    function list_entries($limit=0, $offset=0)
    {
        
        // Setup the basic query using CI Active Records Class
		$this->db->select('
			item_id,
			is_active,
			is_admin,
			CONCAT(fname, " ", lname) AS name
		', FALSE);
		$this->db->from('owners');
		
		// Check for a limit (for recordset paging)
		if ($limit > 0) {
			$this->db->limit($limit, $offset);
		}
		
		// Set the order
		$this->db->order_by('fname, lname', 'asc');
		
		// Run the query and return the results
		$query = $this->db->get();
		return $query;
        
    }
    
    
    /* GET TOTAL ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Returns the total number of entries in the DB
	----------------------------------------------------------------*/
	
	function get_total_entries()
    {
        
        // Get the number using CI helper method
		return $this->db->count_all('owners');
        
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('owners', array('item_id' => $item_id));
		return $query;
        
    }
    
    
    /* AUTHENTICATE METHOD 
	------------------------------------------------------------------
	Description: Authenticates an owner user via username and password
	and instantiates it's data
	----------------------------------------------------------------*/
	
    function authenticate($username='', $password='')
    {
        
        // Set the defaults
        $data['item_id'] = 0;
        $data['name'] = '';
        $data['homes'] = '';
        $data['language_id'] = '';
        $data['authenticated'] = FALSE;
        
        // Use CI Active Records to check the database for the user
        $this->db->select('
        	owners.item_id,
        	CONCAT(owners.fname, " ", owners.lname) AS name,
        	owners.language_id,
        	GROUP_CONCAT(homes.item_id) AS homes,
        	GROUP_CONCAT(homes.name) AS home_names
        ', FALSE);
        $this->db->from('owners');
        $this->db->join('homes', 'owners.item_id = homes.owner_id', 'left');
        $this->db->where('owners.username', $username);
        $this->db->where('owners.password', $password);
        $this->db->where('owners.is_active', 1);
        $this->db->group_by('owners.item_id');
        $query = $this->db->get();
        
        // If a record is found set the data to return
        if($query->num_rows() == 1) {
	        
	        $row = $query->row();
	        
	        $data['item_id'] = $row->item_id;
	        $data['name'] = $row->name;
	        $data['homes'] = $row->homes;
	        $data['home_names'] = $row->home_names;
	        $data['language_id'] = $row->language_id;
	        $data['authenticated'] = TRUE;
	        
        } else {
	        $data['authenticated'] = FALSE;
        }
        
        // Return the data
        return $data;
        
    }
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        // Gather the data for the query into an array
		$data = array(
        	'username' => trim($this->username),
        	'password' => trim($this->password),
        	'language_id' => trim($this->language->item_id),
        	'is_active' => $this->is_active,
        	'is_admin' => $this->is_admin,
        	'tax_id' => trim($this->tax_id),
        	'fname' => trim($this->fname),
        	'lname' => trim($this->lname),
        	'email' => trim($this->email),
        	'telephone' => trim($this->telephone),
        	'mobile' => (trim($this->mobile) ? trim($this->mobile) : NULL),
        	'fax' => (trim($this->fax) ? trim($this->fax) : NULL),
        	'address' => trim($this->address),
        	'city' => trim($this->city),
        	'province' => trim($this->province),
        	'postal_code' => trim($this->postal_code),
        	'country' => trim($this->country),
        	'notes' => (trim($this->notes) ? trim($this->notes) : NULL),
        	'bank_name' => $this->bank->name,
        	'bank_nationality' => $this->bank->nationality,
        	'bank_iban' => $this->bank->iban,
        	'bank_bic' => $this->bank->bic,
        	'bank_account_name' => $this->bank->account->name,
        	'bank_account_number' => $this->bank->account->number
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('owners', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('owners', $data);
			
		}
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database relies on DB
	foreign constraints to delete child elements (homes, bookings, etc.)
	----------------------------------------------------------------*/
	
	function delete_entry($item_id)
	{
	
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('owners');
		
	
	}
	
	
	/* SAVE ACTIVE STATE 
	------------------------------------------------------------------
	Description: Saves the is_active state of an entry directly
	(without using the full model init approach). Cascades the is_active
	state down to the users homes and any bookings related to those
	homes. Requires the item id of the owner and the is_active state
	to set (1=TRUE, 0=FALSE)
	----------------------------------------------------------------*/
	
	function save_active_state($item_id, $is_active)
	{
	
		// Update the owner status
		$this->db->where('item_id', $item_id);
		$this->db->update('owners', array('is_active' => $is_active));
		
		/* TO CASCADE ACTIVE STATES UN-COMMENT THIS AREA
		
		// Update the owners homes
		$this->db->where('owner_id', $item_id);
		$this->db->update('homes', array('is_active' => $is_active));
		
		// Update the bookings related to owners homes
		$sql = "UPDATE bookings INNER JOIN homes ON bookings.home_id = homes.item_id SET bookings.is_active = $is_active WHERE homes.owner_id = $item_id";
		$this->db->query($sql);
		
		*/
	
	}
	
	
	/* SET LANGUAGE METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the language of this entity.
	----------------------------------------------------------------*/
	
	function set_language($language_id)
	{
	
		// Get the language data via the language model
		$ci =& get_instance();
		$ci->load->model('language');
		$query = $ci->language->get_entry($language_id);
		
		// Set the langauge data if a language was found
		if($query->num_rows() == 1) {
			$this->language = $query->row();
		}
	
	}
	
	
	/* SET HOMES METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the homes of this entity.
	----------------------------------------------------------------*/
	
	function set_homes($item_id)
	{
	
		// Get the homes data via the homes model
		$ci =& get_instance();
		$ci->load->model('home');
		$query = $ci->home->list_entries($item_id);
		
		// Set the homes data if homes were found
		if($query->num_rows() > 0) {
			$this->homes = $query->result();
		}
	
	}
    

}