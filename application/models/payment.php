<?php

class Payment extends CI_Model {

    var $is_new;
    var $item_id;
    var $percentage;
    var $amount;
    var $date_due;
    var $date_paid;
  	var $date_liquidated;
    var $trimester_paid;
  	var $trimester_liquidated;
    var $commission_percent;
    var $sort_order;
    var $line_item_id;
	var $booking_id;
    var $booking;


    /* CLASS CONSTRUCTOR
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/

    function __construct()
    {

        // Call the Model constructor
        parent::__construct();

        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->percentage = 0;
        $this->amount = 0;
        $this->date_due = '';
        $this->date_paid = '';
    	$this->date_liquidated = '';
        $this->trimester_paid = 0;
    	$this->trimester_liquidated = 0;
        $this->commission_percent = 0;
        $this->sort_order = 0;
        $this->line_item_id = 0;
		$this->booking_id = 0;

        // Compound properties
        $this->booking = new stdClass;
        $this->booking->item_id = 0;
        $this->booking->reference_num = '';

    }


    /* INITIALIZE METHOD
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/

    function initialize($item_id=0)
    {

        // Check for an item_id is present
		if ($item_id > 0) {

			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);

			// Check for returned data
			if ($query->num_rows() == 1) {

				// Populate this instances primary properties
				$row = $query->row();

				// Insert the 'simple' properties
				$this->is_new = FALSE;
				$this->item_id = $row->item_id;
				$this->percentage = $row->percentage;
				$this->amount = $row->amount;
				$this->date_due = mysqldatetime_to_timestamp($row->date_due);
				$this->date_paid = ($row->date_paid ? mysqldatetime_to_timestamp($row->date_paid) : NULL);
				$this->date_liquidated = ($row->date_liquidated ? mysqldatetime_to_timestamp($row->date_liquidated) : NULL);
				$this->trimester_paid = ($row->trimester_paid ? $row->trimester_paid : NULL);
				$this->trimester_liquidated = ($row->trimester_liquidated ? $row->trimester_liquidated : NULL);
				$this->commission_percent = $row->commission_percent;
				$this->sort_order = $row->sort_order;
				$this->line_item_id = $row->line_item_id;
				$this->booking_id = $row->booking_id;

		        // Set the compund properties with methods
		        $this->set_booking($row->booking_id);

			}

		}

		// Return this instance
		return $this;

    }


    /* LIST ENTRIES METHOD
	------------------------------------------------------------------
	Description: Lists all entries in the DB with optional booking id
	or payment due parameters
	----------------------------------------------------------------*/

    function list_entries($booking_id=0,$is_due=FALSE)
    {

      // Setup the basic query using CI Active Records Class
      $this->db->select('
      	payments.item_id,
      	payments.booking_id,
      	payments.percentage,
      	payments.amount,
      	payments.date_due,
      	payments.date_paid,
  			payments.date_liquidated,
      	payments.trimester_paid,
  			payments.trimester_liquidated,
      	payments.commission_percent,
      	payments.sort_order,
      	receipts.item_id AS receipt_id,
      	receipts.date_created
      ', FALSE);
  		$this->db->from('payments');
  		$this->db->join('receipts', 'payments.item_id = receipts.payment_id', 'left');

  		// Check for a booking id
  		if ($booking_id) {
  			$this->db->where('booking_id', $booking_id);
  		}

  		// Check for an is due parameter
  		if ($is_due) {
  			$this->db->where('date_paid IS NULL', NULL, FALSE);
  		}

  		// Set the order
  		$this->db->order_by('sort_order', 'asc');

  		// Run the query and return the results
  		$query = $this->db->get();
  		return $query;

    }


    /* LIST ENTRIES BY TRIMESTER METHOD
	------------------------------------------------------------------
	Description: Lists all entries in the DB for a sepcific trimester
	with optional booking id or payment due parameters
	----------------------------------------------------------------*/

    function list_entries_by_trimester($booking_id=0,$trimester=0,$year=0,$is_paid=FALSE,$is_liquidated=FALSE)
    {

        // Setup the basic query using CI Active Records Class
        $this->db->select('
        	payments.item_id,
        	payments.percentage,
        	payments.amount,
        	payments.commission_percent,
        	payments.line_item_id,
        	bookings.item_id AS booking_id,
        	bookings.reference_num,
        	bookings.date_from,
        	bookings.date_to
        ', FALSE);
		$this->db->from('payments');
		$this->db->join('bookings', 'payments.booking_id = bookings.item_id', 'inner');

		// Check for a booking id
		if ($booking_id) {
			$this->db->where('bookings.item_id', $booking_id);
		}

		// Check for a trimester and year
		if($trimester && $year) {
			$timespan = get_fiscal_trimester_timespan($trimester,$year);
			if ($is_liquidated) {
				$this->db->where('payments.date_liquidated >=', (timestamp_to_mysqldatetime($timespan->start, FALSE)));
				$this->db->where('payments.date_liquidated <=', (timestamp_to_mysqldatetime($timespan->end, FALSE)));
			} else {
				$this->db->where('payments.date_paid >=', (timestamp_to_mysqldatetime($timespan->start, FALSE)));
				$this->db->where('payments.date_paid <=', (timestamp_to_mysqldatetime($timespan->end, FALSE)));
			}
		}

		// Check for the is_paid parameter
		if($is_paid) {
			$this->db->where('payments.date_paid IS NOT NULL', NULL, FALSE);
		}

		// Check for the is_liquidated parameter
		if($is_liquidated) {
			$this->db->where('payments.date_liquidated IS NOT NULL', NULL, FALSE);
		}

		// Set the order
		$this->db->order_by('payments.sort_order', 'asc');

		// Run the query and return the results
		$query = $this->db->get();
		return $query;

    }


    /* LIST ENTRIES BY BOOKING METHOD
	------------------------------------------------------------------
	Description: Lists all entries in the DB for a sepcific booking
	with optional booking id or payment due parameters
	----------------------------------------------------------------*/

    function list_entries_by_booking($booking_id,$is_paid=FALSE)
    {

        // Setup the basic query using CI Active Records Class
        $this->db->select('
        	item_id,
        	percentage,
        	amount,
        	date_due,
        	date_paid,
    			date_liquidated,
        	commission_percent
        ');
		$this->db->from('payments');
		$this->db->where('booking_id', $booking_id);

		// Check for the is_paid parameter
		if($is_paid) {
			$this->db->where('date_paid IS NOT NULL', NULL, FALSE);
		}

		// Set the order
		$this->db->order_by('sort_order', 'asc');

		// Run the query and return the results
		$query = $this->db->get();
		return $query;

    }


    /* LIST ENTRIES BY HOME METHOD
	------------------------------------------------------------------
	Description: Lists all entries in the DB for a sepcific home
	with optional year and payment due parameters
	----------------------------------------------------------------*/

    function list_entries_by_home($home_id,$year=0,$is_paid=FALSE)
    {

        // Setup the basic query using CI Active Records Class
        $this->db->select('
        	payments.item_id,
        	payments.percentage,
        	payments.amount,
        	payments.date_due,
        	payments.date_paid,
			paymets.date_liquidated,
        	payments.commission_percent,
        	bookings.item_id AS booking_id,
        	CONCAT(bookings.fname, " ", bookings.lname) AS name,
        	bookings.reference_num
        ', FALSE);
		$this->db->from('payments');
		$this->db->join('bookings', 'payments.booking_id = bookings.item_id', 'inner');
		$this->db->where('bookings.home_id', $home_id);

		// Check for year
		if($year && $is_paid) {
			$this->db->where('payments.date_paid IS NOT NULL', NULL, FALSE);
			$this->db->where('payments.date_paid >=', (timestamp_to_mysqldatetime(mktime(0,0,0,1,1,$year), FALSE)));
			$this->db->where('payments.date_paid <=', (timestamp_to_mysqldatetime(mktime(0,0,0,12,31,$year), FALSE)));
			$this->db->order_by('payments.date_paid', 'asc');
		} else if($year && !$is_paid) {
			$this->db->where('payments.date_paid IS NULL', NULL, FALSE);
			$this->db->where('payments.date_due >=', (timestamp_to_mysqldatetime(mktime(0,0,0,1,1,$year), FALSE)));
			$this->db->where('payments.date_due <=', (timestamp_to_mysqldatetime(mktime(0,0,0,12,31,$year), FALSE)));
			$this->db->order_by('payments.date_due', 'asc');
		}

		// Set the order
		$this->db->order_by('payments.sort_order', 'asc');

		// Run the query and return the results
		$query = $this->db->get();
		return $query;

    }


	/* GET ENTRY METHOD
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/

    function get_entry($item_id)
    {

        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('payments', array('item_id' => $item_id));
		return $query;

    }


	/* SAVE ENTRY
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/

    function save_entry()
    {

        // Gather the data for the query into an array
		$data = array(
        	'booking_id' => $this->booking->item_id,
        	'percentage' => $this->percentage,
        	'amount' => $this->amount,
        	'date_due' => timestamp_to_mysqldatetime($this->date_due),
        	'date_paid' => (($this->date_paid and !$this->is_new) ? timestamp_to_mysqldatetime($this->date_paid) : NULL),
			'date_liquidated' => (($this->date_liquidated and !$this->is_new) ? timestamp_to_mysqldatetime($this->date_liquidated) : NULL),
        	'trimester_paid' => (($this->trimester_paid and !$this->is_new) ? $this->trimester_paid : 0),
        	'trimester_liquidated' => (($this->trimester_liquidated and !$this->is_new) ? $this->trimester_liquidated : 0),
        	'commission_percent' => $this->commission_percent,
        	'sort_order' => $this->sort_order,
        	'line_item_id' => ($this->line_item_id ? $this->line_item_id : NULL)
        );

		// Check the 'is_new' status
		if ($this->is_new) {

			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('payments', $data);
			$this->item_id = $this->db->insert_id();

		} else {

			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('payments', $data);

		}

    }


	/* DELETE ENTRY
	------------------------------------------------------------------
	Description: Deletes an entry from the database.
	----------------------------------------------------------------*/

	function delete_entry($item_id)
	{

		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('payments');


	}


	/* SET DEFAULT COMMISSION METHOD
	------------------------------------------------------------------
	Description: Calls the system config model to get the default
	commission value and set it in this entry.
	----------------------------------------------------------------*/

	function set_default_commission()
	{

		// Get the booking data via the booking model
		$ci =& get_instance();
		$ci->load->model('system_config');
		$config = $ci->system_config->initialize(1);
		$this->commission_percent = $config->commission_percent;

	}


	/* SET BOOKING METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the booking of this entity.
	----------------------------------------------------------------*/

	function set_booking($booking_id)
	{

		// Get the booking data via the booking model
		$ci =& get_instance();
		$ci->load->model('booking');
		$query = $ci->booking->get_entry($booking_id);

		// Set the booking data if a booking was found
		if($query->num_rows() == 1) {
			$this->booking = $query->row();
		}

	}


}
