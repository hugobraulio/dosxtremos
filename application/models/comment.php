<?php

class Comment extends CI_Model {
    
    var $is_new;
    var $item_id;
    var $date_comment;
    var $name;
    var $client_comment;
    var $admin_comment;
    var $booking;


    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->date_comment = now();
        $this->name = '';
        $this->client_comment = '';
        $this->admin_comment = '';
        
        // Compound properties
        
        $this->booking = new stdClass;
        $this->booking->item_id = 0;
        $this->booking->reference_num = '';
        $this->booking->name = '';
        $this->booking->date_from = '';
        $this->booking->date_to = '';
        $this->booking->home = new stdClass;
        $this->booking->home->item_id = 0;
        $this->booking->home->name = '';
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0)
    {
        
        // Check for an item_id is present
		if ($item_id > 0) {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
			
			// Check for returned data
			if ($query->num_rows() == 1) {
			
				// Populate this instances primary properties
				$row = $query->row();
				
				// Insert the 'simple' properties
				$this->is_new = FALSE;
				$this->item_id = $row->item_id;
		        $this->date_comment = mysqldatetime_to_timestamp($row->date_comment);
		        $this->name = $row->name;
		        $this->client_comment = $row->client_comment;
		        $this->admin_comment = $row->admin_comment;
		        
		        // Set the compund properties with methods
		        $this->set_booking($row->booking_id);
			
			}
		
		}
		
		// Return this instance
		return $this;
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB with optional year, home
	id and booking id filter
	----------------------------------------------------------------*/
    
    function list_entries($year=0,$booking_id=0,$home_id=0)
    {
        
        // Setup the basic query using CI Active Records Class
		$this->db->select('
			comments.item_id,
			comments.name,
			comments.date_comment,
			bookings.item_id AS booking_id,
			bookings.reference_num,
			bookings.date_from,
			bookings.date_to
		', FALSE);
		$this->db->from('comments');
		$this->db->join('bookings', 'comments.booking_id = bookings.item_id', 'inner');
		
		// Check for year filter
		if($year) {
			
			$start_year = timestamp_to_mysqldatetime(mktime(0,0,0,1,1,$year), FALSE);
			$end_year = timestamp_to_mysqldatetime(mktime(0,0,0,12,31,$year), FALSE);
			
			$this->db->where('comments.date_comment >=', $start_year);
			$this->db->where('comments.date_comment <=', $end_year);
			
		}
		
		// Check for booking id filter
		if($booking_id) {
			$this->db->where('bookings.item_id', $booking_id);
		}
		
		// Check for home id filter
		if($home_id) {
			$this->db->where('bookings.home_id', $home_id);
		}
		
		// Set the order
		$this->db->order_by('date_comment asc');
		
		// Run the query and return the results
		$query = $this->db->get();
		return $query;
        
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('comments', array('item_id' => $item_id));
		return $query;
        
    }
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        
        // Gather the data for the query into an array
		$data = array(
        	'booking_id' => $this->booking->item_id,
        	'date_comment' => timestamp_to_mysqldatetime($this->date_comment, FALSE),
        	'name' => trim($this->name),
        	'client_comment' => trim($this->client_comment),
        	'admin_comment' => (trim($this->admin_comment) ? trim($this->admin_comment) : NULL)
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('comments', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('comments', $data);
			
		}
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database.
	----------------------------------------------------------------*/
	
	function delete_entry($item_id)
	{
	
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('comments');
		
	
	}
	
	
	/* SET HOME METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the booking of this entity.
	----------------------------------------------------------------*/
	
	function set_booking($booking_id)
	{
	
		// Get the booking data via a query
		$this->db->select('
			bookings.item_id,
			bookings.reference_num,
			CONCAT(bookings.fname, " ", bookings.lname) AS name,
			bookings.date_from,
			bookings.date_to,
			homes.item_id AS home_id,
			homes.name AS home_name
		', FALSE);
		$this->db->from('bookings');
		$this->db->join('homes', 'bookings.home_id = homes.item_id', 'inner');
		$this->db->where('bookings.item_id', $booking_id);
		$query = $this->db->get();
		
		// Set the booking data if a booking was found
		if($query->num_rows() == 1) {
			
			$row = $query->row();
			
	        $this->booking->item_id = $row->item_id;
	        $this->booking->reference_num = $row->reference_num;
	        $this->booking->name = $row->name;
	        $this->booking->date_from = mysqldatetime_to_timestamp($row->date_from);
	        $this->booking->date_to = mysqldatetime_to_timestamp($row->date_to);
	        $this->booking->home->item_id = $row->home_id;
	        $this->booking->home->name = $row->home_name;

		}
	
	}
    

}