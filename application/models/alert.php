<?php

class Alert extends CI_Model {
    
    var $is_new;
    var $item_id;
    var $is_active;
    var $display_date;
    var $type;
    var $booking;
    var $payment;

    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->is_active = 0;
        $this->display_date = '';
        
        // Complex properites
        
        $this->type = new stdClass;
        $this->type->item_id = '';
        $this->type->name = '';
        
        $this->booking = new stdClass;
        $this->booking->item_id = 0;
        $this->booking->reference_num = '';
        $this->booking->date_from = '';
        $this->booking->name = '';
        $this->booking->mobile = '';
        $this->booking->email = '';
        
        $this->payment = new stdClass;
        $this->payment->item_id = 0;
        $this->payment->amount = 0;
        $this->payment->date_due = '';
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0)
    {
        
        // Check for an item_id is present
		if ($item_id) {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
			
			// Check for returned data
			if ($query->num_rows() == 1) {
			
				// Populate this instances primary properties
				$row = $query->row();
				
				// Insert the 'simple' properties
				$this->is_new = FALSE;
				$this->item_id = $row->item_id;
		        $this->is_active = $row->is_active;
		        $this->display_date = mysqldatetime_to_timestamp($row->display_date);
		        
		        // Use setters for complex types
		        $this->set_booking($row->booking_id);
		        $this->set_payment($row->payment_id);
		        $this->set_type($row->type_id);
			
			}
		
		}
		
		return $this;
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB with parameters for
	inactive and active (default)
	----------------------------------------------------------------*/
    
    function list_entries($show_all=FALSE)
    {
        
        // Setup the query using CI Active Records Class
        $this->db->select('
        	alerts.item_id,
        	alerts.type_id,
        	alerts.is_active,
        	alerts.display_date,
        	bookings.item_id AS booking_id,
        	CONCAT(bookings.fname, " ", bookings.lname) AS booking_name,
        	bookings.reference_num AS booking_reference_num,
        	bookings.date_from AS booking_date_from,
        	bookings.mobile AS booking_mobile,
        	bookings.email AS booking_email,
        	payments.amount AS payment_amount,
        	payments.date_due AS payment_date_due
        ', FALSE);
        $this->db->from('alerts');
        $this->db->join('bookings', 'alerts.booking_id = bookings.item_id', 'left');
        $this->db->join('payments', 'alerts.payment_id = payments.item_id', 'left');
        
        // Get only active alerts that have passed their display dates (default behavior)
        if(!$show_all) {
	        $this->db->where('alerts.is_active', 1);
			$this->db->where('alerts.display_date <=', timestamp_to_mysqldatetime(now(), FALSE));
			#jun18, Hugo Vazquez: only payments alert are to be showed
			$where = "(alerts.type_id='pay_1_delayed' OR alerts.type_id='pay_2_pending' OR alerts.type_id='pay_2_delayed' OR alerts.type_id='pay_3_pending' OR alerts.type_id='pay_3_delayed' OR alerts.type_id='payment')";
			$this->db->where($where);
        }
        
        // Get oldest first
        $this->db->order_by('payment_date_due', 'asc');
        
        $query = $this->db->get();
		
		return $query;
        
    }
    
    
    /* GET COUNT METHOD 
	------------------------------------------------------------------
	Description: Returns the number of active alerts
	(for notification number in admin menu)
	----------------------------------------------------------------*/
	
    function get_num_alerts()
    {
        
        // Use CI Active Records Class to get the number
		$this->db->where('is_active', 1);
	    $this->db->where('display_date <=', timestamp_to_mysqldatetime(now(), FALSE));
		#jun18, Hugo Vazquez: only payment alerts are to be showed
		$this->db->where("(alerts.type_id='pay_1_delayed' OR alerts.type_id='pay_2_pending' OR alerts.type_id='pay_2_delayed' OR alerts.type_id='pay_3_pending' OR alerts.type_id='pay_3_delayed' OR alerts.type_id='payment')");
		
		$this->db->from('alerts');
		
		$num_alerts = $this->db->count_all_results();

		return $num_alerts;
        
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('alerts', array('item_id' => $item_id));
		return $query;
        
    }
	
	
	/* GET ENTRY WITH PAYMENT METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry_paid($payment_id)
    {
        
        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('alerts', array('payment_id' => $payment_id));
		return $query;
        
    }
    
    
    /* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        // Gather the data for the query into an array
		$data = array(
        	'booking_id' => $this->booking->item_id,
        	'payment_id' => $this->payment->item_id,
        	'type_id' => $this->type->item_id,
        	'is_active' => $this->is_active,
        	'display_date' => timestamp_to_mysqldatetime($this->display_date, FALSE)
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('alerts', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('alerts', $data);
			
		}
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database.
	----------------------------------------------------------------*/
	
	function delete_entry($item_id)
	{
	
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('alerts');
		
	
	}
	
	
	/* SET TYPE METHOD 
	------------------------------------------------------------------
	Description: Sets the type data in the model
	----------------------------------------------------------------*/
	
	function set_type($type_id)
	{
	
		// Set the id
		$this->type->item_id = $type_id;
		
		// Set the name
		switch($type_id) {
			
			case 'directions':
				$this->type->name = 'Enviar Direcciones';
				break;
			case 'reminder':
				$this->type->name = 'Llegada el ' . timestamp_to_date($this->booking->date_from, 'd/m/Y');
				break;
			case 'review':
				$this->type->name = 'Enviar Cuestionario';
				break;
			case 'pay_1_delayed':
				$this->type->name = 'Primer Pago Retrasado';
				break;
			case 'pay_2_pending':
				$this->type->name = 'Segundo Pago Pendiente';
				break;
			case 'pay_2_delayed':
				$this->type->name = 'Segundo Pago Retrasado';
				break;
			case 'pay_3_pending':
				$this->type->name = 'Tercer Pago Pendiente';
				break;
			case 'pay_3_delayed':
				$this->type->name = 'Tercer Pago Retrasado';
				break;
			case 'payment':
			default:
				$this->type->name = 'Pago Pendiente';
			
		}
	
	}
	
	
	/* SET BOOKING METHOD 
	------------------------------------------------------------------
	Description: Sets the booking data in the model
	----------------------------------------------------------------*/
	
	function set_booking($booking_id)
	{
	
		// Get the booking data using CI active records
		$this->db->select('
			item_id,
			reference_num,
			CONCAT(fname, " ", lname) AS name,
			date_from,
			mobile,
			email
		', FALSE);
		$this->db->from('bookings');
		$this->db->where('item_id', $booking_id);
		$query = $this->db->get();
		
		// Set the data
		if($query->num_rows() == 1) {
		
	        $row = $query->row();
	        
	        $this->booking->item_id = $row->item_id;
	        $this->booking->reference_num = $row->reference_num;
	        $this->booking->name = $row->name;
	        $this->booking->date_from = $row->date_from;
	        $this->booking->mobile = $row->mobile;
	        $this->booking->email = $row->email;
        
        }
	
	}
	
	
	/* SET PAYMENT METHOD 
	------------------------------------------------------------------
	Description: Sets the payment data in the model
	----------------------------------------------------------------*/
	
	function set_payment($payment_id)
	{
	
		// If a payment ID is defined, get the data
		if($payment_id) {
		
			// Get the payment data using CI active records
			$this->db->select('
				item_id,
				amount,
				date_due
			');
			$this->db->from('payments');
			$this->db->where('item_id', $payment_id);
			$query = $this->db->get();
			
			// Set the data
			if($query->num_rows() == 1) {
			
		        $row = $query->row();
		        
		        $this->payment->item_id = $row->item_id;
		        $this->payment->amount = $row->amount;
		        $this->payment->date_due = mysqldatetime_to_timestamp($row->date_due);
	        
	        }
        
        }
	
	}
	
	/* CHECK IF ALERTS CREATED 
	------------------------------------------------------------------
	Description: Checks if alerts have been created for today
	----------------------------------------------------------------*/
	
	function check_alerts_not_created()
	{
	
		$today = date("Y-m-d");
		$this->db->from('alerts_date');
		$this->db->where('date', $today);
		$this->db->limit(1);
		$query = $this->db->get();
		return ($query->num_rows() == 0);
	
	}

	/* SET ALERTS CREATED FOR TODAY
	------------------------------------------------------------------
	Description: Sets alerts created for today
	----------------------------------------------------------------*/
	
	function set_alerts_created()
	{
		$data = array(
			'date' => date("Y-m-d"),
		);

		$this->db->insert('alerts_date', $data);
	}
	
	/*------------------------------------------------------------------
	Description: Unsets alerts created for today
	----------------------------------------------------------------*/
	
	function unset_alerts_created()
	{
		$this->db->where('date', date('Y-m-d'));
		$this->db->delete('alerts_date');
	}
	
	/*------------------------------------------------------------------
	Description: Delete alerts associated to payment_id if exists
	----------------------------------------------------------------*/
	
	function delete_alerts($payment_id)
	{
		$alerts_deleted = 0;
		$query = $this->get_entry_paid($payment_id);
		if ($query->num_rows() > 0) {
			$alerts = $query->result();
			foreach($alerts AS $alert) {
				$alerts_deleted++;
				$this->delete_entry($alert->item_id);
			}
		}
		return $alerts_deleted;
	}

}