<?php

class Timeslot extends CI_Model {
    
    
    var $is_new;
    var $item_id;
    var $status;
    var $home;
    var $date_slot;
    var $price;
    var $estimate;
    var $offers;
    var $booking;


    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->date_slot = '';
        $this->price = 0;
        $this->estimate = 0;
        
        // Compound properties
        
        $this->status = new stdClass;
        $this->status->item_id = 0;
        $this->status->name = 'Disponible';
        
        $this->home = new stdClass;
        $this->home->item_id = 0;
        $this->home->name = '';
        
        $this->offers = Array();
        $this->booking = new stdClass;
        $this->booking->item_id = 0;
        $this->booking->name = '';
        $this->booking->payments = Array();
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0)
    {
        
        // Check for an item_id is present
		if ($item_id > 0) {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
			
			// Check for returned data
			if ($query->num_rows() == 1) {
			
				// Populate this instances primary properties
				$row = $query->row();
				
				// Insert the 'simple' properties
				$this->is_new = FALSE;
				$this->item_id = $row->item_id;
				$this->date_slot = mysqldatetime_to_timestamp($row->date_slot);
				$this->price = $row->price;
				$this->estimate = $row->estimate;
		        
		        // Set the compund properties with methods
		        $this->set_home($row->home_id);
		        $this->set_bookings($row->item_id);
			
			}
		
		}
		
		// Return this instance
		return $this;
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB with optional home id
	and year filters
	----------------------------------------------------------------*/
    
    function list_entries($home_id=0,$year=0)
    {
        
        // Setup the basic query using CI Active Records Class
		$this->db->from('timeslots');
		
		// Check for home id filter
		if($home_id) {
			$this->db->where('home_id', $home_id);
		}
		
		// Check for year filter
		if($year) {
			
			$start_year = timestamp_to_mysqldatetime(strtotime('-1 week', mktime(0,0,0,1,1,$year)), FALSE);
			$end_year = timestamp_to_mysqldatetime(strtotime('+1 week', mktime(0,0,0,12,31,$year)), FALSE);
			
			$this->db->where('date_slot >=', $start_year);
			$this->db->where('date_slot <=', $end_year);
			
		}
		
		// Set the order
		$this->db->order_by('date_slot');
		
		// Run the query and return the results
		$query = $this->db->get();
		return $query;
        
    }
    
    
    /* GET CALENDAR METHOD 
	------------------------------------------------------------------
	Description: Returns an array structure with the calendar bookings
	and timeslots as objects for the full year for all homes.
	----------------------------------------------------------------*/
	
    function get_calendar($home_id=0,$year=0)
    {
        
        // Get the first and last days of the year
		$start_year = timestamp_to_mysqldatetime(strtotime('-1 week', mktime(0,0,0,1,1,$year)), FALSE);
		$end_year = timestamp_to_mysqldatetime(strtotime('+1 week', mktime(0,0,0,12,31,$year)), FALSE);
		
		// Get all of the timeslots that fall in the year
		$timeslots = $this->list_entries($home_id,$year)->result();
		
		// Get the first friday and following thursday of the year
		$firstDayOfYear = mktime(0, 0, 0, 1, 1, $year);
		$nextSaturday     = strtotime('saturday', $firstDayOfYear);
		$nextFriday   = strtotime('friday', $nextSaturday);
		
		// Setup the calendar object we will return
		$calendar = new stdClass;
		$calendar->year = $year;
		$calendar->weeks = Array();
		
		// Set the spanish locale for the month names
		setlocale(LC_TIME, "es_ES");
		
		// Loop though the year
		while (date('Y', $nextSaturday) == $year) {
			
			// Create the date row as an object
			$week = new stdClass;
			$week->timestamp = $nextSaturday;
			$week->has_prices = FALSE;
			$week->name = ucfirst(strftime('%B %d', $nextSaturday));
			$week->date_start = $nextSaturday;
			$week->date_end = $nextFriday;
			$week->price = 0;
			$week->estimate = 0;
			
			foreach($timeslots as $timeslot) {
				
				$timeslot_timestamp = mysqldatetime_to_timestamp($timeslot->date_slot);
				
				if(($timeslot_timestamp >= $week->date_start) && ($timeslot_timestamp <= $week->date_end)) {
					$week->has_prices = TRUE;
					$week->price += $timeslot->price;
					$week->estimate += $timeslot->estimate;
				}
				
			}
			
			// Round the price and estimate down to the nearest euro
			$week->price = round($week->price, 0);
			$week->estimate = round($week->estimate, 0);
			
			// Append the week to the calendar
			array_push($calendar->weeks, $week);
			
			// Increment the friday and thursday
			$nextSaturday = strtotime('+1 week', $nextSaturday);
			$nextFriday = strtotime('+1 week', $nextFriday);
			
		}
		
		// Return the calendar object
		return $calendar;
        
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('timeslots', array('item_id' => $item_id));
		return $query;
        
    }
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        
        // Gather the data for the query into an array
		$data = array(
        	'home_id' => $this->home->item_id,
        	'date_slot' => timestamp_to_mysqldatetime($this->date_slot),
        	'price' => round($this->price, 2),
        	'estimate' => round($this->estimate, 2)
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('timeslots', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('timeslots', $data);
			
		}
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database. Uses db foreign
	keys to elimintate bookings associated to this entry
	----------------------------------------------------------------*/
	
	function delete_entry($item_id)
	{
	
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('timeslots');
		
	}
	
	
	/* SET HOME METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the home of this entity.
	----------------------------------------------------------------*/
	
	function set_home($home_id)
	{
	
		// Get the home data via a query
		$this->db->select('item_id, name');
		$this->db->from('homes');
		$this->db->where('item_id', $home_id);
		$query = $this->db->get();
		
		// Set the owner data if a owner was found
		if($query->num_rows() == 1) {
			$this->home = $query->row();
		}
	
	}
	
	
	/* SET OFFERS METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the offers and the booking
	of this entity.
	----------------------------------------------------------------*/
	
	function set_bookings($item_id)
	{
	
		$this->db->select('
			bookings.item_id,
			bookings.status_id,
			bookings.reference_num,
			CONCAT(bookings.fname, " ", bookings.lname) AS name,
			bookings.email,
			bookings.date_from,
			bookings.date_to,
			bookings.date_offered,
			bookings.date_booked,
			bookings.mobile,
			bookings.adults,
			bookings.children,
			bookings.agreed_price
		',FALSE);
		$this->db->from('bookings');
		$this->db->join('booking_timeslot', 'booking_timeslot.booking_id = bookings.item_id', 'inner');
		$this->db->where('booking_timeslot.timeslot_id', $item_id);
		$this->db->order_by('date_offered');
		$bookings = $this->db->get()->result();
		
		// If we have pulled offers or bookings
		if($bookings) {
			
			// Loop to separate the offers from the bookings
			foreach($bookings as $booking) {
				
				$booking->date_from = mysqldatetime_to_timestamp($booking->date_from);
				$booking->date_to = mysqldatetime_to_timestamp($booking->date_to);
				$booking->date_offered = mysqldatetime_to_timestamp($booking->date_offered);
				$booking->date_booked = ($booking->date_booked ? mysqldatetime_to_timestamp($booking->date_booked) : NULL);
				
				if($booking->status_id == 1) {
					
					// Add it to the offers array
					array_push($this->offers, $booking);
					
				} else {
					
					// Set it as the current booking and get the booking payments
					$this->booking = $booking;
					$this->db->from('payments');
					$this->db->where('booking_id', $booking->item_id);
					$this->db->order_by('date_due', 'asc');
					$this->booking->payments = $this->db->get()->result();
					foreach($this->booking->payments as $payment) {
						$payment->date_due = ($payment->date_due ? mysqldatetime_to_timestamp($payment->date_due) : NULL);
						$payment->date_paid = ($payment->date_paid ? mysqldatetime_to_timestamp($payment->date_paid) : NULL);
					}
					
				}
				
			}
			
			// Now set the status of this timeslot by the booking or offer
			if($this->booking->item_id) {
				$this->set_status($this->booking->status_id);
			} else {
				if($this->offers) {
					$this->set_status(1);
				}
			}
			
		}
	
	}
	
	
	/* SET STATUS METHOD
	------------------------------------------------------------------
	Description: A simple 'setter' method to set the status values of
	this entity.
	----------------------------------------------------------------*/
	
	function set_status($status_id=0)
	{
		
		// Get the status data via a query
		$query = $this->db->get_where('statuses', array('item_id' => $status_id));
		
		// Set the status data if a status was found
		if($query->num_rows() == 1) {
			$this->status = $query->row();
		}
		
	}
    

}