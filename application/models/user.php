<?php

class User extends CI_Model {
    
    var $is_new;
    var $item_id;
    var $username;
    var $password;
    var $fname;
    var $lname;
    var $email;
    var $role;


    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
    
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->username = '';
        $this->password = '';
        $this->fname = '';
        $this->lname = '';
        $this->email = '';
        
        // Compound properties
        $this->role = new stdClass;
        $this->role->item_id = '';
        $this->role->name = '';
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0)
    {
        
        // Check for an item_id is present
		if ($item_id > 0) {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
			
			// Check for returned data
			if ($query->num_rows() == 1) {
			
				// Populate this instances primary properties
				$row = $query->row();
				
				// Insert the 'simple' properties
				$this->is_new = FALSE;
				$this->item_id = $row->item_id;
				$this->username = $row->username;
				$this->password = $row->password;
				$this->fname = $row->fname;
				$this->lname = $row->lname;
				$this->email = $row->email;
		        
		        // Set the compund properties with methods
		        $this->set_role($row->role_id);
			
			}
		
		}
		
		// Return this instance
		return $this;
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB with optional limit &
	offset for recordset paging
	----------------------------------------------------------------*/
    
    function list_entries($limit=0,$offset=0)
    {
        
        // Setup the basic query using CI Active Records Class
		$this->db->select('
			users.item_id,
			CONCAT(users.fname, " ", users.lname) AS name,
			roles.item_id AS role_id,
			roles.name AS role_name
		', FALSE);
		$this->db->from('users');
		$this->db->join('roles', 'users.role_id = roles.item_id', 'left');
		
		// Check for a limit (for recordset paging)
		if ($limit > 0) {
			$this->db->limit($limit, $offset);
		}
		
		// Set the order
		$this->db->order_by('users.fname, users.lname', 'asc');
		
		// Run the query and return the results
		$query = $this->db->get();
		return $query;
        
    }
    
    
    /* GET TOTAL ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Returns the total number of entries in the DB
	----------------------------------------------------------------*/
	
	function get_total_entries()
    {
        
        // Get the number using CI helper method
		return $this->db->count_all('users');
        
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('users', array('item_id' => $item_id));
		return $query;
        
    }
    
    
    /* AUTHENTICATE METHOD 
	------------------------------------------------------------------
	Description: Authenticates a user via username and password
	and instantiates it's data
	----------------------------------------------------------------*/
	
    function authenticate($username='', $password='')
    {
        
        // Set the defaults
        $data['item_id'] = 0;
        $data['name'] = '';
        $data['role'] = 0;
        $data['authenticated'] = FALSE;
        
        // Use CI Active Records to check the database for the user
        $this->db->select('
        	item_id,
        	CONCAT(users.fname, " ", users.lname) AS name,
        	role_id
        ', FALSE);
        $this->db->from('users');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get();
        
        // If a record is found set the data to return
        if($query->num_rows() == 1) {
	        
	        $row = $query->row();
	        
	        $data['item_id'] = $row->item_id;
	        $data['name'] = $row->name;
	        $data['role'] = $row->role_id;
	        $data['authenticated'] = TRUE;
	        
        } else {
	        $data['authenticated'] = FALSE;
        }
        
        // Return the data
        return $data;
        
    }
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        // Gather the data for the query into an array
		$data = array(
        	'username' => trim($this->username),
        	'password' => trim($this->password),
        	'role_id' => $this->role->item_id,
        	'fname' => trim($this->fname),
        	'lname' => trim($this->lname),
        	'email' => trim($this->email)
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('users', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('users', $data);
			
		}
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database.
	----------------------------------------------------------------*/
	
	function delete_entry($item_id)
	{
	
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('users');
		
	
	}
	
	
	/* SET LANGUAGE METHOD
	------------------------------------------------------------------
	Description: A 'setter' method to set the language of this entity.
	----------------------------------------------------------------*/
	
	function set_role($role_id)
	{
	
		// Get the role data via the role model
		$ci =& get_instance();
		$ci->load->model('role');
		$query = $ci->role->get_entry($role_id);
		
		// Set the role data if a role was found
		if($query->num_rows() == 1) {
			$this->role = $query->row();
		}
	
	}
    

}