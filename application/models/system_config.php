<?php

class System_config extends CI_Model {
    
    var $is_new;
    var $item_id;
    var $pay_percent_1;
    var $pay_percent_2;
    var $commission_percent;
    var $tax_id;
    var $vat;
    var $bank;


    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
    
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->pay_percent_1 = 0;
        $this->pay_percent_2 = 0;
        $this->commission_percent = 0;
        $this->tax_id = '';
        $this->vat = 0;
        
        // Bank object
        $this->bank = new stdClass;
        $this->bank->name = '';
        $this->bank->nationality = 'es';
        $this->bank->iban = '';
        $this->bank->bic = '';
        $this->bank->account = new stdClass;
        $this->bank->account->name = '';
        $this->bank->account->number = '';
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0)
    {
        
        // Check for an item_id is present
		if ($item_id > 0) {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
			
			// Check for returned data
			if ($query->num_rows() == 1) {
			
				// Populate this instances primary properties
				$row = $query->row();
				
				// Insert the 'simple' properties
				$this->is_new = FALSE;
				$this->item_id = $row->item_id;
				$this->pay_percent_1 = $row->pay_percent_1;
				$this->pay_percent_2 = $row->pay_percent_2;
				$this->commission_percent = $row->commission_percent;
				$this->tax_id = $row->tax_id;
				$this->vat = $row->vat;
				$this->bank->name = $row->bank_name;
				$this->bank->nationality = $row->bank_nationality;
				$this->bank->iban = $row->bank_iban;
				$this->bank->bic = $row->bank_bic;
				$this->bank->account->name = $row->bank_account_name;
				$this->bank->account->number = $row->bank_account_number;
			
			}
		
		}
		
		// Return this instance
		return $this;
        
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('config', array('item_id' => $item_id));
		return $query;
        
    }
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        // Gather the data for the query into an array
		$data = array(
        	'pay_percent_1' => $this->pay_percent_1,
        	'pay_percent_2' => $this->pay_percent_2,
        	'commission_percent' => $this->commission_percent,
        	'tax_id' => trim($this->tax_id),
        	'vat' => $this->vat,
        	'bank_name' => $this->bank->name,
        	'bank_nationality' => $this->bank->nationality,
        	'bank_iban' => $this->bank->iban,
        	'bank_bic' => $this->bank->bic,
        	'bank_account_name' => $this->bank->account->name,
        	'bank_account_number' => $this->bank->account->number
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('config', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('config', $data);
			
		}
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database.
	----------------------------------------------------------------*/
	
	function delete_entry($item_id)
	{
	
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('config');
		
	
	}
    

}