<?php

class Invoice extends CI_Model {
    
    var $is_new;
    var $item_id;
    var $invoice_num;
    var $invoice_date;
    var $invoice_description;
    var $net_amount;
    var $vat_amount;
    var $total_amount;
    var $owner;
    var $line_items;

    /* CLASS CONSTRUCTOR 
	------------------------------------------------------------------
	Description: Inherets parents methods and properties
	----------------------------------------------------------------*/
	
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        // Simple properties
        $this->is_new = TRUE;
        $this->item_id = 0;
        $this->invoice_num = 0;
        $this->invoice_date = '';
        $this->invoice_description = '';
        $this->net_amount = 0;
        $this->vat_amount = 0;
        $this->total_amount = 0;
        
        // Owner Object
        $this->owner = new stdClass;
        $this->owner->item_id = 0;
        $this->owner->name = '';
        $this->owner->address = '';
        $this->owner->city = '';
        $this->owner->province = '';
        $this->owner->postal_code = '';
        $this->owner->country = '';
        $this->owner->tax_id = '';
        
        // Line Items
        $this->line_items = Array();
        
    }
    
    
    /* INITIALIZE METHOD 
	------------------------------------------------------------------
	Description: Pseudo-OOP constructor to instantiate a class with
	data from the DB.
	----------------------------------------------------------------*/
    
    function initialize($item_id=0,$owner_id=0,$year=0,$trimester=0)
    {
        
        // Check for an item_id is present
		if ($item_id) {
		
			// Get the entry data query from the 'get_entry' method
			$query = $this->get_entry($item_id);
			
			// Check for returned data
			if ($query->num_rows() == 1) {
			
				// Populate this instances primary properties
				$row = $query->row();
				
				// Insert the 'simple' properties
				$this->is_new = FALSE;
				$this->item_id = $row->item_id;
				$this->invoice_num = $row->invoice_num;
				$this->invoice_date = mysqldatetime_to_timestamp($row->invoice_date);
				$this->invoice_description = $row->invoice_description;
				$this->net_amount = $row->net_amount;
				$this->vat_amount = $row->vat_amount;
				$this->total_amount = $row->total_amount;
				
				// Set the Owner Object
				$this->set_owner($row->owner_id);
				
				// Set the line items
				$this->line_items = $this->list_line_items($row->item_id)->result();
			
			}
		
		} else if((!$item_id) && ($owner_id)) {
			
			$this->set_default_invoice_num();
			$this->invoice_date = now();
			$this->invoice_description = '';
			$this->set_owner($owner_id);
			$this->set_default_line_items($owner_id,$year,$trimester);
			
		}
		
		return $this;
        
    }
    
    
    /* LIST ENTRIES METHOD 
	------------------------------------------------------------------
	Description: Lists all entries in the DB. Takes a parameter to filter
	on a specific owner and year
	----------------------------------------------------------------*/
    
    function list_entries($owner_id=0,$year=0)
    {
        
        // Setup the basic query using CI Active Records Class
        $this->db->from('invoices');
        
        if($owner_id) {
	        $this->db->where('owner_id', $owner_id);
        }
        
        if($year) {
	        $start_year = timestamp_to_mysqldatetime(mktime(0,0,0,1,1,$year));
			$end_year = timestamp_to_mysqldatetime(mktime(0,0,0,12,31,$year));
			$this->db->where('invoice_date >=', $start_year);
			$this->db->where('invoice_date <=', $end_year);
        }
        
        $this->db->order_by('invoice_date', 'asc');
        
        $query = $this->db->get();
		return $query;
        
    }
    
    
     /* LIST LINE ITEMS METHOD 
	------------------------------------------------------------------
	Description: Lists all line items entries related to this invoice
	----------------------------------------------------------------*/
    
    function list_line_items($item_id)
    {
        
        // Setup the basic query using CI Active Records Class
        $this->db->select('
        	line_items.item_id,
        	line_items.description,
        	line_items.net_amount,
        	line_items.vat_percent,
        	line_items.vat_amount,
        	line_items.total_amount,
        	payments.item_id AS payment_id,
        	expenses.item_id AS expense_id
        ', FALSE);
        $this->db->from('line_items');
        $this->db->join('payments', 'line_items.item_id = payments.line_item_id', 'left');
        $this->db->join('expenses', 'line_items.item_id = expenses.line_item_id', 'left');
        $this->db->where('line_items.invoice_id', $item_id);
        $this->db->order_by('line_items.item_id', 'asc');
        $query = $this->db->get();
		return $query;
        
    }
	
	
	/* GET ENTRY METHOD 
	------------------------------------------------------------------
	Description: Returns an entry based on it's item_id
	----------------------------------------------------------------*/
	
    function get_entry($item_id)
    {
        
        // Use CI Active Records Class to create the query
        $query = $this->db->get_where('invoices', array('item_id' => $item_id));
		return $query;
        
    }
    
    
	/* SAVE ENTRY 
	------------------------------------------------------------------
	Description: Collects the data from this instance and inserts or
	updates the data in the DB based on whether it is new or not
	----------------------------------------------------------------*/
	
    function save_entry()
    {
        
        // Gather the data for the query into an array
		$data = array(
        	'invoice_num' => $this->invoice_num,
        	'invoice_date' => timestamp_to_mysqldatetime($this->invoice_date, FALSE),
        	'invoice_description' => trim($this->invoice_description),
        	'owner_id' => $this->owner->item_id,
        	'net_amount' => $this->net_amount,
        	'vat_amount' => $this->vat_amount,
        	'total_amount' => $this->total_amount
        );
		
		// Check the 'is_new' status
		if ($this->is_new) {
			
			// SQL Insert (for new entries) and set the item_id
			$this->db->insert('invoices', $data);
			$this->item_id = $this->db->insert_id();
		
		} else {
			
			// SQL Update (for existing entries)
			$this->db->where('item_id', $this->item_id);
			$this->db->update('invoices', $data);
			
			// Remove any existing line items (their delete will also set any payment or expense line_item_id to NULL via the DB relation trigger)
			$this->db->where('invoice_id', $this->item_id);
			$this->db->delete('line_items');
			
		}
		
		// Insert the new line items
		if($this->line_items) {
			
			foreach($this->line_items as $line_item) {
				
				// Prep the line item data to be inserted
				$tmp_data = array(
					'invoice_id' => $this->item_id,
					'description' => $line_item->description,
					'net_amount' => $line_item->net_amount,
					'vat_percent' => $line_item->vat_percent,
					'vat_amount' => $line_item->vat_amount,
					'total_amount' => $line_item->total_amount
				);
				
				// Insert the line item and get it's ID
				$this->db->insert('line_items', $tmp_data);
				$line_id = $this->db->insert_id();
				
				// If this is a payment update the payments line_item_id
				if($line_item->payment_id) {
					$this->db->where('item_id', $line_item->payment_id);
					$this->db->update('payments', array('line_item_id' => $line_id));
				}
				
				// If this is an expense update the expenses line_item_id
				if($line_item->expense_id) {
					$this->db->where('item_id', $line_item->expense_id);
					$this->db->update('expenses', array('line_item_id' => $line_id));
				}
				
			}
			
		}
        
    }
	
	
	/* DELETE ENTRY 
	------------------------------------------------------------------
	Description: Deletes an entry from the database and all line items
	of that entry via db cascade rules
	----------------------------------------------------------------*/
	
	function delete_entry($item_id)
	{
		// Delete entry via CI Active Record Class
		$this->db->where('item_id', $item_id);
		$this->db->delete('invoices');
		
	}
	
	
	/* SET DEFAULT INVOICE NUMBER 
	------------------------------------------------------------------
	Description: Setter method to create a default invoice number
	based on the current year and an increment value
	----------------------------------------------------------------*/
	
	function set_default_invoice_num()
	{
	
		// Get the start and end date of this year
		$start_year = timestamp_to_mysqldatetime(mktime(0,0,0,1,1,(date('Y'))));
		$end_year = timestamp_to_mysqldatetime(mktime(0,0,0,12,31,(date('Y'))));
		
		// Get the latest invoice number within this year
		$this->db->select('invoice_num');
		$this->db->from('invoices');
		$this->db->where('invoice_date >=', $start_year);
		$this->db->where('invoice_date <=', $end_year);
		$this->db->order_by('invoice_num', 'desc');
		$this->db->limit(1);
		$query = $this->db->get();
		
		if($query->num_rows() == 1) {
			$row = $query->row();
			$this->invoice_num = $row->invoice_num + 1;
		} else {
			$this->invoice_num = intval((date('y') . '0001'));
		}
	
	}
	
	
	/* SET OWNER 
	------------------------------------------------------------------
	Description: Setter method to set the owners data based on their
	item id number
	----------------------------------------------------------------*/
	
	function set_owner($owner_id)
	{
	
		$this->db->select('
			item_id,
			CONCAT(fname, " ", lname) AS name,
			address,
			city,
			province,
			postal_code,
			country,
			tax_id
		', FALSE);
		$this->db->from('owners');
		$this->db->where('item_id', $owner_id);
		$query = $this->db->get();
		
		if($query->num_rows() == 1) {
			$row = $query->row();
			$this->owner->item_id = $row->item_id;
			$this->owner->name = $row->name;
			$this->owner->address = $row->address;
			$this->owner->city = $row->city;
			$this->owner->province = $row->province;
			$this->owner->postal_code = $row->postal_code;
			$this->owner->country = $row->country;
			$this->owner->tax_id = $row->tax_id;
		}
	
	}
	
	
	/* SET LINE ITEMS 
	------------------------------------------------------------------
	Description: Setter method to set line items (from form). Uses
	lists of payment_ids and expenses_ids to get the data
	----------------------------------------------------------------*/
	
	function set_line_items($item_ids='', $descriptions='', $net_amounts='', $vat_percentages='', $vat_amounts='', $total_amounts='', $payment_ids='', $expense_ids='')
	{
	
		// Convert the lists to arrays
		$item_ids = explode('|', $item_ids);
		$descriptions = explode('|', $descriptions);
		$net_amounts = explode('|', $net_amounts);
		$vat_percentages = explode('|', $vat_percentages);
		$vat_amounts = explode('|', $vat_amounts);
		$total_amounts = explode('|', $total_amounts);
		$payment_ids = explode('|', $payment_ids);
		$expense_ids = explode('|', $expense_ids);
		
		// Clear the current line items array
		$this->line_items = Array();
		
		// Loop through the arrays to re-populate the data
		if(count($item_ids) > 0) {
			for($i=0; $i < count($item_ids); $i++) {
				$line_item = new stdClass;
				$line_item->item_id = $item_ids[$i];
				$line_item->description = $descriptions[$i];
				$line_item->net_amount = $net_amounts[$i];
				$line_item->vat_percent = $vat_percentages[$i];
				$line_item->vat_amount = $vat_amounts[$i];
				$line_item->total_amount = $total_amounts[$i];
				$line_item->payment_id = $payment_ids[$i];
				$line_item->expense_id = $expense_ids[$i];
				array_push($this->line_items, $line_item);
			}
		}
	
	}
	
	
	/* SET DEFAULT LINE ITEMS 
	------------------------------------------------------------------
	Description: Setter method to gather the owners un-invoiced
	commissions and expenses and turn them into line items
	----------------------------------------------------------------*/
	
	function set_default_line_items($owner_id,$year=0,$trimester=0)
	{
	
		$timespan = get_fiscal_trimester_timespan($trimester,$year);
		
		// Get all paid payments that have been paid and have not been invoiced
		$this->db->select('
			payments.item_id,
			(payments.amount * (payments.commission_percent/100)) AS net_amount,
			payments.date_paid,
			bookings.reference_num,
			owners.language_id
		', FALSE);
		$this->db->from('payments');
		$this->db->join('bookings', 'payments.booking_id = bookings.item_id', 'inner');
		$this->db->join('homes', 'bookings.home_id = homes.item_id', 'inner');
		$this->db->join('owners', 'homes.owner_id = owners.item_id', 'inner');
		$this->db->where('owners.item_id', $owner_id);
		$this->db->where('payments.line_item_id IS NULL', NULL, FALSE);
		if($timespan->start != '' && $timespan->end != '') {
			$this->db->where('payments.date_paid >=', timestamp_to_mysqldatetime($timespan->start, FALSE));
			$this->db->where('payments.date_paid <=', timestamp_to_mysqldatetime($timespan->end, FALSE));
		} else {
			$this->db->where('payments.date_paid IS NOT NULL', NULL, FALSE);
		}
		$this->db->order_by('bookings.reference_num asc, payments.sort_order asc');
		$payments = $this->db->get()->result();
		
		// Loop through any payments and add them to the line items array, and increment the totals
		foreach($payments as $payment) {
			$tmp_item = new stdClass;
			$tmp_item->item_id = 0;
			$tmp_item->description = ($payment->language_id == 'en' ? ('Commission for Booking N&deg; ' . $payment->reference_num) : ('Comisi&oacute;n Booking N&deg; ' . $payment->reference_num));
			$tmp_item->net_amount = $payment->net_amount;
			$tmp_item->vat_percent = 21;
			$tmp_item->vat_amount = $payment->net_amount * (21/100);
			$tmp_item->total_amount = $payment->net_amount + ($payment->net_amount * (21/100));
			$tmp_item->payment_id = $payment->item_id;
			$tmp_item->expense_id = NULL;
			array_push($this->line_items, $tmp_item);
			$this->net_amount += $payment->net_amount;
			$this->vat_amount += ($payment->net_amount * (21/100));
			$this->total_amount += ($payment->net_amount + ($payment->net_amount * (21/100)));
		}
		
		// Get all the expenses that have not been invoiced
		$this->db->select('
			expenses.item_id,
			expenses.description,
			expenses.amount AS net_amount,
			bookings.reference_num,
			owners.language_id
		', FALSE);
		$this->db->from('expenses');
		$this->db->join('bookings', 'expenses.booking_id = bookings.item_id', 'inner');
		$this->db->join('homes', 'bookings.home_id = homes.item_id', 'inner');
		$this->db->join('owners', 'homes.owner_id = owners.item_id', 'inner');
		$this->db->where('owners.item_id', $owner_id);
		$this->db->where('bookings.status_id >', 1);
		if($timespan->start != '' && $timespan->end != '') {
			$this->db->where('bookings.date_to >=', timestamp_to_mysqldatetime($timespan->start, FALSE));
			$this->db->where('bookings.date_to <=', timestamp_to_mysqldatetime($timespan->end, FALSE));
		} else {
			$this->db->where('bookings.date_to <=', timestamp_to_mysqldatetime(now(), FALSE));
		}
		$this->db->where('expenses.line_item_id IS NULL', NULL, FALSE);
		$this->db->order_by('bookings.reference_num asc');
		$expenses = $this->db->get()->result();
		
		// Loop through any payments and add them to the line items array
		foreach($expenses as $expense) {
			$tmp_item = new stdClass;
			$tmp_item->item_id = 0;
			$tmp_item->description = 'Booking N&deg; ' . $expense->reference_num . ' : ' . $expense->description;
			$tmp_item->net_amount = $expense->net_amount;
			$tmp_item->vat_percent = 0;
			$tmp_item->vat_amount = 0;
			$tmp_item->total_amount = $expense->net_amount;
			$tmp_item->payment_id = NULL;
			$tmp_item->expense_id = $expense->item_id;
			array_push($this->line_items, $tmp_item);
			$this->net_amount += $expense->net_amount;
			$this->vat_amount += 0;
			$this->total_amount += $expense->net_amount;
		}
	
	}
    

}