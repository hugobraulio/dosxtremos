/* COMMON FUNCTIONALITIES
------------------------------------------------------- 
File: ./assets/js/admin_bookings_list.js
Description: JS Functionality for the main admin bookings
calendar.
------------------------------------------------------- */

$(document).ready(function() {


	/* TIMESLOT MODALS
	--------------------------------------------------- */
	
	// 1. Cache and defaults
	var $modals = $('#modals');
	var $timeslots = $('table.calendar tbody');
	var $clicked_date = '';
	
	// 2. Dates action links
	$timeslots.on('click', 'a.date_action',function(e){
		
		// 1. Prevent the default and set the clicked date
		e.preventDefault();
		$clicked_date = $(this).parents('tr');
		
		// 2. Make the call to the timeslot controller
		var ajax_url = $(this).attr('href');
		$.ajax({
			type: 'GET',
			url: ajax_url,
			dataType: 'json',
			success: function(data){
				initialize_form_modal(data.modal);
			},
			error: function(data, status, e){
				alert('An AJAX error has occured : ' + e);
			}
		});
		
	});
	
	// 3. Delete Links
	$timeslots.on('click', 'a.date_delete', function(e) {
		
		// 1. Prevent the default and set the clicked date
		e.preventDefault();
		$clicked_date = $(this).parents('tr');
		
		// 2. Make the call to the timeslot controller
		var ajax_url = $(this).attr('href');
		$.ajax({
			type: 'GET',
			url: ajax_url,
			dataType: 'json',
			success: function(data){
				
				// Update the values in the row that was clicked
				$clicked_date.find('td.price').text('Sin Precio');
				$clicked_date.find('td.estimate').text('Sin Precio');
				$clicked_date.addClass('no-price');
				
				// Update the action link
				$action_link = $clicked_date.find('a.date_action');
				$action_link.attr('href', (SITE_URL + 'admin/timeslots/action/new/' + data.home_id + '/' + data.date_start + '/' + data.date_end));
				$action_link.children('i').attr('class', 'icon-plus');
				
				// Update the delete link
				$delete_link = $clicked_date.find('a.date_delete');
				$delete_link.addClass('disabled');
				
			},
			error: function(data, status, e){
				alert('An AJAX error has occured : ' + e);
			}
		});
		
	});
	
	// Function to initialize form modals
	function initialize_form_modal(modal_html) {
	
		// Remove any existing modals first
		$modals.children('.modal').remove();
		
		// Convert the html to a jQuery object and set the bootstrap functionality on it
		var $new_modal = $(modal_html);
		$new_modal.modal();
		$new_modal.find('#timeslot_form').bind({'submit' : submit_prices});
		$new_modal.find('.currency').currencyFormat();
		
		// Append it to the modals div
		$modals.append($new_modal);
		
	}
	
	// Function to submit the price form
	function submit_prices(e) {
		
		// 1. Prevent the default
		e.preventDefault();
		
		// 2. Get the elements and variables we will need
		var $target = $(e.target);
		var $form_modal = $target.parent();
		var ajax_url = $target.attr('action');
		var home_id_val = $target.find('#home_id').val();
		var price_val = $target.find('#price').val();
		var estimate_val = $target.find('#estimate').val();
		
		// Post the data via Ajax to the controller
		$.ajax({
			type: 'POST',
			url: ajax_url,
			dataType: 'json',
			data: {
				home_id: home_id_val,
				price: price_val,
				estimate: estimate_val
			},
			success: function(data){
				
				if(data.modal == '') {
				
					// The form was submitted correctly, hide the modal
					$form_modal.modal('hide');
					
					// Update the values in the row that was clicked
					$clicked_date.find('td.price').text(data.price + ' \u20ac');
					$clicked_date.find('td.estimate').text(data.estimate + ' \u20ac');
					$clicked_date.removeClass('no-price');
					
					// Update the action link
					$action_link = $clicked_date.find('a.date_action');
					$action_link.attr('href', (SITE_URL + 'admin/timeslots/action/edit/' + data.home_id + '/' + data.date_start + '/' + data.date_end));
					$action_link.children('i').attr('class', 'icon-pencil');
					
					// Update the delete link
					$delete_link = $clicked_date.find('a.date_delete');
					$delete_link.removeClass('disabled');
					
				
				} else {
				
					// There was an error, replace the form (body only)
					var $modal_body = $(data.modal).find('.modal-body');
					$modal_body.find('.currency').currencyFormat();
					$target.find('.modal-body').remove();
					$target.prepend($modal_body);
					
				}
			},
			error: function(data, status, e){
				alert('An AJAX error has occured : ' + e);
			}
		});
		
	}
	
	
	
	
	
	
	/* SCROLLING CALENDAR TABLE
	--------------------------------------------------- */
	
	// Cache and defaults
	var $scroll_table = $('#scroll-table');
	var $scroll_table_left = $scroll_table.children('.fixed-column').find('a.scroll-left');
	var $scroll_table_right = $scroll_table.children('.fixed-column').find('a.scroll-right');
	var $scroll_table_content = $scroll_table.children('.scroll-pane');
	var scroll_width = 157;
	
	// Left Button
	$scroll_table_left.click(function(e){
		// Prevent the default
		e.preventDefault();
		// Scroll the content
		$scroll_table_content.animate({scrollLeft:'-=' + scroll_width}, 'fast');
		
	});
	
	// Right Button
	$scroll_table_right.click(function(e){
		// Prevent the default
		e.preventDefault();
		// Scroll the content
		$scroll_table_content.animate({scrollLeft:'+=' + scroll_width}, 'fast');
		
	});
	
	
	
	
	

});


