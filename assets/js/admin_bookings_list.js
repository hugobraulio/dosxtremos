/* COMMON FUNCTIONALITIES
------------------------------------------------------- 
File: ./assets/js/admin_bookings_list.js
Description: JS Functionality for the main admin bookings
calendar.
------------------------------------------------------- */

$(document).ready(function() {


	/* TIMESLOT MODALS
	--------------------------------------------------- */
	
	// 1. Cache and defaults
	var $modals = $('#modals');
	var $timeslots = $('#scroll-table .scroll-pane tbody');
	var $clicked_date = '';
	
	// 2. Valid timeslots load view modal
	$timeslots.on('click', 'a:not(.no-price)', function(e){
		
		// 1. Prevent the default
		e.preventDefault();
		$clicked_date = $(this);
		
		// 2. Make the call to the timeslot controller
		var ajax_url = $(this).attr('href');
		$.ajax({
			type: 'GET',
			url: ajax_url,
			dataType: 'html',
			success: function(data){
				initialize_view_modal(data);
			},
			error: function(data, status, e){
				alert('An AJAX error has occured : ' + e);
			}
		});
		
	});
	
	// 3. Dates with no price load form modal
	$timeslots.on('click', 'a.no-price',function(e){
		
		// 1. Prevent the default and set the clicked date
		e.preventDefault();
		$clicked_date = $(this);
		
		// 2. Make the call to the timeslot controller
		var ajax_url = $(this).attr('href');
		$.ajax({
			type: 'GET',
			url: ajax_url,
			dataType: 'json',
			success: function(data){
				initialize_form_modal(data.modal);
			},
			error: function(data, status, e){
				alert('An AJAX error has occured : ' + e);
			}
		});
		
	});
	
	// Function to initialize view modals
	function initialize_view_modal(modal_html) {
	
		// Remove any existing modals first
		$modals.children('*').remove();
		
		// Convert the html to a jQuery object and set the bootstrap functionality on it
		var $new_modal = $(modal_html);
		$new_modal.modal();
		$new_modal.find('.nav-tabs a').click(function (e) {
			e.preventDefault();
			$(this).tab('show');
		});
		$new_modal.find('.nav-tabs a:first').tab('show');
		$new_modal.find('.tab-content .tab-pane:first').addClass('active');
		
		// Append it to the modals div
		$modals.append($new_modal);
		
	}
	
	// Function to initialize form modals
	function initialize_form_modal(modal_html) {
	
		// Remove any existing modals first
		$modals.children('*').remove();
		
		// Convert the html to a jQuery object and set the bootstrap functionality on it
		var $new_modal = $(modal_html);
		$new_modal.modal();
		$new_modal.find('#timeslot_form').bind({'submit' : submit_prices});
		$new_modal.find('.currency').currencyFormat();
		
		// Append it to the modals div
		$modals.append($new_modal);
		
	}
	
	// Function to submit the price form
	function submit_prices(e) {
		
		// 1. Prevent the default
		e.preventDefault();
		
		// 2. Get the elements and variables we will need
		var $target = $(e.target);
		var $form_modal = $target.parent();
		var ajax_url = $target.attr('action');
		var home_id_val = $target.find('#home_id').val();
		var price_val = $target.find('#price').val();
		var estimate_val = $target.find('#estimate').val();
		
		// Post the data via Ajax to the controller
		$.ajax({
			type: 'POST',
			url: ajax_url,
			dataType: 'json',
			data: {
				home_id: home_id_val,
				price: price_val,
				estimate: estimate_val
			},
			success: function(data){
				
				if(data.modal == '') {
				
					// The form was submitted correctly, hide the modal
					$form_modal.modal('hide');
					
					// Update the date link that was clicked
					$clicked_date.removeClass('no-price');
					$clicked_date.text(data.price + ' \u20ac');
					$clicked_date.attr('href', (SITE_URL + 'admin/timeslots/view_week/' + data.home_id + '/' + data.date_start + '/' + data.date_end + '/' + data.timestamp));
				
				} else {
				
					// There was an error, replace the form (body only)
					var $modal_body = $(data.modal).find('.modal-body');
					$modal_body.find('.currency').currencyFormat();
					$target.find('.modal-body').remove();
					$target.prepend($modal_body);
					
				}
			},
			error: function(data, status, e){
				alert('An AJAX error has occured : ' + e);
			}
		});
		
	}

	
	/* SCROLLING CALENDAR TABLE
	--------------------------------------------------- */
	
	// Cache and defaults
	var $scroll_table = $('#scroll-table');
	var $scroll_table_left = $scroll_table.children('.fixed-column').find('a.scroll-left');
	var $scroll_table_right = $scroll_table.children('.fixed-column').find('a.scroll-right');
	var $scroll_table_content = $scroll_table.children('.scroll-pane');
	var scroll_width = 157;
	
	// Left Button
	$scroll_table_left.click(function(e){
		// Prevent the default
		e.preventDefault();
		// Scroll the content
		$scroll_table_content.animate({scrollLeft:'-=' + scroll_width}, 'fast');
		
	});
	
	// Right Button
	$scroll_table_right.click(function(e){
		// Prevent the default
		e.preventDefault();
		// Scroll the content
		$scroll_table_content.animate({scrollLeft:'+=' + scroll_width}, 'fast');
		
	});
	
	
	/* SCROLLING CALENDAR TABLE HEADERS (Ghost Headers)
	--------------------------------------------------- */
	
	// Homes Table Header
	var $homes_header = $scroll_table.find('#homes-header');
	var homes_start_pos = $homes_header.offset().top;
	var homes_height = $homes_header.height();
	
	var $dates_header = $scroll_table.find('#dates-header');
	var dates_start_pos = $dates_header.offset().top;
	var dates_height = $dates_header.height();
	
	$.event.add(window, "scroll", function() {
        
        // Get the current document scroll offset
        var current_scroll = $(window).scrollTop();
        
        // Set positioning and visibility for the homes header
        if(current_scroll > homes_start_pos) {
	        $homes_header.css('top', ((current_scroll - homes_start_pos) + homes_height) + 'px');
	        $homes_header.css('visibility', 'visible');
        } else {
	        $homes_header.css('top', '0px');
	        $homes_header.css('visibility', 'hidden');
        }
        
        // Set positioning and visibility for the dates header
        if(current_scroll > dates_start_pos) {
	        $dates_header.css('top', ((current_scroll - dates_start_pos) + dates_height) + 'px');
	        $dates_header.css('visibility', 'visible');
        } else {
	        $dates_header.css('top', '0px');
	        $dates_header.css('visibility', 'hidden');
        }
        
    });
	
	

});


