/* COMMON FUNCTIONALITIES
------------------------------------------------------- 
File: ./assets/js/admin_bookings_list.js
Description: JS Functionality for the main admin bookings
calendar.
------------------------------------------------------- */

$(document).ready(function() {
	
	// Disable AJAX Caching
	$.ajaxSetup({ cache: false });

	/* HELPER FUNCTIONS
	--------------------------------------------------- */
	
	// Mini jQuery plugin that formats to two decimal places
    (function($) {
        $.fn.currencyFormat = function() {
            this.each(function(i) {
                
                $(this).keyup(function() {
					if(this.value != this.value.replace(/[^0-9\.]/g, '')) {
						this.value = this.value.replace(/[^0-9\.]/g, '');
					}
				});
                
                $(this).blur(function(e){
                    if(isNaN(parseFloat(this.value))) return;
                    this.value = parseFloat(this.value).toFixed(2);
                });
                
            });
            return this; //for chaining
        }
    })( jQuery );
    
    // Mini jQuery plugin that formats percentages
    (function($) {
        $.fn.percentFormat = function() {
            this.each(function(i) {
                
                $(this).keyup(function(e){
				    if (!isNaN(parseInt(this.value,10))) {
				        this.value = parseInt(this.value);
				    } else {
				        this.value = 0;
				    }
				    this.value = this.value.replace(/[^0-9]/g, '');
				    if (parseInt(this.value,10) > 100) {
				        this.value = 100;
				        return;
				    }
				});
                
            });
            return this; //for chaining
        }
    })( jQuery );
    
    
    /* COMMON ELEMENTS
	--------------------------------------------------- */
	
	// Currency Form Inputs
	$('input.currency').currencyFormat();
	
	// Percentage Form Inputs
	$('input.percentage, input.commission').percentFormat();
	
	// Date Picker Inputs
	$('input.datepicker').datepicker({
		format : 'dd/mm/yyyy'
	});
	
	// Timepicker Form Inputs
	$('input.timepicker').timepicker({
		defaultTime : false,
		showMeridian : false,
		disableFocus : true
	});
	
	// WYSIWYG Editor Fields
	$('textarea.wysiwyg').ckeditor({
		height : '600px',
		language : 'es',
		format_tags : 'p;h2;h3;h4;h5;h6',
		skin : 'BootstrapCK-Skin',
		toolbar : [
			['Undo','Redo'], ['Bold','Italic','Underline','Strike','Subscript','Superscript'],
			['Format'], ['NumberedList','BulletedList','-','Outdent','Indent']
		]
	});
	
	// Bootstrap Tabs
	$('#document_tabs a').click(function(e){
		e.preventDefault();
		$(this).tab('show');
	});
	
	$('.disabled').click(function(e){
		e.preventDefault();
	});
	
	
	/* ALERT MODALS
	--------------------------------------------------- */
	
	// 1. Cache and defaults
	var $modals = $('#modals');
	var $alert_trigger = $('#alert_trigger');
	var $num_alerts = $alert_trigger.children('.badge');
	
	// 2. Valid timeslots load view modal
	$alert_trigger.on('click', function(e){
		
		// 1. Prevent the default
		e.preventDefault();
		
		// 2. Make the call to the timeslot controller (only if there are alerts)
		if(parseInt($num_alerts.text()) > 0) {
			var ajax_url = $(this).attr('href');
			$.ajax({
				type: 'GET',
				url: ajax_url,
				dataType: 'html',
				success: function(data){
					initialize_alert_modal(data);
				},
				error: function(data, status, e){
					alert('An AJAX error has occured on displaying the list : ' + e);
				}
			});
		}
		
	});
	
	
	// Function to initialize alert modals
	function initialize_alert_modal(modal_html) {
	
		// Remove any existing modals first
		$modals.children('*').remove();
		
		// Convert the html to a jQuery object and set the bootstrap functionality on it
		var $new_modal = $(modal_html);
		
		// Modal functionality
		$new_modal.modal();
		
		// Deactivate alert ajax call
		$new_modal.find('a[data-dismiss="alert"]').on('click', function(e) {
			
			// Prevent the default method
			e.preventDefault();
			
			// Get this item in the list
			$row = $(this).parent();
			
			// Make the Ajax call
			var ajax_url = $(this).attr('href');
			$.ajax({
				type: 'GET',
				url: ajax_url,
				dataType: 'json',
				success: function(data){
					if(data.status) {
						$row.remove();
						set_num_alerts();
					} else {
						alert('There has been an error deactivating the alert');
					}
				},
				error: function(data, status, e){
					alert('An AJAX error has occured when trying to deactivate the alert : ' + e);
				}
			});
			
			
		});
		
		// Append it to the modals div
		$modals.append($new_modal);
		
	}
	
	// Function to reset the number of alerts
	function set_num_alerts() {
		
		var current_num = parseInt($num_alerts.text());
		var new_num = current_num - 1;
		var $header_num = $('#alert_modal #alert_num');
		
		// Set the new number in the html
		$header_num.text(new_num);
		$num_alerts.text(new_num);
		
		// Remove the alert class on the badge
		if(new_num < 1) {
			$num_alerts.removeClass('badge-important');
		}
		
	}
	
	

});


