/* COMMON FUNCTIONALITIES
------------------------------------------------------- 
File: ./assets/js/admin_maintenance_form.js
Description: JS Functionality for the admin maintenance
form.
------------------------------------------------------- */

$(document).ready(function() {


	/* RECEIPT UPLOAD
	--------------------------------------------------- */
	
	// 1. Get the variables & elements we will be working with
	
	var $receipt_file_field = $('#receipt_file');
	var $receipt_file_upload = $('#receipt_upload');
	var $receipt_file_current = $('#receipt_current');
	var $receipt_file_filename = $('#receipt_filename');
	var receipt_upload_script = SITE_URL + 'admin/upload/maintenance_receipt';
	
	// 2. AJAX File Upload attached to upload button
	
	$receipt_file_upload.on('click', function(e) {
		
		// Prevent the default
		e.preventDefault();
		
		// Disable file fields
		set_file_fields('disable');
		
		// Do the upload
		$.ajaxFileUpload({
			url : receipt_upload_script,
			secureuri : false,
			fileElementId : 'receipt_file',
			dataType : 'json',
			success :	function (data, status) {
							set_file_fields('enable');
							if(data.status != 'error') {
								set_receipt_file(data.file_url, data.file_name);
							} else {
								alert(data.msg);
							}
						},			
			error :		function (data, status, e) {
							set_file_fields('enable');
							alert(e);
						}
								
		});
		 
	 });
	 
	 // 3. Set Receipt Callback Function used by Ajax file upload on success
	 
	 function set_receipt_file(file_url, file_name) {
		
		// Check to see if a receipt file already exists
		if($receipt_file_filename.val() != '') {
		
			// Replace the existing file data element
			$receipt_file_current.find('.filename').text(file_name);
		
		} else {
			
			// Create a new element, file name, and delete link
			var $new_elem = $('<p class="help-block"></p>');
			$new_elem.append('<strong>Archivo Actual :</strong> <span class="filename">' + file_name + '</span> <a class="remove_file" href="#">[Borrar]</a>');
			
			// Add the new element to the DOM
			$receipt_file_current.prepend($new_elem);
			
		}
		
		// Set the data on the hidden form element
		$receipt_file_filename.val(file_name);
		$receipt_file_field.val('');
		
	}
	
	// 4. Remove Receipt Function
	
	function remove_receipt(e) {
	
		// Prevent the default
		e.preventDefault();
		
		// Get the data we need
		var $target = $(e.target);
		var $parent_elem = $target.parent();
		
		// Reset the hidden field
		$receipt_file_filename.val('');
		
		// Remove the file data
		$parent_elem.remove();
	
	}
	
	// 5. Disable/Enable file fields (during upload)
	
	function set_file_fields(status) {
		
		var $icon = $receipt_file_upload.children('i'); 
		
		if(status == 'disable') {
			$receipt_file_field.addClass('disabled');
			$receipt_file_upload.addClass('disabled');
			$icon.attr('class', 'icon-spinner');
		} else {
			$receipt_file_field.removeClass('disabled');
			$receipt_file_upload.removeClass('disabled');
			$icon.attr('class', 'icon-upload');
		}
		
	}
	
	// 5. Bind the remove receipt function to all remove links
	$receipt_file_current.on('click','.remove_file', remove_receipt);
	

});


