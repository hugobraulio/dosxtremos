/* COMMON FUNCTIONALITIES
------------------------------------------------------- 
File: ./assets/js/admin_bookings_list.js
Description: JS Functionality for the main admin bookings
calendar.
------------------------------------------------------- */

$(document).ready(function() {


	/* COMMISSIONS AND EXPENSES MODAL
	--------------------------------------------------- */
	
	// 1. Cache and defaults
	var $modals = $('#modals');
	var $bookings = $('#bookings tbody');
	
	// 2. Detail links open in modal view
	
	// 2. Valid timeslots load view modal
	$bookings.on('click', 'a.detail', function(e){
		
		// 1. Prevent the default
		e.preventDefault();
		
		// 2. Make the call to the booking controllers view method (in URL)
		var ajax_url = $(this).attr('href');
		$.ajax({
			type: 'GET',
			url: ajax_url,
			dataType: 'html',
			success: function(data){
				initialize_view_modal(data);
			},
			error: function(data, status, e){
				alert('An AJAX error has occured : ' + e);
			}
		});
		
	});
	
	
	// Function to initialize view modals
	function initialize_view_modal(modal_html) {
	
		// Remove any existing modals first
		$modals.children('*').remove();
		
		// Convert the html to a jQuery object and set the bootstrap functionality on it
		var $new_modal = $(modal_html);
		$new_modal.modal();
		
		// Append it to the modals div
		$modals.append($new_modal);
		
	}
	

});


