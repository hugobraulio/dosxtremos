/* COMMON FUNCTIONALITIES
------------------------------------------------------- 
File: ./assets/js/admin_bookings_form.js
Description: JS Functionality for the admin bookings
form.
------------------------------------------------------- */

$(document).ready(function() {


	/* PAYMENT TABLE
	--------------------------------------------------- */
	
	// 1. Get the elements we will be working with
	
	var $agreed_price = $('#agreed_price');
	var $add_row = $('#add_row');
	var $payment_table = $('#payments table tbody');
	var $payment_ids = $('#payment_ids');
	var $payment_percentages = $('#payment_percentages');
	var $payment_amounts = $('#payment_amounts');
	var $payment_dates_due = $('#payment_dates_due');
	var $payment_commissions = $('#payment_commissions');
	var $payment_sort_orders = $('#payment_sort_orders');
	var home_commission = $('#home_commission').val();
	
	// 2. Set and Caluclate Payments Function
	
	function set_payments() {
		
		// Get the agreed amount value to work with and set some arrays
		var base_amount = $agreed_price.val();
		var total_percent = 0;
		var id_array = Array();
		var percentage_array = Array();
		var amount_array = Array();
		var dates_due_array = Array();
		var commissions_array = Array();
		var sort_orders_array = Array();
		
		// make sure we have a valid base number to work with
		if(!isNaN(parseFloat(base_amount))) {
		
			var row_counter = 0;
			
			// Loop through the table rows to set each
			$payment_table.children('tr').each(function(){
				
				// Get the ID and due date for each
				var row_id = $(this).data('item_id');
				var row_date_due = $(this).find('.date_due').val();
				var row_commission = $(this).find('.commission').val();
				
				// Get the numbers we will use to calculate
				var row_percent = $(this).find('.percentage').val();
				var row_amount = ((row_percent / 100) * base_amount).toFixed(2);
				total_percent += parseInt(row_percent);
				
				// Set the data text value of the amount
				$(this).data('amount', row_amount);
				$(this).data('commission', row_commission);
				$(this).data('sort_order', row_counter);
				$(this).find('.amount').text(row_amount);
				
				// Add the percentage and amount to the arrays
				id_array.push(row_id);
				percentage_array.push(row_percent);
				amount_array.push(row_amount);
				dates_due_array.push(row_date_due);
				commissions_array.push(row_commission);
				sort_orders_array.push(row_counter);
				
				// Count the row
				row_counter++;
				
			});
			
			// Reset the hidden field values to lists
			$payment_ids.val(id_array.toString());
			$payment_percentages.val(percentage_array.toString());
			$payment_amounts.val(amount_array.toString());
			$payment_dates_due.val(dates_due_array.toString());
			$payment_commissions.val(commissions_array.toString());
			$payment_sort_orders.val(sort_orders_array.toString());
			
			// Add or Remove disabled class on add row button
			if((100-total_percent) > 0) {
				$add_row.removeClass('disabled');
			} else {
				$add_row.addClass('disabled');
			}
		
		}
		
	}
	
	// 3. Remove Payment Row Function
	function remove_row(e) {
		
		var $target = $(e.target);
		var $row = $target.parents('tr');
		
		// Remove the entire row
		$row.remove();
		
		// Reset the payments
		set_payments();
		
		// Prevent the default
		e.preventDefault();
		
	}
	
	// 4. Add Payment Row Button Function
	
	$add_row.click(function(e) {
		
		// Get the current date string to use
		var new_date = new Date();
		var current_date = new_date.getDate() + '/' + (new_date.getMonth() + 1) + '/' + new_date.getFullYear();
		
		// Get the new percent value to use (whatever is left until 100)
		var new_percent = 0;
		var all_percent = 0;
		$payment_table.find('input.percentage').each(function(){
			all_percent += parseInt($(this).val());
		});
		new_percent = 100 - all_percent;
		
		// If the new percent is greater than 0, input the row
		if(new_percent > 0) {
		
			// Create the rows html elements
			var $new_row = $('<tr data-item_id="0" data-amount="0.00" data-comission="0" data-sort_order="0"></tr>');
			$new_row.append('<td><div class="input-append"><input type="text" name="payment_0_percentage" class="span1 percentage" id="payment_0_percentage" placeholder="0" value="' + new_percent + '"/><span class="add-on">%</span></div></td>');
			$new_row.append('<td><span class="amount" id="payment_0_amount">0.00</span> €</td>');
			$new_row.append('<td><div class="input-append"><input type="text" name="payment_0_commission" class="span1 commission" id="payment_0_commission" placeholder="0" value="' + home_commission + '"/><span class="add-on">%</span></div></td>');
			$new_row.append('<td><input type="text" name="payment_0_date_due" class="span2 datepicker date_due" id="payment_0_date_due" placeholder="dd/mm/aaaa" value="' + current_date + '"/></td>');
			$new_row.append('<td><a href="#" class="btn remove_row" title="Borrar pago de la tabla"><i class="icon-trash"></i></a></td>');
			
			// Attach the formatting plugins to the interactive elements
			$new_row.find('input.percentage, input.commission').percentFormat();
			$new_row.find('input.datepicker').datepicker({
				format : 'dd/mm/yyyy'
			});
			
			// Add the new row to the table
			$payment_table.append($new_row);
			
			// Run the Set Payments method to update the hidden fields
			set_payments();
		
		}
		
		// Escape the default
		e.preventDefault();
		
	});
	
	// 5. Bind the payment table elements to the functions
	
	$agreed_price.on('blur', set_payments);
	$payment_table.on('blur', 'input.percentage, input.datepicker, input.commission', set_payments);
	$payment_table.on('click', 'a.remove_row', remove_row);
	
	// 6. Run the set_payments method on the load
	set_payments();
	
	
	
	/* EXPENSES TABLE
	--------------------------------------------------- */
	
	// 1. Get the elements we will be working with
	
	var $add_expense_row = $('#add_expense_row');
	var $expense_table = $('#expenses table tbody');
	var $expense_item_ids = $('#expense_item_ids');
	var $expense_descriptions = $('#expense_descriptions');
	var $expense_amounts = $('#expense_amounts');
	var $expense_line_item_ids = $('#expense_line_item_ids');
	
	// 2. Set Expenses Function
	
	function set_expenses() {
		
		// Set some arrays
		var id_array = Array();
		var description_array = Array();
		var amount_array = Array();
		var line_item_array = Array();
		
		// Loop through the table rows to set each
		$expense_table.children('tr').each(function(){
			
			// Get the ID and due date for each
			var item_id = $(this).data('item_id');
			var description = $(this).find('.description').val();
			var amount = $(this).find('.amount').val();
			var line_item_id = $(this).data('line_item_id');
			
			// Add the values to the arrays
			id_array.push(item_id);
			description_array.push(description);
			amount_array.push(amount);
			line_item_array.push(line_item_id);
			
		});
		
		// Reset the hidden field values to lists
		$expense_item_ids.val(id_array.join('|'));
		$expense_descriptions.val(description_array.join('|'));
		$expense_amounts.val(amount_array.join('|'));
		$expense_line_item_ids.val(line_item_array.join('|'));
		
	}
	
	// 3. Remove Expense Row Function
	
	function remove_expense_row(e) {
		
		var $target = $(e.target);
		var $row = $target.parents('tr');
		
		// Remove the entire row
		$row.remove();
		
		// Reset the payments
		set_expenses();
		
		// Prevent the default
		e.preventDefault();
		
	}
	
	// 4. Add Expense Row Button Function
	
	$add_expense_row.click(function(e) {
		
		// Create the rows html elements
		var $new_row = $('<tr data-item_id="0" data-line_item_id="0"></tr>');
		$new_row.append('<td><input type="text" name="expense_0_description" class="span6 description" id="expense_0_description" placeholder="Escribe una descripción breve del gasto" value="Otros Gastos"/></td>');
		$new_row.append('<td><div class="input-append"><input type="text" name="expense_0_amount" class="span1 currency amount" id="expense_0_amount" placeholder="0.00" value="0.00"/><span class="add-on">€</span></div></td>');
		$new_row.append('<td><a href="#" class="btn remove_expense_row" title="Borrar gasto de la tabla"><i class="icon-trash"></i></a></td>');
		
		// Attach the formatting plugins to the interactive elements
		$new_row.find('input.currency').currencyFormat();
		
		// Add the new row to the table
		$expense_table.append($new_row);
		
		// Run the Set Payments method to update the hidden fields
		set_expenses();
		
		// Escape the default
		e.preventDefault();
		
	});
	
	// 5. Bind the expense table elements to the functions
	
	$expense_table.on('blur', 'input.description, input.amount', set_expenses);
	$expense_table.on('click', 'a.remove_expense_row', remove_expense_row);
	

});


