/* COMMON FUNCTIONALITIES
------------------------------------------------------- 
File: ./assets/js/admin_homes_list.js
Description: JS Functionality for the main admin homes
list.
------------------------------------------------------- */

$(document).ready(function() {


	/* SORTABLE HOMES LIST
	--------------------------------------------------- */
	
	// 1. Cache the elements and set the defaults
	var home_order = '';
	var $sortable_table = $('table.sortable tbody');
	var $save_order_btn = $('#save_order');
	var $message = $('#response_message').hide();
	
	
	// 2. Helper to preserve width of cells
	var fixHelper = function(e, ui) {
		ui.children().each(function() {
			$(this).width($(this).width());
		});
		return ui;
	};
	
	
	// 3. Set values function
	function set_values(arr) {
		
		// loop through the array and get a list of ids by stripping out the prefix 'xxxx_'
		for(var x=0; x < arr.length; x++) {
			arr[x] = arr[x].substring(5);
		}
		
		// Reset the home_array variable
		home_order = arr.toString();
		
	}
	
	
	// 4. Apply the sortable plugin to the table
	$sortable_table.sortable({
		helper : fixHelper,
		handle : '.order',
		cursor : 'drag',
		update: function(e, ui) {
			var reorder_result = $(this).sortable('toArray');
			set_values(reorder_result);
		}
	}).disableSelection();
	
	
	// 5. Set the current home order
	set_values($sortable_table.sortable('toArray'));
	
	
	// 6. Set message function
	function set_message(status, message) {
		
		if(status == 'success') {
			$message.find('.alert-heading').text('Guardado');
			$message.removeClass('alert-error');
			$message.addClass('alert-success');
		} else {
			$message.find('.alert-heading').text('Error');
			$message.removeClass('alert-success');
			$message.addClass('alert-error');
		}
		
		$message.find('.alert-body').text(message);
		$message.show();
		
	}
	
	
	// 6. Apply an onclick event for the save order button
	$save_order_btn.click(function(e){
		
		// Prevent the default action and get the url to call
		e.preventDefault();
		var ajax_url = $(this).attr('href');
		
		// Make an Ajax call to the php script
		$.ajax({
			type: 'POST',
			url: ajax_url,
			data: {home_ids : home_order},
			dataType: 'json',
			success: function(data){
				set_message(data.status, data.msg);
				console.log(home_order);
			},
			error: function(data, status, e){
				set_message('error', status);
			}
		});
		
	});
	
	
	// 7. Apply an onclick event for the alerts close button
	$message.children('a.close').click(function(e) {
		
		// Prevent the default
		e.preventDefault();
		
		// Hide the parent element (the alert)
		$(this).parent().hide();
		
	});
	

});


