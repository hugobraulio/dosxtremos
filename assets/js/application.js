$(document).ready(function() {

	// Set the defaults and cache elements we will use
	var $alert_modal = $('#alert_modal').modal('hide');
	var $alert_trigger = $('#alert_trigger');
	
	var $calendar_links = $('#scroll-table .scroll-pane a, table.calendar td > a');
	var $calendar_modal_free = $('#calendar_modal_free').modal('hide');
	var $calendar_modal_offered = $('#calendar_modal_offered').modal('hide');
	var $calendar_modal_pre_booked = $('#calendar_modal_pre-booked').modal('hide');
	var $calendar_modal_booked = $('#calendar_modal_booked').modal('hide');
	
	var $booking_payment_modal_add = $('#bookings_payment_modal_add').modal('hide');
	
	var $homes_calendar_modal_add = $('#homes_calendar_modal_add').modal('hide');
	var $homes_calendar_modal_edit = $('#homes_calendar_modal_edit').modal('hide');
	
	var $owners_bookings_payments = $('#owners_bookings_payments').modal('hide');
	
	var $owners_bookings_commissions = $('#owners_bookings_commissions').modal('hide');
    
    var $owners_bookings_liquidations = $('#owners_bookings_liquidations').modal('hide');
	
	var $scroll_table = $('#scroll-table');
	var $scroll_table_left = $scroll_table.children('.fixed-column').find('a.scroll-left');
	var $scroll_table_right = $scroll_table.children('.fixed-column').find('a.scroll-right');
	var $scroll_table_content = $scroll_table.children('.scroll-pane');
	var $scroll_width = 157;
	
	var $agreed_price = $('#agreed_price');
	var $payment_1_amount = $('#payment_1_amount');
	var $payment_2_amount = $('#payment_2_amount');
	var payment_1_percent = parseFloat('.20');
	var payment_2_percent = parseFloat('.80');
	
	// Mini jQuery plugin that formats to two decimal places
    (function($) {
        $.fn.currencyFormat = function() {
            this.each( function( i ) {
                $(this).change( function( e ){
                    if( isNaN( parseFloat( this.value ) ) ) return;
                    this.value = parseFloat(this.value).toFixed(2);
                });
            });
            return this; //for chaining
        }
    })( jQuery );
    
    $('.currency').currencyFormat();
	
	// Apply the date picker plugin
	$('.datepicker').datepicker({
		format : 'dd/mm/yyyy'
	});
	
	// Apply the tabs plugin
	$('#document_tabs a').click(function(e){
		e.preventDefault();
		$(this).tab('show');
	});
	
	// Apply Sortable jQuery UI behavior to sortable tables
	
	// Return a helper with preserved width of cells
	var fixHelper = function(e, ui) {
		ui.children().each(function() {
			$(this).width($(this).width());
		});
		return ui;
	};
	
	$('table.sortable tbody').sortable({
		helper : fixHelper,
		handle : '.order',
		cursor : 'drag'
	}).disableSelection();
	
	// Function for agreed price in booking form
	$agreed_price.change(function(e){
		
		if(!isNaN(parseFloat(this.value))) {
		
			var pay_1 = parseFloat(this.value) * payment_1_percent;
			var pay_2 = parseFloat(this.value) * payment_2_percent;
			
			$payment_1_amount.text(pay_1.toFixed(2));
			$payment_2_amount.text(pay_2.toFixed(2));
		
		} else {
		
			$payment_1_amount.text('0.00');
			$payment_2_amount.text('0.00');
			
		}
		
	});
	
	
	// Functions for adding lines to invoice form
	
	var i = 1;
	$("#add_row").click(function(e) {
		
		e.preventDefault();
		
		$new_row = $("table.line_items tbody tr:first").clone();
		
		$new_row.find('input').each(function() {
			$(this).attr({
				'id': function(_, id) { return id + i },
				'name': function(_, name) { return name + i },
				'value': ''
			});
		});
		
		$new_row.find('select').each(function() {
			$(this).attr({
				'id': function(_, id) { return id + i },
				'name': function(_, name) { return name + i }
			});
			$(this).prop('selectedIndex',0);
		});
		
		$new_row.find('a.remove_row').click(function(e){
			
			e.preventDefault();
			
			var $row = $(this).parent().parent().parent();
			var num_rows = $row.siblings('tr').length;
			
			if(num_rows > 0) {
				$row.remove();
			}
			
		});
		
		$new_row.find('.currency').currencyFormat();
		
		$new_row.appendTo("table.line_items tbody");
		i++;
	});
	
	$('a.remove_row').click(function(e){
		
		e.preventDefault();
		
		var $row = $(this).parent().parent().parent();
		var num_rows = $row.siblings('tr').length;
		
		if(num_rows > 0) {
			$row.remove();
		}
		
	});
	
	
	// Open modals via JS
	
	// Alert modal
	$alert_trigger.click(function(e){
		
		e.preventDefault();
		
		// 1. Make the Ajax call
		
		// 2. Populate the modal with data
		
		// 3. Display the modal
		$alert_modal.modal('show');
	});
	
	$calendar_links.click(function(e){
		
		e.preventDefault();
		
		if($(this).hasClass('offered')) {
			
			$calendar_modal_offered.modal('show');
		
		} else if($(this).hasClass('pre-booked')) {
		
			$calendar_modal_pre_booked.modal('show');
			
		} else if($(this).hasClass('booked')) {
		
			$calendar_modal_booked.modal('show');
			
		} else {
			
			$calendar_modal_free.modal('show');
			
		}
		
	});
	
	// Add Booking Payment Modal
	$('a.add_payment').click(function(e){
		
		e.preventDefault();
		
		// 1. Make the Ajax call
		
		// 2. Populate the modal with data
		
		// 3. Display the modal
		$booking_payment_modal_add.modal('show');
		
	});
	
	// Homes calendar modal (Edit)
	$('a.date_edit').click(function(e){
		
		e.preventDefault();
		
		// 1. Make the Ajax call
		
		// 2. Populate the modal with data
		
		// 3. Display the modal
		$homes_calendar_modal_edit.modal('show');
		
	});
	
	// Owners Bookings Payments modal
	$('a.payments').click(function(e){
		
		e.preventDefault();
		
		// 1. Make the Ajax call
		
		// 2. Populate the modal with data
		
		// 3. Display the modal
		$owners_bookings_payments.modal('show');
		
	});
	
	// Owners Bookings Commissions modal
	$('a.commissions').click(function(e){
		
		e.preventDefault();
		
		// 1. Make the Ajax call
		
		// 2. Populate the modal with data
		
		// 3. Display the modal
		$owners_bookings_commissions.modal('show');
		
	});
    
    // Owners Bookings Liquidations modal
	$('a.liquidations').click(function(e){
		
		e.preventDefault();
		
		// 1. Make the Ajax call
		
		// 2. Populate the modal with data
		
		// 3. Display the modal
		$owners_bookings_liquidations.modal('show');
		
	});
	
	
	// Scrolling Calendar Table
	
	// Left Button
	$scroll_table_left.click(function(e){
		// Prevent the default
		e.preventDefault();
		// Scroll the content
		$scroll_table_content.animate({scrollLeft:'-=' + $scroll_width}, 'fast');
		
	});
	
	// Right Button
	$scroll_table_right.click(function(e){
		// Prevent the default
		e.preventDefault();
		// Scroll the content
		$scroll_table_content.animate({scrollLeft:'+=' + $scroll_width}, 'fast');
		
	});
	
	

});


