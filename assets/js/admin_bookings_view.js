/* COMMON FUNCTIONALITIES
-------------------------------------------------------
File: ./assets/js/admin_bookings_list.js
Description: JS Functionality for the main admin bookings
calendar.
------------------------------------------------------- */

$(document).ready(function() {


	/* ADD / DELETE PAYMENTS (Receipts) FUNCTIONALITY
	---------------------------------------------------- */

	// 1. Get the elements we are working on
	$payments_table = $('#payments');

	// 2. Assign the onclick function to the add_payment links in the table
	$payments_table.on('click', 'a.add_payment', function(e) {

		// Prevent the default
		e.preventDefault();

		// Get the data from this element
		var ajax_url = $(this).attr('href');
		var is_disabled = $(this).hasClass('disabled');
		var $date_paid = $(this).parents('tr').find('td.date_paid');
		var $add_payment_link = $(this);
		var $add_liquidation_link = $(this).parents('tr').find('a.add_liquidation');
		var $view_link_es = $(this).parents('tr').find('a[lang=es]');
		var $view_link_en = $(this).parents('tr').find('a[lang=en]');
		var $edit_link = $(this).parents('tr').find('a.edit_receipt');
		var $delete_payment_link = $(this).parents('tr').find('a.delete_payment');
		var $delete_liquidation_link = $(this).parents('tr').find('a.delete_liquidation');

		// If the link is not disabled, perform the receipt creation
		if(!is_disabled) {

			$.ajax({
				type: 'GET',
				url: ajax_url,
				dataType: 'json',
				success: function(data){

					// Add the date, insert the urls for the buttons // enable the other button, and disable this one

					$date_paid.text(data.receipt_date);
					$add_payment_link.attr('href', '');
					$add_liquidation_link.attr('href', (SITE_URL + 'admin/payments/liquidate/' + data.item_id));
					$view_link_es.attr('href', (SITE_URL + 'admin/receipts/view/' + data.receipt_id + '/es'));
					$view_link_en.attr('href', (SITE_URL + 'admin/receipts/view/' + data.receipt_id + '/en'));
					$edit_link.attr('href', (SITE_URL + 'admin/receipts/action/edit/' + data.receipt_id + '/' + data.item_id));
					$delete_payment_link.attr('href', (SITE_URL + 'admin/payments/unpay/' + data.item_id));
					$delete_liquidation_link.attr('href', '');

					$add_payment_link.addClass('disabled');
					$add_liquidation_link.removeClass('disabled');
					$view_link_es.removeClass('disabled');
					$view_link_en.removeClass('disabled');
					$edit_link.removeClass('disabled');
					$delete_payment_link.removeClass('disabled');
					$delete_liquidation_link.addClass('disabled');

					$view_link_es.unbind();
					$view_link_en.unbind();
					$edit_link.unbind();
					$add_payment_link.unbind();
					$delete_liquidation_link.unbind();

					$add_liquidation_link.click(function(e){
						e.preventDefault();
					});
					
					// reduce the number of alerts if it has been deleted
					if (data.alerts_deleted > 0) {
						var $alert_trigger = $('#alert_trigger');
						var $num_alerts = $alert_trigger.children('.badge');
						var current_num = parseInt($num_alerts.text());
						var new_num = current_num - data.alerts_deleted;
						var $header_num = $('#alert_modal #alert_num');
						
						// Set the new number in the html
						$header_num.text(new_num);
						$num_alerts.text(new_num);
						
						// Remove the alert class on the badge
						if(new_num < 1) {
							$num_alerts.removeClass('badge-important');
						}
                    }
					
				},
				error: function(data, status, e){
					alert('An AJAX error has occured : ' + e);
				}
			});

		}

	});

	// 2. Assign the onclick function to the delete_payment links in the table
	$payments_table.on('click', 'a.delete_payment', function(e) {

		// Prevent the default
		e.preventDefault();

		// Get the data from this element
		var ajax_url = $(this).attr('href');
		var is_disabled = $(this).hasClass('disabled');
		var $date_paid = $(this).parents('tr').find('td.date_paid');
		var $date_liquidated = $(this).parents('tr').find('td.date_liquidated');
		var $add_payment_link = $(this).parents('tr').find('a.add_payment');
		var $add_liquidation_link = $(this).parents('tr').find('a.add_liquidation');
		var $view_link_es = $(this).parents('tr').find('a[lang=es]');
		var $view_link_en = $(this).parents('tr').find('a[lang=en]');
		var $edit_link = $(this).parents('tr').find('a.edit_receipt');
		var $delete_payment_link = $(this);
		var $delete_liquidation_link = $(this).parents('tr').find('a.delete_liquidation');

		function delete_payment(item_id) {
			// Remove the date, set the urls for the buttons, disable the other button, and enable the add button
			$date_paid.text('-');

			$add_payment_link.attr('href', (SITE_URL + 'admin/payments/pay/' + item_id));
			$add_liquidation_link.attr('href', '');
			$view_link_es.attr('href', '');
			$view_link_en.attr('href', '');
			$edit_link.attr('href', '');
			$delete_payment_link.attr('href', '');
			$delete_liquidation_link.attr('href', '');

			$add_payment_link.removeClass('disabled');
			$add_liquidation_link.addClass('disabled');
			$view_link_es.addClass('disabled');
			$view_link_en.addClass('disabled');
			$edit_link.addClass('disabled');
			$delete_payment_link.addClass('disabled');
			$delete_liquidation_link.addClass('disabled');

			$delete_liquidation_link.unbind();

			$date_liquidated.text('-');

			$view_link_es.click(function(e) {
				e.preventDefault();
			});
			$view_link_en.click(function(e) {
				e.preventDefault();
			});
			$edit_link.click(function(e) {
				e.preventDefault();
			});
			$delete_payment_link.click(function(e) {
				e.preventDefault();
			});
			$delete_liquidation_link.click(function(e) {
				e.preventDefault();
			});
        }

		// If the link is not disabled, perform the payment deletion
		if(!is_disabled) {
			// Check if the liquidation is done to avoid deletion of payments already liquidated by mistake
			var $liquidation_done = ($date_liquidated.text() != "-" && $date_liquidated.text() != null && $date_liquidated.text() != '');

			if ($liquidation_done) {
				if (confirm("El pago ha sido liquidado, ¿estás seguro de querer borrarlo?"))
				{
					$.ajax({
						type: 'GET',
						url: ajax_url,
						dataType: 'json',
						success: function(data){
							delete_payment(data.item_id);
						},
						error: function(data, status, e){
							alert('An AJAX error has occured : ' + e);
						}
					});
				}
			}else {
				$.ajax({
					type: 'GET',
					url: ajax_url,
					dataType: 'json',
					success: function(data){
						delete_payment(data.item_id);
					},
					error: function(data, status, e){
						alert('An AJAX error has occured : ' + e);
					}
				});
			}
		}

	});


	/* UPLOAD / DELETE BOOKING DOCUMENTS FUNCTIONALITY
	--------------------------------------------------- */

	/* NOTE THESE ARE FROM THE MAINTENANCE RECEIPT - REPLACE WITH CODE FOR DOCUMENT UPLOADS!!!!! */

	// 1. Get the variables and elements will will be working with

	var $document_form = $('#document_form');
	var $document_file_field = $document_form.find('#document_file');
	var $document_file_upload = $document_form.find('#document_upload');
	var $document_list = $('#document_list tbody');
	var document_upload_script = $document_form.attr('action');

	// 2. AJAX File Upload attached to upload button

	$document_file_upload.on('click', function(e) {

		// Prevent the default
		e.preventDefault();

		// Disable file fields
		set_file_fields('disable');

		// Do the upload
		$.ajaxFileUpload({

			url : document_upload_script,
			secureuri : false,
			fileElementId : 'document_file',
			dataType : 'json',
			success :	function (data, status) {
							set_file_fields('enable');
							if(data.status != 'error') {
								add_document_file(data.item_id, data.file_name, data.download_url, data.delete_url);
							} else {
								alert(data.msg);
							}
						},
			error :		function (data, status, e) {
							set_file_fields('enable');
							alert(e);
						}

		});

	});

	// 3. Add document file callback function used by Ajax file upload

	function add_document_file(item_id, file_name, download_url, delete_url) {

		// Create the new row element to insert into the table

		$new_elem = $('<tr></tr>');
		$new_elem.append('<td>' + file_name + '</td>');
		$new_elem.append('<td><nav class="btn-group"><a href="' + download_url + '" class="btn download" title="Descargar el documento"><i class="icon-download"></i></a><a href="' + delete_url + '" class="btn delete" title="Borrar el documento"><i class="icon-trash"></i></a></nav></td>');

		// Add the new element to the DOM
		$document_list.append($new_elem);

	}

	// 4. Remove Document Function

	function remove_document(e) {

		// Prevent the default
		e.preventDefault();

		// Get the url to call and the element we are removing (table row)
		var document_delete_url = $(this).attr('href');
		var $document_row = $(this).parents('tr');

		// Call the ajax function to delete the file and entry in the database

		$.ajax({
			type: 'GET',
			url: document_delete_url,
			dataType: 'json',
			success: function(data){

				// Remove the documents row from the table
				$document_row.remove();

			},
			error: function(data, status, e){
				alert('An AJAX error has occured : ' + e);
			}

		});

	}

	// 5. Disable/Enable file fields (during upload)

	function set_file_fields(status) {

		var $icon = $document_file_upload.children('i');

		if(status == 'disable') {
			$document_file_field.addClass('disabled');
			$document_file_upload.addClass('disabled');
			$icon.attr('class', 'icon-spinner');
		} else {
			$document_file_field.removeClass('disabled');
			$document_file_upload.removeClass('disabled');
			$icon.attr('class', 'icon-upload');
		}

	}

	// 5. Bind the remove document function to all remove links in the table
	$document_list.on('click','a.delete', remove_document);


	/* ADD / DELETE LIQUIDATIONS FUNCTIONALITY
	---------------------------------------------------- */

	// 1. Assign the onclick function to the add_liquidation links in the table
	$payments_table.on('click', 'a.add_liquidation', function(e) {

		// Prevent the default
		e.preventDefault();

		// Get the data from this element
		var ajax_url = $(this).attr('href');
		var is_disabled = $(this).hasClass('disabled');
		var $date_liquidated = $(this).parents('tr').find('td.date_liquidated');
		var $add_link = $(this);
		var $delete_link = $(this).parents('tr').find('a.delete_liquidation');

		// If the link is not disabled, perform the liquidation
		if(!is_disabled) {

			$.ajax({
				type: 'GET',
				url: ajax_url,
				dataType: 'json',
				success: function(data){

					// Add the date, st the urls for the buttons, enabled the other button, and disable this one

					$date_liquidated.text(data.liquidation_date);

					$delete_link.attr('href', (SITE_URL + 'admin/payments/delete_liquidation/' + data.item_id));

					$delete_link.removeClass('disabled');

					$delete_link.unbind();

					$add_link.click(function(e){
						e.preventDefault();
					});
					$add_link.attr('href', '');
					$add_link.addClass('disabled');

				},
				error: function(data, status, e){
					alert('An AJAX error has occured : ' + e);
				}
			});

		}

	});

	// 2. Assign the onclick function to the delete_liquidation links in the table
	$payments_table.on('click', 'a.delete_liquidation', function(e) {

		// Prevent the default
		e.preventDefault();

		// Get the data from this element
		var ajax_url = $(this).attr('href');
		var is_disabled = $(this).hasClass('disabled');
		var $date_liquidated = $(this).parents('tr').find('td.date_liquidated');
		var $add_link = $(this).parents('tr').find('a.add_liquidation');
		var $delete_link = $(this);

		// If the link is not disabled, perform the receipt deletion
		if(!is_disabled) {

			$.ajax({
				type: 'GET',
				url: ajax_url,
				dataType: 'json',
				success: function(data){

					// Remove the date, set the urls for the buttons, disable the other button, and enable the add button

					$date_liquidated.text('-');

					$add_link.attr('href', (SITE_URL + 'admin/payments/liquidate/' + data.item_id));
					$delete_link.attr('href', '');

					$add_link.removeClass('disabled');
					$delete_link.addClass('disabled');

					$add_link.unbind();

					$delete_link.click(function(e) {
						e.preventDefault();
					});

				},
				error: function(data, status, e){
					alert('An AJAX error has occured : ' + e);
				}
			});

		}

	});



});
