/* COMMON FUNCTIONALITIES
------------------------------------------------------- 
File: ./assets/js/admin_bookings_list.js
Description: JS Functionality for the main admin bookings
calendar.
------------------------------------------------------- */

$(document).ready(function() {


	/* SET ACTIVATED/DEACTIVATED STATUS
	--------------------------------------------------- */
	
	
	// 1. Get the elements we will be working with
	
	var $item_is_active_link = $('#item_is_active_link');
	var $active_tag = $('#active_tag');
	
	// 2. Bind the click functionality for the active link
	
	$item_is_active_link.on('click', function(e){
		
		// Prevent the default
		e.preventDefault();
		
		// Get elements and data we will be working with
		var ajax_url = $(this).attr('href');
		
		// 2. Make the call to the controller via AJAX

		$.ajax({
			type: 'GET',
			url: ajax_url,
			dataType: 'json',
			success: function(data){
				// Use the Set Active State Function to make the changes
				set_active_state(data.item_id, data.is_active);
			},
			error: function(data, status, e){
				alert('An AJAX error has occured : ' + e);
			}
			
		});
		
	});
	
	
	// 3. Set active state function
	
	function set_active_state(item_id, is_active) {
		
		// Create the new URL
		var new_url = SITE_URL + 'admin/homes/set_active/' + item_id + '/' + (is_active == 1 ? 0 : 1);
		
		// Set the link parameters
		$item_is_active_link.attr('href', new_url);
		$item_is_active_link.children('i').attr('class', (is_active == 1 ? 'icon-eye-close' : 'icon-eye-open'));
		$item_is_active_link.children('.display_text').text((is_active == 1 ? 'Desactivar' : 'Activar'));
		
		// Set the active tag label
		$active_tag.attr('class', (is_active == 1 ? 'label label-success' : 'label label-important'));
		$active_tag.text((is_active == 1 ? 'Activado' : 'Desactivado'));
		
	}
	

});


