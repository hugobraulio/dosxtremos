/* COMMON FUNCTIONALITIES
------------------------------------------------------- 
File: ./assets/js/admin_bookings_form.js
Description: JS Functionality for the admin bookings
form.
------------------------------------------------------- */

$(document).ready(function() {

	
	/* LINE ITEMS TABLE
	--------------------------------------------------- */
	
	// 1. Get the elements we will be working with
	
	var $line_items_table = $('#line_items tbody');
	var $line_item_ids = $('#line_item_ids');
	var $line_descriptions = $('#line_descriptions');
	var $line_net_amounts = $('#line_net_amounts');
	var $line_vat_percentages = $('#line_vat_percentages');
	var $line_vat_amounts = $('#line_vat_amounts');
	var $line_total_amounts = $('#line_total_amounts');
	var $line_payment_ids = $('#line_payment_ids');
	var $line_expense_ids = $('#line_expense_ids');
	var $net_amount = $('#net_amount');
	var $vat_amount = $('#vat_amount');
	var $total_amount = $('#total_amount');
	var $vat_totals = $('#vat_totals');
	var $net_totals = $('#net_totals');
	var $invoice_totals = $('#invoice_totals');
	
	// 2. Set Expenses Function
	
	function set_line_items() {
		
		// Set some arrays
		var id_array = Array();
		var description_array = Array();
		var net_amount_array = Array();
		var vat_percentage_array = Array();
		var vat_amount_array = Array();
		var total_amount_array = Array();
		var payment_id_array = Array();
		var expense_id_array = Array();
		
		// Set some vars to keep track of the totals
		var net_total = 0;
		var vat_total = 0;
		var gross_total = 0;
		
		// Loop through the table rows to set each
		$line_items_table.children('tr').each(function(){
			
			// Get the ID and due date for each
			var item_id = $(this).data('item_id');
			var description = $(this).find('.description').val();
			var net_amount = $(this).data('net_amount');
			var vat_percent = $(this).data('vat_percent');
			var vat_amount = $(this).data('vat_amount');
			var total_amount = $(this).data('total_amount');
			var payment_id = $(this).data('payment_id');
			var expense_id = $(this).data('expense_id');
			
			// Add the values to the arrays
			id_array.push(item_id);
			description_array.push(description);
			net_amount_array.push(net_amount);
			vat_percentage_array.push(vat_percent);
			vat_amount_array.push(vat_amount);
			total_amount_array.push(total_amount);
			payment_id_array.push(payment_id);
			expense_id_array.push(expense_id);
			
			// Increment the total vars
			net_total += net_amount;
			vat_total += vat_amount;
			gross_total += total_amount;
			
		});
		
		// Round the vat total
		
		// Reset the hidden field values to lists
		$line_item_ids.val(id_array.join('|'));
		$line_descriptions.val(description_array.join('|'));
		$line_net_amounts.val(net_amount_array.join('|'));
		$line_vat_percentages.val(vat_percentage_array.join('|'));
		$line_vat_amounts.val(vat_amount_array.join('|'));
		$line_total_amounts.val(total_amount_array.join('|'));
		$line_payment_ids.val(payment_id_array.join('|'));
		$line_expense_ids.val(expense_id_array.join('|'));
		
		// Reset the hidden fields and display elements for totals
		$net_amount.val(net_total);
		$vat_amount.val(vat_total);
		$total_amount.val(gross_total);
		
		$net_totals.text(net_totals.toFixed(2));
		$vat_totals.text(vat_totals.toFixed(2));
		$invoice_totals.text(gross_total.toFixed(2));
		
	}
	
	// 3. Remove Row Function
	
	function remove_row(e) {
		
		var $target = $(e.target);
		var $row = $target.parents('tr');
		
		// Remove the entire row
		$row.remove();
		
		// Reset the payments
		set_line_items();
		
		// Prevent the default
		e.preventDefault();
		
	}
	
	// 5. Bind the table elements to the functions
	
	$line_items_table.on('blur', 'input.description', set_line_items);
	$line_items_table.on('click', 'a.remove_row', remove_row);
	
	// 6. Run the set_line_items method on the load
	set_line_items();
	

});


